# Introduction

This is material design template created based on materially structure

# Getting Started

1. Installation process
    - run 'yarn install'
    - start dev server run 'yarn start'
2. Deployment process
    - Goto full-version directory and open package.json. Update homepage URL to the production URL
    - Goto full-version directory and run 'yarn build'
