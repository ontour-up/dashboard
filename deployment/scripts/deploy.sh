#!/bin/bash
set -e
cd dashboard

# Load nvm and his tools Node, npm, pm2 into this script.
# Because of non-interactive shell is used when ssh running command.
[ -s "/home/ubuntu/.nvm/nvm.sh" ] && \. "/home/ubuntu/.nvm/nvm.sh"  # This loads nvm

# Load Node version
nvm use v14.19.0

if [ "$(pm2 id dashboard)" == "[]" ]; then
    pm2 start yarn --name "dashboard" --log-date-format 'DD-MM HH:mm' -- start:pipe
else
    pm2 restart dashboard
fi
