import 'moment/locale/es';

import { Card, Col, Row } from 'react-bootstrap';
import React, { useContext, useEffect, useState } from 'react';
import { getSalesByOrganizer, getSalesByPromoter } from '../../../services/reports.service';
import { promoterEvents, takeEventsDashboard } from '../../../services/events.service';

import { BsFillCircleFill } from "react-icons/bs";
import Cookies from 'js-cookie';
import Loading from '../../../component/Loading';
import MUIDataTable from 'mui-datatables';
import Tooltip from "@material-ui/core/Tooltip";
import { UserContext } from '../../../context/UserContext';
import { makeStyles } from '@material-ui/core';
import moment from 'moment';
import { useHistory } from 'react-router-dom';
import { useTheme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    arrowicon: {
        '& svg': {
            width: '20px',
            height: '20px',
            verticalAlign: 'top',
        },
    },
    flatcardbody: {
        padding: '0px !important',
        '& svg': {
            width: '40px',
            height: '40px',
        },
    },
    flatcardblock: {
        padding: '25px 25px',
        borderLeft: '1px solid' + theme.palette.background.default,
        [theme.breakpoints.down('xs')]: {
            borderLeft: 'none',
            borderBottom: '1px solid' + theme.palette.background.default,
        },
        [theme.breakpoints.down('sm')]: {
            borderBottom: '1px solid' + theme.palette.background.default,
        },
    },
    textsuccess: {
        color: theme.palette.success.main,
    },
    texterror: {
        color: theme.palette.error.main,
    },
}));



const Default = (props) => {
    moment.locale('es');
    const theme = useTheme();

    const [apiCookie, setApiCookie] = useState(false);
    const { user } = useContext(UserContext);
    const [events, setEvents] = useState([]);
    const [dashboardInfo, setDashboardInfo] = useState([]);

    const [loading, setLoading] = useState(true);

    let history = useHistory();

    useEffect(() => {
        apisKeys(user);
        if (user.roles.indexOf('olas') >= 0) {
            history.push('/news');
        }
        eventsPromoter();
        if (user.email === "sdamorebieta@ontourup.com") {
            calculateDashboardInfo(true, "")
        } else {
            calculateDashboardInfo(false, "")
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props]);

    const apisKeys = (user) => {

        console.log(process.env.NODE_ENV)
        if (user.email === 'portu@ontourup.com') {
            Cookies.set('x_api_key', '/YU5dsPp+gJMNVv73hz53CH+F/ncJha_');
            setApiCookie(true);
        } else if (user.email === 'bilbao@ontourup.com') {
            Cookies.set('x_api_key', '0LAs-xPlhVzfRWdXRxV.jL1siWbfyh9F');
            setApiCookie(true);
        } else if (user.email === 'getxo@ontourup.com') {
            Cookies.set('x_api_key', 'pU~ztakzzCJs28nZGrd.q-zEyi80foqN');
            setApiCookie(true);
        } else if (user.email === 'sdamorebieta@ontourup.com' || user.email === 'gernikakesb@ontourup.com') {
            if (user.isProd) {
                Cookies.set('x_api_key', 'X7e3HHm8+z2+fwskZmci~bYv/_Hs.g4S');
                setApiCookie(true);
            }else{
                Cookies.set('x_api_key', 'd~vL-9kd1aoYKpdiIY80M401j55B7IB0');
                setApiCookie(true);
            }
        } else {
            Cookies.set('x_api_key', 'Y5gO6/Q/bddQrjtTb+OEMJ_LY8fgoS+I');
            setApiCookie(true);
        }
    };

    const eventsPromoter = async () => {
        let fechaMonthAgo = new Date();
        fechaMonthAgo.setDate(fechaMonthAgo.getMonth() - 1);
        let fechaWeekAgo = new Date();
        fechaWeekAgo.setDate(fechaWeekAgo.getDay() - 7);

        let query = {};
        if (user.email === 'sdamorebieta@ontourup.com') {
            query = {
                organizer: user.organizer,
                //startDate : fechaMonthAgo
            }
        } else {
            query = {
                promoter: user._id,
                // startDate : fechaMonthAgo
            }
        }

        const { status, data } = await promoterEvents(query);
        if (status === 200) {
            const fetcher = async (path) => {
                const infoevents = await takeEventsDashboard(path)
                return await infoevents.data.data[0];
            }
            let inserts = [];
            for (const event of data.data) {
                let eventData = new Date(event.startDate);
                if (fechaWeekAgo < eventData) {
                    inserts.push(fetcher(event._id));
                }
            }
            Promise.all(inserts).then(values => {
                setEvents(fillRows(values));
            });

            setLoading(false);
        } else {
            setEvents([]);
            setLoading(false);
        }
    };

    const fillRows = (events) => {
        let rowArr = [];
        events.forEach((obj, id) => {
            const row = [
                id,
                obj._id,
                obj.name,
                obj.startDate,
                obj.endDate,
                obj.isVisible,
                obj.isWeb,
                obj.tickets,
                obj.tickets,
                obj.points,
                obj.plans
            ];
            rowArr.push(row);
        });
        return rowArr;
    };

    const calculateDashboardInfo = async (isOrganizer, filter) => {
        let report = {}
        if (isOrganizer) {
            report = await getSalesByOrganizer(user.organizer)
        } else {
            report = await getSalesByPromoter(user._id)
        }

        if (report.status === 200) {
            const obj = {
                entrancesCount: report.data.data.count,
                grossPriceCount: report.data.data.amount.toFixed(2)
            }
            setDashboardInfo(obj);
        } else {
            setDashboardInfo({})
        }
    }

    const columns = [{
        name: "column-id",
        options: {
            display: false,
        }
    }, {
        name: "ID",
        options: {
            display: false,
        }
    }, {
        name: "Nombre",
        options: {
            display: true,
        }
    }, {
        name: "fecha",
        label: "Fecha",
        options: {
            display: true,
            customBodyRender: (value, tableMeta, updateValue) => {
                return (moment(value).format('L LT'))
            }
        }
    }, {
        name: "Fecha-fin",
        options: {
            display: false,
            customBodyRender: (value, tableMeta, updateValue) => {
                return (moment(value).format('L LT'))
            }
        }
    }, {
        name: "Publicado",
        options: {
            display: true,
            align: 'center',
            customBodyRender: (value, tableMeta, updateValue) => {
                if (value) {
                    return (
                        <center>
                            <Tooltip title="Publicado">
                                <span><BsFillCircleFill color="green" /></span>
                            </Tooltip>
                        </center>
                    );
                } else {
                    return (
                        <center>
                            <Tooltip title="No Publicado">
                                <BsFillCircleFill color="red" />
                            </Tooltip>
                        </center>
                    );
                }
            },
        }
    }, {
        name: "Visible WEB",
        options: {
            display: true,
            customBodyRender: (value, tableMeta, updateValue) => {
                if (value) {
                    return (
                        <center>
                            <Tooltip title={value.length}>
                                <span><BsFillCircleFill color="green" /></span>
                            </Tooltip>
                        </center>
                    );
                } else {
                    return (
                        <center>
                            <Tooltip title="No Visible">
                                <span><BsFillCircleFill color="red" /></span>
                            </Tooltip>
                        </center>
                    );
                }
            }
        }
    }, {
        name: "Tickets Creados",
        options: {
            display: true,
            customBodyRender: (value, tableMeta, updateValue) => {
                //console.log(value)
                if (value.length > 0) {
                    return (
                        <center>
                            <Tooltip title={value.length}>
                                <span><BsFillCircleFill color="green" /></span>
                            </Tooltip>
                        </center>
                    );
                } else {
                    return (
                        <center>
                            <Tooltip title="No tiene Entradas">
                                <span><BsFillCircleFill color="red" /></span>
                            </Tooltip>
                        </center>
                    );
                }
            }
        }
    }, {
        name: "Estado",
        options: {
            display: true,
            customBodyRender: (value, tableMeta, updateValue) => {
                if (value.length > 0) {
                    let today = new Date();
                    let ticketsAmount = 0;
                    let ticketsBuyed = tableMeta.rowData[9];
                    // Calculamos si el evento tiene entradas disponibles para compar
                    for (let i = 0; i < value.length; i++) {
                        ticketsAmount += value[i].amount;
                    }
                    if (ticketsBuyed >= ticketsAmount) {
                        return (<span style={{ color: 'orange' }}>Agotado </span>);
                    }

                    // Calculamos si el evento esta acabado
                    if (today > new Date(tableMeta[4])) {
                        return (<span>Finalizado</span>);
                    }
                    // Calculamos el startDate mas bajo
                    let startDate = new Date(value[0].sellStartDate);
                    for (let i = 0; i < value.length; i++) {
                        if (startDate > new Date(value[i].sellStartDate)) {
                            startDate = new Date(value[i].sellStartDate);
                        }
                        //console.log(new Date(value[i].startDate));
                    }

                    // Calculamos el endDate mas alto
                    let endDate = new Date(value[0].sellEndDate);
                    for (let i = 0; i < value.length; i++) {
                        if (endDate < new Date(value[i].sellEndDate)) {
                            endDate = new Date(value[i].sellEndDate);
                        }
                        //console.log(new Date(value[i].endDate));
                    }
                    if (startDate < today && endDate > today) {
                        return (<span style={{ color: 'green' }}>En venta</span>)
                    } else if (startDate < today && endDate < today) {
                        return (<span style={{ color: 'red' }}>Fin Venta</span>)
                    } else if (startDate > today && endDate > today) {
                        return (<span style={{ color: 'blue' }}>Sin empezar</span>);
                    }
                    if (value.length > 1) {
                        return (
                            "Multiples entradas"
                        );
                    }
                } else {
                    return (
                        "Sin entradas"
                    );
                }

            }
        }
    }, {
        name: "Creado Acceso",
        options: {
            display: true,
            customBodyRender: (value, tableMeta, updateValue) => {
                if (value.length > 0) {
                    if (value.length > 1) {
                        return (
                            "Multiples Accesos"
                        );
                    } else {
                        return (
                            <center>
                                <Tooltip title={value.length}>
                                    <span><BsFillCircleFill color="green" /></span>
                                </Tooltip>
                            </center>
                        );
                    }
                } else {
                    return (
                        <center>
                            <Tooltip title="No tiene Entradas">
                                <span><BsFillCircleFill color="red" /></span>
                            </Tooltip>
                        </center>
                    );
                }
            }
        }
    }, {
        name: "Asignado empleado",
        options: {
            display: true,
            customBodyRender: (value, tableMeta, updateValue) => {
                if (value.length > 0) {
                    if (value.length > 1) {
                        return (
                            "Multiples empleados"
                        );
                    } else {
                        return (
                            <center>
                                <Tooltip title={value.length}>
                                    <span><BsFillCircleFill color="green" /></span>
                                </Tooltip>
                            </center>
                        );
                    }
                } else {
                    return (
                        <center>
                            <Tooltip title="No tiene Entradas">
                                <span><BsFillCircleFill color="red" /></span>
                            </Tooltip>
                        </center>
                    );
                }
            }
        }
    }
    ];

    const options = {
        expandableRows: true,
        expandableRowsOnClick: true,
        filterType: 'dropdown',
        selectableRows: 'none',
        rowsPerPage: 15,
        print: false,
        sortOrder: {
            name: 'fecha',
            direction: 'asc'
        },
        renderExpandableRow: (rowData, rowMeta) => {
            const event = events[rowData[0]];
            const colSpan = rowData.length + 1;
            return (
                <React.Fragment>
                    <tr>
                        <td colSpan={7}></td>
                        <td colSpan={2}>
                            <>
                                {event[10].length
                                    ? event[10].map((point, i) => {
                                        return (

                                            <Row style={{ padding: '16px 16px 16px 16px' }}>
                                                <Col align="left"><span>{point.name}</span> </Col>
                                                <Col >
                                                    {event[11].length
                                                        ? event[11].map((plan, i) => {
                                                            return (
                                                                <>
                                                                    {point._id === plan.point ? (
                                                                        // Hay que mirar si tiene mas de un trabajador si aparecen 2 bolas verdes
                                                                        <center>
                                                                            <Tooltip title={plan.name}>
                                                                                <span><BsFillCircleFill color="green" /></span>
                                                                            </Tooltip>
                                                                        </center>
                                                                    ) : (
                                                                        ''
                                                                    )}
                                                                </>
                                                            );
                                                        })
                                                        : ''}
                                                </Col>
                                            </Row>
                                        );
                                    })
                                    : ''}
                            </>
                        </td>
                    </tr>
                </React.Fragment>
            );
        },
        customToolbarSelect: () => { },
        textLabels: {
            body: {
                noMatch: 'No se han encontrado registros',
                toolTip: 'Ordenar',
                columnHeaderTooltip: (column) => `Ordenar por ${column.label}`,
            },
            pagination: {
                next: 'Siguiente página',
                previous: 'Página anterior',
                rowsPerPage: 'Filas por página:',
                displayRows: 'de',
            },
            toolbar: {
                search: 'Buscar',
                downloadCsv: 'Descargar CSV',
                print: 'Imprimir',
                viewColumns: 'Mostrar columnas',
                filterTable: 'Filtrar tabla',
            },
            filter: {
                all: 'Todo',
                title: 'Filtros',
                reset: 'Reiniciar',
            },
            viewColumns: {
                title: 'Mostrar columnas',
                titleAria: 'Mostrar/Ocultar columnas de la tabla',
            },
            selectedRows: {
                text: 'fila(s) seleccionada(s)',
                delete: 'Borrar',
                deleteAria: 'Borrar las filas seleccionadas',
            },
        },
    };


    return (
        <>
            {loading === true ? (
                <Loading show={loading} />
            ) : (
                <>
                    {dashboardInfo !== null ? <>
                        <Row className="pb-3">
                            <Col md="3">
                                <Card>
                                    <Card.Body>
                                        <Card.Title>Venta Entradas</Card.Title>
                                        <Card.Text>{dashboardInfo.entrancesCount}</Card.Text>
                                    </Card.Body>
                                    <Card.Footer className="text-muted text-center"></Card.Footer>
                                </Card>
                            </Col>
                            <Col>
                                <Card>
                                    <Card.Body>
                                        <Card.Title>Resumen Ingresos</Card.Title>
                                        <Card.Text>{dashboardInfo.grossPriceCount} €</Card.Text>
                                    </Card.Body>
                                    <Card.Footer className="text-muted text-center"></Card.Footer>
                                </Card>
                            </Col>
                        </Row>
                    </> : ''}
                    <MUIDataTable className="dashboard-table" title='Eventos:' data={events} columns={columns} options={options} />
                </>
            )}
        </>
    );
};

export default Default;
