import { Button, Card, CardContent, Divider, Grid, TextField, Typography, makeStyles } from '@material-ui/core';
import React, {useContext, useEffect, useState} from 'react';
import { authApi, changePassword } from "../../services/auth.service";

import AlertMsg from "../../component/Alert";
import ChangePassword from "../../component/Modals/ChangePassword";
import CircularProgress from '@material-ui/core/CircularProgress';
import Cookies from "js-cookie";
import Logo from './../../assets/images/ontourup-black.png';
import { Link as RouterLink } from 'react-router-dom';
import SendIcon from '@material-ui/icons/Send';
import { UserContext } from "../../context/UserContext";
import clsx from 'clsx';
import { green } from '@material-ui/core/colors';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: '#d7ded4',
        height: '100vh',
        minHeight: '100%',
        display: 'flex',
    },
    backButton: {
        marginLeft: theme.spacing(2),
    },
    card: {
        overflow: 'visible',
        display: 'flex',
        position: 'relative',
        '& > *': {
            flexGrow: 1,
            flexBasis: '50%',
            width: '50%',
        },
        maxWidth: '475px',
        margin: '24px auto',
    },
    content: {
        padding: theme.spacing(5, 4, 3, 4),
    },
    forgot: {
        textDecoration: 'none',
        paddingLeft: '16px',
    },
    margin: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(1),
    },
    button: {
        float: 'right',
        marginTop: '10px'
    },
    buttonSuccess: {
        backgroundColor: green[500],
        '&:hover': {
          backgroundColor: green[700],
        },
      },
    fabProgress: {
        color: green[500],
        position: 'absolute',
        top: -6,
        left: -6,
        zIndex: 1,
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    },
    wrapper: {
        marginTop: theme.spacing(1),
        position: 'relative',
        width: 'max-content',
        alignItems: 'right'
    },
    logo: {
        width: '190px',
    },
}));

const Login = () => {
    let history = useHistory();
    const classes = useStyles();
    const [form, setForm] = useState({
        username: "",
        password: "",
    });
    const { isAuth, setIsAuth, setUser } = useContext(UserContext);
    const [isLoading, setIsLoading] = useState(false);
    const [isOpen, setIsOpen] = useState(false);
    const [openModal, setOpenModal] = useState(false);
    const [message, setMessage] = useState({});
    const [success, setSuccess] = React.useState(false);
    const buttonClassname = clsx({
        [classes.buttonSuccess]: success,
    });

    useEffect(() => {
        if (isAuth) {
            history.push("/");
        }
    }, [isAuth]);

    const handleLogin = async () => {
        setIsOpen(false)
        setIsLoading(true)
        const payload = {
          email: form.username,
          password: form.password,
        };
        const { status, data } = await authApi(payload);
        if (status === 200) {
            setIsLoading(false);
            setIsOpen(false);
            Cookies.set("access", data.data.token);
            Cookies.set("userInfo", data.data.account);
            setUser(data.data.account);
            setSuccess(true)
            setIsAuth(true);
            // await history.push('/');
        } else if (status === 400) {
            setIsLoading(false);
            if (data.message === "User must change password") {
                setOpenModal(true)
            } else {
                setMessage({severity: 'warning', msg: data.message});
                setIsOpen(true);
            }
        } else {
            setMessage({severity: 'warning', msg: data.message});
            setIsOpen(true);
            setIsLoading(false);
        }
    };

    const onChangePass = (e) => {
        setForm({ ...form, password: e.target.value });
    };

    const onChangeUser = (e) => {
        setForm({ ...form, username: e.target.value });
    };

    const handleClose = () => {
        setOpenModal(false);
    };

    const handleChangePassword = async (password) => {
        setOpenModal(false)
        setIsLoading(true)
        const payload = {
          email: form.username,
          password: password,
        };
        const { status, data } = await changePassword(payload);
        if (status === 200) {
            setIsLoading(false);
            setMessage({severity: 'success', msg: '¡Contraseña actualizada correctamente!'});
            setIsOpen(true);
        } else {
            setMessage({severity: 'warning', msg: data.message});
            setIsOpen(true);
            setIsLoading(false);
        }
    };

    return (
        <Grid container justifyContent="center" alignItems="center" className={classes.root}>
            <Grid item xs={11} sm={7} md={6} lg={4}>
                <Card className={classes.card}>
                    <CardContent className={classes.content}>
                        <Grid container direction="column" spacing={4} justifyContent="center">
                            <Grid item xs={12}>
                                <Grid container justifyContent="space-between">
                                    <Grid item>
                                        <Typography color="textPrimary" gutterBottom variant="h2">
                                            Ingreso
                                        </Typography>
                                        <Typography variant="body2" color="textSecondary">
                                            Sistema de administración
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        <RouterLink to="/" className={classes.icon}>
                                            <img alt="Auth method" className={classes.logo} src={Logo} />
                                        </RouterLink>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    autoFocus
                                    label="Email Address / Username"
                                    margin="normal"
                                    name="email"
                                    type="email"
                                    value={form.username}
                                    variant="outlined"
                                    onChange={onChangeUser}
                                />
                                <TextField
                                    fullWidth
                                    label="Password"
                                    margin="normal"
                                    name="password"
                                    type="password"
                                    value={form.password}
                                    variant="outlined"
                                    onChange={onChangePass}
                                />

                                <div className={classes.wrapper}>
                                    <Button
                                    variant="contained"
                                    color="primary"
                                    className={buttonClassname}
                                    disabled={isLoading}
                                    onClick={handleLogin}
                                    endIcon={<SendIcon />}
                                    >
                                    Ingresar
                                    </Button>
                                    {isLoading && <CircularProgress size={24} className={classes.buttonProgress} />}
                                </div>
                            </Grid>
                            {isOpen &&
                                <Grid item xs={12}>
                                    <AlertMsg severity={message.severity} msg={message.msg} />
                                </Grid>
                            }
                            <Divider />
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>
            <ChangePassword open={openModal} onClose={handleClose} onChangePassword={handleChangePassword} />
        </Grid>
    );
};

export default Login;
