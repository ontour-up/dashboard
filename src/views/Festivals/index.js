import {Button, Card, CardContent, Grid, Typography} from '@material-ui/core';
import React, {useEffect, useState} from 'react';
import { deleteFestival, modifyFestival, takeFestival } from "../../services/festival.service";

import AddIcon from '@material-ui/icons/Add';
import AlertMsg from "../../component/Alert";
import Breadcrumb from '../../component/Breadcrumb';
import Confirmation from "../../component/Modals/Confirmation";
import Festival from "../../component/Modals/Festival";
import FestivalCard from "../../component/Cards/Festival";
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import Pagination from '@material-ui/lab/Pagination';
import SnackbarAlert from '../../component/Snackbar';
import { gridSpacing } from '../../store/constant';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    media: {
      height: 140,
    },
    root: {
        width: '100%'
    },
    marginAlert: {
        marginTop: '20px'
    }
});

const Festivals = (props) => {
    const classes = useStyles();
    const [openFestival, setOpenFestival] = useState(false);
    const [isOpen, setIsOpen] = useState(false);
    const [message, setMessage] = useState({});
    const [festivals, setFestivals] = useState([]);
    const [maxPage, setMaxPage] = useState(1);
    const [page, setPage] = useState(1);
    const [limit] = useState(16);
    const [editFestival, setEditFestival] = useState([]);
    const [idFestival, setIdFestival] = useState('');
    const [openConfirmation, setOpenConfirmation] = useState(false);

    useEffect(() => {
        takeFestivals(limit, page);
    }, [props]);

    const takeFestivals = async (limit, page) => {
        const { status, data } = await takeFestival('?limitField='+limit+'&pageField='+page);
        if (status === 200) {
            setFestivals(data.data);
            setMaxPage(data.totalPages)
        } /*else {
           console.log(status, data)
        }*/
    }

    const handleClose = () => {
        setOpenFestival(false);
    };

    const handleCloseSnack = () => {
        setIsOpen(false);
    };
    
    const handleOpenFestival = () => {
        setEditFestival({})
        setOpenFestival(true);
    };

    const onRefresh = () => {
        takeFestivals(limit, page);
    };

    const onOpenSnackBar = (msg) => {
        setMessage(msg);
        setIsOpen(true);
    };

    const handleChangePage = (event, value) => {
        takeFestivals(limit, value);
        setPage(value)
    };

    const onEdit = (value) => {
        setEditFestival(value)
        setOpenFestival(true);
    };

    const onDelete = (value) => {
        setIdFestival(value);
        setOpenConfirmation(true)
    };

    const onConfirm = async () => {
        const { status, data } = await deleteFestival(idFestival);
        if (status === 200) {
            setOpenConfirmation(false);
            setMessage({severity: 'success', msg: data.message});
            setIsOpen(true);
            onRefresh();
        } else {
            setOpenConfirmation(false);
            setMessage({severity: 'warning', msg: data.message});
            setIsOpen(true);
        }
    };

    const handleCloseConfirmation = () => {
        setOpenConfirmation(false);
    };

    const onChangeState = async (id, state) => {
        const { status, data } = await modifyFestival(id, {isActive: state});
        if (status === 200) {
            setOpenConfirmation(false);
            setMessage({severity: 'success', msg: data.message});
            setIsOpen(true);
            onRefresh();
        } else {
            setMessage({severity: 'warning', msg: data.message});
            setIsOpen(true);
            setOpenConfirmation(false);
        }
    };

    return (
        <>
        <Helmet>‍
            <title>Festival - Jazzaldia</title>‍
            <meta name="description" content={"Festival - Jazzaldia"} />
            <meta property="og:title" content={"Festival - Jazzaldia"} />    
            <meta property="og:site_name" content="Jazzaldia" />
            <meta property="og:locale" content="es" />
        </Helmet>
        <React.Fragment>
            <Breadcrumb title="Festivales">
                <Typography component={Link} to="/" variant="subtitle2" color="inherit" className="link-breadcrumb">
                    Inicio
                </Typography>
                <Typography variant="subtitle2" color="primary" className="link-breadcrumb">
                    Festivales
                </Typography>
            </Breadcrumb>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <Card>
                        <CardContent>
                            <Button
                                variant="contained"
                                color="primary"
                                startIcon={<AddIcon />}
                                onClick={handleOpenFestival}
                            >
                                Agregar
                            </Button>
                            <Grid container spacing={gridSpacing}>
                                {festivals.map((festival, index) => {
                                    return (
                                        <FestivalCard key={index} xs={6} md={4} festival={festival} onEdit={onEdit} onDelete={onDelete} onChangeState={onChangeState} />
                                    );
                                })}

                                {festivals.length === 0 && (
                                    <Grid item className={classes.marginAlert}>
                                        <AlertMsg severity="info" msg="No se encontrarón datos" />
                                    </Grid>
                                )}

                                <Grid container direction="row" justifyContent="center" alignItems="center">
                                    <Pagination count={maxPage} page={page} onChange={handleChangePage} color="primary" />
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
                <Festival festival={editFestival} open={openFestival} onClose={handleClose} onRefresh={onRefresh} onOpenSnackBar={onOpenSnackBar} />
                <Confirmation open={openConfirmation} onClose={handleCloseConfirmation} onConfirm={onConfirm} msg="¿Desea eliminar este festival?" txtBtn="SI" />
            </Grid>
            <SnackbarAlert open={isOpen} msg={message.msg} severity={message.severity} duration={4000} onClose={handleCloseSnack} />
        </React.Fragment>
        </>
    );
};

export default Festivals;
