import React, { useEffect, useContext, useState } from 'react';
import { useParams } from 'react-router'
import { makeStyles, Grid, Card, CardHeader, CardContent, Hidden, Typography, Divider, LinearProgress } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import FlightTakeoffIcon from '@material-ui/icons/FlightTakeoff';
import ReportCard from '../Dashboard/Default/ReportCard';
import { takeEventsThumbnail } from '../../services/events.service';
import { gridSpacing } from './../../store/constant';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';

const Cycle = (props) => {
		let { slug } = useParams();
    const theme = useTheme();
		const [events, setEvents] = useState([]);

    useEffect(() => {
		takeEventsPromoter(slug.replace(/-/g, " "));
    }, [slug]);

	const takeEventsPromoter = async (cycle) => {
        const { status, data } = await takeEventsThumbnail(cycle);
        if (status === 200) {
			setEvents(data.data);
        } else {
			setEvents([])
        }
    };
    return (
        <>
        <Helmet>‍
            <title>Eventos</title>‍
            <meta name="description" content={"Jazzaldia"} />
            <meta property="og:title" content={"Jazzaldia"} />    
            <meta property="og:site_name" content="Jazzaldia" />
            <meta property="og:locale" content="es" />
        </Helmet>
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
                <Grid container spacing={gridSpacing}>
                        <>
                        {events.length ? (events.map((event, i) =>{
                            return (
                            <Grid item lg={3} sm={6} xs={12}>
                                <Link to={'events/' + event._id}>
                                    <ReportCard
                                    primary="Evento"
                                    secondary={event.name}
                                    color={theme.palette.success.main}
                                    footerData={event.startDate}
                                    iconPrimary={FlightTakeoffIcon}
                                    iconFooter={TrendingUpIcon}
                                /> 
                                </Link>
                            </Grid>)
                        })) : ''}
                        </>
                </Grid>
            </Grid>
        </Grid>
        </>
    );
};

export default Cycle;
