import { Card, CardContent, CardMedia, Container, Grid, Typography, makeStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { takeBlogSlug, takeOneBlog } from "../../../services/blog.service";

import { Helmet } from 'react-helmet';
import { enUS } from 'date-fns/locale';
import { es } from 'date-fns/locale';
import { eu } from 'date-fns/locale';
import { format } from 'date-fns';
import { fr } from 'date-fns/locale';
import { useParams } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%'
    },
    media: {
        height: 0,
        paddingTop: '30%', // 16:9
    },
    leftM: {
        marginLeft: '10px'
    },
    chip: {
        color: 'white',
        backgroundColor: '#c1565b',
        border: '#c1565b',
        marginTop: '10px'
    },
    chipTwo: {
        color: 'white',
        backgroundColor: '#c1565b',
        border: '#c1565b',
        marginTop: '10px',
        marginLeft: '10px'
    },
    divhtml: {
        width: '100%',
        marginTop: '20px'
    }
}));

const BlogDetail = () => {
    const classes = useStyles();
    let { slug } = useParams();
    let { language } = useParams();
    const [blog, setBlog] = useState({});
    /*var userAgent = navigator.userAgent || navigator.vendor || window.opera;

    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        window.location.href = 'jazzaldia://jazzaldia/'+slug+'/'+language
    }*/

    useEffect(() => {
        takeBlog(slug)
    }, [slug]);

    const takeBlog = async (slug) => {
        const { status, data } = await takeOneBlog('?slug=' + slug);

        if (status === 200) {
            if (data.data.length > 0) {
                setBlog(data.data[0]);
            } else {
                const { status, data } = await takeBlogSlug('?slug=' + slug + '&language=' + language);
                if (status === 200) {
                    if (data.data ) setBlog(data.data[0]);
                }
            }
            
        } /*else {
            console.log(status, data)
        }*/
    }

    const translateName = () => {
        let name = ''
        if (language !== '' && language !== null && language !== undefined) {
            if (language === 'en') name = blog.name_translated?.en;
            if (language === 'fr') name = blog.name_translated?.fr;
            if (language === 'eu') name = blog.name_translated?.eu;
            if (language === 'es') name = blog.name;
        } else {
            name = blog.name;
        }
        return name;
    }

    const translateDescription = () => {
        let description = ''
        if (language !== '' && language !== null && language !== undefined) {
            if (language === 'en') description = blog.description_translated?.en;
            if (language === 'fr') description = blog.description_translated?.fr;
            if (language === 'eu') description = blog.description_translated?.eu;
            if (language === 'es') description = blog.description;
        } else {
            description = blog.description;
        }
        return description;
    }

    const formatDateTranslate = () => {
        let fecha = ''
        if (language !== '' && language !== null && language !== undefined) {
            if (language === 'en') fecha = format(new Date(blog.startDate.toString().substring(0, 16)), 'd MMMM yyyy', { locale: enUS });
            if (language === 'fr') fecha = format(new Date(blog.startDate.toString().substring(0, 16)), 'd MMMM yyyy', { locale: fr });
            if (language === 'eu') fecha = format(new Date(blog.startDate.toString().substring(0, 16)), 'd MMMM yyyy', { locale: eu });
            if (language === 'es') fecha = format(new Date(blog.startDate.toString().substring(0, 16)), 'd MMMM yyyy', { locale: es });
        } else {
            fecha = format(new Date(blog.startDate.toString().substring(0, 16)), 'd MMMM yyyy', { locale: es });
        }

        return fecha;
    }

    return (
        <>
        {blog ? (
            <>
            <Helmet>‍
                <title>{blog.name}</title>‍
                <meta name="description" content={translateName()} />
                <meta property="og:title" content={translateName()} />    
                <meta property="og:description" content={translateName()} />  
                <meta property="og:image" content={blog.banner} />
                <meta property="og:url" content={translateName()} />
                <meta property="og:site_name" content="Jazzaldia" />
                <meta property="og:locale" content="es" />
                <meta property="og:type" content="article" />
            </Helmet>
            <Grid container justifyContent="center" alignItems="center" className={classes.root}>
                <Card className={classes.root}>
                    <CardMedia
                        className={classes.media}
                        image={blog.banner}
                        title="Paella dish"
                    />
                    <CardContent>
                        <Container maxWidth="md">
                            <Typography variant="h1" component="h2">{translateName()}</Typography>
                            {blog.startDate ? (<Typography variant="p" component="p">{formatDateTranslate()}</Typography>) : <></>}
                            <br />
                            <div className={classes.divhtml} dangerouslySetInnerHTML={{ __html: translateDescription() }}></div>
                        </Container>
                    </CardContent>
                </Card>
            </Grid>
            </>
        ):<></>}
        </>
    );
}

export default BlogDetail;