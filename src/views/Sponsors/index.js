import { Button, Card, CardContent, Grid, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { deleteSponsor, modifySponsor, takeSponsor } from "../../services/sponsor.service";

import AddIcon from '@material-ui/icons/Add';
import AlertMsg from "../../component/Alert";
import Breadcrumb from '../../component/Breadcrumb';
import Confirmation from "../../component/Modals/Confirmation";
import DrawerFilter from "../../component/Drawer/Filter";
import { Helmet } from 'react-helmet';
import IconButton from '@material-ui/core/IconButton';
import { Link } from 'react-router-dom';
import Pagination from '@material-ui/lab/Pagination';
import Search from "../../component/Search";
import SnackbarAlert from '../../component/Snackbar';
import Sponsor from "../../component/Modals/Sponsor";
import SponsorCard from "../../component/Cards/Sponsor";
import TuneIcon from '@material-ui/icons/Tune';
import clsx from 'clsx';
import { gridSpacing } from '../../store/constant';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    media: {
        height: 140,
    },
    root: {
        width: '100%'
    },
    marginAlert: {
        marginTop: '20px'
    },
    hide: {
        display: 'none',
    },
});

const Sponsors = (props) => {
    const classes = useStyles();
    const [openSponsor, setOpenSponsor] = useState(false);
    const [isOpen, setIsOpen] = useState(false);
    const [message, setMessage] = useState({});
    const [sponsors, setSponsors] = useState([]);
    const [maxPage, setMaxPage] = useState(1);
    const [page, setPage] = useState(1);
    const [limit] = useState(15);
    const [editSponsor, setEditSponsor] = useState([]);
    const [openDrawerFilter, setOpenDrawerFilter] = useState(false);
    const [formFilter, setFormFilter] = useState({});
    const [idSponsor, setIdSponsor] = useState('');
    const [openConfirmation, setOpenConfirmation] = useState(false);

    useEffect(() => {
        takeSponsors(limit, page);
        addDrawerFilter();
    }, [props]);

    const addDrawerFilter = () => {
        setFormFilter({
            selectFirst:
            {
                name: 'Categorías',
                items: [
                    { _id: 'sponsor', name: 'Patrocinadores' },
                    { _id: 'collaborator', name: 'Colabodadores' }
                ]
            }
        });
    };

    const takeSponsors = async (limit, page) => {
        const { status, data } = await takeSponsor('?populateFields=festival&festival=619e179477275183e38adb93&sortField=position&limitField=' + limit + '&pageField=' + (page-1));
        if (status === 200) {
            setSponsors(data.data);
            setMaxPage(data.data.length / data.limit)
        } /*else {
            console.log(status, data)
        }*/
    }

    const handleClose = () => {
        setOpenSponsor(false);
    };

    const handleCloseSnack = () => {
        setIsOpen(false);
    };

    const handleOpenSponsor = () => {
        setEditSponsor({})
        setOpenSponsor(true);
    };

    const onRefresh = () => {
        takeSponsors(limit, page);
    };

    const onOpenSnackBar = (msg) => {
        setMessage(msg);
        setIsOpen(true);
    };

    const handleChangePage = (event, value) => {
        takeSponsors(limit, value);
        setPage(value)
    };

    const onEdit = (value) => {
        setEditSponsor(value)
        setOpenSponsor(true);
    };

    const onSearch = async (value) => {
        const { status, data } = await takeSponsor('?name=' + value + '&populateFields=festival');
        if (status === 200) {
            setSponsors(data.data);
            setMaxPage(1)
        } /*else {
            console.log(status, data)
        }*/
    };

    const handleDrawerOpen = () => {
        setOpenDrawerFilter(true);
    };

    const handleDrawerClose = () => {
        setOpenDrawerFilter(false);
    };

    const searchCategories = async (value) => {
        const { status, data } = await takeSponsor('?category=' + value + '&populateFields=festival');
        if (status === 200) {
            setSponsors(data.data);
            setMaxPage(1)
            setOpenDrawerFilter(false);
        } /*else {
            console.log(status, data)
        }*/
    };

    const onReset = () => {
        takeSponsors(limit, page);
        setOpenDrawerFilter(false);
    };

    const onDelete = (value) => {
        setIdSponsor(value);
        setOpenConfirmation(true)
    };

    const onConfirm = async () => {
        const { status, data } = await deleteSponsor(idSponsor);
        if (status === 200) {
            setOpenConfirmation(false);
            setMessage({ severity: 'success', msg: data.message });
            setIsOpen(true);
            onRefresh();
        } else {
            setOpenConfirmation(false);
            setMessage({ severity: 'warning', msg: data.message });
            setIsOpen(true);
        }
    };

    const handleCloseConfirmation = () => {
        setOpenConfirmation(false);
    };

    const onChangeState = async (id, state) => {
        const { status, data } = await modifySponsor(id, {isActive: state});
        if (status === 200) {
            setOpenConfirmation(false);
            setMessage({severity: 'success', msg: data.message});
            setIsOpen(true);
            onRefresh();
        } else {
            setMessage({severity: 'warning', msg: data.message});
            setIsOpen(true);
            setOpenConfirmation(false);
        }
    };

    return (
        <>
            <Helmet>‍
                <title>Sponsors - Jazzaldia</title>‍
                <meta name="description" content={"Sponsors - Jazzaldia"} />
                <meta property="og:title" content={"Sponsors - Jazzaldia"} />    
                <meta property="og:site_name" content="Jazzaldia" />
                <meta property="og:locale" content="es" />
            </Helmet>
            <React.Fragment>
                <Breadcrumb title="Patrocinadores">
                    <Typography component={Link} to="/" variant="subtitle2" color="inherit" className="link-breadcrumb">
                        Inicio
                    </Typography>
                    <Typography variant="subtitle2" color="primary" className="link-breadcrumb">
                        Patrocinadores
                    </Typography>
                </Breadcrumb>
                <Grid container spacing={gridSpacing}>
                    <Grid item xs={12}>
                        <Card>
                            <CardContent>
                                <Grid container spacing={gridSpacing}>
                                    <Grid item xs={12} md={6}>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            startIcon={<AddIcon />}
                                            onClick={handleOpenSponsor}
                                        >
                                            Agregar
                                        </Button>
                                    </Grid>
                                    <Grid item xs={12} md={5} align="right">
                                        <Search onSearch={onSearch} />
                                    </Grid>
                                    <Grid item xs={1} align="right">
                                            <IconButton style={{ paddingTop: '8px' }} color="inherit" aria-label="open Search" onClick={handleDrawerOpen} className={clsx(openDrawerFilter && classes.hide)} > <TuneIcon /> </IconButton>
                                    </Grid>
                                </Grid>

                                <Grid container spacing={gridSpacing}>

                                    {sponsors.map((sponsor, index) => {
                                        return (
                                            <SponsorCard key={index} xs={6} md={4} sponsor={sponsor} onEdit={onEdit} onDelete={onDelete} onChangeState={onChangeState} />
                                        );
                                    })}

                                    {sponsors.length === 0 && (
                                        <Grid item className={classes.marginAlert}>
                                            <AlertMsg severity="info" msg="No se encontrarón datos" />
                                        </Grid>
                                    )}


                                    <Grid container direction="row" justifyContent="center" alignItems="center">
                                        <Pagination count={maxPage} page={page} onChange={handleChangePage} color="primary" />
                                    </Grid>

                                    <DrawerFilter open={openDrawerFilter} onClose={handleDrawerClose} forms={formFilter} onSelectFirst={searchCategories} onReset={onReset} />
                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Sponsor sponsor={editSponsor} open={openSponsor} onClose={handleClose} onRefresh={onRefresh} onOpenSnackBar={onOpenSnackBar} />
                    <Confirmation open={openConfirmation} onClose={handleCloseConfirmation} onConfirm={onConfirm} msg="¿Desea eliminar este patrocinador?" txtBtn="SI" />
                </Grid>

            </React.Fragment>
            <SnackbarAlert open={isOpen} msg={message.msg} severity={message.severity} duration={4000} onClose={handleCloseSnack} />
        </>
    );
};

export default Sponsors;
