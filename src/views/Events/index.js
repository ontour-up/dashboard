import {
    Button,
    ButtonGroup,
    Grid,
    makeStyles,
} from '@material-ui/core';
import React, { useCallback, useContext, useEffect, useState } from 'react';
import { promoterEvents, takeEventsCycle, takeEventsThumbnail, organizerEvents } from '../../services/events.service';
import { takeCountElementsPromoter } from '../../services/statistic.service';

import Cookies from 'js-cookie';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import ReportCard from './ReportCard';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import { UserContext } from '../../context/UserContext';
import { gridSpacing } from './../../store/constant';
import { useHistory } from 'react-router-dom';
import { useTheme } from '@material-ui/core/styles';
import CreateEvent from '../../component/Modals/CreateEvent';
import SnackbarAlert from '../../component/Snackbar';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import ConfirmationModal from '../../component/Modals/ConfirmationBootstrap';
import Loading from '../../component/Loading';

const useStyles = makeStyles((theme) => ({
    arrowicon: {
        '& svg': {
            width: '20px',
            height: '20px',
            verticalAlign: 'top',
        },
    },
    flatcardbody: {
        padding: '0px !important',
        '& svg': {
            width: '40px',
            height: '40px',
        },
    },
    flatcardblock: {
        padding: '25px 25px',
        borderLeft: '1px solid' + theme.palette.background.default,
        [theme.breakpoints.down('xs')]: {
            borderLeft: 'none',
            borderBottom: '1px solid' + theme.palette.background.default,
        },
        [theme.breakpoints.down('sm')]: {
            borderBottom: '1px solid' + theme.palette.background.default,
        },
    },
    textsuccess: {
        color: theme.palette.success.main,
    },
    texterror: {
        color: theme.palette.error.main,
    },
}));

const Default = (props) => {
    const theme = useTheme();
    const [data, setData] = useState({});
    const [apiCookie, setApiCookie] = useState(false);
    const [events, setEvents] = useState([]);
    const [eventos, setEventos] = useState([]);
    const [oldEvents, setOldEvents] = useState([]);
    const { user } = useContext(UserContext);
    const [ cicle, setCicle ] = useState("Todos");
    const [ciclos, setCiclos] = useState([]);
    const [loading, setLoading] = useState(true);
    const [ showOldEvents, setShowOldEvents ] = useState(false);

    const [confirmationModalShow, setConfirmationModalShow] = useState(false);
    const [confirmationModalData, setConfirmationModalData] = useState({
        title: '',
        description: ''
    });

    //-------- MODAL CREATE EVENT --------//
    const [editEvent, setEditEvent] = useState(null);
    const [openEvent, setOpenEvent] = useState(false);
    const [isOpen, setIsOpen] = useState(false);
    const [message, setMessage] = useState({});
    //------------------------------------//
    let history = useHistory();

    useEffect(() => {
        if (user.roles.indexOf('promoter') >= 0) {
            takeStatisticCountPromoter(user._id);
        }

        eventsCycle(user._id);
        eventsPromoter(user._id);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props]);

    //-------- MODAL CREATE EVENT --------//
    const handleOpenEvent = () => {
        setEditEvent(null);
        setOpenEvent(true);
    };

    const handleEditEvent = (data) => {
        setEditEvent(data);
        setOpenEvent(true);
    };

    const handleClose = () => {
        setConfirmationModalData({"title": "Confirmation" ,"description": "Desea salir de Crear evento?"})
        setConfirmationModalShow(true);
    };

    const onConfirmClose = () =>{
        setConfirmationModalShow(false);
        setOpenEvent(false);
    }

    const onCancelClose = () =>{
        setConfirmationModalShow(false);
        setOpenEvent(true);
    }

    const onRefresh = () => {
        setEditEvent({});
        apisKeys(user);
        if (user.roles.indexOf('olas') >= 0) {
            history.push('/notices');
        }
        if (user.roles.indexOf('promoter') >= 0) {
            takeStatisticCountPromoter(user._id);
        }

        if (loading && apiCookie) {
            setLoading(false);
            eventsCycle(user._id);
            eventsPromoter(user._id);
        }
    };

    const onFinish = () => {
        setLoading(false);
        setOpenEvent(false);
        eventsCycle(user._id);
        eventsPromoter(user._id);
    
    }

    const onOpenSnackBar = (msg) => {
        setMessage(msg);
        setIsOpen(true);
    };

    const handleCloseSnack = () => {
        setIsOpen(false);
    };

    //------------------------------------//

    const apisKeys = (user) => {
        if (user.email === 'portu@ontourup.com') {
            Cookies.set('x_api_key', '/YU5dsPp+gJMNVv73hz53CH+F/ncJha_');
            setApiCookie(true);
        } else if (user.email === 'bilbao@ontourup.com') {
            Cookies.set('x_api_key', '0LAs-xPlhVzfRWdXRxV.jL1siWbfyh9F');
            setApiCookie(true);
        } else if (user.email === 'getxo@ontourup.com') {
            Cookies.set('x_api_key', 'pU~ztakzzCJs28nZGrd.q-zEyi80foqN');
            setApiCookie(true);
        } else if(user.email === 'sdamorebieta@ontourup.com'){
            Cookies.set('x_api_key', 'd~vL-9kd1aoYKpdiIY80M401j55B7IB0');
            setApiCookie(true);
        } else {
            Cookies.set('x_api_key', 'Y5gO6/Q/bddQrjtTb+OEMJ_LY8fgoS+I');
            setApiCookie(true);
        }
    };

    const eventsPromoter = async () => {
        setCicle("Todos");
        let eve = [];
        let oldEve = [];
        let fecha = new Date();
        fecha.setDate(fecha.getDate()-3);
        if(user._id === "6215254da82d2aec3ece5d20" || user._id === '6271424f9b9199541920e55e'){
            if(user.organizer){
                const { status, data } = await organizerEvents(user.organizer);
                if (status === 200) {
                    for (const infoEvents of data.data) {
                        let eventData = new Date(infoEvents.startDate);
                        if(fecha < eventData){
                            eve.push(infoEvents);
                        }else{
                            oldEve.push(infoEvents);
                        }
                    }
                    setEvents(eve);
                    setOldEvents(oldEve);
                    setLoading(false);
                } else {
                    setEvents([]);
                    setLoading(false);
                }
            }
        }else{
            const query = {
                promoter : user._id,
                isVisible: true
            }
            const { status, data } = await promoterEvents(query);
            if (status === 200) {
                for (const infoEvents of data.data) {
                    let eventData = new Date(infoEvents.startDate);
                    if(fecha < eventData){
                        eve.push(infoEvents);
                    }else{
                        oldEve.push(infoEvents);
                    }
                }
                setEvents(eve);
                setEventos(eve);
                setOldEvents(oldEve);
                setLoading(false);
            } else {
                setEvents([]);
                setLoading(false);
            }
        }
    };

    const takeStatisticCountPromoter = async (id) => {
        const { status, data } = await takeCountElementsPromoter(id);
        if (status === 200) {
            setData(data);
        } else {
            setData({});
        }
    };

    const takeEventsByCycle = useCallback(
        async (cyclo) => {
            let eve = [];
            let oldEve = [];
            let fecha = new Date();
            fecha.setDate(fecha.getDate()-3);

            if(user._id === "6215254da82d2aec3ece5d20"){
                if(user.organizer){
                    const { status, data } = await organizerEvents(user.organizer);
                    if (status === 200) {
                        for (const infoEvents of data.data) {
                            let eventData = new Date(infoEvents.startDate);
                            if(fecha < eventData){
                                eve.push(infoEvents);
                            }else{
                                oldEve.push(infoEvents);
                            }
                        }
                        setEvents(eve);
                        setOldEvents(oldEve);
                        setCicle(cyclo);
                    } else {
                        setEvents([]);
                    }
                }
            }else{
                const query = {
                    tags : cyclo,
                    isWebVisible : true
                }
                const { status, data } = await takeEventsThumbnail(query);
                if (status === 200) {
                    for (const infoEvents of data.data) {
                        let eventData = new Date(infoEvents.startDate);
                        if(fecha < eventData){
                            eve.push(infoEvents);
                        }else{
                            oldEve.push(infoEvents);
                        }
                    }
                    setEvents(eve);
                    setOldEvents(oldEve);
                    setCicle(cyclo);
                } else {
                    setEvents([]);
                }
            }
        },
        [loading]
    );

    const eventsCycle = async () => {
        const { status, data } = await takeEventsCycle(user._id);
        let ciclosArr  = [];
        if (status === 200) {
            for (const infoEvents of data.data) {
                ciclosArr.push(infoEvents.name);
            }
            setCiclos(data.data);
        } else {
            setEvents([]);
            setCiclos([]);
        }
    };

    const showOldEventsHandle = () => {
        if(showOldEvents)
            setShowOldEvents(false);
        else
            setShowOldEvents(true);
     };
console.log(events);
    return (
        <>
            {loading === true ? (
                <Loading show={loading} />
            ) : (
                <>
                    {/*CREATE EVENT BUTTON*/}
                    <Button variant="contained" color="primary" startIcon={<AddIcon />} onClick={handleOpenEvent}>
                        Agregar evento
                    </Button>
                    <br />
                    <br />
                    {/* -------------------------------------- */}
                    <div style={{ marginBottom: '40px', justifyContent: 'center' }}>
                        
                        {ciclos.map((data, i) => {
                            return (
                                
                                <ButtonGroup aria-label="Basic example">
                                    <Button size="large" key={i} onClick={() => takeEventsByCycle(data.name)}>
                                        {data.name}
                                    </Button>
                                    <Button size="large" key={i} style={{ marginRight: '10px' }} onClick={() => handleEditEvent(data)}><EditIcon/></Button>
                              </ButtonGroup>
                            );
                        })}
                    </div>
                    <div className="border-top border-bottom pt-2 pb-2 mb-3 mt-3 border-3 p-2" style={{display: 'flex', justifyContent:'space-between', alignItems:'center'}}>
                        <h5>Listado eventos - {cicle}</h5>
                        <div className="pull-right">
                        <Button variant="primary" onClick={showOldEventsHandle}>{showOldEvents ? 'Ocultar Eventos pasados' : 'Mostrar eventos pasados'}</Button>
                        </div>
                    </div>
                    <Helmet>
                        ‍<title>Eventos</title>‍
                        <meta name="description" content={'Eventos'} />
                        <meta property="og:title" content={'Eventos'} />
                        <meta property="og:site_name" content="Eventos" />
                        <meta property="og:locale" content="es" />
                    </Helmet>

                    <Grid container spacing={gridSpacing}>
                        <Grid item xs={12}>
                            <Grid container spacing={gridSpacing}>
                                <>
                                {showOldEvents === true ? (
                                    <>
                                    {oldEvents.map((event, i) => {
                                        let fecha = new Date(event.startDate);
                                        return (
                                            <Grid key={i} item lg={3} sm={6} xs={12}>
                                                <Link to={'event/' + event._id + '/resume'} style={{ textDecoration: 'none' }}>
                                                    <ReportCard
                                                        img={event.banner ? event.banner : 'Sin imagen'}
                                                        primary={event.name ? event.name : 'Sin nombre'}
                                                        secondary={
                                                            <>
                                                            {event.ubication ? event.ubication.location : 'Sin ubicación'}
                                                            {cicle === "Todos" ? event.tags[0] : 'Nada'}
                                                            </>
                                                        }
                                                        color={theme.palette.success.main}
                                                        footerData={fecha ? fecha.toLocaleString() : 'Sin fecha'}
                                                        iconPrimary={''}
                                                        iconFooter={TrendingUpIcon}
                                                    />
                                                </Link>
                                            </Grid>
                                        );
                                    })}
                                    </>
                                ) : (
                                    ''
                                )}

                                {events.length
                                    ? events.map((event, i) => {
                                            let fecha = new Date(event.startDate);
                                            return (
                                                <Grid key={i} item lg={3} sm={6} xs={12}>
                                                    <Link to={'event/' + event._id + '/resume'} style={{ textDecoration: 'none' }}>
                                                        <ReportCard
                                                            img={event.banner ? event.banner : 'Sin imagen'}
                                                            primary={event.name ? event.name : 'Sin nombre'}
                                                            secondary={
                                                                <>
                                                                <p>{event.ubication ? event.ubication.location : 'Sin ubicación'} </p>
                                                                <>{cicle === "Todos" ? event.tags ? event.tags : ''  : ''}</>
                                                                </>
                                                            }
                                                            color={theme.palette.success.main}
                                                            footerData={fecha ? fecha.toLocaleString() : 'Sin fecha'}
                                                            iconPrimary={''}
                                                            iconFooter={TrendingUpIcon}
                                                        />
                                                    </Link>
                                                </Grid>
                                            );
                                        })
                                    : ''}
                                </>
                            </Grid>
                        </Grid>
                        <CreateEvent
                            open={openEvent}
                            event={editEvent}
                            promoter={user._id}
                            onClose={handleClose}
                            onFinish={onFinish}
                            onRefresh={onRefresh}
                            onOpenSnackBar={onOpenSnackBar}
                        />
                    </Grid>
                    <SnackbarAlert open={isOpen} msg={message.msg} severity={message.severity} duration={4000} onClose={handleCloseSnack} />
                    <ConfirmationModal data={confirmationModalData} show={confirmationModalShow} onConfirm={onConfirmClose} onCancel={onCancelClose} onHide={() => onCancelClose} />
                </>
            )}
        </>
    );
};

export default Default;
