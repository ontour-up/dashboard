import React, { useState } from 'react'
import { Button, FormControl, Grid, InputLabel, MenuItem, Select } from '@material-ui/core';
import { useFormik } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { es } from 'date-fns/locale';
import { format } from 'date-fns';
import * as yup from 'yup';
import { GetApp } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
    formControl: {
        width: '100%',
    },
}));

const validationSchema = yup.object({
    title: yup.string().required(),
});

const ReportUsersExcel = () => {
    const classes = useStyles();
    const [dateCreation, SetDateCreation] = useState(null);
    const formik = useFormik({
        initialValues: {
            creationDate: null,
        },
        validationSchema: validationSchema,
        // onSubmit: (values) => {
        //     onSubmit(values);
        // },
    });
    const handleDatePChange = (e) => {
        SetDateCreation(e)
        formik.setFieldValue('creationDate', format(e, "yyy-MM-dd"))
    }

    return (
        <div>
            <Grid container spacing={3}>
                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel id="label-festival">Festival</InputLabel>
                        <Select
                            labelId="label-festival"
                            id="festival"
                            name="festival"
                            helpertext="hols"
                            label="Seleccionar tipo de usuario"
                            // value={formik.values.festival}
                            // onChange={handleChangeFestival}
                            // error={formik.touched.festival && Boolean(formik.errors.festival)}
                            // helpertext={formik.touched.festival && formik.errors.festival}
                        >
                                <MenuItem value='instrumental'>Instrumental</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={6}>
                <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                    <DatePicker
                        autoOk
                        fullWidth
                        clearable
                        id="dateCreation"
                        name="dateCreation"
                        ampm={false}
                        value={dateCreation}
                        onChange={handleDatePChange}
                        format="yyyy-MM-dd"
                        label="Fecha de creación"
                    />
                </MuiPickersUtilsProvider>
                </Grid>
                <Grid item xs={12} md={6}>
                    <Button
                        variant="contained"
                        color="primary"
                        startIcon={<GetApp />}
                        onClick={()=>{console.log('hola');}}
                    >
                        Descargar
                    </Button>
                </Grid>
            </Grid>
        </div>
    )
}

export default ReportUsersExcel
