import React, { useState, useEffect } from 'react';
import { Button, Card, CardHeader, CardContent, IconButton, Grid, Typography, CardMedia, CardActions } from '@material-ui/core';
import { Link } from 'react-router-dom';
import Breadcrumb from '../../../component/Breadcrumb';
import { makeStyles } from '@material-ui/core/styles';
import { gridSpacing } from '../../../store/constant';
import { Helmet } from 'react-helmet';
import ReportBlog from "../../../component/Modals/ReportBlog";
import SnackbarAlert from '../../../component/Snackbar';

const useStyles = makeStyles({
    media: {
        height: 140,
    },
    root: {
        display: 'flex',
    },
    marginAlert: {
        marginTop: '20px'
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: 151,
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
    },
});


const ReportBlogs = (props) => {
    const classes = useStyles();
    const [openModal, setopenModal] = useState(false);
    const [isOpen, setIsOpen] = useState(false);
    const [message, setMessage] = useState({});
    const [typeReport, setTypeRepor] = useState(1);

    const handleClose = () => {
        setopenModal(false);
    };

    const onOpenSnackBar = (msg) => {
        setMessage(msg);
        setIsOpen(true);
    };

    const handleCloseSnack = () => {
        setIsOpen(false);
    };

    const handleOpenModalOne = () => {
        setTypeRepor(1);
        setopenModal(true);
    };

    const handleOpenModalTwo = () => {
        setTypeRepor(2);
        setopenModal(true);
    };

    useEffect(() => {
    }, [props]);

    return (
        <>
            <Helmet>‍
                <title>Reporte de blogs - Jazzaldia</title>‍
                <meta name="description" content={"Reporte de blogs - Jazzaldia"} />
                <meta property="og:title" content={"Reporte de blogs - Jazzaldia"} />
                <meta property="og:site_name" content="Jazzaldia" />
                <meta property="og:locale" content="es" />
            </Helmet>
            <React.Fragment>
                <Breadcrumb title="Reporte de blogs">
                    <Typography component={Link} to="/" variant="subtitle2" color="inherit" className="link-breadcrumb">
                        Inicio
                    </Typography>
                    <Typography variant="subtitle2" color="inherit" className="link-breadcrumb">
                        Reporte
                    </Typography>
                    <Typography variant="subtitle2" color="primary" className="link-breadcrumb">
                        Blogs
                    </Typography>
                </Breadcrumb>
                <Grid container spacing={gridSpacing}>

                    <Grid item xs={6} sm={4}>
                        <Card className={classes.root}>
                            <div className={classes.details}>
                                <CardContent className={classes.content}>
                                    <Typography component="h5" variant="h5">
                                        Reporte general
                                    </Typography>
                                    <Button onClick={handleOpenModalOne} color="primary" size="small">Generar</Button>
                                </CardContent>
                            </div>
                            <CardMedia
                                className={classes.cover}
                                image="https://ontour-up.s3.eu-central-1.amazonaws.com/report-general-1.png"
                                title="Reporte general"
                            />
                        </Card>

                    </Grid>
                    <Grid item xs={6} sm={4}>
                        <Card className={classes.root}>
                            <div className={classes.details}>
                                <CardContent className={classes.content}>
                                    <Typography component="h5" variant="h5">
                                        Reporte Likes
                                    </Typography>
                                    <Button onClick={handleOpenModalTwo} color="primary" size="small">Generar</Button>
                                </CardContent>
                            </div>
                            <CardMedia
                                className={classes.cover}
                                image="https://ontour-up.s3.eu-central-1.amazonaws.com/report-like-2.png"
                                title="Reporte Likes"
                            />
                        </Card>
                    </Grid>

                </Grid>
                <ReportBlog typeReport={typeReport} open={openModal} onClose={handleClose} onOpenSnackBar={onOpenSnackBar} />
                <SnackbarAlert open={isOpen} msg={message.msg} severity={message.severity} duration={4000} onClose={handleCloseSnack} />
            </React.Fragment>
        </>
    );
};

export default ReportBlogs;
