import { Button, Card, CardContent, Grid, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { deleteNotification, takesNotifications } from "../../../services/notification.service";

import AddIcon from '@material-ui/icons/Add';
import AlertMsg from "../../../component/Alert";
import Breadcrumb from '../../../component/Breadcrumb';
import Confirmation from "../../../component/Modals/Confirmation";
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import NotificationOlas from "../../../component/Modals/NotificationOlas";
import Pagination from '@material-ui/lab/Pagination';
import PaginationTable from "../../../component/Tables/PaginationTable";
import SnackbarAlert from '../../../component/Snackbar';
import { gridSpacing } from '../../../store/constant';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    media: {
        height: 140,
    },
    root: {
        width: '100%'
    },
    marginAlert: {
        marginTop: '20px'
    }
});

const NotificationsOlas = (props) => {
    const classes = useStyles();
    const [openNotification, setOpenNotification] = useState(false);
    const [isOpen, setIsOpen] = useState(false);
    const [message, setMessage] = useState({});
    const [notifications, setNotifications] = useState([]);
    const [maxPage, setMaxPage] = useState(1);
    const [page, setPage] = useState(1);
    const [limit] = useState(10);
    const [editNotification, setEditNotification] = useState([]);
    const [idNotification, setIdNotification] = useState('');
    const [openConfirmation, setOpenConfirmation] = useState(false);
    const blogID = '619e179477275183e38adb93';
    const columns = [
        { id: 'title', label: 'Título', minWidth: 170, align: 'left' },
        { id: 'type', label: 'Tipo', align: 'left' },
        { id: 'state', label: 'Estado', align: 'left' },
        // { id: 'accounts', label: 'Número de enviados', align: 'left' },
        { id: 'scheduledDate', label: 'Fecha de envió', align: 'left' },
        { id: 'created', label: 'Fecha de creación', align: 'right' },
    ];

    useEffect(() => {
        takeNotifications(limit, page);
    }, [props]);

    const takeNotifications = async (limit, page) => {
        const { status, data } = await takesNotifications('?festival=' + blogID + '&limitField=' + limit + '&pageField=' + (page-1));
        if (status === 200) {
            setNotifications(data.data.reverse());
            setMaxPage(data.totalPages)
        } /*else {
            console.log(status, data)
        }*/
    }

    const handleClose = () => {
        setOpenNotification(false);
    };

    const handleCloseSnack = () => {
        setIsOpen(false);
    };

    const handleOpenNotification = () => {
        setEditNotification({})
        setOpenNotification(true);
    };

    const onRefresh = () => {
        takeNotifications(limit, page);
    };

    const onOpenSnackBar = (msg) => {
        setMessage(msg);
        setIsOpen(true);
    };

    const handleChangePage = (event, value) => {
        takeNotifications(limit, value);
        setPage(value)
    };

    const onEdit = (value) => {
        setEditNotification(value)
        setOpenNotification(true);
    };

    const onDelete = (value) => {
        setIdNotification(value);
        setOpenConfirmation(true)
    };

    const onConfirm = async () => {
        const { status, data } = await deleteNotification(idNotification);
        if (status === 200) {
            setOpenConfirmation(false);
            setMessage({severity: 'success', msg: data.message});
            setIsOpen(true);
            onRefresh();
        } else {
            setOpenConfirmation(false);
            setMessage({severity: 'warning', msg: data.message});
            setIsOpen(true);
        }
    };

    const handleCloseConfirmation = () => {
        setOpenConfirmation(false);
    };

    return (
        <>
        <Helmet>‍
            <title>Notificaciones - Punta Galea Challenge</title>‍
            <meta name="description" content={"Notificaciones - Punta Galea Challenge"} />
            <meta property="og:title" content={"Notificaciones - Punta Galea Challenge"} />
            <meta property="og:site_name" content="Punta Galea Challenge" />
            <meta property="og:locale" content="es" />
        </Helmet>
        <React.Fragment>
            <Breadcrumb title="Notificaciones Punta Galea Challenge">
                <Typography component={Link} to="/" variant="subtitle2" color="inherit" className="link-breadcrumb">
                    Inicio
                </Typography>
                <Typography variant="subtitle2" color="primary" className="link-breadcrumb">
                    Notificaciones de Punta Galea Challenge
                </Typography>
            </Breadcrumb>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <Card>
                        <CardContent>
                            <Button
                                variant="contained"
                                color="primary"
                                startIcon={<AddIcon />}
                                onClick={handleOpenNotification}
                            >
                                Agregar
                            </Button>
                            <Grid container spacing={gridSpacing}>

                                {notifications.length > 0 && (
                                    <PaginationTable columns={columns} rows={notifications} title="Listado de notificaciones" onDelete={onDelete} />

                                )}

                                {notifications.length === 0 && (
                                    <Grid item className={classes.marginAlert}>
                                        <AlertMsg severity="info" msg="No se encontrarón datos" />
                                    </Grid>
                                )}

                                <Grid container direction="row" justifyContent="center" alignItems="center">
                                    <Pagination count={maxPage} page={page} onChange={handleChangePage} color="primary" />
                                </Grid>

                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
                <NotificationOlas notification={editNotification} open={openNotification} onClose={handleClose} onRefresh={onRefresh} onOpenSnackBar={onOpenSnackBar} blogID={blogID} />
                <Confirmation open={openConfirmation} onClose={handleCloseConfirmation} onConfirm={onConfirm} msg="¿Desea eliminar esta notificación?" txtBtn="SI" />
            </Grid>
            <SnackbarAlert open={isOpen} msg={message.msg} severity={message.severity} duration={4000} onClose={handleCloseSnack} />
        </React.Fragment>
        </>
    );
};

export default NotificationsOlas;
