import 'react-circular-progressbar/dist/styles.css';
import 'moment/locale/es';

import { Button, ButtonGroup, Card, Col, Row } from 'react-bootstrap';
import { Grid, InputLabel, MenuItem, Select } from '@material-ui/core';
import { getSalesByEmployer, getSalesByEvent, getSalesByPoint } from '../../services/sales.service';
import { takeEvent } from '../../services/events.service';
import { useContext, useEffect, useState } from 'react';

import ChartGraph from '../../component/ChartGraph';
import Loading from '../../component/Loading';
import React from 'react';
import { UserContext } from '../../context/UserContext';
import { getEmployers } from '../../services/employers.service';
import { gridSpacing } from '../../store/constant';
import { takePointsInfo } from '../../services/point';
import { useParams } from 'react-router';

import { es } from 'date-fns/locale';
import DateFnsUtils from '@date-io/date-fns';
import { DateTimePicker } from '@material-ui/pickers';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';

const Casslesh = () => {

    const [event, setEvent] = useState([]);
    const [point, setPoint] = useState({});
    const [points, setPoints] = useState([]);
    const [sales, setSales] = useState({})
    const [loading, setLoading] = useState(true);
    const { user } = useContext(UserContext);
    const [employers, setEmployers] = useState([])
    const [employersLabels, setEmployersLabels] = useState([])
    const [data, setData] = useState([])
    const [dataType, setDataType] = useState("total");
    //let nowminusone = new Date().setHours(new Date().getHours() - 1)
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    let { slug } = useParams(); 

    useEffect(() => {
        takeEventId(slug);
    }, [slug]);

    useEffect(() => {
        takeEmployers(user._id)

    }, [user]);

    useEffect(() => {
        takeStats(dataType)

    }, [dataType, point, startDate, endDate]);

    useEffect(() => {
        if (event._id != undefined) {
            takePoints(event._id);
            takeSales(event._id);
            setStartDate(event.startDate);
            setEndDate(event.endDate);
        }
    }, [event]);

    const takeEventId = async (id) => {
        const { status, data } = await takeEvent(id);
        if (status === 200) {
            setEvent(data.data);
        } else {
            setEvent([]);
        }
    };

    const takeSales = async (eventID) => {
        let nowminusone = new Date().setHours(new Date().getHours() - 1)
        //const { status, data } = await getSalesByEvent(eventID, new Date(nowminusone).toISOString(), new Date().toISOString());
        const { status, data } = await getSalesByEvent(eventID, new Date(nowminusone).toISOString(), new Date().toISOString());
        if (status === 200) {
            setSales(data.data);
            setLoading(false);
        } else {
            setSales({});
            setLoading(false);
        }
    };

    const takeEmployers = async (promoter) => {
        const { status, data } = await getEmployers(promoter);
        if (status === 200) {
            const employers_labels = data.data.map((e) => ({ "key": e.email, "color": "#"+Math.floor(Math.random()*16777215).toString(16) }))
            setEmployersLabels(employers_labels)
            setEmployers(data.data)
            setLoading(false);
        } else {
            setEmployers([]);
            setLoading(false);
        }
    };

    const takePoints = async (eventID) => {
        const { status, data } = await takePointsInfo(eventID);
        if (status === 200) {
            let pointArr = [];
            for( const point of data.data){
                if(point.type === "bar"){
                    pointArr.push(point);
                }
            }
            setPoint(pointArr[0]);
            setPoints(pointArr);
            setLoading(false);
        } else {
            setPoint();
            setPoints([]);
            setLoading(false);
        }
    };

    const takeStats = async (type) => {
        let nowminusone = new Date().setHours(new Date().getHours() - 6)
        if(point !== undefined && point._id !== undefined){
            switch (type) {
                case "total":
                    //const { status, data } = await getSalesByPoint(point._id, new Date(nowminusone).toISOString(), new Date().toISOString());
                    const { status, data } = await getSalesByPoint(point._id, startDate, endDate);
                    if (status === 200) {
                        let dataConsumition = data.data?.map((item) => ({time: item._id.overallTime + ":00", totalAmount: item.totalAmount}));
                        setData(dataConsumition);
                        setLoading(false);
                    } else {
                        setData([]);
                        setLoading(false);
                    }

                    break;

                case "employer":

                    let data_employers = []

                    for (const employer of employers) {
                        const { status, data } = await getSalesByEmployer(point._id, employer._id, startDate, endDate);
                        if (status === 200) {
                            let tag = employer.email
                            let dataConsumition = data.data?.map((item) => ({time: item._id.overallTime + ":00",  [employer.email]: item.totalAmount}));
                            data_employers.push({ employer: employer.email, data: dataConsumition })
                        }

                    }
                    setData(data_employers)
                    setLoading(false);
                    break;
                default:
                    break
            }
        }

    }
    const handleChangePoint = (e) => {
        const filtered = !e.target.value
        ? points
        : points.filter((point) =>
        point._id.includes(e.target.value.toLowerCase())
        );
        setPoint(filtered[0]);
    };

    const handleDateSChange = (e) => {
        if (e !== '' && e !== null) {
            setStartDate(e.toISOString());
        }
    };

    const handleDateEChange = (e) => {
        setEndDate(e);
    };

    let inputData = {
        dataKey: "hour",
        oyLabel: "KWh",
        oxLabel: "hours",
        yLimit: [0, 20000],
        values: [
            { hour: 0, prosumer1: 4000, prosumer2: 2400, prosumer3: 2120 },
            { hour: 1, prosumer1: 3000, prosumer2: 1398, prosumer3: 2120 },
            { hour: 2, prosumer1: 2000, prosumer2: 9800, prosumer3: 3220 },
            { hour: 3, prosumer1: 2780, prosumer2: 3908, prosumer3: 2120 },
            { hour: 4, prosumer1: 1890, prosumer2: 4800, prosumer3: 1220 },
            { hour: 5, prosumer1: 2390, prosumer2: 3800, prosumer3: 2120 },
            { hour: 6, prosumer1: 3490, prosumer2: 4300, prosumer3: 2120 },
            { hour: 7, prosumer1: 4000, prosumer2: 2400, prosumer3: 2120 },
            { hour: 9, prosumer1: 3000, prosumer2: 1398, prosumer3: 2120 },
            { hour: 10, prosumer1: 2000, prosumer2: 9800, prosumer3: 3220 },
            { hour: 11, prosumer1: 2780, prosumer2: 3908, prosumer3: 2120 },
            { hour: 12, prosumer1: 1890, prosumer2: 4800, prosumer3: 1220 },
            { hour: 13, prosumer1: 2390, prosumer2: 3800, prosumer3: 2120 },
            { hour: 14, prosumer1: 3490, prosumer2: 4300, prosumer3: 2120 },
            { hour: 15, prosumer1: 1890, prosumer2: 4800, prosumer3: 1220 },
            { hour: 16, prosumer1: 2390, prosumer2: 3800, prosumer3: 2120 },
            { hour: 17, prosumer1: 3490, prosumer2: 4300, prosumer3: 2120 },
            { hour: 18, prosumer1: 4000, prosumer2: 2400, prosumer3: 2120 },
            { hour: 19, prosumer1: 3000, prosumer2: 1398, prosumer3: 2120 },
            { hour: 20, prosumer1: 2000, prosumer2: 9800, prosumer3: 3220 },
            { hour: 21, prosumer1: 2780, prosumer2: 3908, prosumer3: 2120 },
            { hour: 22, prosumer1: 1890, prosumer2: 4800, prosumer3: 1220 },
            { hour: 23, prosumer1: 2390, prosumer2: 3800, prosumer3: 2120 }
        ]
    };

    let inputLabels = [
        { key: "prosumer1", color: "#8884d8" },
        { key: "prosumer2", color: "#82ca9d" },
        { key: "prosumer3", color: "#81cc2d" }
    ];


    return (
        <>
            {loading === true ? (
                <>
                    <Loading show={loading} />
                </>
            ) : (
                <>
                    <Grid container spacing={gridSpacing}>
                        <Row style={{ width: "100%" }}>
                            <h1 style={{ margin: '25px' }}>Gestión Cashless</h1>
                        </Row>
                        <Row>
                            <Col>
                                <Card>
                                    <Card.Header>
                                        Ventas Totales
                                    </Card.Header>
                                    <Card.Body>
                                        {sales.total_sales !== "" ? sales.total_sales?.totalAmount.toFixed(2) : "0.00"}{"€"}
                                    </Card.Body>
                                </Card>
                            </Col>

                            <Col>
                                <Card>
                                    <Card.Header>
                                        Ventas última hora
                                    </Card.Header>
                                    <Card.Body>
                                        {sales.time_sales !== "" ? sales.time_sales?.totalAmount?.toFixed(2) : "0.00"}{"€"}
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                        <Row style={{ width: "100%", marginTop: "25px" }}>
                            <Col>
                                <Card>
                                    <Card.Body style={{ padding: "20px" }}>
                                        { point !== undefined ?
                                        (
                                            <Row>
                                                    <Row style={{padding: "20px"}}>
                                                    <Col lg={4} xs={4}>
                                                        <InputLabel id="category"><h5>Seleccione un punto</h5></InputLabel>
                                                            <Select
                                                                key={point._id}
                                                                defaultValue={point._id}
                                                                labelId="point"
                                                                id="point"
                                                                name="point"
                                                                onChange={handleChangePoint}
                                                                label="point"
                                                                style={{minWidth: '100%'}}
                                                            >
                                                            {points.map((val, index) => {
                                                                return (
                                                                    <MenuItem key={index} value={val._id}>
                                                                        {val.name}
                                                                    </MenuItem>
                                                                );
                                                            })}
                                                        </Select>
                                                        </Col>
                                                        <Col>
                                                            <Row>
                                                                <Col>
                                                            <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                                                                <DateTimePicker
                                                                    autoOk
                                                                    fullWidth
                                                                    clearable
                                                                    id="startDate"
                                                                    name="startDate"
                                                                    ampm={false}
                                                                    value={startDate}
                                                                    onChange={handleDateSChange}
                                                                    format="yyyy-MM-dd HH:mm"
                                                                    label="Fecha y hora de inicio"

                                                                />
                                                            </MuiPickersUtilsProvider>
                                                            </Col>
                                                            <Col>
                                                            <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                                                                <DateTimePicker
                                                                    autoOk
                                                                    fullWidth
                                                                    clearable
                                                                    id="endDate"
                                                                    name="endDate"
                                                                    ampm={false}
                                                                    value={endDate}
                                                                    onChange={handleDateEChange}
                                                                    format="yyyy-MM-dd HH:mm"
                                                                    label="Fecha y hora de fin"
                                                                />
                                                            </MuiPickersUtilsProvider>
                                                            </Col>
                                                            </Row>
                                                            <Row></Row>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                    <Col lg={3} xs={3}>
                                                        <ButtonGroup aria-label="Button group" className='p-3'>
                                                            <Button variant={dataType === "total" ? "primary" : "outline-secondary"} onClick={() => { setDataType("total") }}>Total</Button>
                                                            <Button variant={dataType === "employer" ? "primary" : "outline-secondary"} onClick={() => { setDataType("employer") }}>Segmentado</Button>
                                                        </ButtonGroup>
                                                        </Col>
                                                    </Row>
                                                
                                                <Col lg={8} style={{ margin: "25px" }}>
                                                    <ChartGraph
                                                        title={"Consumos " + point.name}
                                                        values={data}
                                                        labels={employersLabels}
                                                        type={dataType}
                                                    />
                                                </Col>
                                            </Row>
                                        ) : (<h3><center>Este evento no tiene ningun punto para seleccionar</center></h3>)}
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                    </Grid>
                </>)}
        </>
    );
};

export default Casslesh;