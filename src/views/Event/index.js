import 'react-circular-progressbar/dist/styles.css';
import 'moment/locale/es';

import { Grid } from '@material-ui/core';
import { getReservasCount, getReservasEntrances } from '../../services/entrances.service';
import { takeEvent, takeTicketsInfo } from '../../services/events.service';
import { takeTicketEventMetrics, takeTransactionsInfoFiltered } from '../../services/transactions.service';
import { useContext, useEffect, useState } from 'react';

import ConfirmationModal from '../../component/Modals/ConfirmationBootstrap';
import CreateEvent from '../../component/Modals/CreateEvent';
import { Helmet } from 'react-helmet';
import Loading from '../../component/Loading';
import React from 'react';
import SnackbarAlert from '../../component/Snackbar';
import { URL_S3 } from '../../store/constant';
import { UserContext } from '../../../src/context/UserContext';
import { gridSpacing } from '../../store/constant';
import { makeStyles } from '@material-ui/core/styles';
import { modifyEvent } from '../../services/events.service';
import moment from 'moment';
import { uploadFile } from '../../services/upload.service';
import { useParams } from 'react-router';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    boxInfo: {
        borderRadius: '10px',
        marginBottom: '30px',
        marginLeft: '35px',
        height: '225px',
        marginTop: '0px',
        width: '220px',
    },
    boxTicket: {
        borderRadius: '10px',
        marginBottom: '30px',
        width: 'auto',
        height: 'auto',
        marginLeft: '15px',
        marginRight: '20px',
        marginTop: '0px',
    },
    textInfo: {
        fontSize: '20px',
        marginBottom: '8px',
        marginTop: '20px',
        justifyContent: 'center',
        display: 'flex',
        marginLeft: '-11px',
    },
    textTitle: {
        fontSize: '20px',
        marginBottom: '8px',
        color: 'green',
        justifyContent: 'center',
        display: 'flex',
    },
    circularProgress: {
        width: 120,
        height: 120,
        margin: "0 auto"
    },
    imgBanner: {
        width: '290px',
        height: '130px',
        objectFit: 'contain',
    },
    img_container: {
        position: 'relative',
        display:'inline-block',
        textAlign:'center',
    },
    
    button: {
        position:'absolute',
        right:'15px',
        height:'30px',
        background:"white",
        borderColor:"white",
        borderRadius:"30px"
        
    }
}));


const Event = () => {
    moment.locale('es');
    const classes = useStyles();
    const [event, setEvent] = useState([]);
    const [ tickets, setTickets ] = useState([]);
    const [ ticket, setTicket ] = useState([]);
    const [transactions, setTransactions] = useState([]);
    const [eventMetrics, setEventMetrics] = useState([]);
    const [eventMetricsData, setEventMetricsData] = useState({
        amountTotal: 0,
        entrancesCountTotal: 0,
        percentage: 0,
    });
    const [haveTaquilla, setHaveTaquilla] = useState(false);
    const [tabActiveKey, setTabActiveKey] = useState('online');
    const [loading, setLoading] = useState(false);
    const [ entranceModalShow, setEntranceModalShow ] = useState(false);
    let { slug } = useParams();
    const { user } = useContext(UserContext);

    const [confirmationModalShow, setConfirmationModalShow] = useState(false);
    const [confirmationModalData, setConfirmationModalData] = useState({
        title: '',
        description: ''
    });

    const [openEvent, setOpenEvent] = useState(false);
    const [isOpen, setIsOpen] = useState(false);
    const [message, setMessage] = useState({});

    const inputFile = useState(null);

    useEffect(() => {
        takeEventId(slug);
    }, [slug]);

    useEffect(() => {
        if (event._id != undefined) {
        }
    }, [event]);

    useEffect(() => {
        if (eventMetrics.length) {
            calculateMetrics();
        }
    }, [eventMetrics]);

    useEffect(() => {
        if (transactions.length) {
            if (loading) setLoading(false);
        }
    }, [transactions]);

    const takeEventId = async (id) => {
        const { status, data } = await takeEvent(id);
        if (status === 200) {
            setEvent(data.data);
        } else {
            setEvent({});
        }
    };

    const takeTickets = async (event) => {
        const metricsArr = [];
        const { status, data } = await takeTicketsInfo(event);
        if (status === 200) {
           for (const element of data.data) {
                const { status, data } = await getReservasCount(element._id);
                if (status === 200) {
                    const obj = {
                        _id : element._id,
                        ticketName : element.name,
                        ticketAmount : element.amount,
                        entrancesCount : data.data,
                    }
                    metricsArr.push(obj);
                }
            }
            setEventMetrics(metricsArr);
            setTickets(data.data);
        } else {
            setEventMetrics();
            setTickets([]);
        }

        
    };

    const takeBuyersTransactions = async (id) => {
        const { status, data } = await takeTransactionsInfoFiltered(id);
        if (status === 200) {
            for (const infoTransaction of data.data) {
                if (infoTransaction.transactionDetail === 'Taquilla') {
                    setHaveTaquilla(true);
                    setTabActiveKey('taquilla');
                    break;
                }
            }
            setTransactions(data.data);
        } else {
            setLoading(false);
            setTransactions([]);
        }
    };

    const takeEventMetrics = async (id) => {
        const { status, data } = await takeTicketEventMetrics(id);
        if (status === 200) {
            setEventMetrics(data.data);
        } else {
            setEventMetrics([]);
        }
    };

    const onButtonClick = () => {
        // `current` points to the mounted file input element
       inputFile.current.click();
      };

    const takeTicketByEvent = async (id) => {
        const query = {
            eventID : id
        }
        const metricsArr = [];
        const { status, data } = await takeTickets(query);
        if (status === 200) {
           for (const element of data.data) {
                const { status, data } = await getReservasEntrances(element._id);
                if (status === 200) {
                    const obj = {
                        _id : element._id,
                        ticketName : element.name,
                        ticketAmount : element.amount,
                        entrancesCount : data.data.length,
                    }
                    metricsArr.push(obj);
                }
            }
            setEventMetrics(metricsArr);
        } else {
            setEventMetrics();
        }
    }

    const calculateMetrics = () => {
        let amountTotal = 0;
        let entrancesCountTotal = 0;
        for (const metric of eventMetrics) {
            amountTotal += metric.ticketAmount;
            entrancesCountTotal += metric.entrancesCount;
        }
        let total = (entrancesCountTotal * 100) / amountTotal;
        setEventMetricsData({
            amountTotal: amountTotal,
            entrancesCountTotal: entrancesCountTotal,
            percentage: Number.parseFloat(total).toFixed(2),
        });
    };

    const handleCreateEntrance = () => {
        setTicket([]);
        setEntranceModalShow(true);
        
    }

    const handleEditEntrance = async (value) => {
        setTicket(value)
        setEntranceModalShow(true);
    }
    
    const handleOpenEvent = () => {
        //setEditEvent({});
        setOpenEvent(true);
    };

    const handleClose = () => {
        setConfirmationModalData({"title": "Confirmation" ,"description": "Desea salir de Crear evento?"})
        setConfirmationModalShow(true);
    };

    const onFinish = () => {
        takeEventId(slug);
        setOpenEvent(false);
        setEntranceModalShow(false);
    }

    const onConfirmClose = () =>{
        setConfirmationModalShow(false);
        setOpenEvent(false);
    }

    const onCancelClose = () =>{
        setConfirmationModalShow(false);
        setOpenEvent(true);
    }

    const onOpenSnackBar = (msg) => {
        setMessage(msg);
        setIsOpen(true);
    };

    const onRefresh = () => { 
        takeEventId(slug);
    }

    const handleCloseSnack = () => {
        setIsOpen(false);
    };

    const handleImageChanged = async (file) => {
        let urlfile = '';
        const formData = new FormData();
        formData.append('file', file);
        const { status } = await uploadFile(formData);
        if (status === 201) {
            urlfile = URL_S3 + file.name;

        } else {
            urlfile = '';
        }
        sendImage(urlfile)
    }

    const sendImage = async (urlFile) => {
        const valuesEvent = {
            banner : urlFile
        }
        const { status, data } = await modifyEvent(event?._id, valuesEvent);
        if (status === 200) {
            takeEventId(data.data._id);
        }
    }
    

    return (
        <>
            {loading === true ? (
                <>
                    <Loading show={loading} />
                </>
            ) : (
                <>
            <Helmet>
                ‍<title>Eventos</title>
                <meta name="description" content={'Eventos'} />
                <meta property="og:title" content={'Eventos'} />
                <meta property="og:site_name" content="Eventos" />
                <meta property="og:locale" content="es" />
            </Helmet>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    {/*<h2> {event.name}</h2>
                    CREATE EVENT BUTTON
                    <Button startIcon={<EditIcon />} onClick={handleOpenEvent}>
                        Editar evento
                    </Button>
                    {/*}
                    <Grid container spacing={gridSpacing}>
                        <Grid item xs={12}>
                            <Container className="container-fluid">
                                <h2
                                    style={{
                                        fontSize: '2rem',
                                        marginTop: '0',
                                        marginBottom: '0.5rem',
                                        fontWeight: '500',
                                        linehHeight: '1.2',
                                    }}
                                >
                                    {event.name}
                                </h2>
                                <Row style={{ marginTop: '40px' }}>
                                    <Col xs={4}>
                                        <h6
                                            style={{
                                                fontSize: '1rem',
                                                marginTop: '0',
                                                marginBottom: '0.5rem',
                                                fontWeight: '500',
                                                linehHeight: '1.2',
                                            }}
                                        >
                                            {moment(event.startDate).format('L LT')}
                                        </h6>
                                        <h6
                                            style={{
                                                fontSize: '1rem',
                                                marginTop: '0',
                                                marginBottom: '0.5rem',
                                                fontWeight: '500',
                                                linehHeight: '1.2',
                                            }}
                                        >
                                            {event.location} {event.ubication ?  event.ubication.address  : ''}
                                        </h6>
                                        <div className={classes.img_container} >
                                            <img className={classes.imgBanner} src={event.banner}/>
                                            <button onClick={onButtonClick} className={classes.button}> <EditIcon/></button>
                                            <input type='file' id='file' ref={inputFile} 
                                                onChange={(event) => {
                                                    const imageFile = event.currentTarget.files[0];
                                                    event.banner = URL.createObjectURL(imageFile);
                                                    handleImageChanged(imageFile)
                                                }}
                                            style={{display: 'none'}}/>
                                        </div>
                                        

                                      
                                        <div>
                                        <Button className="m-3" startIcon={<EditIcon />} onClick={handleOpenEvent}>
                                            Editar evento
                                        </Button>
                                        </div>
                                        
                                    </Col>
                                    <Col xs={3}>
                                        <Grid container>
                                            <div className={classes.boxInfo}>
                                                <InputLabel className={classes.textTitle}>Entradas Totales</InputLabel>
                                                {eventMetrics.length ? (
                                                    <>
                                                    <InputLabel className={classes.textInfo}>
                                                    {eventMetricsData.entrancesCountTotal} / {eventMetricsData.amountTotal}
                                                </InputLabel>
                                                <div className={classes.circularProgress}>
                                                    <CircularProgressbar
                                                        value={eventMetricsData.percentage}
                                                        maxValue={100}
                                                        text={`${eventMetricsData.percentage}%`}
                                                    />
                                                </div>
                                                    </>
                                                ) : (
                                                <InputLabel
                                                    style={{
                                                        fontSize: '15px',
                                                        width: 'auto',
                                                        marginLeft: 'auto',
                                                        textAlign: 'center'
                                                    }}
                                                >
                                                    No hay entradas disponibles
                                                </InputLabel>
                                                )}
                                               
                                            </div>
                                        </Grid>
                                    </Col>
                                    <Col xs={5}>
                                        <Row>
                                            <Col className="col-10">
                                            <InputLabel className={classes.textTitle}>Por Entrada </InputLabel>
                                            </Col>
                                            <Col className="col-2"><Button title="Añadir nueva entrada" onClick={handleCreateEntrance}> <AddIcon /></Button></Col>
                                        </Row>
                                    
                                    
                                        {eventMetrics.length ? (
                                            <div> 
                                                {eventMetrics.map((data, i) => {
                                                    return (
                                                        <Row style={{display: 'flex',
                                                                    alignItems: 'center',
                                                                    justifyContent: 'center'}}>
                                                            <Col style={{
                                                                paddingBottom: '2%'
                                                                    }}>
                                                                <InputLabel
                                                                    style={{
                                                                        fontSize: '17px',
                                                                        width: '152px',
                                                                        marginLeft: '15px',
                                                                    }}
                                                                >
                                                                {data.ticketName}
                                                            </InputLabel>
                                                            </Col>
                                                            <Col style={{
                                                                paddingBottom: '2%'
                                                                    }}>
                                                                <InputLabel
                                                                    style={{
                                                                        fontSize: '17px',
                                                                        marginLeft: '52px',
                                                                    }}
                                                                >
                                                                    {data.entrancesCount} / {data.ticketAmount}
                                                                </InputLabel>
                                                            </Col>
                                                            <Col style={{
                                                                paddingBottom: '2%'
                                                                    }}>
                                                            <Button key={i} onClick={() => {handleEditEntrance(data)}}><BsPencilFill/></Button>
                                                            </Col>
                                                        </Row>
                                                    );
                                                })}
                                                
                                            </div>
                                        ) : (

                                        <div className={classes.boxTicket}>

                                                <InputLabel
                                                    style={{
                                                        fontSize: '15x',
                                                        width: 'auto',
                                                        marginLeft: 'auto',
                                                        textAlign: 'center'
                                                    }}
                                                >
                                                    No hay entradas disponibles
                                                </InputLabel>
                                        </div>

                                        )}
                                    </Col>
                                </Row>
                            </Container>

                            <Grid container spacing={gridSpacing}>
                                {event.isChart ? (
                                    <Grid item lg={12} sm={12} xs={12}>
                                        <div id="chart-manager"></div>
                                        <SeatsioEventManager
                                            divId="chart-manager"
                                            secretKey="e547f533-6c15-42e8-a30a-cd52886bae74"
                                            event={event.eventKey}
                                            onObjectSelected={(selected) => {
                                                //  console.log(selected);
                                            }}
                                            onRenderStarted={(createdChart) => {
                                                setChart(createdChart);
                                            }}
                                            mode="select"
                                            region="eu"
                                            language="es"
                                        />
                                    </Grid>
                                ) : (
                                    <></>
                                )}
                                <Grid item lg={12} sm={12} xs={12}>
                                    {user._id === "60e301ff5fa890135091e2bc" ? <>{tickets.length ? (
                                        tickets.map((ticket) => {
                                            return (
                                                <BuyersInfo chart={chart} ticketId={ticket._id} ticketName={ticket.name} category={event.tags[0]} eventId={event._id} />
                                            );
                                        })
                                    ) : (
                                        <center>
                                            <h3>Este evento no tiene ninguna compra</h3>
                                        </center>
                                    )}
                                    </> : <>
                                        {transactions.length ? (
                                            <>

                                                <Tabs defaultActiveKey={tabActiveKey} id="tab-detail" className="mb-3 mt-3 nav-justified">
                                                    {haveTaquilla ? (
                                                        <Tab eventKey="taquilla" title="TAQUILLA VIRTUAL">
                                                            <BuyersInfo detail="Taquilla" chart={chart} ticketId={event._id} ticketName={"ffff"} event={event} />
                                                        </Tab>
                                                    ) : (
                                                        <></>
                                                    )}

                                                    <Tab eventKey="online" title="ONLINE">
                                                        <BuyersInfo detail="Online" chart={chart} ticketId={event._id} ticketName={"ffff"} event={event} />
                                                    </Tab>
                                                </Tabs>

                                            </>
                                        ) : (
                                            <center><h3>Este evento no tiene ninguna compra</h3></center>
                                        )}
                                    </>}

                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    
                    <EntranceModal event={event} ticket={ticket} onFinish={onFinish} onOpenSnackBar={onOpenSnackBar} open={entranceModalShow} onHide={() => setEntranceModalShow(false)} />
                    {*/}
                </Grid>
            
            </Grid>
            
            { openEvent ? (
                <CreateEvent
                event={event}
                open={openEvent}
                onClose={handleClose}
                onFinish={onFinish}
                onRefresh={onRefresh}
                onOpenSnackBar={onOpenSnackBar}
            />
            ): ('')}
            
            <SnackbarAlert open={isOpen} msg={message.msg} severity={message.severity} duration={4000} onClose={handleCloseSnack} />
            <ConfirmationModal data={confirmationModalData} show={confirmationModalShow} onConfirm={onConfirmClose} onCancel={onCancelClose} onHide={() => onCancelClose} />
            </>
            
                )}
        </>
    );
};

export default Event;
