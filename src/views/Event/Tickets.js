
import { takeEvent } from '../../services/events.service';
import { useContext, useEffect, useState } from 'react';
import { UserContext } from '../../../src/context/UserContext';
import Loading from '../../component/Loading';
import React from 'react';
import { takeTickets } from '../../services/ticket.service';
import { useParams } from 'react-router';
import 'react-circular-progressbar/dist/styles.css';
import 'moment/locale/es';

const Tickets = () => {

    const [event, setEvent] = useState([]);
    const [loading, setLoading] = useState(false);

    let { slug } = useParams();

    useEffect(() => {
        takeEventId(slug);
    }, [slug]);

    useEffect(() => {
        if (event._id != undefined) {
            takeTickets(event._id);
        }
    }, [event]);

    const takeEventId = async (id) => {
        const { status, data } = await takeEvent(id);
        if (status === 200) {
            setEvent(data.data);
            setLoading(false);
        } else {
            setEvent({});
            setLoading(false);
        }
    };

    return (
        <>
        {loading === true ? (
            <>
                <Loading show={loading} />
            </>
        ) : (
            <>
        <h2>Tickets</h2>
        </>)}</>
    );

};

export default Tickets;