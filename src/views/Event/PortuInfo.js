import { takeAllAccessIn, takeBuyersInfo } from '../../services/events.service';
import { useContext, useEffect, useState } from 'react';

import MUIDataTable from 'mui-datatables';
import React from 'react';
import { UserContext } from '../../../src/context/UserContext';
import { getReportsTicket } from '../../services/report.service';

const BuyersInfo1 = (props) => {
    const [accounts, setAccounts] = useState([]);
    const [accessIn, setAccessIn] = useState();
    const { user } = useContext(UserContext);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        if (user._id === '60e301ff5fa890135091e2bc') {
            takeBuyersPortu(props.ticketId);
        } else {
            takeBuyersBilbao(props.ticketId);
            takeAccessIn(props.eventId);
        }
        if (loading) {
            setLoading(false);
        }
    }, [props.ticketId]);

    let columns;
    let rows;

    const takeAccessIn = async (eventId) => {
        const {status, data} = await takeAllAccessIn(eventId);
        if (status){
            setAccessIn(data.data[0]);
        } else{
            setAccessIn(null);
        }
    };

    const takeBuyersPortu = async (ticketId) => {
        const { status, data } = await getReportsTicket(ticketId);
        if (status === 200) {
            setAccounts(data.data);
        } else {
            setAccounts([]);
        }
        if (loading) {
            setLoading(false);
        }
    };

    const takeBuyersBilbao = async (ticketId) => {
        const { status, data } = await takeBuyersInfo(ticketId);
        if (status === 200) {
            setAccounts(data.data);
        } else {
            setAccounts([]);
        }
        if (loading) {
            setLoading(false);
        }
    };

    switch (props.category) {
        case 'Espectáculos':
            const fillRowsEspectaculo = (accounts) => {
                let rowArr = [];
                accounts.forEach((account, id) => {
                    const row = [
                        account.persona_contacto + ' ' + account.apelliod_persona_contacto,
                        account.correo_contacto,
                        account.telefono_contacto,
                        account.qr,
                    ];
                    rowArr.push(row);
                });
                return rowArr;
            };

            if (accounts.length) {
                columns = ['Nombre', 'Correo', 'Teléfono', 'Qr'];
                rows = fillRowsEspectaculo(accounts);
            }
            break;
        case 'Centro Cultural Santa Clara':
            const fillRowsSantaCLara = (accounts) => {
                let rowArr = [];
                accounts.forEach((account, id) => {
                    const row = [
                        account.nombre + ' ' + account.apellido,
                        account.edad,
                        account.persona_contacto + ' ' + account.apelliod_persona_contacto,
                        account.correo_contacto,
                        account.telefono_contacto,
                        account.relacion,
                        account.qr,
                    ];
                    rowArr.push(row);
                });
                return rowArr;
            };

            if (accounts.length) {
                columns = ['Nombre Menor', 'Edad Menor', 'Nombre Contacto', 'Correo Contacto', 'Telefono Contacto', 'Parentesco', 'Qr'];
                rows = fillRowsSantaCLara(accounts);
            }
            break;
        case 'Biblioteca Municipal':
            const fillRowsBiblioteca = (accounts) => {
                let rowArr = [];
                accounts.forEach((account, id) => {
                    const row = [
                        account.nombre + ' ' + account.apellido,
                        account.edad,
                        account.persona_contacto + ' ' + account.apelliod_persona_contacto,
                        account.correo_contacto,
                        account.telefono_contacto,
                        account.relacion,
                        account.qr,
                    ];
                    rowArr.push(row);
                });
                return rowArr;
            };

            if (accounts.length) {
                columns = ['Nombre Menor', 'Edad Menor', 'Nombre Contacto', 'Correo Contacto', 'Telefono Contacto', 'Parentesco', 'Qr'];
                rows = fillRowsBiblioteca(accounts);
            }
            break;
        case 'Museo de la Industria Rialia':
            const fillRowsMuseo = (accounts) => {
                let rowArr = [];
                accounts.forEach((account, id) => {
                    const row = [
                        account.persona_contacto + ' ' + account.apelliod_persona_contacto,
                        account.correo_contacto,
                        account.telefono_contacto,
                        account.relacion,
                        account.nombre + ' ' + account.apellido,
                        account.edad,
                        account.qr,
                    ];
                    rowArr.push(row);
                });
                return rowArr;
            };

            if (accounts.length) {
                columns = ['Nombre Contacto', 'Correo Contacto', 'Telefono Contacto', 'Parentesco', 'Nombre Menor', 'Edad Menor', 'Qr'];
                rows = fillRowsMuseo(accounts);
            }
            break;

        default:
            const fillRows = (accounts) => {
                let rowArr = [];
                accounts.forEach((account, id) => {
                    // console.log(account);
                    // let accessState;
                    let accessState = accessIn?.filter((access) => {
                        return access.entrance._id === account._id;
                    });
                    // console.log("state", accessState);
                    let buy;
                    if (account.buy === 'Taquilla') buy = "Taquilla virtual";
                    else buy = "Compra OnLine";
                    const row = [account.seat, account.name + ' ' + account.surname, account.email, account.phone, buy, account.qr,  accessState?.length ? "Dentro" : "Fuera"];
                    rowArr.push(row);
                });
                return rowArr;
            };

            if (accounts.length) {
                columns = ['Localidad', 'Nombre', 'Correo', 'Teléfono', 'Compra', 'Qr', 'Acceso'];
                rows = fillRows(accounts);
            }
            break;
    }

    const options = {
        filterType: 'dropdown',
        selectableRows: 'none',
        rowsPerPage: 15,
        print: false,
        customToolbarSelect: () => {},
        textLabels: {
            body: {
                noMatch: 'No se han encontrado registros',
                toolTip: 'Ordenar',
                columnHeaderTooltip: (column) => `Ordenar por ${column.label}`,
            },
            pagination: {
                next: 'Siguiente página',
                previous: 'Página anterior',
                rowsPerPage: 'Filas por página:',
                displayRows: 'de',
            },
            toolbar: {
                search: 'Buscar',
                downloadCsv: 'Descargar CSV',
                print: 'Imprimir',
                viewColumns: 'Mostrar columnas',
                filterTable: 'Filtrar tabla',
            },
            filter: {
                all: 'Todo',
                title: 'Filtros',
                reset: 'Reiniciar',
            },
            viewColumns: {
                title: 'Mostrar columnas',
                titleAria: 'Mostrar/Ocultar columnas de la tabla',
            },
            selectedRows: {
                text: 'fila(s) seleccionada(s)',
                delete: 'Borrar',
                deleteAria: 'Borrar las filas seleccionadas',
            },
        },
    };

    if (accounts.length) {
        return (
            <>
                {loading === true ? (
                    <div></div>
                ) : (
                    <>
                        {rows.length ? (
                            <>
                                <MUIDataTable title={'Ticket: ' + props.ticketName} data={rows} columns={columns} options={options} />
                            </>
                        ) : (
                            ''
                        )}
                    </>
                )}
            </>
        );
    } else {
        return <></>;
    }
};

export default BuyersInfo1;
