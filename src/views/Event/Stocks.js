import { Grid, InputLabel } from '@material-ui/core';
import { Button, Card } from 'react-bootstrap';
import { takeEvent } from '../../services/events.service';
import { useEffect, useState } from 'react';
import MUIDataTable from 'mui-datatables';
import Loading from '../../component/Loading';
import React from 'react';
import { gridSpacing } from '../../store/constant';
import { useParams } from 'react-router';
import 'react-circular-progressbar/dist/styles.css';
import AddIcon from '@material-ui/icons/Add';
import { BsPencilFill } from "react-icons/bs";
import moment from 'moment';
import 'moment/locale/es';
import { takeStocksInfo } from '../../services/stock';
import StockModal from '../../component/Modals/Stock';

const Stocks = () => {

    const [event, setEvent] = useState([]);
    const [ stock, setStock ] = useState([]);
    const [ stocks, setStocks ] = useState([]);
    const [ stocksRows, setStocksRows ] = useState([]);

    const [isOpen, setIsOpen] = useState(false);
    const [message, setMessage] = useState({});

    const [loading, setLoading] = useState(true);
    const [ stockModalShow, setStockModalShow ] = useState(false);

    let { slug } = useParams();

    useEffect(() => {
        takeEventId(slug);
    }, [slug]);
    
    useEffect(() => {
        if (event._id != undefined) {
            takeStocks(event._id);
        }
    }, [event]);

    const takeEventId = async (id) => {
        const { status, data } = await takeEvent(id);
        if (status === 200) {
            setEvent(data.data);
        } else {
            setEvent([]);
        }
    };

    const takeStocks = async (eventID) => {
        const { status, data } = await takeStocksInfo(eventID);
        if (status === 200) {
            setStocks(data.data);
            setStocksRows(fillRows(data.data));
            setLoading(false);
        }else{
            setStocks([]);
            setStocksRows([]);
            setLoading(false);
        }
    };

    const fillRows = (stocks) => {
        let rowArr = [];
        stocks.forEach((obj, id) => {
            const row = [
                id,
                obj._id,
                obj.product,
                obj.created,
                obj.amount,
                obj.isActive,
                obj
            ];
            rowArr.push(row);
        });
        return rowArr;
    };

    const handleCreateStock = () => {
        setStock([]);
        setStockModalShow(true);   
    }

    const handleEditStock = async (value) => {
        setStock(value)
        setStockModalShow(true);
    }

    const onOpenSnackBar = (msg) => {
        setMessage(msg);
        setIsOpen(true);
    };

    const onFinish = () => {
        takeEventId(slug);
        setStockModalShow(false);
    }   

    const columns = [{
        name: "column-id",
        options: {
            display: false,
        }
    }, {
        name: "ID",
        options: {
            display: false,
            }
    }, {
        name: "Nombre",
        options: {
            display: true,
        }
    }, {
        name: 'fecha',
        label: "Fecha Creación",
            options: {
            display: true,
            customBodyRender: (value, tableMeta, updateValue) => {
                return( moment(value).format('L LT'))
           },
        }
    }, {
        name: "Cantidad",
        options: {
            display: true,
        }
    }, {
        name: "Activo",
        options: {
            display: true,
            customBodyRender: (value, tableMeta, updateValue) => {
                if(value)
                    return("SI")
                else
                    return("No")
           },
        }
    }, {
        label: "Actions",
        options: {
            customBodyRender: (value, tableMeta, updateValue) => {
                return (
                    <Button key={value._id} onClick={() => {handleEditStock(value)}}><BsPencilFill/></Button>
                )
            }
        }
    }];

    const options = {
        filterType: 'dropdown',
        selectableRows: 'none',
        rowsPerPage: 15,
        print: false,
        customToolbarSelect: () => {},
        sortOrder: {
            name: 'fecha-compra',
            direction: 'desc'
          },
        textLabels: {
            body: {
                noMatch: 'No se han encontrado registros',
                toolTip: 'Ordenar',
                columnHeaderTooltip: (column) => `Ordenar por ${column.label}`,
            },
            pagination: {
                next: 'Siguiente página',
                previous: 'Página anterior',
                rowsPerPage: 'Filas por página:',
                displayRows: 'de',
            },
            toolbar: {
                search: 'Buscar',
                downloadCsv: 'Descargar CSV',
                print: 'Imprimir',
                viewColumns: 'Mostrar columnas',
                filterTable: 'Filtrar tabla',
            },
            filter: {
                all: 'Todo',
                title: 'Filtros',
                reset: 'Reiniciar',
            },
            viewColumns: {
                title: 'Mostrar columnas',
                titleAria: 'Mostrar/Ocultar columnas de la tabla',
            },
            selectedRows: {
                text: 'fila(s) seleccionada(s)',
                delete: 'Borrar',
                deleteAria: 'Borrar las filas seleccionadas',
            },
        },
    };

    return (
        <>
        {loading === true ? (
            <>
                <Loading show={loading} />
            </>
        ) : (
            <>
        <Grid container spacing={gridSpacing}>
            <Grid item lg={12} sm={12} xs={12}>
                <Card>
                <Card.Header className="d-flex justify-content-between align-items-center">
                    <InputLabel>Stocks</InputLabel> 
                    <Button title="Añadir nueva entrada" onClick={handleCreateStock}> <AddIcon /></Button>
                </Card.Header>
                    <Card.Body>
                    {stocks.length ? (
                <MUIDataTable title={'Stocks'} data={stocksRows} columns={columns} options={options} />
            ) : (
                <center>
                    <h3>Este evento no tiene Stocks</h3>
                </center>
            )}
                    </Card.Body>
                
                </Card>
            
            </Grid>
        </Grid>
        </>)}
        { stockModalShow ? (
                <StockModal 
                    event={event} 
                    stock={stock} 
                    onFinish={onFinish} 
                    onOpenSnackBar={onOpenSnackBar} 
                    open={stockModalShow} 
                    onHide={() => setStockModalShow(false)} />
            ): ('')}
            </>
    );
};

export default Stocks;