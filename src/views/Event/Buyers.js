import { Grid } from '@material-ui/core';
import { Tabs, Tab } from 'react-bootstrap';
import { takeEvent, takeTicketsInfo } from '../../services/events.service';
import { useContext, useEffect, useState } from 'react';
import { UserContext } from '../../../src/context/UserContext';
import BuyersInfo from './BuyersInfo';
import Loading from '../../component/Loading';
import React from 'react';
import { takeTransactionsInfoFiltered } from '../../services/transactions.service';
import {  getReservasCount } from '../../services/entrances.service';
import { gridSpacing } from '../../store/constant';
import { useParams } from 'react-router';
import 'react-circular-progressbar/dist/styles.css';

const Buyers = () => {

    const { user } = useContext(UserContext);
    const [event, setEvent] = useState([]);
    const [ tickets, setTickets ] = useState([]);
    const [transactions, setTransactions] = useState([]);
    const [chart, setChart] = useState(null);

    const [haveTaquilla, setHaveTaquilla] = useState(false);
    const [tabActiveKey, setTabActiveKey] = useState('online');

    const [loading, setLoading] = useState(true);

    let { slug } = useParams();

    useEffect(() => {
        takeEventId(slug);
    }, [slug]);

    useEffect(() => {
        if (event._id != undefined) {
            takeBuyersTransactions(event._id);
            takeTickets(event._id);
        }
    }, [event]);

    const takeEventId = async (id) => {
        const { status, data } = await takeEvent(id);
        if (status === 200) {
            setEvent(data.data);
        } else {
            setEvent({});
        }
    };

    const takeBuyersTransactions = async (id) => {
        const { status, data } = await takeTransactionsInfoFiltered(id);
        if (status === 200) {
            for (const infoTransaction of data.data) {
                if (infoTransaction.transactionDetail === 'Taquilla') {
                    setHaveTaquilla(true);
                    setTabActiveKey('taquilla');
                    break;
                }
            }
            setTransactions(data.data);
            setLoading(false);
        } else {
            setTransactions([]);
            setLoading(false);
        }
    };

    const takeTickets = async (event) => {
        const metricsArr = [];
        const { status, data } = await takeTicketsInfo(event);
        if (status === 200) {
           for (const element of data.data) {
                const { status, data } = await getReservasCount(element._id);
                if (status === 200) {
                    const obj = {
                        _id : element._id,
                        ticketName : element.name,
                        ticketAmount : element.amount,
                        entrancesCount : data.data,
                    }
                    metricsArr.push(obj);
                }
            }
            setTickets(data.data);
        } else {
            setTickets([]);
        }
    };


    return (
        <>
        {loading === true ? (
            <>
                <Loading show={loading} />
            </>
        ) : (
            <>
        <Grid container spacing={gridSpacing}>
            <Grid item lg={12} sm={12} xs={12}>
                {user._id === "60e301ff5fa890135091e2bc" ? <> {tickets.length ? (
                    tickets.map((ticket) => {
                        return (
                            <BuyersInfo chart={chart} ticketId={ticket._id} ticketName={ticket.name} category={event.tags[0]} eventId={event._id} />
                        );
                    })
                ) : (
                    <center>
                        <h3>Este evento no tiene ninguna compra</h3>
                    </center>
                )}
                </> : <>
                    {transactions.length ? (
                        <>
                            <Tabs defaultActiveKey={tabActiveKey} id="tab-detail" className="mb-3 mt-3 nav-justified">
                                {haveTaquilla ? (
                                    <Tab eventKey="taquilla" title="TAQUILLA VIRTUAL">
                                        <BuyersInfo detail="Taquilla" chart={chart} ticketId={event._id} ticketName={"ffff"} event={event} />
                                    </Tab>
                                ) : (
                                    <></>
                                )}
                                <Tab eventKey="online" title="ONLINE">
                                    <BuyersInfo detail="Online" chart={chart} ticketId={event._id} ticketName={"ffff"} event={event} />
                                </Tab>
                            </Tabs>
                        </>
                    ) : (
                        <center><h3>Este evento no tiene ninguna compra</h3></center>
                    )}
                </>
                }
            </Grid>
        </Grid> 
        </>)}</>
    );
};
export default Buyers;