import 'moment/locale/es';
import 'react-circular-progressbar/dist/styles.css';

import { takeEvent, takeTicketsInfo } from '../../services/events.service';
import { useEffect, useState } from 'react';

import { Accordion } from 'react-bootstrap';
import AssistantsInfo from './AssistantsInfo';
import { Grid } from '@material-ui/core';
import Loading from '../../component/Loading';
import React from 'react';
import { SeatsioEventManager } from '@seatsio/seatsio-react';
import { getReservasCount } from '../../services/entrances.service';
import { gridSpacing } from '../../store/constant';
import { useParams } from 'react-router';

const Assistants = () => {

    const [event, setEvent] = useState([]);
    const [ tickets, setTickets ] = useState([]);
    const [chart, setChart] = useState(null);

    const [loading, setLoading] = useState(true);

    let { slug } = useParams();

    useEffect(() => {
        takeEventId(slug);
    }, [slug]);

    useEffect(() => {
        if (event._id != undefined) {
            takeTickets(event._id);
            //takeTicketByEvent(event._id)
        }
    }, [event]);

    const takeEventId = async (id) => {
        const { status, data } = await takeEvent(id);
        if (status === 200) {
            setEvent(data.data);
        } else {
            setEvent([]);
        }
    };

    const takeTickets = async (event) => {
        const metricsArr = [];
        const { status, data } = await takeTicketsInfo(event);
        if (status === 200) {
           for (const element of data.data) {
                const { status, data } = await getReservasCount(element._id);
                if (status === 200) {
                    if(data.data > 0){
                        const obj = {
                            _id : element._id,
                            name : element.name,
                            grossPrice : element.grossPrice,
                            ticketAmount : element.amount,
                            entrancesCount : data.data,
                        }
                        metricsArr.push(obj);
                    }
                }
            }
            setTickets(metricsArr);
            setLoading(false);
        } else {
            setTickets([]);
            setLoading(false);
        }
    };



    return (
        <>
        {loading === true ? (
            <>
                <Loading show={loading} />
            </>
        ) : (
            <>
        <Grid container spacing={gridSpacing}>
            {event.isChart ? (
                <>{event._id === '63187ef06a7a7e0eaa7e677f' || event._id === '6387d2cba4b5eaea3435546c' ? <Grid item lg={10} sm={12} xs={12}>
                <div id="chart-manager"></div>
                <SeatsioEventManager
                    divId="chart-manager"
                    secretKey="e95d7e7b-4dd1-4fe5-998a-e293baa1e474"
                    event={event.eventKey}
                    onObjectSelected={(selected) => {
                        //  console.log(selected);
                    }}
                    onRenderStarted={(createdChart) => {
                        setChart(createdChart);
                    }}
                    mode="manageObjectStatuses"
                    region="eu"
                    language="es"
                />
            </Grid> : <Grid item lg={12} sm={12} xs={12}>
                    <div id="chart-manager"></div>
                    <SeatsioEventManager
                        divId="chart-manager"
                        secretKey="e547f533-6c15-42e8-a30a-cd52886bae74"
                        event={event.eventKey}
                        onObjectSelected={(selected) => {
                            //  console.log(selected);
                        }}
                        onRenderStarted={(createdChart) => {
                            setChart(createdChart);
                        }}
                        mode= 'select'
                        region="eu"
                        language="es"
                    />
                </Grid> }
                </>
                
            ) : (
           ''
            )}
            <Grid item lg={12} sm={12} xs={12}>
            {tickets.length ? (
                <Accordion defaultActiveKey={['0']} alwaysOpen>
                {tickets.map((ticket, key) => {
                    return (
                        <Accordion.Item eventKey={key}>
                            <Accordion.Header>{ticket.name}</Accordion.Header>
                            <Accordion.Body>
                                <AssistantsInfo chart={chart} detail={ticket.name} ticket={ticket} ticketName={ticket.name} category={event.tags[0]} event={event} />
                            </Accordion.Body>
                        </Accordion.Item>
                    );
                })}
                </Accordion>
            ) : (
                <center>
                    <h3>Este evento no tiene Asistentes</h3>
                </center>
            )}
            </Grid>
        </Grid>
        </>)}</>
    );

};

export default Assistants;