import 'moment/locale/es';
import 'moment/locale/es';

import { useContext, useEffect, useState } from 'react';
import { utils, write } from 'xlsx'

import Loading from '../../component/Loading';
import MUIDataTable from 'mui-datatables';
import React from 'react';
import { UserContext } from '../../../src/context/UserContext';
import { getReportsTicket } from '../../services/report.service';
import moment from 'moment';
import { saveAs } from 'file-saver'

const AssistantsInfo = (props) => {
    moment.locale('es');
    const [ticket, setTicket] = useState([]);
    const [accounts, setAccounts] = useState([]);
    const [transactions, setTransactions] = useState([]);
    const [accessIn, setAccessIn] = useState();
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        setTicket(props.ticket)
        takeAssistants(props.ticket._id);
        if (loading) {
            setLoading(false);
        }
    }, [props]);


    let columns;
    let rows;

    const takeAssistants = async (ticketId) => {
        const { status, data } = await getReportsTicket(ticketId);
        if (status === 200) {
            setAccounts(data.data);
        } else {
            setAccounts([]);
        }
    };


    switch (props.category) {
        case 'Espectáculos':
            const fillRowsEspectaculo = (accounts) => {
                let rowArr = [];
                accounts.forEach((account, id) => {
                    const row = [
                        account.persona_contacto + ' ' + account.apellido_persona_contacto,
                        account.correo_contacto,
                        account.telefono_contacto,
                        account.qr,
                    ];
                    rowArr.push(row);
                });
                return rowArr;
            };
            if (accounts.length) {
                columns = ['Nombre', 'Correo', 'Teléfono', 'Qr'];
                rows = fillRowsEspectaculo(accounts);
            }
            break;
        case 'Centro Cultural Santa Clara':
            const fillRowsSantaCLara = (accounts) => {
                let rowArr = [];
                accounts.forEach((account, id) => {
                    const row = [
                        account.nombre + ' ' + account.apellido,
                        account.edad,
                        account.persona_contacto + ' ' + account.apellido_persona_contacto,
                        account.correo_contacto,
                        account.telefono_contacto,
                        account.relacion,
                        account.qr,
                    ];
                    rowArr.push(row);
                });
                return rowArr;
            };
            if (accounts.length) {
                columns = ['Nombre Menor', 'Edad Menor', 'Nombre Contacto', 'Correo Contacto', 'Telefono Contacto', 'Parentesco', 'Qr'];
                rows = fillRowsSantaCLara(accounts);
            }
            break;
        case 'Biblioteca Municipal':
            const fillRowsBiblioteca = (accounts) => {
                let rowArr = [];
                accounts.forEach((account, id) => {
                    const row = [
                        account.nombre + ' ' + account.apellido,
                        account.edad,
                        account.persona_contacto + ' ' + account.apellido_persona_contacto,
                        account.correo_contacto,
                        account.telefono_contacto,
                        account.relacion,
                        account.qr,
                    ];
                    rowArr.push(row);
                });
                return rowArr;
            };

            if (accounts.length) {
                columns = ['Nombre Menor', 'Edad Menor', 'Nombre Contacto', 'Correo Contacto', 'Telefono Contacto', 'Parentesco', 'Qr'];
                rows = fillRowsBiblioteca(accounts);
            }
            break;
        case 'Museo de la Industria Rialia':
            if (props.event._id === "624487bd04de14b13a9da6ed" || props.event._id === "627cd7866f9618c20ea6647e" ||  
                props.event._id === "626ba5cc0902a1b110d33c7c" || props.event._id === "626bac570902a1b110d34f8b" ||
                props.event._id === "627d5a746f9618c20ea6f947") {
                const fillRowsEspectaculo = (accounts) => {
                    let rowArr = [];      
                    accounts.forEach((account, id) => {
                        if (account.nombre === undefined ) {
                            account.nombre = account.persona_contacto;
                        }
                        if (account.apellido === undefined) {
                            account.apellido = account.apellido_persona_contacto;
                        }
                        const row = [
                            account.persona_contacto + ' ' + account.apellido_persona_contacto,
                            account.correo_contacto,
                            account.telefono_contacto,
                            account.nombre + ' ' + account.apellido,
                            account.qr,
                        ];       
                        rowArr.push(row);
                    });
                    return rowArr;
                };
                if (accounts.length) {
                    columns = ['Nombre', 'Correo', 'Teléfono', 'Nombre acompañante', 'Qr'];
                    rows = fillRowsEspectaculo(accounts);
                }
            }else {
                const fillRowsMuseo = (accounts) => {
                    let rowArr = [];
                    accounts.forEach((account, id) => {
                        const row = [
                            account.persona_contacto + ' ' + account.apellido_persona_contacto,
                            account.correo_contacto,
                            account.telefono_contacto,
                            account.relacion,
                            account.nombre + ' ' + account.apellido,
                            account.edad,
                            account.qr,
                        ];
                        rowArr.push(row);
                    });
                    return rowArr;
                };
                if (accounts.length) {
                    columns = ['Nombre Contacto', 'Correo Contacto', 'Telefono Contacto', 'Parentesco', 'Nombre Menor', 'Edad Menor', 'Qr'];
                    rows = fillRowsMuseo(accounts);
                }
            }
            break;

        default:
            let haveLocality = false;
            let carrera = false;
            let haveMiniClub = false;
            const fillRows = (accounts) => {
                let rowArr = [];
                accounts.forEach((account, id) => {
                    //console.log(account);
                    // let accessState;
                    let accessState = accessIn?.filter((access) => {
                        return access.entrance._id === account._id;
                    });
                    // console.log("state", accessState);
                    let buy;
                    if (account.buy === 'Taquilla') buy = "Taquilla virtual";
                    else buy = "Compra OnLine";

                    if (account.seat !== undefined && account.seat !== null && account.seat !== "") {
                        haveLocality = true;
                    }
                    let invitation = "";
                    if (account.transactionDetail !== undefined && ticket.grossPrice !== undefined) {
                        if (ticket.grossPrice > 0 && account.transactionDetail.includes("Enviada por el promotor")) {
                            invitation = "Si";
                        }
                    }

                    if (props.event._id === "623cae86e8ae7d29deca4d71" || props.event._id === "62850678ada473143d0bba39"){
                        carrera = true
                    }
                    if (props.event._id === "63188a546a7a7e0eaa7e6ae0"){
                        haveMiniClub = true
                    }
                    const row = [invitation ,account.seat, account.city, account.companion,  account.persona_contacto + ' ' + account.apellido_persona_contacto, account.correo_contacto, account.telefono_contacto, buy, account.qr,  accessState?.length ? "Dentro" : "Fuera"];
                    rowArr.push(row);
                });
                return rowArr;
            };

            if (accounts.length) {
                rows = fillRows(accounts);
                columns = [
                    {
                        name: 'Invitacion',
                        options: {
                            display: false,
                        }
                    }, {
                        name: "Localidad",
                        options: {
                            display: haveLocality,
                        }
                        
                    },
                    {
                        name: "Ciudad",
                        options: {
                            display: carrera,
                        }
                        
                    }, {
                        name: "Guarderia",
                        options: {
                            display: haveMiniClub,
                            customBodyRender: (value, tableMeta, updateValue) => {
                                if(value?.miniclub === true){
                                    return "Si"
                                }else{
                                    return ""
                                }
                           },
                        }
                        
                    }, 'Nombre', 'Correo', 'Teléfono', 'Compra', 'Qr', {
                        name: "Fecha Compra",
                        options: {
                            display: haveLocality,
                            customBodyRender: (value, tableMeta, updateValue) => {
                                return (moment(value).format('L LT'));
                            },
                        }
                    }, 'Acceso'];
            }
            break;
    }
    const options = {
                filterType: 'dropdown',
                selectableRows: 'none',
                rowsPerPage: 15,
                print: false,
                customToolbarSelect: () => {},
                textLabels: {
                    body: {
                        noMatch: 'No se han encontrado registros',
                        toolTip: 'Ordenar',
                        columnHeaderTooltip: (column) => `Ordenar por ${column.label}`,
                    },
                    pagination: {
                        next: 'Siguiente página',
                        previous: 'Página anterior',
                        rowsPerPage: 'Filas por página:',
                        displayRows: 'de',
                    },
                    toolbar: {
                        search: 'Buscar',
                        downloadCsv: 'Descargar',
                        print: 'Imprimir',
                        viewColumns: 'Mostrar columnas',
                        filterTable: 'Filtrar tabla',
                    },
                    filter: {
                        all: 'Todo',
                        title: 'Filtros',
                        reset: 'Reiniciar',
                    },
                    viewColumns: {
                        title: 'Mostrar columnas',
                        titleAria: 'Mostrar/Ocultar columnas de la tabla',
                    },
                    selectedRows: {
                        text: 'fila(s) seleccionada(s)',
                        delete: 'Borrar',
                        deleteAria: 'Borrar las filas seleccionadas',
                    },
                },

        onDownload: (buildHead, buildBody, columns, values) => {
            const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8'
            const fileExtension = '.xlsx'
            // build the json, this is one way to do it
            const json = values.reduce((result, val) => {
                const temp = {}
                val.data.forEach((v, idx) => {
                    temp[columns[idx].name] = v
                })
                result.push(temp)
                return result
            }, [])

            const fileName = `$assistants`
            const ws = utils.json_to_sheet(json)
            const wb = { Sheets: { 'data': ws }, SheetNames: ['data'] }
            const excelBuffer = write(wb, { bookType: 'xlsx', type: 'array' })
            const data = new Blob([excelBuffer], { type: fileType })
            saveAs(data, fileName + fileExtension)
            // cancel default  CSV download from table
            return false
        }
    };


    if (accounts.length || transactions.length) {
        return (
            <>
                {loading === true ? (
                    <>
                        <Loading show={loading} />
                    </>
                ) : (
                    <>
                      <MUIDataTable title={'Asistentes: ' + props.detail} data={rows} columns={columns} options={options} />
                    </>
                )}
            </>
        );
    } else {
        return <></>;
    }
};

export default AssistantsInfo;
