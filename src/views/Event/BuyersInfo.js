import 'moment/locale/es';

import { useEffect, useState } from 'react';

import Loading from '../../component/Loading';
import MUIDataTable from 'mui-datatables';
import React from 'react';
import TransactionModal from '../../component/TransactionModal/index';
import moment from 'moment';
import { takeTransactionsInfoFiltered } from '../../services/transactions.service';

const BuyersInfo = (props) => {
    moment.locale('es');
    const [accounts, setAccounts] = useState([]);
    const [data, setData] = useState([]);
    const [transactions, setTransactions] = useState([]);
    const [transaction, setTransaction] = useState(null);
    const [loading, setLoading] = useState(true);
    const [transactionModalShow, setTransactionModalShow] = useState(false);

    useEffect(() => {
            takeBuyersTransactions(props.event._id);
        if (loading) {
            setLoading(false);
        }
    }, [props.event]);

    useEffect(() => {
        setData(fillRows(transactions))
    }, [transactions]);

    const takeBuyersTransactions = async (eventId) => {
        const { status, data } = await takeTransactionsInfoFiltered(eventId);
        if (status === 200) {
            setTransactions(data.data);
        } else {
            setTransactions([]);
        }
    };

    const onRowClick = (rowData, rowMeta) => {
        const rowID = rowData[0];
        const object = transactions[rowID];
        setTransaction(object);
        setTransactionModalShow(true);
    }

    const fillRows = (transactions) => {
        let rowArr = [];
        transactions.forEach((obj, id) => {
        if(props.detail == "Online"){
            if(obj.transactionDetail != "Taquilla") {
            if(obj.transactionState == "completed") {
                const row = [
                    id,
                    obj._id,
                    moment(obj.transactionCreated).format('L LT'),
                    obj.accountName,
                    obj.accountSurnames,
                    obj.accountEmail,
                    obj.accountPhone,
                    obj.entrances,
                    obj.entrances
                ];
                rowArr.push(row);
            }
            }
        }else{
            if(obj.transactionDetail == props.detail) {
            const row = [
                    id,
                    obj._id,
                    moment(obj.transactionCreated).format('L LT'),
                    obj.accountName,
                    obj.accountSurnames,
                    obj.accountEmail,
                    obj.accountPhone,
                    obj.entrances,
                    obj.entrances
                ];
                rowArr.push(row);
                }
            }
        });
        return rowArr;
    };

    const columns = [{
        name: "column-id",
        options: {
            display: false,
        }
    }, {
        name: "ID",
        options: {
            display: true,
            }
        },{
            name: 'fecha-compra',
            label: "Fecha compra",
            options: {
            display: true,
        }
    }, {
        name: "Nombre",
        options: {
            display: true,
        }
    }, {
        name: "Apellidos",
        options: {
            display: true,
        }
    }, {
        name: "Correo",
        options: {
            display: true,
        }
    }, {
        name: "Teléfono",
        options: {
            display: true,
        }
    }, {
        name: "Entradas",
        options: {
            display: true,
            customBodyRender: (value, tableMeta, updateValue) => {
                 let cont = 0;
                 value.forEach(element => {
                     if(element === true)
                        cont += 1
                 });
                 return(cont);
            },
        }
        
    }, {
        name: "Canceladas",
        options: {
            display: true,
            customBodyRender: (value, tableMeta, updateValue) => {
                let cont = 0;
                value.forEach(element => {
                    if(element === false)
                       cont += 1
                });
                return(cont);
           },
        }
    }
    ];
   
    const options = {
        onRowClick: onRowClick,
        filterType: 'dropdown',
        selectableRows: 'none',
        rowsPerPage: 15,
        print: false,
        customToolbarSelect: () => {},
        sortOrder: {
            name: 'fecha-compra',
            direction: 'desc'
          },
        textLabels: {
            body: {
                noMatch: 'No se han encontrado registros',
                toolTip: 'Ordenar',
                columnHeaderTooltip: (column) => `Ordenar por ${column.label}`,
            },
            pagination: {
                next: 'Siguiente página',
                previous: 'Página anterior',
                rowsPerPage: 'Filas por página:',
                displayRows: 'de',
            },
            toolbar: {
                search: 'Buscar',
                downloadCsv: 'Descargar CSV',
                print: 'Imprimir',
                viewColumns: 'Mostrar columnas',
                filterTable: 'Filtrar tabla',
            },
            filter: {
                all: 'Todo',
                title: 'Filtros',
                reset: 'Reiniciar',
            },
            viewColumns: {
                title: 'Mostrar columnas',
                titleAria: 'Mostrar/Ocultar columnas de la tabla',
            },
            selectedRows: {
                text: 'fila(s) seleccionada(s)',
                delete: 'Borrar',
                deleteAria: 'Borrar las filas seleccionadas',
            },
        },
    };

    if (accounts.length || transactions.length) {
        return (
            <>
                {loading === true ? (
                    <>
                        <Loading show={loading} />
                    </>
                ) : (
                    <>
                       <MUIDataTable title={'Compras: ' + props.detail} data={data} columns={columns} options={options} />

                        {transaction != null ? (
                            <>
                                { }
                                <TransactionModal transaction={transaction} detail={props.detail} category={props.event.category} ischart={props.event.isChart.toString()} show={transactionModalShow} onHide={() => setTransactionModalShow(false)} />
                            </>
                        ) : (
                            ''
                        )}
                    </>
                )}
            </>
        );
    } else {
        return <></>;
    }
};

export default BuyersInfo;
