import { Grid, InputLabel } from '@material-ui/core';
import { Button, Card } from 'react-bootstrap';
import { takeEvent } from '../../services/events.service';
import { useEffect, useState } from 'react';
import MUIDataTable from 'mui-datatables';
import Loading from '../../component/Loading';
import React from 'react';
import { gridSpacing } from '../../store/constant';
import { useParams } from 'react-router';
import 'react-circular-progressbar/dist/styles.css';
import AddIcon from '@material-ui/icons/Add';
import { BsPencilFill } from "react-icons/bs";
import moment from 'moment';
import 'moment/locale/es';
import { takePointsInfo } from '../../services/point';
import PointModal from '../../component/Modals/Point';

const Points = () => {

    const [event, setEvent] = useState([]);
    const [ point, setPoint ] = useState([]);
    const [ points, setPoints ] = useState([]);
    const [ pointsRows, setPointsRows ] = useState([]);

    const [isOpen, setIsOpen] = useState(false);
    const [message, setMessage] = useState({});

    const [loading, setLoading] = useState(true);
    const [ pointModalShow, setPointModalShow ] = useState(false);

    let { slug } = useParams();

    useEffect(() => {
        takeEventId(slug);
    }, [slug]);
    
    useEffect(() => {
        if (event._id != undefined) {
            takePoints(event._id);
        }
    }, [event]);

    const takeEventId = async (id) => {
        const { status, data } = await takeEvent(id);
        if (status === 200) {
            setEvent(data.data);
        } else {
            setEvent([]);
        }
    };

    const takePoints = async (eventID) => {
        const { status, data } = await takePointsInfo(eventID);
        if (status === 200) {
            setPoints(data.data);
            setPointsRows(fillRows(data.data));
            setLoading(false);
        }else{
            setPoints([]);
            setPointsRows([]);
            setLoading(false);
        }
    };

    const fillRows = (zones) => {
        let rowArr = [];
        zones.forEach((obj, id) => {
            if(obj.type === "access"){
                const row = [
                    id,
                    obj._id,
                    obj.name,
                    obj.description,
                    obj.created,
                    obj.isActive,
                    obj
                ];
                rowArr.push(row);
            }
        });
        return rowArr;
    };

    const handleCreatePoint = () => {
        setPoint([]);
        setPointModalShow(true);   
    }

    const handleEditZone = async (value) => {
        setPoint(value)
        setPointModalShow(true);
    }

    const onOpenSnackBar = (msg) => {
        setMessage(msg);
        setIsOpen(true);
    };

    const onFinish = () => {
        takeEventId(slug);
        setPointModalShow(false);
    }   

    const columns = [{
        name: "column-id",
        options: {
            display: false,
        }
    }, {
        name: "ID",
        options: {
            display: false,
            }
    }, {
        name: "Nombre",
        options: {
            display: true,
        }
    }, {
        name: "Descripción",
        options: {
            display: true,
        }
    },{
        name: 'fecha',
        label: "Fecha Creación",
            options: {
            display: true,
            customBodyRender: (value, tableMeta, updateValue) => {
                return( moment(value).format('L LT'))
           },
        }
    }, {
        name: "Activo",
        options: {
            display: true,
            customBodyRender: (value, tableMeta, updateValue) => {
                if(value)
                    return("SI")
                else
                    return("No")
           },
        }
    }, {
        label: "Actions",
        options: {
            customBodyRender: (value, tableMeta, updateValue) => {
                return (
                    <Button key={value._id} onClick={() => {handleEditZone(value)}}><BsPencilFill/></Button>
                )
            }
        }
    }];

    const options = {
        filterType: 'dropdown',
        selectableRows: 'none',
        rowsPerPage: 15,
        print: false,
        customToolbarSelect: () => {},
        sortOrder: {
            name: 'fecha-compra',
            direction: 'desc'
          },
        textLabels: {
            body: {
                noMatch: 'No se han encontrado registros',
                toolTip: 'Ordenar',
                columnHeaderTooltip: (column) => `Ordenar por ${column.label}`,
            },
            pagination: {
                next: 'Siguiente página',
                previous: 'Página anterior',
                rowsPerPage: 'Filas por página:',
                displayRows: 'de',
            },
            toolbar: {
                search: 'Buscar',
                downloadCsv: 'Descargar CSV',
                print: 'Imprimir',
                viewColumns: 'Mostrar columnas',
                filterTable: 'Filtrar tabla',
            },
            filter: {
                all: 'Todo',
                title: 'Filtros',
                reset: 'Reiniciar',
            },
            viewColumns: {
                title: 'Mostrar columnas',
                titleAria: 'Mostrar/Ocultar columnas de la tabla',
            },
            selectedRows: {
                text: 'fila(s) seleccionada(s)',
                delete: 'Borrar',
                deleteAria: 'Borrar las filas seleccionadas',
            },
        },
    };

    return (
        <>
        {loading === true ? (
            <>
                <Loading show={loading} />
            </>
        ) : (
            <>
        <Grid container spacing={gridSpacing}>
            <Grid item lg={12} sm={12} xs={12}>
                <Card>
                <Card.Header className="d-flex justify-content-between align-items-center">
                    <InputLabel>Accesos</InputLabel> 
                    <Button title="Añadir nueva entrada" onClick={handleCreatePoint}> <AddIcon /></Button>
                </Card.Header>
                    <Card.Body>
                    {points.length ? (
                <MUIDataTable title={'Accesos'} data={pointsRows} columns={columns} options={options} />
            ) : (
                <center>
                    <h3>Este evento no tiene Accesos</h3>
                </center>
            )}
                    </Card.Body>
                
                </Card>
            
            </Grid>
        </Grid>
        </>)}
        { pointModalShow ? (
                <PointModal 
                    event={event} 
                    point={point} 
                    onFinish={onFinish} 
                    onOpenSnackBar={onOpenSnackBar} 
                    open={pointModalShow} 
                    onHide={() => setPointModalShow(false)} />
            ): ('')}
            </>
    );

};

export default Points;