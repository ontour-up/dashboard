import 'react-circular-progressbar/dist/styles.css';
import 'moment/locale/es';

import { Button, Card, Col, Container, ListGroup, Row } from 'react-bootstrap';
import { Chip, InputLabel } from '@material-ui/core';
import { takeEvent, takeTicketsInfo } from '../../services/events.service';
import { useEffect, useState } from 'react';

import AddIcon from '@material-ui/icons/Add';
import { BsPencilFill } from "react-icons/bs";
import { CircularProgressbar } from 'react-circular-progressbar';
import ConfirmationModal from '../../component/Modals/ConfirmationBootstrap';
import CreateEvent from '../../component/Modals/CreateEvent';
import EditIcon from '@material-ui/icons/Edit';
import EntranceModal from '../../component/Modals/Entrance';
import InvitationModal from '../../component/Modals/Invitation';
import Loading from '../../component/Loading';
import React from 'react';
import SnackbarAlert from '../../component/Snackbar';
import { URL_S3 } from '../../store/constant';
import { WEB_URL } from '../../store/constant';
import { getReservasCount } from '../../services/entrances.service';
import { makeStyles } from '@material-ui/core/styles';
import { modifyEvent } from '../../services/events.service';
import moment from 'moment';
import { uploadFile } from '../../services/upload.service';
import { useParams } from 'react-router';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    boxInfo: {
        borderRadius: '10px',
        marginBottom: '30px',
        marginLeft: '35px',
        height: '225px',
        marginTop: '0px',
        width: '220px',
    },
    boxTicket: {
        borderRadius: '10px',
        marginBottom: '30px',
        width: 'auto',
        height: 'auto',
        marginLeft: '15px',
        marginRight: '20px',
        marginTop: '0px',
    },
    textInfo: {
        fontSize: '20px',
        marginBottom: '8px',
        marginTop: '20px',
        justifyContent: 'center',
        display: 'flex',
        marginLeft: '-11px',
    },
    textTitle: {
        fontSize: '20px',
        marginBottom: '8px',
        color: 'green',
        justifyContent: 'center',
        display: 'flex',
    },
    circularProgress: {
        width: 120,
        height: 120,
        margin: "0 auto"
    },

    button: {
        position: 'absolute',
        right: '15px',
        margin: '15px',
        height: '30px',
        background: "white",
        borderColor: "white",
        borderRadius: "30px"

    },
    chip: {
        marginTop: '10px',
        marginRight: '5px',
    },
}))

const Resume = () => {

    const classes = useStyles();
    const [event, setEvent] = useState([]);
    const [ticket, setTicket] = useState([]);
    const [tickets, setTickets] = useState([]);

    const [openEvent, setOpenEvent] = useState(false);

    const [loading, setLoading] = useState(true);

    const [eventMetrics, setEventMetrics] = useState([]);
    const [eventMetricsData, setEventMetricsData] = useState({
        amountTotal: 0,
        entrancesCountTotal: 0,
        percentage: 0,
    });

    const [entranceModalShow, setEntranceModalShow] = useState(false);
    const [invitationModalShow, setInvitationModalShow] = useState(false);

    const [isOpen, setIsOpen] = useState(false);
    const [message, setMessage] = useState({});

    const [confirmationModalShow, setConfirmationModalShow] = useState(false);
    const [confirmationModalData, setConfirmationModalData] = useState({
        title: '',
        description: ''
    });

    let { slug } = useParams();
    const inputFile = useState(null);
    useEffect(() => {
        takeEventId(slug);
    }, [slug]);

    useEffect(() => {
        if (event._id != undefined) {
            takeTickets(event._id);
        }
    }, [event]);

    useEffect(() => {
        if (eventMetrics.length) {
            calculateMetrics();
        }
    }, [eventMetrics]);

    const takeEventId = async (id) => {
        const { status, data } = await takeEvent(id);
        if (status === 200) {
            setEvent(data.data);
        } else {
            setEvent({});
        }
    };

    const onButtonClick = () => {
        // `current` points to the mounted file input element
        inputFile.current.click();
    };

    const handleImageChanged = async (file) => {
        let urlfile = '';
        const formData = new FormData();
        formData.append('file', file);
        const { status } = await uploadFile(formData);
        if (status === 201) {
            urlfile = URL_S3 + file.name;

        } else {
            urlfile = '';
        }
        sendImage(urlfile)
    }

    const takeTickets = async (event) => {
        const metricsArr = [];
        const { status, data } = await takeTicketsInfo(event);
        if (status === 200) {
            for (const element of data.data) {
                const { status, data } = await getReservasCount(element._id);
                if (status === 200) {
                    const obj = {
                        _id: element._id,
                        ticketName: element.name,
                        ticketAmount: element.amount,
                        entrancesCount: data.data,
                    }
                    metricsArr.push(obj);
                }
            }
            setEventMetrics(metricsArr);
            setTickets(data.data);
            setLoading(false);
        } else {
            setEventMetrics();
            setTickets([]);
            setLoading(false);
        }
    };

    const calculateMetrics = () => {
        let amountTotal = 0;
        let entrancesCountTotal = 0;
        for (const metric of eventMetrics) {
            amountTotal += metric.ticketAmount;
            entrancesCountTotal += metric.entrancesCount;
        }
        let total = (entrancesCountTotal * 100) / amountTotal;
        setEventMetricsData({
            amountTotal: amountTotal,
            entrancesCountTotal: entrancesCountTotal,
            percentage: Number.parseFloat(total).toFixed(2),
        });
    };

    const handleGoToLanding = () => {

        window.open(WEB_URL + "event/" + event.qr, "_blank");
    };

    const onRefresh = () => {
        takeEventId(slug);
    }

    const handleCreateEntrance = () => {
        setTicket([]);
        setEntranceModalShow(true);
    }

    const handleEditEntrance = async (value) => {
        setTicket(value)
        setEntranceModalShow(true);
    }

    const handleCreateInvitation = () => {
        setInvitationModalShow(true);
    }

    const onFinish = () => {
        takeEventId(slug);
        //setOpenEvent(false);
        setEntranceModalShow(false);
        setOpenEvent(false)
    }

    const onFinishInvitation = () => {
        setInvitationModalShow(false);
        takeTickets(event._id);
    }

    const onConfirmClose = () => {
        setConfirmationModalShow(false);
        setOpenEvent(false);
    }

    const onCancelClose = () => {
        setConfirmationModalShow(false);
        setOpenEvent(true);
    }

    const onOpenSnackBar = (msg) => {
        setMessage(msg);
        setIsOpen(true);
    };

    const handleCloseSnack = () => {
        setIsOpen(false);
    };

    const handleOpenEvent = () => {
        //setEditEvent({});
        setOpenEvent(true);
    };

    const handleClose = () => {
        setConfirmationModalData({ "title": "Confirmation", "description": "Desea salir de Crear evento?" })
        setConfirmationModalShow(true);
    };

    const sendImage = async (urlFile) => {
        const valuesEvent = {
            banner: urlFile
        }
        const { status, data } = await modifyEvent(event?._id, valuesEvent);
        if (status === 200) {
            takeEventId(data.data._id);
        }
    }
    console.log(event);
    return (
        <>
            {loading === true ? (
                <>
                    <Loading show={loading} />
                </>
            ) : (
                <>
                    <Container fluid>

                        <Row style={{ marginTop: '40px' }}>
                            <Col xs={4}>
                                <Card >
                                    <Card.Header className="text-center"><h4> {event.name}</h4></Card.Header>
                                    <Card.Body className="align-items-center" >
                                        <Card.Img variant="top" src={event.banner} />
                                        <button onClick={onButtonClick} className={classes.button}> <EditIcon /></button>
                                        <input type='file' id='file' ref={inputFile}
                                            onChange={(event) => {
                                                const imageFile = event.currentTarget.files[0];
                                                event.banner = URL.createObjectURL(imageFile);
                                                handleImageChanged(imageFile)
                                            }}
                                            style={{ display: 'none' }} />
                                        <div class="d-flex justify-content-between">
                                            <h6
                                                style={{
                                                    fontSize: '1rem',
                                                    marginTop: '1rem',
                                                    marginBottom: '0.5rem',
                                                    fontWeight: '500',
                                                    linehHeight: '1.2',
                                                }}
                                            >
                                                {moment(event.startDate).format('L LT')}
                                            </h6>
                                            <h6
                                                style={{
                                                    fontSize: '1rem',
                                                    marginTop: '1rem',
                                                    marginBottom: '0.5rem',
                                                    fontWeight: '500',
                                                    linehHeight: '1.2',
                                                }}
                                            >
                                                {event.category}
                                            </h6>
                                        </div>
                                        {event.tags &&
                                            event.tags.map((tag, i) => {
                                                return (
                                                    <Chip
                                                        size="small"
                                                        key={i}
                                                        label={tag}
                                                        color="primary"
                                                        className={classes.chip}
                                                    />
                                                );
                                            })}
                                        <h6
                                            style={{
                                                fontSize: '1rem',
                                                marginTop: '0',
                                                marginBottom: '0.5rem',
                                                fontWeight: '500',
                                                linehHeight: '1.2',
                                            }}
                                        >

                                            <ListGroup variant="flush">
                                                <ListGroup.Item>

                                                </ListGroup.Item>
                                            </ListGroup>
                                            {event.ubication ? (
                                                <ListGroup variant="flush">
                                                    <ListGroup.Item>
                                                        {event.ubication.location}
                                                    </ListGroup.Item>
                                                    <ListGroup.Item>
                                                        {event.ubication.address}
                                                    </ListGroup.Item>
                                                </ListGroup>
                                            ) : ''}
                                        </h6>
                                    </Card.Body>
                                    <Card.Footer style={{ backgroundColor: "transparent", borderWidth: 0 }}>
                                        <Row>
                                            <Col>
                                                <Button onClick={handleGoToLanding}>Landing</Button>
                                            </Col>
                                            <Col>
                                                <Button className="float-end" startIcon={<EditIcon />} onClick={handleOpenEvent}>
                                                    Editar
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Card.Footer>
                                </Card>
                            </Col>
                            <Col xs={8}>
                                <Row>
                                    <Col xs={4}>
                                        {/*}
                            <Grid container>
                                <div className={classes.boxInfo}>
                                    <InputLabel className={classes.textTitle}>Entradas Totales</InputLabel>
                                    {eventMetrics.length ? (
                                        <>
                                        <InputLabel className={classes.textInfo}>
                                        {eventMetricsData.entrancesCountTotal} / {eventMetricsData.amountTotal}
                                    </InputLabel>
                                    <div className={classes.circularProgress}>
                                        <CircularProgressbar
                                            value={eventMetricsData.percentage}
                                            maxValue={100}
                                            text={`${eventMetricsData.percentage}%`}
                                        />
                                    </div>
                                        </>
                                    ) : (
                                    <InputLabel
                                        style={{
                                            fontSize: '15px',
                                            width: 'auto',
                                            marginLeft: 'auto',
                                            textAlign: 'center'
                                        }}
                                    >
                                        No hay entradas disponibles
                                    </InputLabel>
                                    )}
                                    
                                </div>
                            </Grid>
                                    */}
                                        <Card>
                                            <Card.Header className="text-center">
                                                <InputLabel className={classes.textTitle}>Entradas Totales</InputLabel>
                                            </Card.Header>
                                            <Card.Body>
                                                {eventMetrics.length ? (
                                                    <>
                                                        <InputLabel className={classes.textInfo}>
                                                            {/* ID promotor de barakarldo */}
                                                            {event.promoter === "62c58d8544cb3ff794e0d5f6" || event.promoter === "63285afc468abd0dec531aed" ? (
                                                                <>
                                                                    {event.totalAmount === undefined ||  event.totalAmount === null?
                                                                        (
                                                                            <> {eventMetricsData.entrancesCountTotal} / 0 </>
                                                                        ) : (
                                                                            <>{eventMetricsData.entrancesCountTotal} / {event.totalAmount}</>
                                                                        )}
                                                                </>
                                                            ) : (
                                                                <>{eventMetricsData.entrancesCountTotal} / {eventMetricsData.amountTotal}</>
                                                            )}


                                                        </InputLabel>
                                                        <div className={classes.circularProgress}>
                                                            <CircularProgressbar
                                                                value={eventMetricsData.percentage}
                                                                maxValue={100}
                                                                text={`${eventMetricsData.percentage}%`}
                                                            />
                                                        </div>
                                                    </>
                                                ) : (
                                                    <InputLabel
                                                        style={{
                                                            fontSize: '15px',
                                                            width: 'auto',
                                                            marginLeft: 'auto',
                                                            textAlign: 'center'
                                                        }}
                                                    >
                                                        No hay entradas disponibles
                                                    </InputLabel>
                                                )}
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                    <Col xs={8}>
                                        {/*
                            <Row>
                                <Col xs={10}>
                                <InputLabel className={classes.textTitle}>Por Entrada </InputLabel>
                                </Col>
                                <Col xs={2}><Button title="Añadir nueva entrada" onClick={handleCreateEntrance}> <AddIcon /></Button></Col>
                            </Row>
                        
                        
                            {eventMetrics.length ? (
                                <div> 
                                    {eventMetrics.map((data, i) => {
                                        return (
                                            <Row style={{display: 'flex',
                                                        alignItems: 'center',
                                                        justifyContent: 'center'}}>
                                                <Col style={{
                                                    paddingBottom: '2%'
                                                        }}>
                                                    <InputLabel
                                                        style={{
                                                            fontSize: '17px',
                                                            width: '152px',
                                                            marginLeft: '15px',
                                                        }}
                                                    >
                                                    {data.ticketName}
                                                </InputLabel>
                                                </Col>
                                                <Col style={{
                                                    paddingBottom: '2%'
                                                        }}>
                                                    <InputLabel
                                                        style={{
                                                            fontSize: '17px',
                                                            marginLeft: '52px',
                                                        }}
                                                    >
                                                        {data.entrancesCount} / {data.ticketAmount}
                                                    </InputLabel>
                                                </Col>
                                                <Col style={{
                                                    paddingBottom: '2%'
                                                        }}>
                                                <Button key={i} onClick={() => {handleEditEntrance(data)}}><BsPencilFill/></Button>
                                                </Col>
                                            </Row>
                                        );
                                    })}
                                    
                                </div>
                            ) : (

                            <div className={classes.boxTicket}>

                                    <InputLabel
                                        style={{
                                            fontSize: '15x',
                                            width: 'auto',
                                            marginLeft: 'auto',
                                            textAlign: 'center'
                                        }}
                                    >
                                        No hay entradas disponibles
                                    </InputLabel>
                            </div>

                            )}
                            */}
                                        <Card>
                                            <Card.Header className="d-flex justify-content-between align-items-center">
                                                <InputLabel className={classes.textTitle}>Entradas </InputLabel>
                                                <Button title="Añadir nueva entrada" onClick={handleCreateEntrance}> <AddIcon /></Button>
                                            </Card.Header>
                                            <Card.Body>

                                                {eventMetrics.length ? (
                                                    <ListGroup className="scroll" variant="flush">
                                                        {eventMetrics.map((data, i) => {
                                                            return (
                                                                <ListGroup.Item>
                                                                    <Row>
                                                                        <Col md={7}>
                                                                            <InputLabel
                                                                                style={{
                                                                                    fontSize: '17px',
                                                                                }}
                                                                            >
                                                                                {data.ticketName}
                                                                            </InputLabel>
                                                                        </Col>
                                                                        <Col md={3}>
                                                                            <InputLabel
                                                                                style={{
                                                                                    fontSize: '17px',
                                                                                }}
                                                                            >
                                                                                {data.entrancesCount} / {data.ticketAmount}
                                                                            </InputLabel>
                                                                        </Col>
                                                                        <Col md={2}>
                                                                            <Button key={i} onClick={() => { handleEditEntrance(data) }}><BsPencilFill /></Button>
                                                                        </Col>
                                                                    </Row>
                                                                </ListGroup.Item>
                                                            );
                                                        })}

                                                    </ListGroup>
                                                ) : (

                                                    <div className={classes.boxTicket}>

                                                        <InputLabel
                                                            style={{
                                                                fontSize: '15x',
                                                                width: 'auto',
                                                                marginLeft: 'auto',
                                                                textAlign: 'center'
                                                            }}
                                                        >
                                                            No hay entradas disponibles
                                                        </InputLabel>
                                                    </div>

                                                )}
                                            </Card.Body>
                                        </Card>

                                        <Button onClick={handleCreateInvitation}>Enviar invitación</Button>

                                    </Col>



                                </Row>

                                {/*
                        <Row>
                            <Card>
                                <Card.Header className="text-center"><InputLabel className={classes.textTitle}>Grafica </InputLabel></Card.Header>
                                <Card.Body></Card.Body>
                            </Card>
                            
                        </Row>
                        */}
                            </Col>
                        </Row>
                    </Container>
                    {openEvent ? (
                        <CreateEvent
                            event={event}
                            open={openEvent}
                            promoter={event.promoter}
                            onClose={handleClose}
                            onFinish={onFinish}
                            onRefresh={onRefresh}
                            onOpenSnackBar={onOpenSnackBar}
                        />
                    ) : ('')}
                    {entranceModalShow ? (
                        <EntranceModal
                            event={event}
                            ticket={ticket}
                            onFinish={onFinish}
                            onOpenSnackBar={onOpenSnackBar}
                            open={entranceModalShow}
                            onHide={() => setEntranceModalShow(false)} />
                    ) : ('')}
                    {invitationModalShow ? (
                        <InvitationModal
                            event={event}
                            tickets={tickets}
                            onFinish={onFinishInvitation}
                            onOpenSnackBar={onOpenSnackBar}
                            open={invitationModalShow}
                            onHide={() => setInvitationModalShow(false)} />
                    ) : ('')}
                    <SnackbarAlert open={isOpen} msg={message.msg} severity={message.severity} duration={4000} onClose={handleCloseSnack} />
                    <ConfirmationModal data={confirmationModalData} show={confirmationModalShow} onConfirm={onConfirmClose} onCancel={onCancelClose} onHide={() => onCancelClose} />
                </>)}</>

    );

};

export default Resume;