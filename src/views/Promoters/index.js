import {Button, Card, CardContent, Grid, Typography} from '@material-ui/core';
import React, {useEffect, useState} from 'react';

import AddIcon from '@material-ui/icons/Add';
import AlertMsg from "../../component/Alert";
import Breadcrumb from '../../component/Breadcrumb';
import { Link } from 'react-router-dom';
import Pagination from '@material-ui/lab/Pagination';
import Promoter from "../../component/Modals/Promoter";
import PromoterCard from "../../component/Cards/Promoter";
import SnackbarAlert from '../../component/Snackbar';
import { gridSpacing } from '../../store/constant';
import { makeStyles } from '@material-ui/core/styles';
import { takePromoters } from "../../services/promoter.service";

const useStyles = makeStyles({
    media: {
      height: 140,
    },
    root: {
        width: '100%'
    },
    marginAlert: {
        marginTop: '20px'
    }
});

const Promoters = (props) => {
    const classes = useStyles();
    const [openPromoter, setOpenPromoter] = useState(false);
    const [isOpen, setIsOpen] = useState(false);
    const [message, setMessage] = useState({});
    const [promoters, setPromoters] = useState([]);
    const [maxPage, setMaxPage] = useState(1);
    const [page, setPage] = useState(1);
    const [limit] = useState(16);
    const [editPromoter, setEditPromoter] = useState([]);

    useEffect(() => {
        takePromoter(limit, page);
    }, [props]);

    const takePromoter = async (limit, page) => {
        const { status, data } = await takePromoters('?limitField='+limit+'&pageField='+page+'&role=promoter');
        if (status === 200) {
            setPromoters(data.data);
            setMaxPage(data.totalPages)
        } /*else {
           console.log(status, data)
        }*/
    }

    const handleClose = () => {
        setOpenPromoter(false);
    };

    const handleOpenPromoter = () => {
        setOpenPromoter(true);
    };

    const onRefresh = () => {
        takePromoter(limit, page);
    };

    const onOpenSnackBar = (msg) => {
        setMessage(msg);
        setIsOpen(true);
    };

    const handleCloseSnack = () => {
        setIsOpen(false);
    };

    const handleChangePage = (event, value) => {
        takePromoter(limit, value);
        setPage(value)
    };

    const onEdit = (value) => {
        setEditPromoter(value)
        setOpenPromoter(true);
    };

    return (
        <React.Fragment>
            <Breadcrumb title="Promotores">
                <Typography component={Link} to="/" variant="subtitle2" color="inherit" className="link-breadcrumb">
                    Inicio
                </Typography>
                <Typography variant="subtitle2" color="primary" className="link-breadcrumb">
                    Promotores
                </Typography>
            </Breadcrumb>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <Card>
                        <CardContent>
                            <Button
                                variant="contained"
                                color="primary"
                                startIcon={<AddIcon />}
                                onClick={handleOpenPromoter}
                            >
                                Agregar
                            </Button>
                            <Grid container spacing={gridSpacing}>
                                {promoters.map((promoter, index) => {
                                    return (
                                        <PromoterCard key={index} xs={6} md={4} promoter={promoter} onEdit={onEdit}/>
                                    );
                                })}

                                {promoters.length === 0 && (
                                    <Grid item className={classes.marginAlert}>
                                        <AlertMsg severity="info" msg="No se encontrarón datos" />
                                    </Grid>
                                )}

                                <Grid container direction="row" justifyContent="center" alignItems="center">
                                    <Pagination count={maxPage} page={page} onChange={handleChangePage} color="primary" />
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
                <Promoter promoter={editPromoter} open={openPromoter} onClose={handleClose} onRefresh={onRefresh} onOpenSnackBar={onOpenSnackBar} />
            </Grid>
            <SnackbarAlert open={isOpen} msg={message.msg} severity={message.severity} duration={4000} onClose={handleCloseSnack} />
        </React.Fragment>
    );
};

export default Promoters;
