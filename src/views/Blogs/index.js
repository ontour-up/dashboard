import {Button, Card, CardContent, Grid, Typography} from '@material-ui/core';
import React, {useEffect, useState} from 'react';
import { deleteBlog, modifyBlog, takeSearchsBlogs, takesBlogs } from "../../services/blog.service";

import AddIcon from '@material-ui/icons/Add';
import AlertMsg from "../../component/Alert";
import Blog from "../../component/Modals/Blog";
import BlogCard from "../../component/Cards/Blog";
import Breadcrumb from '../../component/Breadcrumb';
import Confirmation from "../../component/Modals/Confirmation";
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import Pagination from '@material-ui/lab/Pagination';
import Search from "../../component/Search";
import SnackbarAlert from '../../component/Snackbar';
import { gridSpacing } from '../../store/constant';
import { makeStyles } from '@material-ui/core/styles';

const useStyles =  makeStyles({
    media: {
      height: 140,
    },
    root: {
        width: '100%'
    },
    leftM: {
        marginLeft: '10px'
    },
});


const Blogs = (props) => {
    const classes = useStyles();
    const [openEvent, setOpenEvent] = useState(false);
    const [isOpen, setIsOpen] = useState(false);
    const [message, setMessage] = useState({});
    const [blogs, setBlogs] = useState([]);
    const [editBlog, setEditBlog] = useState([]);
    const [maxPage, setMaxPage] = useState(1);
    const [page, setPage] = useState(1);
    const [idBlog, setIdBlog] = useState('');
    const [openConfirmation, setOpenConfirmation] = useState(false);
    const [limit] = useState(12);

    useEffect(() => {
        takeBlogs(limit, page);
    }, [props]);

    const takeBlogs = async (limit, page) => {
        const { status, data } = await takesBlogs('?limitField='+limit+'&pageField='+(page-1));
        if (status === 200) {
            setBlogs(data.data);
            setMaxPage(data.totalPages)
        } /*else {
           console.log(status, data)
        }*/
    }

    const handleClose = () => {
        setOpenEvent(false);
    };

    const handleCloseConfirmation = () => {
        setOpenConfirmation(false);
    };

    const handleOpenEvent = () => {
        setEditBlog({})
        setOpenEvent(true);
    };

    const onRefresh = () => {
        setEditBlog({})
        takeBlogs(limit, page);
    };

    const onOpenSnackBar = (msg) => {
        setMessage(msg);
        setIsOpen(true);
    };

    const handleCloseSnack = () => {
        setIsOpen(false);
    };

    const handleChangePage = (event, value) => {
        takeBlogs(limit, value);
        setPage(value)
    };

    const onEdit = (value) => {
        setEditBlog(value)
        setOpenEvent(true);
    };

    const onDelete = (value) => {
        setIdBlog(value);
        setOpenConfirmation(true)
    };

    const onConfirm = async () => {
        const { status, data } = await deleteBlog(idBlog);
        if (status === 200) {
            setOpenConfirmation(false);
            setMessage({severity: 'success', msg: data.message});
            setIsOpen(true);
            onRefresh();
        } else {
            setMessage({severity: 'warning', msg: data.message});
            setIsOpen(true);
            setOpenConfirmation(false);
        }
    };

    const onSearch = async (value) => {
        const { status, data } = await takeSearchsBlogs('?search='+value);
        if (status === 200) {
            setBlogs(data.data);
            setMaxPage(1)
        } /*else {
           console.log(status, data)
        }*/
    };
    
    const onChangeState = async (id, state) => {
        const { status, data } = await modifyBlog(id, {isActive: state});
        if (status === 200) {
            setOpenConfirmation(false);
            setMessage({severity: 'success', msg: data.message});
            setIsOpen(true);
            onRefresh();
        } else {
            setMessage({severity: 'warning', msg: data.message});
            setIsOpen(true);
            setOpenConfirmation(false);
        }
    };

    return (
        <>
        <Helmet>‍
            <title>Blogs - Jazzaldia</title>‍
            <meta name="description" content={"Blogs - Jazzaldia"} />
            <meta property="og:title" content={"Blogs - Jazzaldia"} />    
            <meta property="og:site_name" content="Jazzaldia" />
            <meta property="og:locale" content="es" />
        </Helmet>
        <React.Fragment>
            <Breadcrumb title="Blogs - Noticias">
                <Typography component={Link} to="/" variant="subtitle2" color="inherit" className="link-breadcrumb">
                    Inicio
                </Typography>
                <Typography variant="subtitle2" color="primary" className="link-breadcrumb">
                    Blogs - Noticias
                </Typography>
            </Breadcrumb>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <Card>
                        <CardContent>
                            <Grid container spacing={gridSpacing}>
                                <Grid item xs={12} md={6}>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        startIcon={<AddIcon />}
                                        onClick={handleOpenEvent}
                                    >
                                        Agregar
                                    </Button>
                                </Grid>
                                <Grid item xs={12} md={6} align="right">
                                    <Search onSearch={onSearch} />
                                </Grid>
                            </Grid>
                            <Grid container spacing={gridSpacing}>
                                {blogs.map((blog, index) => {
                                    return (
                                        <BlogCard key={index} xs={6} md={4} blog={blog} onEdit={onEdit} onDelete={onDelete} onChangeState={onChangeState} />
                                    );
                                })}

                                {blogs.length === 0 && (
                                    <Grid item>
                                        <AlertMsg severity="info" msg="No se encontrarón datos" />
                                    </Grid>
                                )}
                                

                                <Grid container direction="row" justifyContent="center" alignItems="center">
                                    <Pagination count={maxPage} page={page} onChange={handleChangePage} color="primary" />
                                </Grid>
                                
                            </Grid>
                            
                        </CardContent>
                    </Card>
                </Grid>
                <Blog blog={editBlog} open={openEvent} onClose={handleClose} onRefresh={onRefresh} onOpenSnackBar={onOpenSnackBar} />
                <Confirmation open={openConfirmation} onClose={handleCloseConfirmation} onConfirm={onConfirm} msg="¿Desea eliminar este registro?" txtBtn="SI" />
            </Grid>
            <SnackbarAlert open={isOpen} msg={message.msg} severity={message.severity} duration={4000} onClose={handleCloseSnack} />
        </React.Fragment>
        </>
    );
};

export default Blogs;
