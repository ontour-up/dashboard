import * as yup from 'yup';

import { Button, Card, CardContent, CircularProgress, FormControl, Grid, InputLabel, MenuItem, Select, TextField, Typography } from '@material-ui/core';
import React, {useContext, useEffect, useState} from 'react';
import { changePassword, modifyUser } from "../../services/auth.service";

import Breadcrumb from '../../component/Breadcrumb';
import ChangePassword from "../../component/Modals/ChangePassword";
import Cookies from "js-cookie";
import DateFnsUtils from '@date-io/date-fns';
import { DatePicker } from "@material-ui/pickers";
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import SnackbarAlert from '../../component/Snackbar';
import { UserContext } from "../../context/UserContext";
import { es } from 'date-fns/locale';
import { format } from 'date-fns';
import { gridSpacing } from '../../store/constant';
import { makeStyles } from '@material-ui/core/styles';
import { useFormik } from 'formik';

const useStyles = makeStyles({
    media: {
      height: 140,
    },
    root: {
        width: '100%'
    },
    marginAlert: {
        marginTop: '20px'
    },
    formControl: {
        minWidth: '100%',
        marginTop: '10px'
    },
    divBtn: {
        marginTop: '20px',
        marginBottom: '20px',
    },
    buttonProgress: {
        marginLeft: '10px'
    },
    buttonChangePassword: {
        marginRight: '10px'
    }
});

const validationSchema = yup.object({
    names: yup.string().required(),
    surnames: yup.string().required(),
    phone: yup.string().required(),
});

const Profile = (props) => {
    const classes = useStyles();
    const [isOpen, setIsOpen] = useState(false);
    const [message, setMessage] = useState({});
    const { user, setUser } = useContext(UserContext);
    const [isLoading, setIsLoading] = useState(false);
    const [birth, SetBirth] = useState(null);
    const [openModal, setOpenModal] = useState(false);

    useEffect(() => {
        SetBirth(user.birthday);
    }, [props]);

    const handleCloseSnack = () => {
        setIsOpen(false);
    };

    const handleDatePChange = (e) => {
        SetBirth(e)
        formik.setFieldValue('birthday', format(e, "yyy-MM-dd"))
    }

    const handleSubmit = async (values) => {
        setIsLoading(true);
        const { status, data } = await modifyUser(user?._id, values);        
        if (status === 200) {
            setUser(data.data)
            Cookies.set("userInfo", data.data);
            setMessage({severity: 'success', msg: data.message})
            setIsLoading(false);
            setIsOpen(true);
        } else {
            setMessage({severity: 'warning', msg: data.message})
            setIsOpen(true);
            setIsLoading(false);
        }
    }

    const handleClose = () => {
        setOpenModal(false);
    };

    const openChangePassword = () => {
        setOpenModal(true);
    };

    const handleChangePassword = async (password) => {
        setOpenModal(false)
        setIsLoading(true)
        const payload = {
          email: user.email,
          password: password,
        };
        const { status, data } = await changePassword(payload);
        if (status === 200) {
            setIsLoading(false);
            setMessage({severity: 'success', msg: '¡Contraseña actualizada correctamente!'});
            setIsOpen(true);
        } else {
            setMessage({severity: 'warning', msg: data.message});
            setIsOpen(true);
            setIsLoading(false);
        }
    };

    const formik = useFormik({
        initialValues: {
            names: user.names,
            surnames: user.surnames,
            birthday: user.birthday ? user.birthday : '',
            phone: user.phone,
            sex: user.sex ? user.sex : '',
            email: user.email,
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            handleSubmit(values);
        },
    });

    return (
        <>
            <Helmet>‍
                <title>Perfil - Jazzaldia</title>‍
                <meta name="description" content={"Perfil - Jazzaldia"} />
                <meta property="og:title" content={"Perfil - Jazzaldia"} />    
                <meta property="og:site_name" content="Jazzaldia" />
                <meta property="og:locale" content="es" />
            </Helmet>
        <React.Fragment>
            <Breadcrumb title="Mi Perfil">
                <Typography component={Link} to="/" variant="subtitle2" color="inherit" className="link-breadcrumb">
                    Inicio
                </Typography>
                <Typography variant="subtitle2" color="primary" className="link-breadcrumb">
                    Perfil
                </Typography>
            </Breadcrumb>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <Card>
                        <CardContent>
                            <form onSubmit={formik.handleSubmit}>
                                <Grid container direction="column" spacing={4} justifyContent="center">
                                    <Grid item xs={12} sm={6}>
                                    <Typography color="primary">Mi información</Typography>
                                        <TextField
                                            fullWidth
                                            autoFocus
                                            label="Nombres"
                                            margin="normal"
                                            name="names"
                                            value={formik.values.names}
                                            variant="outlined"
                                            onChange={formik.handleChange}
                                            error={formik.touched.names && Boolean(formik.errors.names)}
                                            helpertext={formik.touched.names && formik.errors.names}
                                        />
                                        <TextField
                                            fullWidth
                                            label="Apellidos"
                                            margin="normal"
                                            name="surnames"
                                            value={formik.values.surnames}
                                            variant="outlined"
                                            onChange={formik.handleChange}
                                            error={formik.touched.surnames && Boolean(formik.errors.surnames)}
                                            helpertext={formik.touched.surnames && formik.errors.surnames}
                                        />
                                        <TextField
                                            fullWidth
                                            label="Teléfono"
                                            margin="normal"
                                            name="phone"
                                            value={formik.values.phone}
                                            variant="outlined"
                                            onChange={formik.handleChange}
                                            error={formik.touched.phone && Boolean(formik.errors.phone)}
                                            helpertext={formik.touched.phone && formik.errors.phone}
                                        />
                                        <TextField
                                            fullWidth
                                            label="Correo"
                                            margin="normal"
                                            name="email"
                                            value={formik.values.email}
                                            variant="outlined"
                                            disabled
                                            onChange={formik.handleChange}
                                        />
                                        <FormControl className={classes.formControl}>
                                            <InputLabel id="label-sex">Sex</InputLabel>
                                            <Select
                                                labelId="label-sex"
                                                id="sex"
                                                name="sex"
                                                value={formik.values.sex}
                                                onChange={formik.handleChange}
                                            >
                                                <MenuItem value="male">Masculino</MenuItem>
                                                <MenuItem value="female">Femenino</MenuItem>
                                                <MenuItem value="not_indicate">No indicar</MenuItem>
                                            </Select>
                                        </FormControl>

                                        <FormControl className={classes.formControl}>
                                            <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                                                <DatePicker
                                                    autoOk
                                                    fullWidth
                                                    clearable
                                                    id="birth"
                                                    name="birth"
                                                    ampm={false}
                                                    value={birth}
                                                    onChange={handleDatePChange}
                                                    format="yyyy-MM-dd"
                                                    label="Fecha de nacimiento"
                                                />
                                            </MuiPickersUtilsProvider>
                                        </FormControl>
                                        <div className={classes.divBtn}>
                                            <Button className={classes.buttonChangePassword} onClick={openChangePassword}>Cambiar Contraseña</Button>
                                            <Button type="submit" variant="contained" color="primary">
                                                Guardar
                                                {isLoading && <CircularProgress color="secondary" size={24} className={classes.buttonProgress} />}
                                            </Button>
                                        </div>
                                    </Grid>
                                </Grid>
                            </form>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
            <ChangePassword open={openModal} onClose={handleClose} onChangePassword={handleChangePassword} />
            <SnackbarAlert open={isOpen} msg={message.msg} severity={message.severity} duration={4000} onClose={handleCloseSnack} />
        </React.Fragment>
        </>
    );
};

export default Profile;
