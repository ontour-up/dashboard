import {Button, Card, CardContent, Grid, Typography} from '@material-ui/core';
import React, { useContext, useEffect, useState } from 'react';
import { deleteArtist, takesArtists } from '../../services/artist.service';
import { deleteBlog, modifyBlog, takeSearchsBlogs, takesBlogs } from "../../services/blog.service";

import AddIcon from '@material-ui/icons/Add';
import AlertMsg from "../../component/Alert";
import ArtistCard from "../../component/Cards/Artist";
import Blog from "../../component/Modals/Blog";
import BlogCard from "../../component/Cards/Blog";
import Breadcrumb from '../../component/Breadcrumb';
import Confirmation from "../../component/Modals/Confirmation";
import CreateArtist from '../../component/Modals/Artist';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import Notice from "../../component/Modals/Notice";
import Pagination from '@material-ui/lab/Pagination';
import Search from "../../component/Search";
import SnackbarAlert from '../../component/Snackbar';
import { UserContext } from '../../context/UserContext';
import { gridSpacing } from '../../store/constant';
import { makeStyles } from '@material-ui/core/styles';

const useStyles =  makeStyles({
    media: {
      height: 140,
    },
    root: {
        width: '100%'
    },
    leftM: {
        marginLeft: '10px'
    },
});


const Artists = (props) => {
    const classes = useStyles();
    const { user } = useContext(UserContext);
    const [openEvent, setOpenEvent] = useState(false);
    const [isOpen, setIsOpen] = useState(false);
    const [message, setMessage] = useState({});
    const [artists, setArtists] = useState([]);
    const [editArtist, setEditArtist] = useState([]);
    const [idArtist, setIdArtist] = useState('');
    const [openConfirmation, setOpenConfirmation] = useState(false);

    useEffect(() => {
        takeArtists();

    }, [props]);

    const takeArtists = async () => {
        const { status, data } = await takesArtists({festival: '619e179477275183e38adb93'});
        if (status === 200) {
            setArtists(data.data);
        } /*else {
           console.log(status, data)
        }*/
    }

    const handleClose = () => {
        setOpenEvent(false);
    };

    const handleCloseConfirmation = () => {
        setOpenConfirmation(false);
    };

    const handleOpenEvent = () => {
        setEditArtist({})
        setOpenEvent(true);
    };

    const onRefresh = () => {
        setEditArtist({})
        takeArtists();
    };

    const onOpenSnackBar = (msg) => {
        setMessage(msg);
        setIsOpen(true);
    };

    const handleCloseSnack = () => {
        setIsOpen(false);
    };

    // const handleChangePage = (event, value) => {
    //     takeBlogs(limit, value);
    //     setPage(value)
    // };

    const onEdit = (value) => {
        setEditArtist(value)
        setOpenEvent(true);
    };

    const onDelete = (value) => {
        setIdArtist(value);
        setOpenConfirmation(true)
    };

    const onConfirm = async () => {
        const { status, data } = await deleteArtist(idArtist);
        if (status === 200) {
            setOpenConfirmation(false);
            setMessage({severity: 'success', msg: data.message});
            setIsOpen(true);
            onRefresh();
        } else {
            setMessage({severity: 'warning', msg: data.message});
            setIsOpen(true);
            setOpenConfirmation(false);
        }
    };

    // const onSearch = async (value) => {
    //     const { status, data } = await takeSearchsBlogs('?search='+value);
    //     if (status === 200) {
    //         setBlogs(data.data);
    //         setMaxPage(1)
    //     } else {
    //        console.log(status, data)
    //     }
    // };
    
    // const onChangeState = async (id, state) => {
    //     const { status, data } = await modifyBlog(id, {isActive: state});
    //     if (status === 200) {
    //         setOpenConfirmation(false);
    //         setMessage({severity: 'success', msg: data.message});
    //         setIsOpen(true);
    //         onRefresh();
    //     } else {
    //         setMessage({severity: 'warning', msg: data.message});
    //         setIsOpen(true);
    //         setOpenConfirmation(false);
    //     }
    // };

    if (user.roles.indexOf('olas') < 0) {
        return <>No tienes permisos</>;
    }

    return (
        <>
        <Helmet>‍
            <title>Competidores - Punta Galea Challenge</title>‍
            <meta name="description" content={"Competidores - Punta Galea Challenge"} />
            <meta property="og:title" content={"Competidores - Punta Galea Challenge"} />
            <meta property="og:site_name" content="Punta Galea Challenge" />
            <meta property="og:locale" content="es" />
        </Helmet>
        <React.Fragment>
            <Breadcrumb title="Competidores - Punta Galea Challenge">
                <Typography component={Link} to="/" variant="subtitle2" color="inherit" className="link-breadcrumb">
                    Inicio
                </Typography>
                <Typography variant="subtitle2" color="primary" className="link-breadcrumb">
                    Competidores - Punta Galea Challenge
                </Typography>
            </Breadcrumb>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <Card>
                        <CardContent>
                            <Grid container spacing={gridSpacing}>
                                <Grid item xs={12} md={6}>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        startIcon={<AddIcon />}
                                        onClick={handleOpenEvent}
                                    >
                                        Agregar
                                    </Button>
                                </Grid>
                                {/*<Grid item xs={12} md={6} align="right">*/}
                                {/*    <Search onSearch={onSearch} />*/}
                                {/*</Grid>*/}
                            </Grid>
                            <Grid container spacing={gridSpacing}>
                                {artists.map((artist, index) => {
                                    return (
                                        <ArtistCard key={index} xs={6} md={4} artist={artist} onEdit={onEdit} onDelete={onDelete} />
                                    );
                                })}

                                {artists.length === 0 && (
                                    <Grid item>
                                        <AlertMsg severity="info" msg="No se encontrarón datos" />
                                    </Grid>
                                )}
                                

                                {/*<Grid container direction="row" justifyContent="center" alignItems="center">*/}
                                {/*    <Pagination count={maxPage} page={page} onChange={handleChangePage} color="primary" />*/}
                                {/*</Grid>*/}
                                
                            </Grid>
                            
                        </CardContent>
                    </Card>
                </Grid>
                <CreateArtist artist={editArtist} open={openEvent} onClose={handleClose} onRefresh={onRefresh} onOpenSnackBar={onOpenSnackBar} />
                <Confirmation open={openConfirmation} onClose={handleCloseConfirmation} onConfirm={onConfirm} msg="¿Desea eliminar este registro?" txtBtn="SI" />
            </Grid>
            <SnackbarAlert open={isOpen} msg={message.msg} severity={message.severity} duration={4000} onClose={handleCloseSnack} />
        </React.Fragment>
        </>
    );
};

export default Artists;
