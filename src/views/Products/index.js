import { Grid, InputLabel, TabScrollButton } from '@material-ui/core';
import { Button, Card } from 'react-bootstrap';
import { useContext, useEffect, useState } from 'react';
import MUIDataTable from 'mui-datatables';
import { UserContext } from '../../../src/context/UserContext';
import Loading from '../../component/Loading';
import React from 'react';
import { gridSpacing } from '../../store/constant';
import { useParams } from 'react-router';
import 'react-circular-progressbar/dist/styles.css';
import AddIcon from '@material-ui/icons/Add';
import { BsPencilFill } from "react-icons/bs";
import moment from 'moment';
import 'moment/locale/es';
import { takeProductsInfo } from '../../services/product';
import ProductModal from '../../component/Modals/Product';

const Products = () => {

    const [ product, setProduct ] = useState([]);
    const [ products, setProducts ] = useState([]);
    const [ productsRows, setProductsRows ] = useState([]);

    const [isOpen, setIsOpen] = useState(false);
    const [message, setMessage] = useState({});

    const [loading, setLoading] = useState(true);
    const [ productModalShow, setProductModalShow ] = useState(false);

    let { slug } = useParams();
    const { user } = useContext(UserContext);
    
    useEffect(() => {
        takeProducts(user._id);
    }, [user]);
    

    const takeProducts = async (enterpriseID) => {
        const { status, data } = await takeProductsInfo(enterpriseID);
        if (status === 200) {
            setProducts(data.data);
            setProductsRows(fillRows(data.data));
            setLoading(false);
        }else{
            setProducts([]);
            setProductsRows([]);
            setLoading(false);
        }
    };

    const fillRows = (zones) => {
        let rowArr = [];
        zones.forEach((obj, id) => {
            const row = [
                id,
                obj._id,
                obj.name,
                obj.description,
                obj.created,
                obj.type,
                obj.grossPrice,
                obj.ivaPercent,
                obj
            ];
            rowArr.push(row);
        });
        return rowArr;
    };

    const handleCreateProduct = () => {
        setProduct([]);
        setProductModalShow(true);   
    }

    const handleEditZone = async (value) => {
        setProduct(value)
        setProductModalShow(true);
    }

    const onOpenSnackBar = (msg) => {
        setMessage(msg);
        setIsOpen(true);
    };

    const onFinish = () => {
        //takeEventId(slug);
        takeProducts(user._id);
        //setOpenEvent(false);
        setProductModalShow(false);
        //setOpenEvent(false)
    }   

    const columns = [{
        name: "column-id",
        options: {
            display: false,
        }
    }, {
        name: "ID",
        options: {
            display: false,
            }
    }, {
        name: "Nombre",
        options: {
            display: true,
        }
    }, {
        name: "Descripción",
        options: {
            display: true,
        }
    },{
        name: 'fecha',
        label: "Fecha Creación",
            options: {
            display: true,
            customBodyRender: (value, tableMeta, updateValue) => {
                return( moment(value).format('L LT'))
           },
        }
    }, {
        name: "Tipo",
        options: {
            display: true,
        }
    },{
        name: "Gross Price",
        options: {
            display: true,
        }
    },{
        name: "Iva",
        options: {
            display: true,
        }
    }, {
        label: "Actions",
        options: {
            customBodyRender: (value, tableMeta, updateValue) => {
                return (
                    <Button key={value._id} onClick={() => {handleEditZone(value)}}><BsPencilFill/></Button>
                )
            }
        }
    }];

    const options = {
        filterType: 'dropdown',
        selectableRows: 'none',
        rowsPerPage: 15,
        print: false,
        customToolbarSelect: () => {},
        sortOrder: {
            name: 'fecha-compra',
            direction: 'desc'
          },
        textLabels: {
            body: {
                noMatch: 'No se han encontrado registros',
                toolTip: 'Ordenar',
                columnHeaderTooltip: (column) => `Ordenar por ${column.label}`,
            },
            pagination: {
                next: 'Siguiente página',
                previous: 'Página anterior',
                rowsPerPage: 'Filas por página:',
                displayRows: 'de',
            },
            toolbar: {
                search: 'Buscar',
                downloadCsv: 'Descargar CSV',
                print: 'Imprimir',
                viewColumns: 'Mostrar columnas',
                filterTable: 'Filtrar tabla',
            },
            filter: {
                all: 'Todo',
                title: 'Filtros',
                reset: 'Reiniciar',
            },
            viewColumns: {
                title: 'Mostrar columnas',
                titleAria: 'Mostrar/Ocultar columnas de la tabla',
            },
            selectedRows: {
                text: 'fila(s) seleccionada(s)',
                delete: 'Borrar',
                deleteAria: 'Borrar las filas seleccionadas',
            },
        },
    };

    return (
        <>
        {loading === true ? (
            <>
                <Loading show={loading} />
            </>
        ) : (
            <>
        <Grid container spacing={gridSpacing}>
            <Grid item lg={12} sm={12} xs={12}>
                <Card>
                <Card.Header className="d-flex justify-content-between align-items-center">
                    <InputLabel>Productos</InputLabel> 
                    <Button title="Añadir nuevo producto" onClick={handleCreateProduct}> <AddIcon /></Button>
                </Card.Header>
                    <Card.Body>
                    {products.length ? (
                <MUIDataTable title={'Productos'} data={productsRows} columns={columns} options={options} />
            ) : (
                <center>
                    <h3>Este evento no tiene Products</h3>
                </center>
            )}
                    </Card.Body>
                </Card>
            </Grid>
        </Grid>
        </>)}
        { productModalShow ? (
                <ProductModal 
                    product={product} 
                    onFinish={onFinish} 
                    onOpenSnackBar={onOpenSnackBar} 
                    open={productModalShow} 
                    onHide={() => setProductModalShow(false)} />
            ): ('')}
            </>
    );

};

export default Products;