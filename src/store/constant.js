export const gridSpacing = 3;
export const drawerWidth = 280;
export const API_URL = process.env.REACT_APP_API_URL;
export const URL_S3 = process.env.REACT_APP_URL_S3;
export const WEB_URL = "https://eventos.ontourup.com/"
