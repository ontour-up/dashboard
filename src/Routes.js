import React, { lazy, Suspense, useContext, useState, useEffect } from 'react';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';
import { AnimatePresence } from 'framer-motion';
import { GuardProvider, GuardedRoute } from 'react-router-guards';
import Loader from './component/Loader/Loader';
import NavMotion from './layout/NavMotion';
import MainLayout from './layout/MainLayout';
import MinimalLayout from './layout/MinimalLayout';
import { UserContext } from "./context/UserContext";
import NotificationsOlas from './views/Notifications/olas';
//import Buyers from './views/Event/Buyers';

const AuthLogin = lazy(() => import('./views/Login'));
const Price = lazy(() => import('./views/Application/Price/Price2'));
const DashboardDefault = lazy(() => import('./views/Dashboard/Default'));
const Blogs = lazy(() => import('./views/Blogs'));
const Notices = lazy(() => import('./views/Notices'));
const Artists = lazy(() => import('./views/Artists'));

const Sponsors = lazy(() => import('./views/Sponsors'));
const Promoters = lazy(() => import('./views/Promoters'));
const Festivals = lazy(() => import('./views/Festivals'));
const Profile = lazy(() => import('./views/Profile'));
const BlogDetail = lazy(() => import('./views/Jazzaldia/BlogDetail'));
const NotificationsJazzaldia = lazy(() => import('./views/Notifications/Jazzaldia'));
const ReportBlogs = lazy(() => import('./views/Report/blogs'));
const ReportUsersExcel = lazy(() => import('./views/Report/usersExcel'));
const Events = lazy(() => import ('./views/Events'));
const Event = lazy(() => import ('./views/Event'));
const Buyers = lazy(() => import ('./views/Event/Buyers'));
const Resume = lazy(() => import ('./views/Event/Resume'));
const Tickets = lazy(() => import ('./views/Event/Tickets'));
const Assistants = lazy(() => import ('./views/Event/Assistants'));
const Zones = lazy(() => import ('./views/Event/Zones'));
const Points = lazy(() => import ('./views/Event/Points'));
const Stocks = lazy(() => import ('./views/Event/Stocks'));
const Cashless = lazy(() => import ('./views/Event/Cashless'));
const Cycle = lazy(() => import ('./views/Cycle'));
const Products = lazy(() => import ('./views/Products'));
//const TableBasic = lazy(() => import('./views/Tables/TableBasic'));
//const UtilsIcons = lazy(() => import('./views/Utils/Icons'));
//const UtilsTypography = lazy(() => import('./views/Utils/Typography'));
//const MultiLanguage = lazy(() => import('./views/MultiLanguage'));
//const RtlLayout = lazy(() => import('./views/RtlLayout'));

const requireLogin = (to, from, next) => {
    if (to.meta.auth) {
        next();
    } else {
        next.redirect('/login');
    }
};

const Routes = () => {
    const location = useLocation();
    const { isAuth } = useContext(UserContext);
    
    return (
        <AnimatePresence>
            <GuardProvider guards={[requireLogin]} loading={Loader} error={Price}>
                <Suspense fallback={<Loader />}>
                    
                    <Switch>
                        <Redirect exact from="/" to="/home" />
                        <Route path={[]}>
                            <MinimalLayout>
                                <Switch location={location} key={location.pathname}>
                                    <NavMotion></NavMotion>
                                </Switch>
                            </MinimalLayout>
                        </Route>
                        <Route path="/login" component={AuthLogin} />
                        <Route path="/jazzaldia/:slug/:language" component={BlogDetail} />

												{/* <Route path="/cycle/events/:slug" component={Events}  meta={{ auth: isAuth }} />
                        <Route path="/cycle/:slug" component={Cycle}  meta={{ auth: isAuth }} /> */}
                        <Route
                            path={[
                                '/home',
                                // '/blogs',
                                '/news',
                                '/competitors',
                                '/sponsors',
                                '/promoters',
                                '/festivals',
                                '/user/profile',
                                '/notifications/jazzaldia',
                                '/notifications/olas',
                                // '/report/blogs',
                                '/report/usersExcel',
                                '/events',
                                '/event/:slug/resume',
                                '/event/:slug/tickets',
                                '/event/:slug/assistants',
                                '/event/:slug/buyers',
                                '/event/:slug/zones',
                                '/event/:slug/access',
                                '/event/:slug/stocks',
                                '/event/:slug/cashless',
                                '/products',
																// '/cycle/:slug',
                            ]}
                        >
														<MainLayout>
														<Switch location={location} key={location.pathname}>
																<NavMotion>
																		<GuardedRoute path="/home" component={DashboardDefault}  meta={{ auth: isAuth }} />
																		{/*<GuardedRoute path="/blogs" component={Blogs}  meta={{ auth: isAuth }} />*/}
																		<GuardedRoute path="/news" component={Notices}  meta={{ auth: isAuth }} />
																		<GuardedRoute path="/competitors" component={Artists}  meta={{ auth: isAuth }} />
																		<GuardedRoute path="/sponsors" component={Sponsors}  meta={{ auth: isAuth }} />
																		<GuardedRoute path="/promoters" component={Promoters}  meta={{ auth: isAuth }} />
																		<GuardedRoute path="/festivals" component={Festivals}  meta={{ auth: isAuth }} />
																		<GuardedRoute path="/administration/accounts" component={Festivals}  meta={{ auth: isAuth }} />
																		<GuardedRoute path="/user/profile" component={Profile}  meta={{ auth: isAuth }} />
																		<GuardedRoute path="/notifications/jazzaldia" component={NotificationsJazzaldia}  meta={{ auth: isAuth }} />
																		<GuardedRoute path="/notifications/olas" component={NotificationsOlas}  meta={{ auth: isAuth }} />
																		{/*<GuardedRoute path="/report/blogs" component={ReportBlogs}  meta={{ auth: isAuth }} />*/}
																		<GuardedRoute path="/report/usersExcel" component={ReportUsersExcel}  meta={{ auth: isAuth }} />
																		<GuardedRoute path="/events" component={Events}  meta={{ auth: isAuth }} />
                                                                        <GuardedRoute path="/event/:slug" component={Event}  meta={{ auth: isAuth }} />
                                                                        <GuardedRoute path="/event/:slug/resume" component={Resume}  meta={{ auth: isAuth }} />
                                                                        <GuardedRoute path="/event/:slug/tickets" component={Tickets}  meta={{ auth: isAuth }} />
                                                                        <GuardedRoute path="/event/:slug/assistants" component={Assistants}  meta={{ auth: isAuth }} />
                                                                        <GuardedRoute path="/event/:slug/buyers" component={Buyers}  meta={{ auth: isAuth }} />
                                                                        <GuardedRoute path="/event/:slug/zones" component={Zones}  meta={{ auth: isAuth }} /> 
                                                                        <GuardedRoute path="/event/:slug/access" component={Points}  meta={{ auth: isAuth }} />
                                                                        <GuardedRoute path="/event/:slug/stocks" component={Stocks}  meta={{ auth: isAuth }} />
                                                                        <GuardedRoute path="/event/:slug/cashless" component={Cashless}  meta={{ auth: isAuth }} />
                                                                        <GuardedRoute path="/products" component={Products}  meta={{ auth: isAuth }} />
																		{/* <GuardedRoute path="/cycle/:slug" component={Cycle}  meta={{ auth: isAuth }} /> */}
																</NavMotion>
														</Switch>
												</MainLayout>
                        </Route>
                    </Switch>
                </Suspense>
            </GuardProvider>
        </AnimatePresence>
    );
};

export default Routes;

