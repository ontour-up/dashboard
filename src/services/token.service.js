import { methodGet } from "./methods";


export const takeTokensApp = async (search, query) => {
    return methodGet("/token/accounts-for-app" + search, query);
};
