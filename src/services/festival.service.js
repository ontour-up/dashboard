import { methodPost, methodGet, methodDelete, methodPatch } from "./methods";

export const takeFestival = async (search = '', query) => {
  return methodGet("/festival" + search, query);
};

export const addFestival = async (query) => {
  return methodPost("/festival", query);
};

export const modifyFestival = async (id, query) => {
  return methodPatch("/festival?blogID="+id, query);
};

export const deleteFestival = async (id, query) => {
  return methodDelete("/festival?festivalID="+id, query);
};