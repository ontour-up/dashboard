import { methodGet, methodDelete, methodPatch, methodPost } from "./methods";

export const addSponsor = async (query) => {
  return methodPost("/sponsor", query);
};

export const takeSponsor = async (search = '', query) => {
  return methodGet("/sponsor" + search, query);
};

export const modifySponsor = async (id, query) => {
  return methodPatch("/sponsor?sponsorID="+id, query);
};

export const deleteSponsor = async (id, query) => {
  return methodDelete("/sponsor?sponsorID="+id, query);
};