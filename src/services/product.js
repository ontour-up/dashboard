import { methodGet, methodPostWithout, methodPatch } from "./methods";

export const takeProductsInfo = async (enterpriseID) => {
    return methodGet("/product?enterprise="+enterpriseID);
  }
  export const addProduct = async (body) => {
    return methodPostWithout("/product",body);
  }

  export const modifyProduct = async (id, body) => {
    return methodPatch("/product/" +id, body);
  };
