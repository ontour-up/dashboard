import { methodGet, methodPostWithout, methodPatch } from "./methods";

export const takeStocksInfo = async (enterpriseID) => {
    return methodGet("/stock?enterprise="+enterpriseID);
  }
  export const addStock = async (body) => {
    return methodPostWithout("/stock",body);
  }

  export const modifyStock = async (id, body) => {
    return methodPatch("/stock/" +id, body);
  };
