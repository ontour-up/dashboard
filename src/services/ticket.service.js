import { methodGet, methodPostWithout, methodPatch } from "./methods";

export const addTicket = async (query) => {
    return methodPostWithout("/ticket", query);
  }

  export const takeTicket = async (query) => {
    return methodGet("/ticket/getTicket", query);
  }

  export const takeTickets = async (query) => {
    return methodGet("/ticket/getTickets", query);
  }

  export const modifyTicket = async (id, query) => {
    return methodPatch("/ticket/" +id, query);
  };