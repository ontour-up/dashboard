import { methodGet, methodDelete, methodPatch, methodPostWithout } from "./methods";

export const addblog = async (query) => {
  return methodPostWithout("/blog", query);
};

export const takesBlogs = async (search, query) => {
    return methodGet("/blog" + search, query);
};

export const takeSearchsBlogs = async (search, query) => {
  return methodGet("/blog/searchByName" + search, query);
};

export const takeOneBlog = async (search, query) => {
  return methodGet("/blog" + search, query);
};

export const takeBlogSlug = async (search, query) => {
  return methodGet("/blog/slug" + search, query);
};

export const modifyBlog = async (id, query) => {
  return methodPatch("/blog?blogID="+id, query);
};

export const deleteBlog = async (id, query) => {
  return methodDelete("/blog?blogID="+id, query);
};

export const takeAccountsConcert = async (id, query) => {
  return methodGet("/blog/accounts-for-concert?blogID="+id, query);
};

  