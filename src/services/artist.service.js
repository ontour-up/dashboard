import { methodGet, methodDelete, methodPatch, methodPostWithout } from "./methods";

export const addArtist = async (query) => {
  return methodPostWithout("/artist", query);
};

export const takesArtists = async (query) => {
    return methodGet("/artist", query);
};

export const takeSearchsArtists = async (search, query) => {
  return methodGet("/artist/searchByName" + search, query);
};

export const takeOneArtist = async (search, query) => {
  return methodGet("/artist" + search, query);
};

export const takeArtistSlug = async (search, query) => {
  return methodGet("/artist/slug" + search, query);
};

export const modifyArtist = async (id, query) => {
  return methodPatch("/artist/"+id, query);
};

export const deleteArtist = async (id, query) => {
  return methodDelete("/artist/"+id, query);
};

export const takeAccountsConcert = async (id, query) => {
  return methodGet("/artist/accounts-for-concert?artistID="+id, query);
};

  