import { methodGet } from "./methods";

export const takeCountElements = async (query) => {
  return methodGet("/statistic/counts-elements", query);
};

export const takeCountFestivals = async (id, query) => {
  return methodGet("/statistic/counts-festivals?userID="+id, query);
};

export const takeCountElementsPromoter = async (id, query) => {
  return methodGet("/statistic/counts-elements?userID="+id, query);
};