import { methodGet, methodDelete, methodPatch, methodPost } from "./methods";

export const addNotification = async (query) => {
  return methodPost("/notification/createNotificationJazzaldi", query);
};

export const addNotificationOlas = async (query) => {
  return methodPost("/notification/createNotificationOlas", query);
};

export const modifyNotification = async (id, query) => {
  return methodPatch("/notification", query);
};

export const deleteNotification = async (id, query) => {
  return methodDelete("/notification?notificationID="+id, query);
};

export const takesNotifications = async (search, query) => {
    return methodGet("/notification/search" + search, query);
};
