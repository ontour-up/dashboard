import { methodGet } from "./methods";

export const getReportsTicket = (ticket) => {
    return methodGet("/api/v1/entrance/"+ticket+"/reservas-info")
}

export const getSalesByPromoter = (promoter) => {
    return methodGet("report/metricsByPromoter/"+promoter)
}

export const getSalesByOrganizer = (organizer) => {
    return methodGet("report/metricsByOrganizer/"+organizer)
}