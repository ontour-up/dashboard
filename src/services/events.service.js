import { methodGet, methodPatch, methodPostWithout } from "./methods";

// export const takeEvents = async (promotor) => {
// 	return methodGet("/event/all?promotor=" + promotor);
//   };

export const takeEventsThumbnail = async (query) => {
    //return methodGet("/event/thumbnail-info?isWebVisible=true&tags=BAD")
    return methodGet('/event/thumbnail-info', query);
};

export const takeEventsDashboard = async (param) => {
    //return methodGet("/event/thumbnail-info?isWebVisible=true&tags=BAD")
    return methodGet('/event/dashboard-info/'+ param);
};

export const promoterEvents = async (query) => {
    return methodGet('/event',query);
};

export const organizerEvents = async (organizer) => {
    return methodGet('/event?organizer=' + organizer + "&isActive=true");
};

export const takeEventsCycle = async (promoter) => {
    return methodGet('/event?tags=ciclo&promoter=' + promoter + "&isActive=true");
};

export const takeEvent = async (id) => {
    return methodGet("/event/" + id);
}; 

// export const takeSeatInfo = async (seat) => {
//     return methodGet("/entrance/seat?seatId=" + seat);
// };

// export const takeEntrancesByTicket = async (ticketId) => {
// 	console.log("ID", ticketId);
// 	return methodGet("entrance/ticket?ticketId=" + ticketId);
// }

export const takeTicketsInfo = async (eventId) => {
    return methodGet("ticket/getTickets?eventID=" + eventId);
}

export const takeBuyersInfo = async (ticketId) => {
    return methodGet("entrance/" + ticketId + "/buyers-info");
}

export const takeEmailList = async (promoter) => {
    return methodGet("/email?promoter=" + promoter);
}

export const addEvent = async (event) => {
  return methodPostWithout("/event", event);
}

export const modifyEvent = async (id, query) => {
  return methodPatch("/event/" +id, query);
};
export const takeAllAccessIn = async (eventId) => {
    return methodGet("https://eventos.ontourup.com/api/dashboard/reports/allAccessIn?event=" + eventId)
}
