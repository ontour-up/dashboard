import { methodGet } from "./methods";

export const getSalesByEvent = (event, from, to) => {
    return methodGet("/sale/salesByEvent/"+event+"?from="+from+"&to="+to)
}

export const getSalesByPoint = (point, from, to) => {
    return methodGet("/sale/salesByPoint/"+point+"?from="+from+"&to="+to)
}

export const getSalesByEmployer = (point, employer , from, to) => {
    return methodGet("/sale/salesByEmployer/"+point+"/"+employer+"?from="+from+"&to="+to)
}