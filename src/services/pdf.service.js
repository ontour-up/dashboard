import { methodGet, methodPostWithout, methodPatch } from "./methods";

  export const takePdfData = async (event, transaction) => {
    return methodGet("/pdf/generate?event="+event+"&transaction="+transaction);
  };
