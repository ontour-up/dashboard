import { methodGet } from "./methods";

export const getEmployers = (promoter) => {
    return methodGet("/account/list?promoter="+promoter)
}