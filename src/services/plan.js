import { methodGet, methodPostWithout, methodPatch } from "./methods";

export const takeplanInfoByPoint = async (id) => {
  return methodGet("/plan?point="+id);
}

  export const addPlan = async (body) => {
    return methodPostWithout("/plan",body);
  }

  export const modifyPlan = async (id, body) => {
    return methodPatch("/plan/" +id, body);
  };
