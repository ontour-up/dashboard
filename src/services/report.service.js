import { methodGet } from "./methods";

export const getReportsTicket = (ticket) => {
    return methodGet("entrance/"+ticket+"/reservas-info")
}