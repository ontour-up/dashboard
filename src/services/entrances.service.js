import { methodGet } from "./methods";
import { methodPatch } from "./methods";

export const getEntrances = async (transactionID) => {
    const query = {
        transactionID : transactionID
    }
    return methodGet("/entrance/getEntrancesTransaction",query);
}

export const getEntrancesByTicket = async (ticketId) => {
    const query = {
        ticketId : ticketId
    }
    return methodGet("/entrance/ticket",query);
}

export const getReservasEntrances = async (ticketID) => {
    return methodGet("/entrance/"+ticketID+"/buyers-info");
}

export const getReservasCount = async (ticketID) => {
    return methodGet("/entrance/"+ticketID+"/buyers-count");
}

export const changeSeat = async (eventID, query) => {
    return methodPatch("/entrance/"+eventID+"/changeseat", query);
}

export const cancelEntrance = async (entranceID, query) => {
    return methodPatch("/entrance/"+entranceID+"/cancelEntrance", query);
}