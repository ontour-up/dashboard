const { SeatsioClient, Region } = require('seatsio');

let client = new SeatsioClient(Region.EU(), 'e547f533-6c15-42e8-a30a-cd52886bae74');

export const getChart = async (chartKey) => {
    return await client.charts.retrievePublishedVersion(chartKey);
  };