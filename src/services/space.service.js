import { methodGet } from "./methods";


export const takeSpaces = async (query) => {
    return methodGet("/space", query);
};
