import { methodFormData, methodGet, methodDelete, methodPatch } from "./methods";

export const uploadFile = async (query) => {
  return methodFormData("/file/upload", query);
};
