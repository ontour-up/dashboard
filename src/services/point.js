import { methodGet, methodPostWithout, methodPatch } from "./methods";

export const takePointsInfo = async (eventID) => {
    return methodGet("/point?event="+eventID);
  }
  export const addPoint = async (body) => {
    return methodPostWithout("/point",body);
  }

  export const modifyPoint = async (id, body) => {
    return methodPatch("/point/" +id, body);
  };
