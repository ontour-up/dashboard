import { methodPost, methodPatch } from "./methods";

export const authApi = async (query) => {
  return methodPost("/auth/loginDashboard", query);
};

export const changePassword = async (query) => {
  return methodPost("/auth/changePassword", query);
};

export const modifyUser = async (id, query) => {
  return methodPatch("/account?accountID=" + id, query);
};