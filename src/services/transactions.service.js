import { methodGet, methodPost } from "./methods";
import { methodPatch } from "./methods";

export const takeTransaction = async (transactionID) => {
    return methodGet("/transaction/"+transactionID);
}

export const takeTransactionsInfo = async (eventID) => {
    return methodGet("/report/transactions/"+eventID);
}

export const takeTransactionsInfoFiltered = async (eventID) => {
    return methodGet("/report/transactions/"+eventID+"/filtered");
}

export const takeTicketEventMetrics = async (eventID) => {
    return methodGet("/report/metrics/"+eventID);
}

export const changeBook = async (transactionID, query) => {
    return methodPatch("/transaction/changeBook?transactionID="+transactionID, query);
}

export const sendInvitation = async (query) => {
    return methodPost("/transaction/sendInvitation", query);
}