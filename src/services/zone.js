import { methodGet, methodPostWithout, methodPatch } from "./methods";

export const takeZoneInfo = async (id) => {
  return methodGet("/zone?_id="+id);
}

export const takeZonesInfoByEvent = async (eventID) => {
    return methodGet("/zone?event="+eventID);
  }

  export const addZone = async (body) => {
    return methodPostWithout("/zone",body);
  }

  export const modifyZone = async (id, body) => {
    return methodPatch("/zone/" +id, body);
  };
