import { methodPost, methodGet, methodDelete, methodPatch } from "./methods";

export const addPromoter = async (query) => {
  return methodPost("/account/registerdashboard", query);
};

export const takePromoters = async (search = '', query) => {
  return methodGet("/account/list" + search, query);
};

export const modifyPromoter = async (id, query) => {
  return methodPatch("/account?accountID="+id, query);
};