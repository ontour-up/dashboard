import React, { useState } from 'react';
import { Card, CardHeader, CardContent, IconButton, Grid, CardMedia, Chip, Menu, MenuItem } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import CheckIcon from '@material-ui/icons/Check';
import ErrorIcon from '@material-ui/icons/Error';

const useStyles = makeStyles({
    media: {
        height: 140,
        backgroundColor: 'gray'
    },
    root: {
        width: '100%'
    },
    leftM: {
        marginLeft: '10px'
    },
});

const SponsorCard = ({ sponsor, xs, md, onEdit, onDelete, onChangeState }) => {
    const classes = useStyles();

    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleEdit = () => {
        setAnchorEl(null);
        onEdit(sponsor);
    };


    const returnSubheader = (title, subtitle) => {
        let text = title;
        if (subtitle) {
            text = text + ' - ' + 'Posición #' + subtitle
        }
        return text;
    };

    const HandleDelete = () => {
        setAnchorEl(null);
        onDelete(sponsor._id);
    };

    const HandleDisabled = () => {
        setAnchorEl(null);
        onChangeState(sponsor._id, false);
    };

    const HandleActivated = () => {
        setAnchorEl(null);
        onChangeState(sponsor._id, true);
    };

    return (
        <Grid item xs={xs} md={md}>
            <Card className={classes.root}>
                <CardHeader
                    action={
                        <>
                            <IconButton aria-label="more" aria-controls="long-menu" aria-haspopup="true" onClick={handleClick}>
                                <MoreVertIcon />
                            </IconButton>
                            <Menu
                                id="long-menu"
                                anchorEl={anchorEl}
                                keepMounted
                                open={open}
                                onClose={handleClose}
                                PaperProps={{
                                    style: {
                                        maxHeight: 130,
                                        width: '20ch',
                                    },
                                }}
                            >
                                <MenuItem onClick={handleEdit}> Editar</MenuItem>
                                <MenuItem onClick={HandleDelete}> Eliminar</MenuItem>
                                {sponsor.isActive && <MenuItem onClick={HandleDisabled}> Desactivar</MenuItem>}
                                {!sponsor.isActive && <MenuItem onClick={HandleActivated}> Activar</MenuItem>}
                            </Menu>
                        </>
                    }
                    title={sponsor.name}
                    subheader={returnSubheader(sponsor.festival?.name, sponsor.position)}
                />
                {sponsor.banner && (
                    <CardMedia
                        className={classes.media}
                        component="img"
                        image={sponsor.banner}
                        title={sponsor.name}
                    />
                )}
                <CardContent>
                    <Chip label={sponsor.category} variant="outlined" />
                    {sponsor.isActive && <Chip className={classes.leftM} label="Activado" clickable color="primary" deleteIcon={<CheckIcon />} variant="outlined" />}
                    {!sponsor.isActive && <Chip className={classes.leftM} label="Desactivado" clickable color="secundary" deleteIcon={<ErrorIcon />} variant="outlined" />}
                </CardContent>
            </Card>
        </Grid>
    );
};

export default SponsorCard;
