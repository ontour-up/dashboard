import React, {useState} from 'react';
import {Card, CardHeader, CardContent, IconButton, Grid, CardMedia, Menu, MenuItem, Chip} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { format } from 'date-fns';
import { es } from 'date-fns/locale';
import CheckIcon from '@material-ui/icons/Check';
import ErrorIcon from '@material-ui/icons/Error';

const useStyles = makeStyles({
    media: {
      height: 140,
    },
    root: {
        width: '100%'
    },
    leftM: {
        marginLeft: '10px'
    }
});

const FestivalCard = ({festival, xs, md, onEdit, onDelete, onChangeState}) => {
    const classes = useStyles();

    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleEdit = () => {
        setAnchorEl(null);
        onEdit(festival);
    };

    const HandleDelete = () => {
        setAnchorEl(null);
        onDelete(festival._id);
    };

    const HandleDisabled = () => {
        setAnchorEl(null);
        onChangeState(festival._id, false);
    };

    const HandleActivated = () => {
        setAnchorEl(null);
        onChangeState(festival._id, true);
    };

    return (
        <Grid item xs={xs} md={md}>
            <Card className={classes.root}>
                <CardHeader
                    action={
                        <>
                            <IconButton aria-label="more" aria-controls="long-menu" aria-haspopup="true" onClick={handleClick}>
                                <MoreVertIcon />
                            </IconButton>
                            <Menu
                                id="long-menu"
                                anchorEl={anchorEl}
                                keepMounted
                                open={open}
                                onClose={handleClose}
                                PaperProps={{
                                style: {
                                    maxHeight: 130,
                                    width: '20ch',
                                },
                                }}
                            >
                                <MenuItem onClick={handleEdit}> Editar</MenuItem>
                                <MenuItem onClick={HandleDelete}> Eliminar</MenuItem>
                                {festival.isActive && <MenuItem onClick={HandleDisabled}> Desactivar</MenuItem>}
                                {!festival.isActive &&<MenuItem onClick={HandleActivated}> Activar</MenuItem>}
                            </Menu>
                        </>
                    }
                    title={festival.name}
                    subheader={format(new Date(festival.created), 'd MMMM yyyy', { locale: es })}
                />
                <CardMedia
                    className={classes.media}
                    image={festival.banner}
                    title={festival.name}
                />
                <CardContent>
                    {festival.description}
                    {festival.isActive && <Chip className={classes.leftM} label="Activado" clickable color="primary" deleteIcon={<CheckIcon />} variant="outlined"/>}
                    {!festival.isActive && <Chip className={classes.leftM} label="Desactivado" clickable color="secundary" deleteIcon={<ErrorIcon />} variant="outlined"/>}
                </CardContent>
            </Card>
        </Grid>
    );
};

export default FestivalCard;
