import React, {useState} from 'react';
import {Card, CardHeader, CardContent, IconButton, Grid, Typography, Menu, MenuItem} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { format } from 'date-fns';
import { es } from 'date-fns/locale';

const useStyles = makeStyles({
    media: {
      height: 140,
    },
    root: {
        width: '100%'
    },
    leftM: {
        marginLeft: '10px'
    }
});

const PromoterCard = ({promoter, xs, md, onEdit}) => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleEdit = () => {
        onEdit(promoter);
    };

    return (
        <Grid item xs={xs} md={md}>
            <Card className={classes.root}>
                <CardHeader

                    action={
                        <>
                            <IconButton aria-label="more" aria-controls="long-menu" aria-haspopup="true" onClick={handleClick}>
                                <MoreVertIcon />
                            </IconButton>
                            <Menu
                                id="long-menu"
                                anchorEl={anchorEl}
                                keepMounted
                                open={open}
                                onClose={handleClose}
                                PaperProps={{
                                style: {
                                    maxHeight: 100,
                                    width: '20ch',
                                },
                                }}
                            >
                                <MenuItem onClick={handleEdit}> Editar</MenuItem>
                                <MenuItem onClick={handleClose}> Eliminar</MenuItem>
                            </Menu>
                        </>
                    }
                    title={promoter.names + ' ' + promoter.surnames}
                    subheader={format(new Date(promoter.created), 'd MMMM yyyy', { locale: es })}
                />
                <CardContent>
                    <Typography variant="body2" color="textSecondary" >
                        {promoter.email}
                    </Typography>
                </CardContent>
            </Card>
        </Grid>
    );
};

export default PromoterCard;
