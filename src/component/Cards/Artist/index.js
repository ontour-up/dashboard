import React, {useState} from 'react';
import {Card, CardHeader, CardContent, IconButton, Grid, CardMedia, Chip, Menu, MenuItem } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const useStyles = makeStyles({
    media: {
      height: 280,
    },
    root: {
        width: '100%'
    },
    leftM: {
        marginLeft: '10px'
    },
    title:{
        '& p': {
            marginTop: '5px',
            marginBottom: '5px'
        },
    }
});

const ArtistCard = ({artist, xs, md, onEdit, onDelete}) => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleEdit = () => {
        setAnchorEl(null);
        onEdit(artist);
    };

    const HandleDelete = () => {
        setAnchorEl(null);
        onDelete(artist._id);
    };

    const titleHtml  = (title) => {
        return (<div className={classes.title} dangerouslySetInnerHTML={{ __html: title}}></div>)
    }

    return (
        <Grid item xs={xs} md={md}>
            <Card className={classes.root}>
                <CardHeader
                    action={
                        <>
                            <IconButton aria-label="more" aria-controls="long-menu" aria-haspopup="true" onClick={handleClick}>
                                <MoreVertIcon />
                            </IconButton>
                            <Menu
                                id="long-menu"
                                anchorEl={anchorEl}
                                keepMounted
                                open={open}
                                onClose={handleClose}
                                PaperProps={{
                                style: {
                                    maxHeight: 130,
                                    width: '20ch',
                                },
                                }}
                            >
                                <MenuItem onClick={handleEdit}> Editar</MenuItem>
                                <MenuItem onClick={HandleDelete}> Eliminar</MenuItem>
                            </Menu>
                        </>
                    }
                    title={titleHtml(artist.name)}
                    // subheader={format(new Date(blog.startDate ? blog.startDate.toString().substring(0, 16) : blog.created), 'd MMMM yyyy', { locale: es })}
                />
                <CardMedia
                    className={classes.media}
                    image={artist.image}
                    title={artist.name}
                />
                <CardContent>
                    {/*<Chip label={blog.category === 'notice-olas' ? 'Noticia' : blog.category} variant="outlined" />*/}
                    {artist.country && <Chip className={classes.leftM} label={artist.country} variant="outlined" />}
                    {/*{blog.isActive && <Chip className={classes.leftM} label="Activado" clickable color="primary" deleteIcon={<CheckIcon />} variant="outlined"/>}*/}
                    {/*{!blog.isActive && <Chip className={classes.leftM} label="Desactivado" clickable color="secundary" deleteIcon={<ErrorIcon />} variant="outlined"/>}*/}
                </CardContent>
            </Card>
        </Grid>
    );
};

export default ArtistCard;
