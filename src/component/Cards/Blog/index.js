import React, {useState} from 'react';
import {Card, CardHeader, CardContent, IconButton, Grid, CardMedia, Chip, Menu, MenuItem } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { format } from 'date-fns';
import { es } from 'date-fns/locale';
import CheckIcon from '@material-ui/icons/Check';
import ErrorIcon from '@material-ui/icons/Error';

const useStyles = makeStyles({
    media: {
      height: 140,
    },
    root: {
        width: '100%'
    },
    leftM: {
        marginLeft: '10px'
    },
    title:{
        '& p': {
            marginTop: '5px',
            marginBottom: '5px'
        },
    }
});

const BlogCard = ({blog, xs, md, onEdit, onDelete, onChangeState}) => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleEdit = () => {
        setAnchorEl(null);
        onEdit(blog);
    };

    const HandleDelete = () => {
        setAnchorEl(null);
        onDelete(blog._id);
    };
    
    const HandleDisabled = () => {
        setAnchorEl(null);
        onChangeState(blog._id, false);
    };

    const HandleActivated = () => {
        setAnchorEl(null);
        onChangeState(blog._id, true);
    };

    const titleHtml  = (title) => {
        return (<div className={classes.title} dangerouslySetInnerHTML={{ __html: title}}></div>)
    }

    return (
        <Grid item xs={xs} md={md}>
            <Card className={classes.root}>
                <CardHeader
                    action={
                        <>
                            <IconButton aria-label="more" aria-controls="long-menu" aria-haspopup="true" onClick={handleClick}>
                                <MoreVertIcon />
                            </IconButton>
                            <Menu
                                id="long-menu"
                                anchorEl={anchorEl}
                                keepMounted
                                open={open}
                                onClose={handleClose}
                                PaperProps={{
                                style: {
                                    maxHeight: 130,
                                    width: '20ch',
                                },
                                }}
                            >
                                <MenuItem onClick={handleEdit}> Editar</MenuItem>
                                <MenuItem onClick={HandleDelete}> Eliminar</MenuItem>
                                {blog.isActive && <MenuItem onClick={HandleDisabled}> Desactivar</MenuItem>}
                                {!blog.isActive &&<MenuItem onClick={HandleActivated}> Activar</MenuItem>}
                            </Menu>
                        </>
                    }
                    title={titleHtml(blog.name)}
                    subheader={format(new Date(blog.startDate ? blog.startDate.toString().substring(0, 16) : blog.created), 'd MMMM yyyy', { locale: es })}
                />
                <CardMedia
                    className={classes.media}
                    image={blog.banner}
                    title={blog.name}
                />
                <CardContent>
                    <Chip label={blog.category === 'notice-olas' ? 'Noticia' : blog.category} variant="outlined" />
                    {blog.subcategory && <Chip className={classes.leftM} label={blog.subcategory} variant="outlined" />}
                    {blog.isActive && <Chip className={classes.leftM} label="Activado" clickable color="primary" deleteIcon={<CheckIcon />} variant="outlined"/>}
                    {!blog.isActive && <Chip className={classes.leftM} label="Desactivado" clickable color="secundary" deleteIcon={<ErrorIcon />} variant="outlined"/>}
                </CardContent>
            </Card>
        </Grid>
    );
};

export default BlogCard;
