import { Grid } from '@material-ui/core';
import React, {useState, useEffect} from 'react';
import { Modal } from 'react-bootstrap';
import { SeatsioSeatingChart } from "@seatsio/seatsio-react";
import { changeSeat } from '../../services/entrances.service';
import { changeBook, takeTransaction } from '../../services/transactions.service';
import AlertModal from '../AlertModal';

function SeatsModal(props) {
    const [chart, setChart] = useState([]);
    const [data, setData] = useState([]);
    const [alertModalShow, setAlertModalShow] = useState(false);

    const [alertModalData, setAlertModalData] = useState({
        title: '',
        description: ''
    });

    const [ transaction, setTransaction ] = useState([]);
    const [ transactionBook, setTransactionBook ] = useState([]);
    const [ loading, setLoading] = useState(true);

    useEffect(() => {
        getTransaction(props.transaction._id);
        setData(props.selectedRow);        
    }, [ props]);

    useEffect(() => {
        setLoading(false);
    }, [data])

    const getTransaction = async (transactionID) => {
      const { status, data } = await takeTransaction(transactionID);
      if (status === 200) {
          setTransaction(data.data);
          const bookJson = JSON.parse(data.data.book);
          setTransactionBook(bookJson);
        }
    };

    const changeEntrance = async (entranceID, newSeat) => {
      const query = {
        seat: newSeat,
    }

    const { status, data } = await changeSeat(entranceID,query);
      if (status === 200) {
          return true;
      }
      else{
        return false;
      }
    };

    const changeTransaction = async (oldSeat, newSeat) => {
      // Creamos nuevo array de book para quitar la silla antigua.
      const newSeats = [];
      for (let i = 0; i < transactionBook.seats.length; i++) {
        const element = transactionBook.seats[i];
        // Añadimos todas las sillas menos la antigua
        if(element != oldSeat){
          newSeats.push(element);
        }  
      }
      // añadimos al final la nueva silla
      newSeats.push(newSeat);

      // Añadimos los nuevos asientos al objecto book
      transactionBook.seats = newSeats;
      transactionBook.oldSeat = oldSeat;
      transactionBook.newSeat = newSeat;    
      
      const query = {
        book: JSON.stringify(transactionBook),
    }
    
      const { status, data } = await changeBook(transaction._id,query);
        if (status === 200) {
            return true;
        }else{
          return false;
        }
      };

    const saveSeat = async () => {
      setLoading(true);
      // Comprobamos que el usuario a seleccionado un asiento
      if(chart.selectedSeats.length > 0){
        // TO DO oldSeat cambiarlo por el new en Transaction
        const oldSeat = data[3];
        const newSeat = chart.selectedSeats[0];

        // Cambiar seat en Transactions y en SEATS.io
        const transactionReturn = await changeTransaction(oldSeat, newSeat);

        if(transactionReturn){
          // Cambiar seat en la Entrance
          const entranceReturn = await changeEntrance(data[0], newSeat);

          if(entranceReturn){
            // Cerrramos el modal
            props.onHide();
            // Exito
            setAlertModalData({"title": "Exito" ,"description": "El asiento sé ha cambiado con éxito."})
            setAlertModalShow(true);
          }else{
            // Error
            setAlertModalData({"title": "Error" ,"description": "Error al guardar el asiento en Entradas."})
            setAlertModalShow(true);
          }
        }else{
          // Error
          setAlertModalData({"title": "Error" ,"description": "Error al guardar el asiento en Transaction."})
          setAlertModalShow(true);
        }      
      }else{ 
        setAlertModalData({"title": "Error" ,"description": "No hay ningún asiento seleccionado."})
        setAlertModalShow(true);
      }
      setLoading(false);
    }
      

    return (
        <>
        {loading === true ? (
          <>
            </>
        ) : (
            <>
        <Modal {...props} size="lg" centered>
          <Modal.Header closeButton>
            <Modal.Title >Cambiar asiento</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <Grid item lg={12} sm={12} xs={12}>
          <div id="chart-modal"></div>
          <SeatsioSeatingChart
                  id='chart-modal'
                  workspaceKey={
                    "03659072-f55b-493a-ad8a-904854238a5b"
                  }
                  //EventKey
                  event={transactionBook.eventKey}
                  
                  onRenderStarted={(
                    createdChart
                  ) => {
                    setChart(createdChart);
                    //Seats
                    //data[3].length = 0;
                  }}
                  onObjectSelected={(selected) => {
                    
                    //Hold
                    transactionBook.hold =
                      selected.chart.holdToken;
                    //setSeats(data.seats);
                  }}
                  
                  onObjectDeselected={(selected) => {
                    //handleDelete(selected);

                  }}
                  numberOfPlacesToSelect={parseInt(1)}
                
                  session="continue"
                  language={"es"}
                  region="eu"
                />
                </Grid>
          </Modal.Body>
          <Modal.Footer><button type="button" class="btn btn-primary" onClick={saveSeat}>Guardar</button></Modal.Footer>
        </Modal>
        <AlertModal data={alertModalData} show={alertModalShow} onHide={() => setAlertModalShow(false)} />
      </>
        )}
        </>
      );
}

export default SeatsModal;