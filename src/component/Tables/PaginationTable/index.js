import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Table, TableBody, TableCell, TableContainer, Button, TableRow, Paper, TableHead } from '@material-ui/core';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';

const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
  table: {
    marginTop: '20px'
  }
}));

const useStyles2 = makeStyles({
  table: {
    minWidth: 500,
    marginTop: '20px'
  },
});

export default function PaginationTable({ title, columns, rows, onDelete }) {
  const classes = useStyles2();

  const handleOnClick = (id) => {
    onDelete(id);
  };

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label={title}>
        {columns && (
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
        )}
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row._id}>
              {columns.map((column) => (
                <TableCell
                  key={row._id + column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {typeof row[column.id] === 'object' ? row[column.id] ? row[column.id].length : '' : row[column.id]}
                </TableCell>
              ))}
              <TableCell >
                <Button
                  variant="contained"
                  color="secondary"
                  type="button"
                  className={classes.button}
                  onClick={() => handleOnClick(row._id)}
                >
                  <DeleteOutlineIcon />
                </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}