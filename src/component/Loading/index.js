import React from 'react';
import ReactLoading from 'react-loading';
import { Modal } from 'react-bootstrap';

const Loading = (props) => (

  <Modal {...props} centered contentClassName="modal-loading">
    <div id="loading" style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}>
        <ReactLoading type="bars" color="#3366ff" height={'50%'} width={'50%'} />
    </div>
    </Modal>

);

export default Loading;
