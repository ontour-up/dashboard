import React, {useState, useEffect, useContext} from 'react';
import { Row, Modal, Col, Dropdown, Button } from 'react-bootstrap';
import MUIDataTable from 'mui-datatables';
import { getEntrances } from '../../services/entrances.service'
import moment from 'moment';
import 'moment/locale/es';
import QRModal from './../QRModal';
import SeatsModal from './../SeatsModal';
import { BsThreeDotsVertical } from "react-icons/bs";
import Done from "@material-ui/icons/Done";
import Cancel from "@material-ui/icons/Cancel";
import GetAppIcon from '@material-ui/icons/GetApp';
import Tooltip from "@material-ui/core/Tooltip";
import AlertModal from '../AlertModal';
import Loading from '../Loading';

import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { takePdfData } from '../../services/pdf.service';
import { takeTicket } from '../../services/ticket.service';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

function TransactionModal(props) {
  moment.locale('es');

  const [ loading, setLoading] = useState(true);
  const [ data, setData] = useState([]);
  const [ columns, setColumns ] = useState([]);
  const [ transaction, setTransaction ] = useState([]);
  const [ qrKey, setQrKey] = useState([]);
  const [ selectedRow, setSelectedRow] = useState([]);
  const [ qrModalShow, setQrModalShow] = useState(false);
  const [ seatsModalShow, setSeatsModalShow] = useState(false);
  // TODO: El role en taquilla lo coje de TicketBox, en el Dasboard?
  //const { role } = useContext(TicketBoxContext);
  const [alertModalShow, setAlertModalShow] = useState(false);
  const [alertModalData, setAlertModalData] = useState({
      title: '',
      description: ''
  });
  const [ ticketId, setTicketId ] = useState();
  const [ eventId, setEventId ] = useState();

  useEffect(() => {
    setLoading(true);
    if(props.transaction.transactionResponseTPV !== null){
      const json = JSON.parse(props.transaction.transactionResponseTPV);
      setTransaction({...props.transaction,  
        transactionResponseTPV: json.PAYMENT_OPERATION,
      })
    }else{
      setTransaction(props.transaction);
    }
    takeEntrances(props.transaction._id);
  }, [ props.transaction]);

  useEffect(() => {
    takeEntrances(props.transaction._id);
    console.log("transaction",props.transaction)
  }, [ seatsModalShow]);

  useEffect(() => {
    if(ticketId !== undefined && ticketId !== null){
      takeEvent();
    }
  }, [ticketId])

  const getPDF = async () => {
    console.log('getPDF');
    pdfMake.fonts = {
      Roboto: {
        normal: 'Roboto-Regular.ttf',
        bold: 'Roboto-Medium.ttf',
        italics: 'Roboto-Italic.ttf',
        bolditalics: 'Roboto-Italic.ttf',
      },
    };

    let dataPdf;
    console.log("Event",eventId)
    if(eventId !== undefined){
      try {
        dataPdf = await takePdfData(eventId, transaction._id);
      } catch (e) {
        console.log(e);
      }
  
      if (dataPdf.data != undefined) {
        console.log('camino 1');
        const docDefinition = {
          pageSize: 'A4',
          pageMargins: [100, 100, 100, 100],
          header: dataPdf.data.data.header,
  
          footer: dataPdf.data.data.footer,
  
          content: dataPdf.data.data.content,
        };
        pdfMake.createPdf(docDefinition).download('entradas.pdf');
      }
    }
  };

  const takeEntrances = async (transactionID) => {
    const { status, data } = await getEntrances(transactionID);
    if (status === 200) {
        setData(fillRow(data.data));
        setTicketId(data.data[0]?.ticket);
        setLoading(false);
    } else {
        setData([]);
        setLoading(false);
    }
  };

  const takeEvent = async () => {
    const obj = {
      ticketID : ticketId
    }
    const { status, data } = await takeTicket(obj);
    if (status === 200) {
      console.log("TIcketBarrun",data)
      setEventId(data.data.event);
    }else{
      console.log("tixkwet kanpun")
      setEventId();
    }
  }

  const fillRow = (entrances) => {
    let rows;
    let haveLocation = false;
    for (const entrance of entrances) {
      if(entrance.seat != null){
        haveLocation = true;
        break;
      }
    }

    switch (props.category) {
      /*
      TODO: Mirar si se quieren modificar las columnas de las entradas
      case 'Espectáculos':
          const fillRowsEspectaculo = (entrances) => {
              let rowArr = [];
              entrances.forEach((ent, id) => {
                  const row = [
                    ent.persona_contacto + ' ' + ent.apelliod_persona_contacto,
                    ent.correo_contacto,
                    ent.telefono_contacto,
                    ent.qr,
                  ];
                  rowArr.push(row);
              });
              return rowArr;
          };

          if (entrances.length) {
            setColumns(['Nombre', 'Correo', 'Teléfono', 'Qr']);
              rows = fillRowsEspectaculo(entrances);
          }
          break;
      case 'Centro Cultural Santa Clara':
          const fillRowsSantaCLara = (entrances) => {
              let rowArr = [];
              entrances.forEach((ent, id) => {
                  const row = [
                    ent.nombre + ' ' + ent.apellido,
                    ent.edad,
                    ent.persona_contacto + ' ' + ent.apelliod_persona_contacto,
                    ent.correo_contacto,
                    ent.telefono_contacto,
                    ent.relacion,
                    ent.qr,
                  ];
                  rowArr.push(row);
              });
              return rowArr;
          };

          if (entrances.length) {
              columns = ['Nombre Menor', 'Edad Menor', 'Nombre Contacto', 'Correo Contacto', 'Telefono Contacto', 'Parentesco', 'Qr'];
              rows = fillRowsSantaCLara(entrances);
          }
          break;
      case 'Biblioteca Municipal':
          const fillRowsBiblioteca = (entrances) => {
              let rowArr = [];
              entrances.forEach((ent, id) => {
                  const row = [
                    ent.nombre + ' ' + ent.apellido,
                    ent.edad,
                    ent.persona_contacto + ' ' + ent.apelliod_persona_contacto,
                    ent.correo_contacto,
                    ent.telefono_contacto,
                    ent.relacion,
                    ent.qr,
                  ];
                  rowArr.push(row);
              });
              return rowArr;
          };

          if (entrances.length) {
              columns = ['Nombre Menor', 'Edad Menor', 'Nombre Contacto', 'Correo Contacto', 'Telefono Contacto', 'Parentesco', 'Qr'];
              rows = fillRowsBiblioteca(entrances);
          }
          break;
      case 'Museo de la Industria Rialia':
          const fillRowsMuseo = (entrances) => {
              let rowArr = [];
              entrances.forEach((ent, id) => {
                  const row = [
                    ent.persona_contacto + ' ' + ent.apelliod_persona_contacto,
                    ent.correo_contacto,
                    ent.telefono_contacto,
                    ent.relacion,
                    ent.nombre + ' ' + ent.apellido,
                    ent.edad,
                    ent.qr,
                  ];
                  rowArr.push(row);
              });
              return rowArr;
          };

          if (entrances.length) {
              columns = ['Nombre Contacto', 'Correo Contacto', 'Telefono Contacto', 'Parentesco', 'Nombre Menor', 'Edad Menor', 'Qr'];
              rows = fillRowsMuseo(entrances);
          }
          break;

          */

      default:
        const fillRowDefault = (entrances) => {
          let rowArr = [];
         
          entrances.forEach((ent, id) => {
            const row = [
                ent._id,
                id+1,
                ent.isActive,
                ent.seat,
                ent.qr
            ];
            rowArr.push(row);
          });
          return rowArr;
        };

        if (entrances.length) {
          setColumns([ 
            {
              name: "id",
              options: {
                display: false,
              }
            },
            {
              name: "Entrada",
              options: {
                display: true,
              }
            },
            {
              name: "Activo",
              options: {
                customBodyRender: (value, tableMeta, updateValue) => {
                  if (value == true)
                    return (
                      <Tooltip title="Activo">
                        <Done color="primary" />
                      </Tooltip>
                    );
                  else
                    return (
                      <Tooltip title="Inactivo">
                        <Cancel color="error" />
                      </Tooltip>
                    );
                }
              }
            },
          {
            name: "Localidad",
            options: {
              // Calculate haveLoacation and  bchart show Localidad
              display: props.ischart && haveLocation,
            }
          },
          {
            name: "QR",
            options: {
              display: true,
            }
          },
          {
            name: "Acciones",
            options: {
              filter: true,
              sort: false,
              empty: true,
              customBodyRender: (value, tableMeta, updateValue) => {
                return (
                  <Dropdown drop="start">
                    <Dropdown.Toggle as={CustomToggle}  id="dropdown-basic">
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                    {tableMeta.rowData[2] === true ? (
                      <Dropdown.Item 
                          onClick={() =>
                              // Obtenemos el id QR
                              viewQrClick(tableMeta.rowData[4])
                            }>
                          Ver QR</Dropdown.Item>
                        ) : (
                        <></>
                        )}
          
                      {/*role.hasEdit === true ? ( }
                      { tableMeta.rowData[2] === true && props.ischart ? (
                            <Dropdown.Item 
                              onClick={() =>
                                // Editamos el Seat
                                editSeat(tableMeta)
                              }>
                              Editar localidad
                              </Dropdown.Item>
                              ) : (
                            <></>
                            )}
                          {/*) : (
                          <></>
                          )}*/}
          
                      {/*role.hasCancel === true ? ( }
                            <Dropdown.Item onClick={() =>
                              // Cancelamos entrada
                              cancelEntranceClick(tableMeta)
                            }>
                              Cancelar entrada
                              </Dropdown.Item>
                          {/*}) : (
                          <></>
                          )}*/}
          
                    </Dropdown.Menu>
                  </Dropdown>
                        );
                      }
                    }
                  }
          ]);
          rows = fillRowDefault(entrances);
      }
      break;
    }
    return rows;
  }

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <a
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      <BsThreeDotsVertical />
    </a>
  ));

  const viewQrClick = (value) => {
   setQrKey(value);
   setQrModalShow(true);  
  }

/*
  const editSeat = (value) => {
    setSelectedRow(value.rowData);
    setSeatsModalShow(true);  
  }
 

  const cancelEntranceClick = async (value) => {
    const query = {
      isActive: false,
    }
    
    const { status, data } = await cancelEntrance(value.rowData[0],query);
    if (status === 200) {
        takeEntrances(data.data.transaction);
        // Exito
        setAlertModalData({"title": "Exito" ,"description": "El asiento se ha cancelado con éxito."})
        setAlertModalShow(true);
    }
  }
 */

const options = {
  selectableRows: "none",
  search : false,
  download: false,
  print: false,
  filter: false,
  viewColumns: false,
  customToolbar: () => {
    return (
      <Button
        variant="contained"
        color="secondary"
        style={{ marginLeft: 20 }}
        onClick={() => {
          getPDF();
        }}
      ><GetAppIcon />
        Descargar PDF
      </Button>
    );
  },
  textLabels: {
    body: {
        noMatch: 'No se han encontrado registros',
        toolTip: 'Ordenar',
        columnHeaderTooltip: (column) => `Ordenar por ${column.label}`,
    },
    pagination: {
        next: 'Siguiente página',
        previous: 'Página anterior',
        rowsPerPage: 'Filas por página:',
        displayRows: 'de',
    },
    toolbar: {
        search: 'Buscar',
        downloadCsv: 'Descargar CSV',
        print: 'Imprimir',
        viewColumns: 'Mostrar columnas',
        filterTable: 'Filtrar tabla',
    },
    filter: {
        all: 'Todo',
        title: 'Filtros',
        reset: 'Reiniciar',
    },
    viewColumns: {
        title: 'Mostrar columnas',
        titleAria: 'Mostrar/Ocultar columnas de la tabla',
    },
    selectedRows: {
        text: 'fila(s) seleccionada(s)',
        delete: 'Borrar',
        deleteAria: 'Borrar las filas seleccionadas',
    },
  },
};
  
    return (
        <>
        {loading === true ? (
          <>
            <Loading show={loading} />
            </>
        ) : (
            <>
        <Modal {...props} size="xl" centered backdropClassName="modalBackDrop">
          <Modal.Header closeButton>
            <Modal.Title >Compra #{transaction._id}</Modal.Title>
          </Modal.Header>
          <Modal.Body>      
            <Row className="pb-4">
              <Col lg={6}>
              <h4>Cuenta</h4>
                <div className="p-3">
                  <Row>
                    <Col>
                    <p>Nombre:</p>
                    <p>Apellidos:</p>
                    <p>Telefono:</p>
                    <p>Email:</p>
                    </Col>
                    <Col xs={6} lg={8}>
                      <p>{transaction.accountName}</p>
                      <p>{transaction.accountSurnames ? transaction.accountSurnames : '-'}</p>
                      <p>{transaction.accountPhone}</p>
                      <p>{transaction.accountEmail}</p>
                      </Col>
                    </Row>
                  </div>
              </Col>
              <Col lg={6}>
              <h4>Detalles compra</h4>
                <div className="p-3">
                <Row>
                    <Col>
                      <p>Creado:</p>
                      <p>Metodo de pago:</p>
                      <p>Detail:</p>
                      <p>Total:</p>
                      <p>{transaction.transactionResponseTPV ? 'ID Pasarela:' : ''}</p>
                    </Col>
                    <Col xs={6}>
                      <p>{moment(transaction.transactionCreated).format('L LT')}</p>
                      <p>{transaction.transactionPaymentMethod ? transaction.transactionPaymentMethod : props.detail === "Online" ? 'Online' : 'null'}</p>
                      <p>{props.detail}</p>
                      <p>{transaction.transactionTotal}</p>
                      <p>{transaction.transactionResponseTPV ? transaction.transactionResponseTPV : ''}</p>
                      </Col>
                    </Row>
                </div>
              </Col>
            </Row>
            <Row>
              <h4>Entradas</h4>
              <div className="p-3">
                  <MUIDataTable
                    title={props.detail}
                      data={data} 
                      columns={columns}
                      options={options}
                  />
              </div>
            </Row>

          </Modal.Body>
        </Modal>
        <QRModal qrKey={qrKey} show={qrModalShow} onHide={() => setQrModalShow(false)} />
        {selectedRow.length > 0 ? (
          <>
          <SeatsModal selectedRow={selectedRow} transaction={transaction} show={seatsModalShow} onHide={() => setSeatsModalShow(false)} />
          </>
        ) : (
          <></>
        )}
        <AlertModal data={alertModalData} show={alertModalShow} onHide={() => setAlertModalShow(false)} />
          </>
        )}
      </>
      );

}

export default TransactionModal;