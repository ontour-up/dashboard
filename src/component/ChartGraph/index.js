import { CartesianGrid, Legend, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis } from "recharts";
import React, { useState } from "react";

const BarGraph = ({
  title,
  dataKey,
  oxLabel,
  oyLabel,
  values,
  yLimit,
  labels,
  type
}) => {
  const [barProps, setBarProps] = useState(
    labels.reduce(
      (a, { key }) => {
        a[key] = false;
        return a;
      },
      { hover: null }
    )
  );

  /* const intervals = () => {
    let array = Array(12)
    let hours = 3
    let interval_count = 0
   if(new Date().getMinutes() < 15){
    interval_count = 0
   }else if(new Date().getMinutes() < 30){

   }

   if()
    

    for(let i=0; i<array.length; i++){
      let time = new Date().setHours(new Date().getHours() - hours)
      let interval = interval_count
      interval_count--
      
      if(interval_count === -1) {
        hours--
        interval_count = 3
      }

      array[i] = {time: time, interval: interval}
    }
    

    console.log(array)
  } */



  const handleLegendMouseEnter = (e) => {
    if (!barProps[e.dataKey]) {
      setBarProps({ ...barProps, hover: e.dataKey });
    }
  };

  const handleLegendMouseLeave = (e) => {
    setBarProps({ ...barProps, hover: null });
  };

  const selectBar = (e) => {
    setBarProps({
      ...barProps,
      [e.dataKey]: !barProps[e.dataKey],
      hover: null
    });
  };

  return (
    <div>
      <h4>{title}</h4>
      <LineChart width={730} height={550} 
        margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
        <CartesianGrid strokeDasharray="3 3" />

        <YAxis type="number" domain={yLimit}>

        </YAxis>
        <Tooltip />
        <Legend
          onClick={selectBar}
          onMouseOver={handleLegendMouseEnter}
          onMouseOut={handleLegendMouseLeave}
          layout="horizontal"
        />
        {type === "total" ? <><Line type="monotone" dataKey="totalAmount" data={values}></Line><XAxis dataKey="time" /></> : <> {labels.map((label, index) => {
          return <><Line
          type="monotone"
            key={index}
            name={label.key}
            data={values[index]?.data}
            dataKey={label.key}
            stroke={label.color}
            hide={barProps[label.key] === true}
          /><XAxis dataKey="time" /></>
        })}</>}

      </LineChart>
    </div>
  );
};

export default BarGraph;
