import React from 'react';
import { Modal } from 'react-bootstrap';
import QRCode from "react-qr-code";

function QRModal(props) {

    return (
        <>
        <Modal {...props} centered >
          <Modal.Header closeButton>
            <Modal.Title >QR</Modal.Title>
          </Modal.Header>
          <Modal.Body className="text-center">
            <QRCode value={props.qrKey} />
          </Modal.Body>
        </Modal>
      </>
      );
}

export default QRModal;