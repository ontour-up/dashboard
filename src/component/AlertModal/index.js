import React, {useState, useEffect} from 'react';
import { Modal } from 'react-bootstrap';
import { BsFillExclamationCircleFill, BsFillCheckCircleFill } from "react-icons/bs";

function ModalBox(props) {

  const [icon, setIcon] = useState();

  useEffect(() => {
    if(props.data.title === "Error"){
      setIcon(<BsFillExclamationCircleFill className="mb-2" />);
    }else if(props.data.title === "Exito"){
      setIcon(<BsFillCheckCircleFill className="mb-2" />)
    }
    
}, [ props]);

    return (
        <>
 
        <Modal {...props} centered >
          <Modal.Header closeButton>
            <Modal.Title>{icon} {props.data.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <pre id="alertPre">{props.data.description}</pre>
          </Modal.Body>
        </Modal>
      </>

      );

}

export default ModalBox;