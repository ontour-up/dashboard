import React, {useState, useRef} from 'react';
import JoditEditor from "jodit-react";

const EditorJodit = ({}) => {
    const editor = useRef(null)
    const [content, setContent] = useState('')

    const config = {
        enableDragAndDropFileToEditor: true, // all options from https://xdsoft.net/jodit/doc/
        uploader: {
            format: 'json',
            url:"http://localhost:3000/api/dashboard/upload?file=",
        },
        filebrowser: {
            ajax: {
              url: "https://s3.eu-central-1.amazonaws.com/ontour-up",
            },
            uploader: {
               url:"http://localhost:3000/api/dashboard/upload?file="
            },
        },
    }

    return (
        <JoditEditor
                ref={editor}
                value={content}
                config={config}
                tabIndex={1} // tabIndex of textarea
                onBlur={newContent => setContent(newContent)} // preferred to use only this option to update the content for performance reasons
                onChange={newContent => {}}
        />
    );
};

export default EditorJodit;
