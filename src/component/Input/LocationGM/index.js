import React from 'react';
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';

const LocationGM = (props) => {

    return (
        <>
            <GooglePlacesAutocomplete
                selectProps={{
                value: props.value,
                onChange: props.onChange,
                styles: {
                    control: (provided) => ({
                    ...provided,
                    background: '#ECECEC',
                    fontSize: '.8rem',
                    fontFamily: 'Montserrat , sans-serif',
                    border: 'none',
                    }),
                    input: (provided) => ({
                    ...provided,
                    fontFamily: 'Montserrat , sans-serif',
                    }),
                },
                }}
                apiKey="AIzaSyAON7uTmBr-bZ_ehCaxvpToTnHgy7Ken70"
            />
        </>
    );
};

export default LocationGM;
