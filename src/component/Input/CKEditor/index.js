import React, {useState} from 'react';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { API_URL, URL_S3 } from "../../../store/constant";
import { uploadFile } from "../../../services/upload.service";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    editor: {
        height: '300px',
        minHeight: '300px',
    },
}));

const EditorJodit = ({onChange, content, options}) => {
    const classes = useStyles();
    const [article, setArticle] = useState();
    let editorConfiguration = {}
    if (options == 1){
        editorConfiguration = {
            height: '100%',
            toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
        };
    } else {
        editorConfiguration = {
            extraPlugins: [ MyCustomUploadAdapterPlugin ],
            height: '100%'
        };
    }
    

    function MyCustomUploadAdapterPlugin(editor) {
        editor.plugins.get( 'FileRepository' ).createUploadAdapter = (loader) => {
          return new MyUploadAdapter(loader)
        }
    }

    const handleChange = (value) => {
        onChange(value);
    };

    const handleReady = () => {
        setArticle(content);
    };

    return (
        <CKEditor
            className={classes.editor}
            editor={ ClassicEditor }
            config={editorConfiguration}
            data={article}
            onReady={handleReady}
            onChange={(event, editor) => {
                const data = editor.getData();
                handleChange(data);
            }}
        />
    );
};

class MyUploadAdapter {
    constructor(props) {
      this.loader = props;
      this.url = `${API_URL}/file/upload`;
    }

    upload() {
        return new Promise((resolve, reject) => {
            this._sendRequest(resolve, reject);
        } );
    }

    _sendRequest(resolve, reject) {
        const data = new FormData();
        this.loader.file.then(async result => {
                data.append('file', result);
                const { status } = await uploadFile(data);
                resolve({
                    default: URL_S3 + result.name
                });
                
            }
        )        
    }

}

export default EditorJodit;
