import React, { useState, useContext, useEffect } from 'react';
import {
    DialogContent, MenuItem, DialogTitle, Dialog, TextField, Chip, Checkbox, FormControlLabel, Accordion, AccordionDetails,
    Button, Stepper, Step, StepLabel, Typography, Grid, InputLabel, FormControl, Select, Divider, AccordionSummary
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useFormik } from 'formik';
import * as yup from 'yup';
import LocationGM from "../../Input/LocationGM";
import CKEditor from "../../Input/CKEditor";
import { UserContext } from "../../../context/UserContext";
import CircularProgress from '@material-ui/core/CircularProgress';
import { URL_S3 } from "../../../store/constant";
import { addblog, modifyBlog } from "../../../services/blog.service";
import { uploadFile } from "../../../services/upload.service";
import AlertMsg from "../../Alert";
import { takeFestival } from "../../../services/festival.service";
import { takeSpaces } from "../../../services/space.service";
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { DateTimePicker } from "@material-ui/pickers";
import { es } from 'date-fns/locale';
import { format } from 'date-fns';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

// Google Api
import { getLatLng, geocodeByAddress } from 'react-google-places-autocomplete';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    backButton: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    formControl: {
        width: '100%',
    },
    chip: {
        marginTop: '10px',
        marginRight: '5px'
    },
    divBtn: {
        marginTop: '20px',
        marginBottom: '20px',
    },
    buttonProgress: {
        marginLeft: '10px'
    },
    imgBanner: {
        width: '150px',
        height: '130px',
        objectFit: 'contain',
        marginTop: '10px',
        marginBottom: '10px'
    }
}));

function getSteps() {
    return ['Información Básica', 'Banner y Contenido', 'Multi Lenguaje', 'Horario', 'Adicional'];
}

function getStepContent(stepIndex) {
    switch (stepIndex) {
        case 0:
            return 'Ingresa la información básica del evento';
        case 1:
            return 'Ingrese una imagen para el banner y el contenido';
        case 2:
            return 'Ingrese la traducción en el lenguaje que lo necesite';
        case 3:
            return 'Ingrese el horario';
        case 4:
            return 'Ingrese información adicional';
        default:
            return 'Unknown stepIndex';
    }
}

const validationSchema = yup.object({
    name: yup.string().required(),
    datePublication: yup.string().required(),
    /*description: yup.string().required(),
    category: yup.string().required(),*/
});

const Blog = ({ open, onClose, onRefresh, onOpenSnackBar, blog }) => {
    const classes = useStyles();
    const { user } = useContext(UserContext);
    const [activeStep, setActiveStep] = useState(0);
    const [tag, setTag] = useState('');
    const [ubication] = useState({ address: '', city: '', postalCode: '', country: '', gps: { latitude: '', longitude: '' } });
    const [contact] = useState({ name: '', phone: '', cellphone: '', url: '', email: '', note: '', url_en: '', url_eu: '', url_fr: '' });
    const [errorInput, setErrorInput] = useState('');
    const steps = getSteps();
    const [isLoading, setIsLoading] = useState(false);
    const [isOpenAlert, setIsOpenAlert] = useState(false);
    const [messageAlert, setMessageAlert] = useState({});
    const [festivals, setFestivals] = useState([]);
    const [dateStart, SetDateStart] = useState(null);
    const [dateEnd, SetDateEnd] = useState(null);
    const [datePublication, SetDatePublication] = useState(null);
    const [datePublicationAnt, SetDatePublicationAnt] = useState(null);
    const [expanded, setExpanded] = useState('');
    const [titleEn, setTitleEn] = useState('');
    const [descriptionEn, setDescriptionEn] = useState('');
    const [titleEu, setTitleEu] = useState('');
    const [descriptionEu, setDescriptionEu] = useState('');
    const [titleFr, setTitleFr] = useState('');
    const [descriptionFr, setDescriptionFr] = useState('');
    const [latitude, setLatitude] = useState('');
    const [longitude, setLongitude] = useState('');
    const [spaces, setSpaces] = useState([]);

    useEffect(() => {
        resetForm();
        takeFestivals();
        resetElements();
        if (blog?._id !== undefined && blog?._id !== '') {
            loadData();
        }
    }, [open]);

    const resetElements = () => {
        setActiveStep(0);
        SetDateStart(null);
        SetDateEnd(null);
        setExpanded('');
        setTitleEn('');
        setDescriptionEn('');
        setTitleEu('');
        setDescriptionEu('');
        setTitleFr('');
        setDescriptionFr('');
    }

    const handleDateSChange = (e) => {
        SetDateStart(e)
        if (e !== '' && e !== null) {
            formik.setFieldValue('startDate', format(e, "yyy-MM-dd'T'HH:mm"));
        } else {
            formik.setFieldValue('startDate', e);
        }
        
    }

    const handleDateEChange = (e) => {
        SetDateEnd(e)
        if (e !== '' && e !== null) {
            formik.setFieldValue('endDate', format(e, "yyy-MM-dd'T'HH:mm"));
        } else {
            formik.setFieldValue('endDate', e);
        }
    }

    const handleDatePChange = (e) => {
        SetDatePublication(e)
        if (e !== '' && e !== null) {
            formik.setFieldValue('datePublication', format(e, "yyy-MM-dd'T'HH:mm"));
        } else {
            formik.setFieldValue('datePublication', e);
        }
    }

    const takeFestivals = async () => {
        const { status, data } = await takeFestival();
        if (status === 200) {
            setFestivals(data.data);
        }/* else {
            console.log(status, data)
        }*/
    }

    const takeSpacesList = async (festival) => {
        const { status, data } = await takeSpaces('?festival='+festival+'&sortField=name&limitField=100&selectFields=_id,name');
        if (status === 200) {
            setSpaces(data.data);
        } /*else {
            console.log(status, data)
        }*/
    }

    const loadData = () => {
        formik.setFieldValue('name', blog?.name)
        formik.setFieldValue('_id', blog?._id)
        formik.setFieldValue('description', blog?.description)
        formik.setFieldValue('tags', blog?.tags)
        formik.setFieldValue('banner', blog?.banner)
        formik.setFieldValue('contact', blog?.contact ? blog?.contact : contact)
        formik.setFieldValue('category', blog?.category)
        formik.setFieldValue('subcategory', blog?.subcategory)
        formik.setFieldValue('pay', blog?.pay)
        formik.setFieldValue('ubication', blog?.ubication ? blog?.ubication : ubication)
        formik.setFieldValue('startDate', blog?.startDate ? blog?.startDate.toString().substring(0, 16) : '')
        formik.setFieldValue('endDate', blog?.endDate ? blog?.endDate.toString().substring(0, 16) : '')
        formik.setFieldValue('datePublication', blog?.datePublication ? blog?.datePublication.toString().substring(0, 16) : '')
        formik.setFieldValue('festival', blog?.festival)
        formik.setFieldValue('artist', blog?.artist)
        formik.setFieldValue('isActive', blog?.isActive)
        formik.setFieldValue('isLeading', blog?.isLeading)
        formik.setFieldValue('position', blog?.position ? blog?.position : 0)
        formik.setFieldValue('price', blog?.price ? blog?.price : 0.00)
        formik.setFieldValue('space', blog?.space ? blog?.space : null)
        takeSpacesList(blog?.festival);
        SetDatePublicationAnt(blog?.datePublication ? blog?.datePublication.toString().substring(0, 16) : '');
        if (blog?.datePublication) {
            SetDatePublication(format(new Date(blog?.datePublication.toString().substring(0, 16)), "yyy-MM-dd'T'HH:mm"))
        }
        if (blog?.startDate) {
            SetDateStart(format(new Date(blog?.startDate.toString().substring(0, 16)), "yyy-MM-dd'T'HH:mm"))
        }
        if (blog?.endDate) {
            SetDateEnd(format(new Date(blog?.endDate.toString().substring(0, 16)), "yyy-MM-dd'T'HH:mm"))
        }
        if (blog?.name_translated) {
            setTitleEn(blog?.name_translated?.en);
            setTitleEu(blog?.name_translated?.eu);
            setTitleFr(blog?.name_translated?.fr);
        }
        if (blog?.description_translated) {
            setDescriptionEn(blog?.description_translated?.en);
            setDescriptionEu(blog?.description_translated?.eu);
            setDescriptionFr(blog?.description_translated?.fr);
        }
        if (blog?.ubication) {
            setLatitude(blog?.ubication.gps.latitude);
            setLongitude(blog?.ubication.gps.longitude);
        }
    }

    const handleNext = () => {
        if (activeStep === 0) {
            validationFirstStep();
        }
        if (activeStep === 1) {
            validationSecondStep();
        }
        if (activeStep === 2) {
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
        if (activeStep === 3) {
            validationThreeStep();
        }
        if (activeStep === 4) {
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    const handleClose = () => {
        resetForm();
        onClose();
    };

    const handleAddTag = (e) => {
        if (e.key === 'Enter') {
            if (tag.length === 0) return;
            formik.setFieldValue('tags', [...formik.values.tags, tag]);
            setTag('');
        } else {
            setTag(e.target.value);
        }
    };

    const handleDeleteTag = (index) => {
        const filterTags = formik.values.tags.filter((tag, i) => i !== index);
        formik.setFieldValue('tags', filterTags);
        setTag('');
    };

    const changeAddressHandler = (e) => {
        let directList = e.value.terms;
        let direction = e.value.description;
        if (directList.length > 0) {
            cleanUbication();
            let direArray = direction.split(',');
            if (direArray.length > 1) {
                direArray = direArray.splice(0, (direArray.length - 2))
                ubication.address = direArray.toString();

            } else {
                ubication.address = direction;
            }
            geocodeByAddress(direction).then((results) => {
                const direccion = results[0]['address_components'];
                direccion.forEach((item) => {
                    const type = item.types;
                    type.forEach((itep) => {
                        if (itep === 'locality') {
                            ubication.city = item.long_name;
                        }
                        if (itep === 'country') {
                            ubication.country = item.long_name;
                        }
                        if (itep === 'postal_code') {
                            ubication.postalCode = item.postal_code;
                        }
                    });
                });
                getLatLng(results[0]).then(({ lat, lng }) => {
                    ubication.gps.latitude = lat;
                    ubication.gps.longitude = lng;
                    setLatitude(lat);
                    setLongitude(lng);
                });
                formik.setFieldValue('ubication', ubication);
            });
        }
    };

    const cleanUbication = () => {
        ubication.address = '';
        ubication.city = '';
        ubication.country = '';
        ubication.postalCode = '';
        ubication.gps.latitude = '';
        ubication.gps.longitude = '';
    };

    const handleDescription = (value) => {
        formik.setFieldValue('description', value);
    };

    const handleDescriptionEn = (value) => {
        setDescriptionEn(value);
    };

    const handleTitleEn = (e) => {
        setTitleEn(e.target.value);
    };

    const handleTitleFr = (e) => {
        setTitleFr(e.target.value);
    };

    const handleDescriptionFr = (value) => {
        setDescriptionFr(value);
    };

    const handleTitleEu = (e) => {
        setTitleEu(e.target.value);
    };

    const handleChangeLatitude = (e) => {
        setLatitude(e.target.value);
        formik.setFieldValue('ubication.gps.latitude', e.target.value);
    };

    const handleChangeLongitude = (e) => {
        setLongitude(e.target.value);
        formik.setFieldValue('ubication.gps.longitude', e.target.value);
    };

    const handleChangeFestival = (e) => {
        takeSpacesList(e.target.value);
        formik.setFieldValue('festival', e.target.value);
    };

    const handleDescriptionEu = (value) => {
        setDescriptionEu(value);
    };

    const validationFirstStep = () => {
        setErrorInput('');
        let count = 0;
        if (formik.values.name === '') {
            setErrorInput('name');
            count++;
        }
        /*if(formik.values.category === '') {
            setErrorInput('category');
            count++;
        }
        if(formik.values.pay === '') {
            setErrorInput('pay');
            count++;
        }*/

        if (count === 0) {
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
    };

    const validationSecondStep = () => {
        setErrorInput('');
        let count = 0;

        /*if(formik.values.description === '') {
            setErrorInput('description');
            count++;
        }*/

        if (count === 0) {
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
    };

    const validationThreeStep = () => {
        setErrorInput('');
        let count = 0;

        if (formik.values.datePublication === '') {
            setErrorInput('datePublication');
            count++;
        }

        if (count === 0) {
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
    };

    const generateSlug = (text) => {
        let slug = text;
        if (slug != '') {
            slug = slug.toLowerCase();
            slug = slug.trim();
            slug = slug.replace('"', '');
            slug = slug.replace("'", '');
            slug = slug.replace(",", '');
            slug = slug.replace('&', '');
            slug = slug.replace('“', '');
            slug = slug.replace('”', '');
            slug = slug.replace(':', '');
            slug = slug.replace('+', '');
            slug = slug.replace('/', '');
            slug = slug.replace('!', '');
            slug = slug.replace('¡', '');
            slug = slug.replace('*', '');
            slug = slug.replace('í', 'i');
            slug = slug.replace('á', 'a');
            slug = slug.replace('é', 'e');
            slug = slug.replace('ó', 'o');
            slug = slug.replace('ú', 'u');
            slug = slug.replace(/["']/g, "");
            slug = slug.replace(/\s+/g, '-');
        }
        return slug;
    };

    const onSubmit = async (values) => {
        setIsLoading(true);
        let urlfile = '';
        if (values.file !== '') {
            const formData = new FormData();
            formData.append('file', values.file);
            const { status } = await uploadFile(formData);
            if (status === 201) {
                urlfile = URL_S3 + values.file.name;
            } else {
                urlfile = '';
            }
            values.banner = urlfile;
        }
        if (values.startDate !== '' && values.startDate !== null) {
            values.startDate = values.startDate + ':00.000Z';
        }
        if (values.endDate !== '' && values.endDate !== null) {
            values.endDate = values.endDate + ':00.000Z';
        }
        if (datePublicationAnt !== values.datePublication) {
            if (values.datePublication !== '' && values.datePublication !== null) {
                values.datePublication = values.datePublication + ':00.000Z';
            }
        } else {
            values.datePublication = values.datePublication + ':00.000Z';
        }
        
        values.description_translated = { 'fr': descriptionFr, 'eu': descriptionEu, 'en': descriptionEn }
        values.name_translated = { 'fr': titleFr, 'eu': titleEu, 'en': titleEn }
        values.slug = generateSlug(values.name);
        values.slug_translated = { 'fr': generateSlug(titleFr), 'eu': generateSlug(titleEu), 'en': generateSlug(titleEn) }
        values.price = values.price * 1;
        let dataL;
        let statusL;
        if (blog?._id !== undefined && blog?._id !== '') {
            const { status, data } = await modifyBlog(blog?._id, values);
            dataL = data;
            statusL = status;
        } else {
            delete values._id;
            const { status, data } = await addblog(values);
            dataL = data;
            statusL = status;
        }

        if (statusL === 200) {
            onOpenSnackBar({ severity: 'success', msg: dataL.message })
            setIsLoading(false);
            onRefresh()
            handleClose();
            resetForm();
        } else {
            setMessageAlert({ severity: 'warning', msg: dataL.message })
            setIsOpenAlert(true)
            setIsLoading(false);
        }
    }

    const handleChangeAccordion = (panel) => (event, newExpanded) => {
        setExpanded(newExpanded ? panel : false);
    };

    const resetForm = async () => {
        formik.handleReset();
    }

    const formik = useFormik({
        initialValues: {
            _id: '',
            name: '',
            description: '',
            tags: [],
            banner: '',
            contact: contact,
            category: '',
            subcategory: '',
            pay: '',
            ubication: ubication,
            startDate: '',
            endDate: '',
            datePublication: '',
            festival: '',
            artist: '',
            author: user._id,
            isActive: true,
            isLeading: false,
            file: '',
            likes: [],
            name_translated: {},
            description_translated: {},
            slug_translated: {},
            position: 0,
            price: 0.00,
            space: null,
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            onSubmit(values);
        },
    });

    return (
        <>
            <Dialog open={open} onClose={handleClose} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Blog - Noticia</DialogTitle>
                <DialogContent>

                    <Stepper activeStep={activeStep} alternativeLabel>
                        {steps.map((label) => (
                            <Step key={label} >
                                <StepLabel >{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                    <div>
                        <form encType="multipart/form-data" onSubmit={formik.handleSubmit}>
                            {activeStep === steps.length ? (
                                <div>
                                    <Typography className={classes.instructions}>Todos los pasos completados</Typography>
                                    <div className={classes.divBtn}>
                                        <Button onClick={handleReset}>Retroceder</Button>
                                        <Button type="submit" variant="contained" color="primary">
                                            Guardar
                                            {isLoading && <CircularProgress color="secondary" size={24} className={classes.buttonProgress} />}
                                        </Button>
                                    </div>
                                </div>
                            ) : <></>}
                            {activeStep === 0 ? (
                                <div>
                                    <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                                    <Grid container spacing={3}>
                                        <Grid item xs={12}>
                                            <FormControl className={classes.formControl}>
                                                <InputLabel id="label-festival">Festival</InputLabel>
                                                <Select
                                                    labelId="label-festival"
                                                    id="festival"
                                                    name="festival"
                                                    value={formik.values.festival}
                                                    onChange={handleChangeFestival}
                                                    error={formik.touched.festival && Boolean(formik.errors.festival)}
                                                    helpertext={formik.touched.festival && formik.errors.festival}
                                                >
                                                    {festivals.map((festival, index) => {
                                                        return (
                                                            <MenuItem key={index} value={festival._id}>
                                                                {festival.name}
                                                            </MenuItem>
                                                        );
                                                    })}
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                fullWidth
                                                id="name"
                                                name="name"
                                                label="Título"
                                                value={formik.values.name}
                                                onChange={formik.handleChange}
                                                error={formik.touched.name && Boolean(formik.errors.name)}
                                                helpertext={formik.touched.name && formik.errors.name}
                                            />
                                            <small style={{ color: errorInput === 'name' ? 'red' : 'black' }} className={classes.counter}>
                                                {errorInput === 'name' ? 'Ingrese el título.' : ''}
                                            </small>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <FormControl className={classes.formControl}>
                                                <InputLabel id="label-category">Categoría</InputLabel>
                                                <Select
                                                    labelId="label-category"
                                                    id="category"
                                                    name="category"
                                                    value={formik.values.category}
                                                    onChange={formik.handleChange}
                                                    error={formik.touched.category && Boolean(formik.errors.category)}
                                                    helpertext={formik.touched.category && formik.errors.category}
                                                >
                                                    <MenuItem value='notice'>Noticia</MenuItem>
                                                    <MenuItem value='artist'>Artista</MenuItem>
                                                    <MenuItem value='concert'>Concierto</MenuItem>
                                                </Select>
                                                <small style={{ color: errorInput === 'category' ? 'red' : 'black' }} className={classes.counter}>
                                                    {errorInput === 'category' ? 'Seleccione la categoría' : ''}
                                                </small>
                                            </FormControl>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <FormControl className={classes.formControl}>
                                                <InputLabel id="label-subcategory">Sub Categoría</InputLabel>
                                                <Select
                                                    labelId="label-subcategory"
                                                    id="subcategory"
                                                    name="subcategory"
                                                    value={formik.values.subcategory}
                                                    onChange={formik.handleChange}
                                                    error={formik.touched.subcategory && Boolean(formik.errors.subcategory)}
                                                    helpertext={formik.touched.subcategory && formik.errors.subcategory}
                                                >
                                                    <MenuItem value='instrumental'>Instrumental</MenuItem>
                                                    <MenuItem value='soloist'>Solista</MenuItem>
                                                    <MenuItem value='in_program'>En programa</MenuItem>
                                                    <MenuItem value='jazzeñe'>Jazzeñe</MenuItem>
                                                    <MenuItem value='txikijazz'>Txikijazz</MenuItem>
                                                    <MenuItem value='local_artists'>Artistas Locales</MenuItem>
                                                    <MenuItem value='main_artists'>Artistas Principales</MenuItem>
                                                </Select>
                                                <small style={{ color: errorInput === 'subcategory' ? 'red' : 'black' }} className={classes.counter}>
                                                    {errorInput === 'subcategory' ? 'Seleccione la categoría' : ''}
                                                </small>
                                            </FormControl>
                                        </Grid>

                                        {formik.values.category == 'artist' ? (<Grid item xs={6}>
                                            <FormControl className={classes.formControl}>
                                                <TextField
                                                    fullWidth
                                                    id="position"
                                                    name="position"
                                                    label="Posición"
                                                    type="number"
                                                    value={formik.values.position}
                                                    onChange={formik.handleChange}
                                                    error={formik.touched.position && Boolean(formik.errors.position)}
                                                    helpertext={formik.touched.position && formik.errors.position}
                                                />
                                            </FormControl>
                                        </Grid>) : <></>}

                                        {formik.values.category == 'concert' ? (<Grid item xs={6}>
                                            <FormControl className={classes.formControl}>
                                                <InputLabel id="label-space">Escenario</InputLabel>
                                                <Select
                                                    labelId="label-space"
                                                    id="space"
                                                    name="space"
                                                    value={formik.values.space}
                                                    onChange={formik.handleChange}
                                                    error={formik.touched.space && Boolean(formik.errors.space)}
                                                    helpertext={formik.touched.space && formik.errors.space}
                                                >
                                                    {spaces.map((space, index) => {
                                                        return (
                                                            <MenuItem key={index} value={space._id}>
                                                                {space.name}
                                                            </MenuItem>
                                                        );
                                                    })}
                                                </Select>
                                                <small style={{ color: errorInput === 'space' ? 'red' : 'black' }} className={classes.counter}>
                                                    {errorInput === 'space' ? 'Seleccione el escenario' : ''}
                                                </small>
                                            </FormControl>
                                        </Grid>) : <></>}

                                        <Grid item xs={6}>
                                            <FormControl className={classes.formControl}>
                                                <InputLabel id="label-pay">Pago</InputLabel>
                                                <Select
                                                    labelId="label-pay"
                                                    id="pay"
                                                    name="pay"
                                                    value={formik.values.pay}
                                                    onChange={formik.handleChange}
                                                    error={formik.touched.pay && Boolean(formik.errors.pay)}
                                                    helpertext={formik.touched.pay && formik.errors.pay}
                                                >
                                                    <MenuItem value='free'>Gratis</MenuItem>
                                                    <MenuItem value='buy'>Pagado</MenuItem>
                                                </Select>
                                                <small style={{ color: errorInput === 'pay' ? 'red' : 'black' }} className={classes.counter}>
                                                    {errorInput === 'pay' ? 'Seleccione el pago' : ''}
                                                </small>
                                            </FormControl>
                                        </Grid>

                                        {formik.values.pay == 'buy' ? (<Grid item xs={6}>
                                            <FormControl className={classes.formControl}>
                                                <TextField
                                                    fullWidth
                                                    id="price"
                                                    name="price"
                                                    label="Precio"
                                                    value={formik.values.price}
                                                    onChange={formik.handleChange}
                                                    error={formik.touched.price && Boolean(formik.errors.price)}
                                                    helpertext={formik.touched.price && formik.errors.price}
                                                />
                                            </FormControl>
                                        </Grid>) : <></>}

                                        <Grid item xs={6}>
                                            <FormControl className={classes.formControl}>
                                                <TextField
                                                    fullWidth
                                                    id="tags"
                                                    name="tags"
                                                    label="Tags"
                                                    value={tag}
                                                    onChange={(e) => { setTag(e.target.value) }}
                                                    onKeyPress={handleAddTag}
                                                />
                                            </FormControl>
                                            {formik.values.tags &&
                                                formik.values.tags.map((tag, i) => {
                                                    return (
                                                        <Chip
                                                            size="small"
                                                            key={i}
                                                            label={tag}
                                                            color="primary"
                                                            onDelete={() => handleDeleteTag(i)}
                                                            className={classes.chip}
                                                        />
                                                    );
                                                })}
                                        </Grid>

                                        <Grid item xs={3}>
                                            <FormControlLabel
                                                className={classes.formControl}
                                                control={<Checkbox id="isActive" checked={formik.values.isActive} onChange={formik.handleChange} name="isActive" />}
                                                label="Activo"
                                            />
                                        </Grid>

                                        <Grid item xs={3}>
                                            <FormControlLabel
                                                className={classes.formControl}
                                                control={<Checkbox id="isLeading" checked={formik.values.isLeading} onChange={formik.handleChange} name="isLeading" />}
                                                label="Principal"
                                            />
                                        </Grid>

                                        <Grid item xs={12}>
                                            <Typography component="p">Ubicación</Typography>
                                            <FormControl className={classes.formControl}>
                                                <LocationGM labelId="label-ubication" onChange={changeAddressHandler} />
                                            </FormControl>
                                        </Grid>
                                        {formik.values.ubication?.address !== '' || formik.values.ubication?.city !== ''
                                            || formik.values.ubication?.country !== '' ? (
                                            <>
                                                <Grid item xs={6}>
                                                    <TextField
                                                        fullWidth
                                                        id="ubication.address"
                                                        name="ubication.address"
                                                        label="Dirección"
                                                        value={formik.values.ubication.address}
                                                        onChange={formik.handleChange}
                                                        error={formik.touched.ubication?.address && Boolean(formik.errors.ubication?.address)}
                                                        helpertext={formik.touched.ubication?.address && formik.errors.ubication?.address}
                                                    />
                                                </Grid>

                                                <Grid item xs={6}>
                                                    <TextField
                                                        fullWidth
                                                        id="ubication.postalCode"
                                                        name="ubication.postalCode"
                                                        label="Código postal"
                                                        value={formik.values.ubication.postalCode}
                                                        onChange={formik.handleChange}
                                                        error={formik.touched.ubication?.postalCode && Boolean(formik.errors.ubication?.postalCode)}
                                                        helpertext={formik.touched.ubication?.postalCode && formik.errors.ubication?.postalCode}
                                                    />
                                                </Grid>

                                                <Grid item xs={6}>
                                                    <TextField
                                                        fullWidth
                                                        id="ubication.city"
                                                        name="ubication.city"
                                                        label="Ciudad"
                                                        value={formik.values.ubication.city}
                                                        onChange={formik.handleChange}
                                                        error={formik.touched.ubication?.city && Boolean(formik.errors.ubication?.city)}
                                                        helpertext={formik.touched.ubication?.city && formik.errors.ubication?.city}
                                                    />
                                                </Grid>

                                                <Grid item xs={6}>
                                                    <TextField
                                                        fullWidth
                                                        id="ubication.country"
                                                        name="ubication.country"
                                                        label="País"
                                                        value={formik.values.ubication.country}
                                                        onChange={formik.handleChange}
                                                        error={formik.touched.ubication?.country && Boolean(formik.errors.ubication?.country)}
                                                        helpertext={formik.touched.ubication?.country && formik.errors.ubication?.country}
                                                    />
                                                </Grid>

                                                <Grid item xs={6}>
                                                    <TextField
                                                        fullWidth
                                                        id="ubication.gps.latitude"
                                                        name="ubication.gps.latitude"
                                                        label="Latitud"
                                                        value={latitude}
                                                        onChange={handleChangeLatitude}
                                                        error={formik.touched.ubication?.gps.latitude && Boolean(formik.errors.ubication?.gps.latitude)}
                                                        helpertext={formik.touched.ubication?.gps.latitude && formik.errors.ubication?.gps.latitude}
                                                    />
                                                </Grid>

                                                <Grid item xs={6}>
                                                    <TextField
                                                        fullWidth
                                                        id="ubication.gps.longitude"
                                                        name="ubication.gps.longitude"
                                                        label="Longitud"
                                                        value={longitude}
                                                        onChange={handleChangeLongitude}
                                                        error={formik.touched.ubication?.gps.longitude && Boolean(formik.errors.ubication?.gps.longitude)}
                                                        helpertext={formik.touched.ubication?.gps.longitude && formik.errors.ubication?.gps.longitude}
                                                    />
                                                </Grid>
                                            </>
                                        ) : <></>}

                                    </Grid>
                                    <div className={classes.divBtn}>
                                        <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>Atras</Button>
                                        <Button variant="contained" color="primary" onClick={handleNext}>{activeStep === steps.length - 1 ? 'Finish' : 'Siguiente'} </Button>
                                    </div>
                                </div>
                            ) : <></>}

                            {activeStep === 1 ? (
                                <>
                                    <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <Typography className={classes.subtitle}>Banner </Typography>
                                            {formik.values.banner !== '' ? (
                                                <img src={formik.values.banner} className={classes.imgBanner} alt={formik.values.name} />
                                            ) : <></>}
                                            <FormControl className={classes.formControl}>
                                                {formik.values.file !== '' ? (
                                                    <p>Imagen seleccionada {formik.values.file.name}</p>

                                                ) : <></>}
                                                <input accept="image/*" id="file" name="file" type="file" onChange={(event) => {
                                                    formik.setFieldValue("banner", '');
                                                    formik.setFieldValue("file", event.currentTarget.files[0]);
                                                }} />

                                            </FormControl>
                                            <small style={{ color: errorInput === 'file' ? 'red' : 'black' }} className={classes.counter}>
                                                {errorInput === 'file' ? 'Seleccione una imagen.' : ''}
                                            </small>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Typography className={classes.subtitle}>Descripción </Typography>
                                            <CKEditor content={formik.values.description} onChange={handleDescription} />
                                            <small style={{ color: errorInput === 'description' ? 'red' : 'black' }} className={classes.counter}>
                                                {errorInput === 'description' ? 'Ingrese una descripción' : ''}
                                            </small>
                                        </Grid>

                                        <Grid item xs={12}>
                                            <TextField
                                                fullWidth
                                                id="artist"
                                                name="artist"
                                                label="Artistas"
                                                value={formik.values.artist}
                                                onChange={formik.handleChange}
                                                error={formik.touched.artist && Boolean(formik.errors.artist)}
                                                helpertext={formik.touched.artist && formik.errors.artist}
                                            />
                                        </Grid>

                                    </Grid>

                                    <div className={classes.divBtn}>
                                        <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>Atras</Button>
                                        <Button variant="contained" color="primary" onClick={handleNext}>{activeStep === steps.length - 1 ? 'Finish' : 'Siguiente'} </Button>
                                    </div>
                                </>
                            ) : <></>}

                            {activeStep === 2 ? (
                                <div>
                                    <Typography className={classes.instructions}>{getStepContent(activeStep)} (Opcional)</Typography>
                                    <Grid container spacing={3}>
                                        <Grid item xs={12}>
                                            <Accordion square expanded={expanded === 'panel1'} onChange={handleChangeAccordion('panel1')}>
                                                <AccordionSummary aria-controls="panel1d-content" id="panel1d-header" expandIcon={<ExpandMoreIcon />}>
                                                    <   Typography>Inglés</Typography>
                                                </AccordionSummary>
                                                <AccordionDetails className={classes.formControl}>
                                                    <Grid container spacing={3}>
                                                        <Grid item xs={12}>
                                                            <TextField
                                                                fullWidth
                                                                id="titleEn"
                                                                name="titleEn"
                                                                label="Título"
                                                                value={titleEn}
                                                                onChange={handleTitleEn}
                                                            />
                                                        </Grid>
                                                        <Grid item xs={12}>
                                                            <Typography className={classes.subtitle}>Descripción </Typography>
                                                            <CKEditor content={descriptionEn} onChange={handleDescriptionEn} />
                                                        </Grid>
                                                    </Grid>
                                                </AccordionDetails>
                                            </Accordion>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Accordion square expanded={expanded === 'panel2'} onChange={handleChangeAccordion('panel2')}>
                                                <AccordionSummary aria-controls="panel2d-content" id="panel2d-header" expandIcon={<ExpandMoreIcon />}>
                                                    <   Typography>Francés</Typography>
                                                </AccordionSummary>
                                                <AccordionDetails className={classes.formControl}>
                                                    <Grid container spacing={3}>
                                                        <Grid item xs={12}>
                                                            <TextField
                                                                fullWidth
                                                                id="titleFr"
                                                                name="titleFr"
                                                                label="Título"
                                                                value={titleFr}
                                                                onChange={handleTitleFr}
                                                            />
                                                        </Grid>
                                                        <Grid item xs={12}>
                                                            <Typography className={classes.subtitle}>Descripción </Typography>
                                                            <CKEditor content={descriptionFr} onChange={handleDescriptionFr} />
                                                        </Grid>
                                                    </Grid>
                                                </AccordionDetails>
                                            </Accordion>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Accordion square expanded={expanded === 'panel3'} onChange={handleChangeAccordion('panel3')}>
                                                <AccordionSummary aria-controls="panel3d-content" id="panel3d-header" expandIcon={<ExpandMoreIcon />}>
                                                    <   Typography>Euskera</Typography>
                                                </AccordionSummary>
                                                <AccordionDetails className={classes.formControl}>
                                                    <Grid container spacing={3}>
                                                        <Grid item xs={12}>
                                                            <TextField
                                                                fullWidth
                                                                id="titleEu"
                                                                name="titleEu"
                                                                label="Título"
                                                                value={titleEu}
                                                                onChange={handleTitleEu}
                                                            />
                                                        </Grid>
                                                        <Grid item xs={12}>
                                                            <Typography className={classes.subtitle}>Descripción </Typography>
                                                            <CKEditor content={descriptionEu} onChange={handleDescriptionEu} />
                                                        </Grid>
                                                    </Grid>
                                                </AccordionDetails>
                                            </Accordion>
                                        </Grid>
                                    </Grid>
                                    <div className={classes.divBtn}>
                                        <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>Atras</Button>
                                        <Button variant="contained" color="primary" onClick={handleNext}>{activeStep === steps.length - 1 ? 'Finish' : 'Siguiente'} </Button>
                                    </div>
                                </div>
                            ) : <></>}

                            {activeStep === 3 ? (
                                <>

                                    <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                                    <Grid container spacing={3}>
                                        <Grid item xs={12}>
                                            <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                                                <DateTimePicker
                                                    autoOk
                                                    fullWidth
                                                    clearable
                                                    id="datePublication"
                                                    name="datePublication"
                                                    ampm={false}
                                                    value={datePublication}
                                                    onChange={handleDatePChange}
                                                    format="yyyy-MM-dd HH:mm"
                                                    label="Fecha y hora de Publicación"
                                                />
                                            </MuiPickersUtilsProvider>
                                            <small style={{ color: errorInput === 'datePublication' ? 'red' : 'black' }} className={classes.counter}>
                                                {errorInput === 'datePublication' ? 'Seleccione una fecha y hora.' : ''}
                                            </small>

                                        </Grid>
                                        <Grid item xs={6}>
                                            <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                                                <DateTimePicker
                                                    autoOk
                                                    fullWidth
                                                    clearable
                                                    id="startDate"
                                                    name="startDate"
                                                    ampm={false}
                                                    value={dateStart}
                                                    onChange={handleDateSChange}
                                                    format="yyyy-MM-dd HH:mm"
                                                    label="Fecha y hora de inicio"
                                                />
                                            </MuiPickersUtilsProvider>
                                            <small style={{ color: errorInput === 'startDate' ? 'red' : 'black' }} className={classes.counter}>
                                                {errorInput === 'startDate' ? 'Seleccione una fecha y hora.' : ''}
                                            </small>

                                        </Grid>
                                        <Grid item xs={6}>
                                            <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                                                <DateTimePicker
                                                    autoOk
                                                    fullWidth
                                                    clearable
                                                    id="endDate"
                                                    name="endDate"
                                                    ampm={false}
                                                    value={dateEnd}
                                                    onChange={handleDateEChange}
                                                    format="yyyy-MM-dd HH:mm"
                                                    label="Fecha y hora de finalización"
                                                />
                                            </MuiPickersUtilsProvider>
                                            <small style={{ color: errorInput === 'endDate' ? 'red' : 'black' }} className={classes.counter}>
                                                {errorInput === 'endDate' ? 'Seleccione una fecha y hora.' : ''}
                                            </small>
                                        </Grid>

                                    </Grid>

                                    <div className={classes.divBtn}>
                                        <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>Atras</Button>
                                        <Button variant="contained" color="primary" onClick={handleNext}>{activeStep === steps.length - 1 ? 'Finish' : 'Siguiente'} </Button>
                                    </div>
                                </>
                            ) : <></>}

                            {activeStep === 4 ? (
                                <>
                                    <Typography className={classes.instructions}>{getStepContent(activeStep)} (opcional)</Typography>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <TextField
                                                fullWidth
                                                id="contact.name"
                                                name="contact.name"
                                                label="Nombre de contacto"
                                                value={formik.values.contact.name}
                                                onChange={formik.handleChange}
                                                error={formik.touched.contact?.name && Boolean(formik.errors.contact?.name)}
                                                helpertext={formik.touched.contact?.name && formik.errors.contact?.name}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                fullWidth
                                                id="contact.email"
                                                name="contact.email"
                                                label="Correo Electrónico"
                                                value={formik.values.contact.email}
                                                onChange={formik.handleChange}
                                                error={formik.touched.contact?.email && Boolean(formik.errors.contact?.email)}
                                                helpertext={formik.touched.contact?.email && formik.errors.contact?.email}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                fullWidth
                                                id="contact.phone"
                                                name="contact.phone"
                                                label="Teléfono"
                                                value={formik.values.contact.phone}
                                                onChange={formik.handleChange}
                                                error={formik.touched.contact?.phone && Boolean(formik.errors.contact?.phone)}
                                                helpertext={formik.touched.contact?.phone && formik.errors.contact?.phone}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                fullWidth
                                                id="contact.cellphone"
                                                name="contact.cellphone"
                                                label="Celular"
                                                value={formik.values.contact.cellphone}
                                                onChange={formik.handleChange}
                                                error={formik.touched.contact?.cellphone && Boolean(formik.errors.contact?.cellphone)}
                                                helpertext={formik.touched.contact?.cellphone && formik.errors.contact?.cellphone}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                fullWidth
                                                id="contact.url"
                                                name="contact.url"
                                                label="Url Es"
                                                value={formik.values.contact.url}
                                                onChange={formik.handleChange}
                                                error={formik.touched.contact?.url && Boolean(formik.errors.contact?.url)}
                                                helpertext={formik.touched.contact?.url && formik.errors.contact?.url}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                fullWidth
                                                id="contact.url_en"
                                                name="contact.url_en"
                                                label="Url En"
                                                value={formik.values.contact.url_en}
                                                onChange={formik.handleChange}
                                                error={formik.touched.contact?.url_en && Boolean(formik.errors.contact?.url_en)}
                                                helpertext={formik.touched.contact?.url_en && formik.errors.contact?.url_en}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                fullWidth
                                                id="contact.url_eu"
                                                name="contact.url_eu"
                                                label="Url Eu"
                                                value={formik.values.contact.url_eu}
                                                onChange={formik.handleChange}
                                                error={formik.touched.contact?.url_eu && Boolean(formik.errors.contact?.url_eu)}
                                                helpertext={formik.touched.contact?.url_eu && formik.errors.contact?.url_eu}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                fullWidth
                                                id="contact.url_fr"
                                                name="contact.url_fr"
                                                label="Url Fr"
                                                value={formik.values.contact.url_fr}
                                                onChange={formik.handleChange}
                                                error={formik.touched.contact?.url_fr && Boolean(formik.errors.contact?.url_fr)}
                                                helpertext={formik.touched.contact?.url_fr && formik.errors.contact?.url_fr}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                fullWidth
                                                id="contact.note"
                                                name="contact.note"
                                                label="Nota"
                                                value={formik.values.contact.note}
                                                onChange={formik.handleChange}
                                                error={formik.touched.contact?.note && Boolean(formik.errors.contact?.note)}
                                                helpertext={formik.touched.contact?.note && formik.errors.contact?.note}
                                            />
                                        </Grid>

                                    </Grid>

                                    <div className={classes.divBtn}>
                                        <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>Atras</Button>
                                        <Button variant="contained" color="primary" onClick={handleNext}>{activeStep === steps.length - 1 ? 'Finish' : 'Siguiente'} </Button>
                                    </div>
                                </>
                            ) : <></>}

                            {isOpenAlert &&
                                <Grid item xs={12}>
                                    <AlertMsg severity={messageAlert.severity} msg={messageAlert.msg} />
                                </Grid>
                            }
                            <Divider />
                        </form>
                    </div>

                </DialogContent>
            </Dialog>
        </>
    );
};

export default Blog;
