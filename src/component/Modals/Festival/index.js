import React, {useState, useEffect, useContext} from 'react';
import {Dialog, DialogTitle, DialogContent, Button, DialogActions, Grid, TextField, FormControl,  
        Typography, Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useFormik } from 'formik';
import { addFestival, modifyFestival } from "../../../services/festival.service";
import { uploadFile } from "../../../services/upload.service";
import * as yup from 'yup';
import { URL_S3 } from "../../../store/constant";
import CircularProgress from '@material-ui/core/CircularProgress';
import { UserContext } from "../../../context/UserContext";
import AlertMsg from "../../../component/Alert";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    formControl: {
        width: '100%',
    },
    btnActions: {
        margin: '10px',
    },
    buttonProgress: {
        marginLeft: '10px'
    },
    imgBanner: {
        width: '150px',
        height: '130px',
        objectFit: 'contain',
        marginTop: '10px',
        marginBottom: '10px'
    }
}));

const validationSchema = yup.object({
    name: yup.string().required(),
    description: yup.string().required(),
});

const Festival = ({open, onClose, onRefresh, onOpenSnackBar, festival}) => {
    const classes = useStyles();
    const { user } = useContext(UserContext);
    const [isLoading, setIsLoading] = useState(false);
    const [isOpenAlert, setIsOpenAlert] = useState(false);
    const [messageAlert, setMessageAlert] = useState({});

    useEffect(() => {
        resetForm();
        if (festival?._id !== undefined && festival?._id !== '') {
            loadData();
        }
    }, [open]);

    const loadData = () => {
        formik.setFieldValue('name', festival?.name)
        formik.setFieldValue('_id', festival?._id)
        formik.setFieldValue('description', festival?.description)
        formik.setFieldValue('banner', festival?.banner)
        formik.setFieldValue('promoter', festival?.promoter)
    }

    const handleClose = () => {
        onClose();
    };

    const onSubmit = async (values) => {
        setIsOpenAlert(false)
        setIsLoading(true);
        let urlfile = '';
        if (values.file !== '') {
            const formData = new FormData();
            formData.append('file', values.file);
            const { status, data } = await uploadFile(formData);
            if (status === 201) {
                urlfile = URL_S3 + values.file.name;
            } else {
                urlfile = '';
            }
            values.banner = urlfile;
        }
        let dataL; 
        let statusL; 
        if (festival?._id !== undefined && festival?._id !== '') {
            const { status, data } = await modifyFestival(festival?._id, values);
            dataL = data;
            statusL = status;
        } else {
            delete values._id;
            const { status, data } = await addFestival(values);
            dataL = data;
            statusL = status;
        }
        if (statusL === 200) {
            onOpenSnackBar({severity: 'success', msg: dataL.message})
            setIsLoading(false);
            onRefresh()
            handleClose();
            resetForm();
        } else {
            setMessageAlert({severity: 'warning', msg: dataL.message})
            setIsOpenAlert(true)
            setIsLoading(false);
        }
    }

    const resetForm = async () => {
        formik.handleReset();
    }

    const formik = useFormik({
        initialValues: {
            name: '',
            banner: '',
            promoter: user._id,
            isActive: true,
            description:'',
            file:'',

        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            onSubmit(values);
        },
    });

    return (
        <>
            <Dialog open={open} onClose={handleClose} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Festival</DialogTitle>
                <form encType="multipart/form-data" onSubmit={formik.handleSubmit}>
                    <DialogContent>
                            <Grid container spacing={3}>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        id="name"
                                        name="name"
                                        label="Nombre"
                                        value={formik.values.name}
                                        onChange={formik.handleChange}
                                        error={formik.touched.name && Boolean(formik.errors.name)}
                                        helpertext={formik.touched.name && formik.errors.name}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        id="description"
                                        name="description"
                                        label="Descripción"
                                        value={formik.values.description}
                                        onChange={formik.handleChange}
                                        error={formik.touched.description && Boolean(formik.errors.description)}
                                        helpertext={formik.touched.description && formik.errors.description}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <Typography component="p">Imagen</Typography>
                                    {formik.values.banner !== '' ? (
                                        <img src={formik.values.banner} className={classes.imgBanner} alt={formik.values.name} />
                                    ) : <></>}
                                    <FormControl className={classes.formControl}>
                                        <input id="file" name="file" type="file" onChange={(event) => {
                                            formik.setFieldValue("banner", '');
                                            formik.setFieldValue("file", event.currentTarget.files[0]);
                                        }} />
                                    </FormControl>
                                </Grid>
                            </Grid>
                        
                    </DialogContent>
                    <DialogActions className={classes.btnActions}>
                        <Button type="button" onClick={handleClose} color="secondary">
                            Cerrar
                        </Button>
                        <Button type="submit" variant="contained" color="primary">
                            Guardar
                            {isLoading && <CircularProgress color="secondary" size={24} className={classes.buttonProgress} />}
                        </Button>
                    </DialogActions>

                    {isOpenAlert &&
                        <Grid item xs={12}>
                            <AlertMsg severity={messageAlert.severity} msg={messageAlert.msg} />
                        </Grid>
                    }
                    <Divider />
                </form>
            </Dialog>
            
        </>
    )
};

export default Festival;