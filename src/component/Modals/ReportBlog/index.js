import React, { useState, useEffect, useContext } from 'react';
import {
    Dialog, DialogTitle, DialogContent, Button, DialogActions, Grid, TextField, FormControl, InputLabel,
    Select, MenuItem, Typography, Divider
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useFormik } from 'formik';
import { takesBlogs } from "../../../services/blog.service";
import CircularProgress from '@material-ui/core/CircularProgress';
import AlertMsg from "../../Alert";
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { DateTimePicker } from "@material-ui/pickers";
import { es } from 'date-fns/locale';
import { format } from 'date-fns';
import { API_URL } from "../../../store/constant";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    formControl: {
        width: '100%',
    },
    btnActions: {
        margin: '10px',
    },
    buttonProgress: {
        marginLeft: '10px'
    },
    imgBanner: {
        width: '150px',
        height: '130px',
        objectFit: 'contain',
        marginTop: '10px',
        marginBottom: '10px',
        backgroundColor: 'gray'
    }
}));


const ReportBlog = ({ open, onClose, onOpenSnackBar, typeReport }) => {
    const classes = useStyles();
    const [blogs, setBlogs] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [isOpenAlert, setIsOpenAlert] = useState(false);
    const [messageAlert, setMessageAlert] = useState({});
    const [dateDesde, SetDateDesde] = useState(null);
    const [dateHasta, SetDateHasta] = useState(null);

    useEffect(() => {
        resetForm();
        resetElements();
    }, [open]);

    const resetElements = () => {
        setIsLoading(false);
        setIsOpenAlert(false);
        setMessageAlert({});
    }

    const handleClose = () => {
        resetForm();
        onClose();
    };

    const takeBlogs = async (type) => {
        const { status, data } = await takesBlogs('?category=' + type + '&limitField=100&selectFields=_id,name');
        if (status === 200) {
            setBlogs(data.data);
        } /*else {
            console.log(status, data)
        }*/
    }

    const handleDesde = (e) => {
        SetDateDesde(e)
        if (e !== '' && e !== null) {
            formik.setFieldValue('desde', format(e, "yyy-MM-dd'T'HH:mm"));
        } else {
            formik.setFieldValue('desde', e);
        }
    }

    const handleHasta = (e) => {
        SetDateHasta(e)
        if (e !== '' && e !== null) {
            formik.setFieldValue('hasta', format(e, "yyy-MM-dd'T'HH:mm"));
        } else {
            formik.setFieldValue('hasta', e);
        }
    }

    const handleChangeCategory = (e) => {
        formik.setFieldValue('category', e.target.value);
        takeBlogs(e.target.value);
    }

    const onSubmit = async (values) => {
        setIsOpenAlert(false);
        setIsLoading(true);
        if (typeReport === 1) {
            window.open(API_URL + '/report/blogs/'+values.desde+'/'+values.hasta+'/', '_blank');
            setIsLoading(false);
        }
        if (typeReport === 2) {
            window.open(API_URL + '/report/blogs/likes/'+values.blog, '_blank');
            setIsLoading(false);
        }
        
    }

    const resetForm = async () => {
        formik.handleReset();
    }

    const formik = useFormik({
        initialValues: {
            desde: '',
            hasta: '',
            category: '',
            blog: '',
        },
        onSubmit: (values) => {
            onSubmit(values);
        },
    });

    return (
        <>
            <Dialog open={open} onClose={handleClose} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Reportes</DialogTitle>
                <form onSubmit={formik.handleSubmit}>
                    <DialogContent>
                        <Grid container spacing={3}>
                            {typeReport === 1 ?(
                                <>
                                    <Grid item xs={6}>
                                        <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                                            <DateTimePicker
                                                autoOk
                                                fullWidth
                                                clearable
                                                id="desde"
                                                name="desde"
                                                ampm={false}
                                                value={dateDesde}
                                                onChange={handleDesde}
                                                format="yyyy-MM-dd HH:mm"
                                                label="Desde"
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Grid>

                                    <Grid item xs={6}>
                                        <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                                            <DateTimePicker
                                                autoOk
                                                fullWidth
                                                clearable
                                                id="hasta"
                                                name="hasta"
                                                ampm={false}
                                                value={dateHasta}
                                                onChange={handleHasta}
                                                format="yyyy-MM-dd HH:mm"
                                                label="Hasta"
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Grid>
                                </>
                            ):<></>}

                            {typeReport === 2 ? (
                                <>
                                    <Grid item xs={12}>
                                        <FormControl className={classes.formControl}>
                                            <InputLabel id="label-category">Categoría</InputLabel>
                                            <Select
                                                labelId="label-category"
                                                id="category"
                                                name="category"
                                                value={formik.values.category}
                                                onChange={handleChangeCategory}
                                            >
                                                <MenuItem value="artist">Artista</MenuItem>
                                                <MenuItem value="notice">Noticia</MenuItem>
                                                <MenuItem value="concert">Concierto</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <FormControl className={classes.formControl}>
                                            <InputLabel id="label-blog">Blog</InputLabel>
                                            <Select
                                                labelId="label-blog"
                                                id="blog"
                                                name="blog"
                                                value={formik.values.blog}
                                                onChange={formik.handleChange}
                                            >
                                                {blogs.map((blog, index) => {
                                                    return (
                                                        <MenuItem key={index} value={blog._id}>
                                                            {blog.name}
                                                        </MenuItem>
                                                    );
                                                })}
                                            </Select>
                                        </FormControl>
                                    </Grid>
                                </>
                            ):<></>}
                        </Grid>

                    </DialogContent>
                    <DialogActions className={classes.btnActions}>
                        <Button type="button" onClick={handleClose} color="secondary">
                            Cerrar
                        </Button>
                        <Button type="submit" variant="contained" color="primary">
                            Generar Reporte
                            {isLoading && <CircularProgress color="secondary" size={24} className={classes.buttonProgress} />}
                        </Button>
                    </DialogActions>
                    {isOpenAlert &&
                        <Grid item xs={12}>
                            <AlertMsg severity={messageAlert.severity} msg={messageAlert.msg} />
                        </Grid>
                    }
                    <Divider />
                </form>
            </Dialog>
        </>
    )
};

export default ReportBlog;
