import 'moment/locale/es';

import {
    Button,
    Checkbox,
    Dialog,
    DialogContent,
    DialogTitle,
    FormControlLabel,
    Grid,
    TextField,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    FormLabel,
    FormControl,
    InputLabel,
    Select,
    MenuItem
} from '@material-ui/core';
import React, { useContext, useEffect, useState } from 'react';

import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import { UserContext } from '../../../context/UserContext';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import { useFormik } from 'formik';
import { addPoint, modifyPoint } from '../../../services/point.js';
import { getEmployers } from '../../../services/employers.service'
import { takeZoneInfo, takeZonesInfoByEvent } from '../../../services/zone';
import { addPlan, modifyPlan, takeplanInfoByPoint } from '../../../services/plan';
import { takeTicketsInfo } from '../../../services/events.service';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    backButton: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    formControl: {
        width: '100%',
    },
    chip: {
        marginTop: '10px',
        marginRight: '5px',
    },
    divBtn: {
        marginTop: '30px',
        marginBottom: '20px',
    },
    buttonProgress: {
        marginLeft: '10px',
    },
    imgBanner: {
        width: '150px',
        height: '130px',
        objectFit: 'contain',
        marginTop: '10px',
        marginBottom: '10px',
    },
    ticket: {
        backgroundColor: 'white',
        boxShadow: '0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)',
        borderRadius: '10px',
        marginBottom: '30px',
        marginLeft: '10px',
        width: '600px',
        height: '70px',
    },
    textTicket: {
        marginLeft: '10px',
        fontSize: '20px',
        marginBottom: '8px',
    },
    buttonRight: {
        float: 'right',
    },
}));

    function Point({open, onHide, onRefresh, onFinish, onOpenSnackBar, event, point}) {
    moment.locale('es');
    const classes = useStyles();
    const { user } = useContext(UserContext);
    const [errorInput, setErrorInput] = useState('');
    const [messageAlert, setMessageAlert] = useState({});
    const [ checkedEmployers, setCheckedEmployers ] = useState([]);
    const [ checkedTickets, setCheckedTickets ] = useState([]);
    const [ employers, setEmployers ] = useState([]);
    const [ plan, setPlan ] = useState();
    const [ zone, setZone ] = useState();
    const [ zones, setZones ] = useState([]);
    const [ tickets, setTickets ] = useState([]);

    useEffect(() => {
        setErrorInput('');
        formik.handleReset();
        if (point !== null) {
            loadData();
            takePlan(point._id);
        }
        takeEmployers(user._id);
        takeZones(event._id);
        takeTickets(event._id)
    }, [point, event]);

    useEffect(()=> {
        if(plan !== undefined && plan[0] !== undefined){            
            if(plan[0].employers.length > 0){
                let employersArr = [];
                for(const employer of plan[0].employers){
                    employersArr.push(employer);
                }
                setCheckedEmployers(employersArr);
            }
            if(plan[0].tickets.length > 0){
                let ticketsArr = [];
                for(const ticket of plan[0].tickets){
                    ticketsArr.push(ticket);
                }
                setCheckedTickets(ticketsArr);
            }
            if(plan[0] !== undefined){
                takeZone(plan[0].zone);
            }
        }
    }, [plan])

    useEffect(() => {
        loadData();
    },[point, event, plan, zone])

    const takeEmployers = async (id) => {
        const { status, data } = await getEmployers(id);
        if (status === 200) {
            setEmployers(data.data);
        } else {
            setEmployers({});
        }
    };

    const takePlan = async (id) => {
        const { status, data } = await takeplanInfoByPoint(id);
        if (status === 200) {
            setPlan(data.data);
        } else {
            setPlan({});
        }
    };

    const takeZone = async (id) => {
        const { status, data } = await takeZoneInfo(id);
        if (status === 200) {
            setZone(data.data);
        } else {
            setZone({});
        }
    };

    const takeZones = async (id) => {
        const { status, data } = await takeZonesInfoByEvent(id);
        if (status === 200) {
            setZones(data.data);
        } else {
            setZones({});
        }
    };

    const takeTickets = async (id) => {
        const { status, data } = await takeTicketsInfo(id);
        if (status === 200) {
            setTickets(data.data);
        } else {
            setTickets({});
        }
    };

    const formik = useFormik({
        initialValues: {
            formPoint: {
                name: '',
                description: '',
                isActive: true,
                type: 'access',
                event: '',
                promoter: '',
                enterprise: '',
                zone: ''
            },
        },
        // validationSchema: validationSchema,
        onSubmit: (values) => {
            onSubmit(values.formPoint);
            /*  console.log(values); */
        },
    });

    const loadData = () => {
        formik.setFieldValue('formPoint.name', point?.name)
        formik.setFieldValue('formPoint.description', point?.description)
        formik.setFieldValue('formPoint.isActive', point?.isActive)     
        if(zone !== undefined){  
            formik.setFieldValue('formPoint.zone', zone[0]?._id)     
        }
    }

    const validation = () => {
        setErrorInput('');
        if (formik.values.formPoint.name === undefined || formik.values.formPoint.name === '') {
            setErrorInput('formPoint.name');
            return false;
        }
        else if (formik.values.formPoint.zone === undefined || formik.values.formPoint.zone === '') {
            setErrorInput('formPoint.zone');
            return false;
        }
        else if (formik.values.formPoint.description === undefined || formik.values.formPoint.description === '') {
            setErrorInput('formPoint.description');
            return false;
        }
        return true;
    }

    const onSubmit = async (valuesPoint) => {
        const validated = validation();

        if(validated){
            valuesPoint.event = event._id;
            valuesPoint.promoter = user._id;
            valuesPoint.enterprise = user._id;

            let employersArr = [];
            if(checkedEmployers.length > 0 ){
                for (let i = 0; i < checkedEmployers.length; i++) {
                    employersArr.push(checkedEmployers[i]);
                }
            }

            let ticketsArr = [];
            if(checkedTickets.length > 0 ){
                for (let i = 0; i < checkedTickets.length; i++) {
                    ticketsArr.push(checkedTickets[i]);
                }
            }

            let message;
            if(point._id !== undefined){
                const { status, data } = await modifyPoint(point._id,valuesPoint);
                if(status === 200){
                    message = data.message;
                    onOpenSnackBar({ severity: 'success', msg: message });
                }else{
                    message = data.message;
                    setMessageAlert({ severity: 'warning', msg: message });
                }

                const planValues = {
                    name: `Plan of access ${data.data.name}`,
                    description: '...',
                    zone: valuesPoint.zone,
                    point: data.data._id,
                    promoter: user._id,
                    employers: employersArr,
                    tickets: ticketsArr,
                }
                const { statusP, dataP } = await modifyPlan(plan[0]._id, planValues);
                
            }else{      
                const { status, data } = await addPoint(valuesPoint);
                let pointAdded = false;
                if(status === 200){
                    pointAdded = true;
                    message = data.message;
                    onOpenSnackBar({ severity: 'success', msg: message });
                }else{
                    message = data.message;
                    setMessageAlert({ severity: 'warning', msg: message });
                }

                const planValues = {
                    name: `Plan of access ${data.data.name}`,
                    description: '...',
                    zone: valuesPoint.zone,
                    point: data.data._id,
                    promoter: user._id,
                    employers: employersArr,
                    tickets: ticketsArr,
                
                }
                const { statusP, dataP } = await addPlan(planValues);
            }
            onFinish();
        }
    }

    const handleToggleEmployers = (value) => () => {
        const currentIndex = checkedEmployers.indexOf(value);
        const newChecked = [...checkedEmployers];

        if (currentIndex === -1) {
        newChecked.push(value);
        } else {
        newChecked.splice(currentIndex, 1);
        }

        setCheckedEmployers(newChecked);
    };

    const handleToggleTickets = (value) => () => {
        const currentIndex = checkedTickets.indexOf(value);
        const newChecked = [...checkedTickets];

        if (currentIndex === -1) {
        newChecked.push(value);
        } else {
        newChecked.splice(currentIndex, 1);
        }

        setCheckedTickets(newChecked);
    };

    const handleChangeZone = (e) => {
        formik.setFieldValue('formPoint.zone', e.target.value);
    }
    const handleClose = (event, reason) => {
        formik.handleReset();
        if (reason !== 'backdropClick') {
            onHide();
        }
    };

    return (
        <>

    <Dialog key={point} open={open} onHide={handleClose} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
        <DialogTitle>
            <Grid container direction="row" justifyContent="space-between" alignItems="center">
                {point._id !== undefined ? <>Editar acceso {point._id}</> : <>Crear nuevo acceso  </>}
                <IconButton aria-label="close" onClick={handleClose}>
                    <CloseIcon />
                </IconButton>
            </Grid>
        </DialogTitle>
        <DialogContent>
        <form encType="multipart/form-data" onSubmit={formik.handleSubmit}>
          <Grid container spacing={3}>
            {/*nombre entrada */}
            <Grid item xs={7}>
                <TextField
                    fullWidth
                    id="formPoint.name"
                    name="formPoint.name"
                    label="Nombre Acceso"
                    value={formik.values.formPoint.name}
                    onChange={formik.handleChange}
                    error={formik.touched.formPoint && Boolean(formik.errors.formPoint)}
                    helpertext={formik.touched.formPoint && formik.errors.formPoint}
                />
                <small
                    style={{ color: errorInput === 'formPoint.name' ? 'red' : 'black' }}
                    className={classes.counter}
                >
                    {errorInput === 'formPoint.name' ? 'Ingrese nombre de la zona.' : ''}
                </small>
            </Grid>

             {/*Zona */}
             <Grid item xs={5}>
                    <FormControl className={classes.formControl}>
                        <InputLabel id="label-zone">Selecciona Zona</InputLabel>
                        <Select
                            labelId="label-zone"
                            id="formPoint.zone"
                            name="formPoint.zone"
                            value={formik.values.formPoint.zone}
                            onChange={handleChangeZone}
                            error={formik.touched.formPoint && Boolean(formik.errors.formPoint)}
                            helpertext={formik.touched.formPoint && formik.errors.formPoint}
                        >
                            {zones.map((zone, index) => {
                                return (
                                    <MenuItem key={index} value={zone._id}>
                                        {zone.name}
                                    </MenuItem>
                                );
                            })}
                        </Select>
                        <small
                            style={{ color: errorInput === 'formPoint.zone' ? 'red' : 'black' }}
                            className={classes.counter}
                        >
                            {errorInput === 'formPoint.zone' ? 'Seleccione una zona' : ''}
                        </small>
                    </FormControl>
                </Grid>

            {/*Descripcion */}
            <Grid item xs={12}>
                <TextField
                    fullWidth
                    id="formPoint.description"
                    name="formPoint.description"
                    label="Descripcion"
                    value={formik.values.formPoint.description}
                    onChange={formik.handleChange}
                    error={formik.touched.formPoint && Boolean(formik.errors.formPoint)}
                    helpertext={formik.touched.formPoint && formik.errors.formPoint}
                />
                <small
                    style={{ color: errorInput === 'formPoint.description' ? 'red' : 'black' }}
                    className={classes.counter}
                >
                    {errorInput === 'formPoint.description'
                        ? 'Ingrese la descripción de la zona.'
                        : ''}
                </small>
            </Grid>
            
            { point._id !== undefined ? <>
            
             {/* Checkbox publicar */}
             <Grid item xs={4}>
                <FormControlLabel
                    className={classes.formControl}
                    control={
                        <Checkbox
                            id="formPoint.isActive"
                            checked={formik.values.formPoint.isActive}
                            value={formik.values.formPoint.isActive}
                            onChange={formik.handleChange}
                            name="formPoint.isActive"
                        />
                    }
                    label="Activo"
                />
            </Grid>
            
            </> : '' }

            </Grid>
            <Grid container spacing={3}>
            <Grid col xs={6}>
            <FormLabel component="legend">Asignar trabajadores</FormLabel>
            { employers.length > 0 ? (
                <List className={classes.root}>
      {employers.map((value, index) => {
        const labelId = `checkbox-list-label-${index}`;

        return (
          <ListItem key={index} role={undefined} dense button onClick={handleToggleEmployers(value._id)}>
            <ListItemIcon>
              <Checkbox
                edge="start"
                checked={checkedEmployers.indexOf(value._id) !== -1}
                tabIndex={-1}
                disableRipple
                inputProps={{ 'aria-labelledby': labelId }}
              />
            </ListItemIcon>
            <ListItemText id={labelId} primary={value.names + ' ' +value.surnames} />
            
          </ListItem>
        );
      })}
    </List>
            ) : ('No hay trabajadores para asignar')}
            </Grid>
            <Grid col xs={6}>
            <FormLabel component="legend">Asignar Tickets</FormLabel>
            { tickets.length > 0 ? (
                <List className={classes.root}>
      {tickets.map((value, index) => {
        const labelId = `checkbox-list-label-${index}`;

        return (
          <ListItem key={index} role={undefined} dense button onClick={handleToggleTickets(value._id)}>
            <ListItemIcon>
              <Checkbox
                edge="start"
                checked={checkedTickets .indexOf(value._id) !== -1}
                tabIndex={-1}
                disableRipple
                inputProps={{ 'aria-labelledby': labelId }}
              />
            </ListItemIcon>
            <ListItemText id={labelId} primary={value.name} />
            
          </ListItem>
        );
      })}
    </List>
            ) : ('No hay tickets para asignar')}
            </Grid>
    </Grid>
        <div className={classes.divBtn}>
            <Button onClick={onHide} variant="primary">Cancelar</Button>
            <Button className={classes.buttonRight} type="submit" variant="contained" color="primary">
                Guardar
            </Button>
        </div>
        </form>
        </DialogContent>
    </Dialog>
      </>
      );
}

export default Point;