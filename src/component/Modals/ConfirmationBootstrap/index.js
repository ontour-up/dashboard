import React, {useState, useEffect} from 'react';
import { Modal, Button } from 'react-bootstrap';
import { BsFillExclamationCircleFill, BsFillCheckCircleFill, BsFillQuestionCircleFill } from "react-icons/bs";

function Confirmation(props) {

  const [icon, setIcon] = useState();

  useEffect(() => {
    if(props.data.title === "Error"){
      setIcon(<BsFillExclamationCircleFill className="mb-2" />);
    }else if(props.data.title === "Exito"){
      setIcon(<BsFillCheckCircleFill className="mb-2" />)
    }
    
}, [ props]);

    return (
        <>
        <Modal {...props} centered >
          <Modal.Header closeButton>
            <Modal.Title><BsFillQuestionCircleFill /> {props.data.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>{props.data.description}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={props.onCancel} variant="primary">Cancelar</Button>
            <Button onClick={props.onConfirm} variant="secondary">Confirmar</Button>
          </Modal.Footer>
        </Modal>
      </>
      );
}

export default Confirmation;