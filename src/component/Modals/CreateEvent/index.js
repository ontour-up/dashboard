/* eslint-disable react-hooks/exhaustive-deps */

import * as yup from 'yup';

import {
    Button,
    Checkbox,
    Chip,
    Dialog,
    DialogContent,
    DialogTitle,
    Divider,
    FormControl,
    FormControlLabel,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    Step,
    StepLabel,
    Stepper,
    TextField,
    Typography,
} from '@material-ui/core';
import React, { useCallback, useContext, useEffect, useState } from 'react';
import { addEvent, modifyEvent } from '../../../services/events.service';
import { format } from 'date-fns';
import { geocodeByAddress, getLatLng } from 'react-google-places-autocomplete';

import AddIcon from '@material-ui/icons/Add';
import AlertMsg from '../../Alert';
import CloseIcon from '@material-ui/icons/Close';
import DateFnsUtils from '@date-io/date-fns';
import { DateTimePicker } from '@material-ui/pickers';
import Delete from '@material-ui/icons/Delete';
import Edit from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import ImageUploading from 'react-images-uploading';
import Loading from '../../Loading';
import LocalActivityIcon from '@material-ui/icons/LocalActivity';
import LocationGM from '../../Input/LocationGM';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { URL_S3 } from '../../../store/constant';
import { UserContext } from '../../../context/UserContext';
import { addTicket } from '../../../services/ticket.service.js';
import { es } from 'date-fns/locale';
import { getChart } from '../../../services/charts.service';
import { makeStyles } from '@material-ui/core/styles';
import { takeEmailList } from '../../../services/events.service';
import { takeSpaces } from '../../../services/space.service';
import { uploadFile } from '../../../services/upload.service';
import { useFormik } from 'formik';

// Google Api

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    backButton: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    formControl: {
        width: '100%',
    },
    chip: {
        marginTop: '10px',
        marginRight: '5px',
    },
    divBtn: {
        marginTop: '30px',
        marginBottom: '20px',
    },
    buttonRight: {
        float: 'right',
    },
    buttonProgress: {
        marginLeft: '10px',
    },
    imgBanner: {
        width: '150px',
        height: '130px',
        objectFit: 'contain',
        marginTop: '10px',
        marginBottom: '10px',
    },
    ticket: {
        backgroundColor: 'white',
        boxShadow: '0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)',
        borderRadius: '10px',
        marginBottom: '30px',
        marginLeft: '10px',
        width: '600px',
        height: '70px',
    },
    textTicket: {
        marginLeft: '10px',
        fontSize: '20px',
        marginBottom: '8px',
    },
    iconAdd: {
        color: '#3366ff',
        cursor: 'pointer',
       
      },
}));

const validationSchema = yup.object({
    name: yup.string().required(),
    datePublication: yup.string().required(),
    description: yup.string().required(),
    category: yup.string().required(),
    email: yup.string().required(),
    startDate: yup.date().required(),
});

const CreateEvent = ({ open, onClose, onFinish, onRefresh, onOpenSnackBar, event, promoter }) => {
    const classes = useStyles();
    const { user } = useContext(UserContext);
    const [activeStep, setActiveStep] = useState(0);
    const [tag, setTag] = useState('');
    const [ubication] = useState({ address: '', location: '', city: '', postalCode: '', country: '', province: '', gps: { latitude: '', longitude: '' } });
    const [errorInput, setErrorInput] = useState('');
    const steps = getSteps();
    const [loading, setLoading] = useState(false);
    const [isOpenAlert, setIsOpenAlert] = useState(false);
    const [messageAlert, setMessageAlert] = useState({});
    const [emails, setEmails] = useState([]);
    const [dateStart, SetDateStart] = useState(null);
    const [dateEnd, SetDateEnd] = useState(null);
    const [dateStartEntrance, SetDateStartEntrance] = useState(null);
    const [dateEndEntrance, SetDateEndEntrance] = useState(null);
    const [dateStartSellEntrance, SetDateStartSellEntrance] = useState(null);
    const [dateEndSellEntrance, SetDateEndSellEntrance] = useState(null);
    const [expanded, setExpanded] = useState('');
    const [latitude, setLatitude] = useState('');
    const [longitude, setLongitude] = useState('');
    const [spaces, setSpaces] = useState([]);
    const [numEntrances, setNumEntrances] = useState(false);
    const [ticketsArr, setTicketsArr] = useState([]);
    const [addEntrance, setAddEntrance] = useState(false);
    const [countEntrance, setCountEntrance] = useState(false);
    const [basePrecio, setBasePrecio] = useState();
    const [ivaPrecio, setIvaPrecio] = useState();
    const [totalPrecio, setTotalPrecio] = useState();
    const [comisionPrecio, setComisionPrecio] = useState();
    const [fileUrl, setFileUrl] = useState(null);
    const [multipleDias, setMultipleDias] = useState(false);
    const [ entranceCategories, setEntranceCategories ] = useState(); 
    const [images, setImages] = useState([]);
    const maxNumber = 20;

    //-------------SELECTS-----------//
    const [eventCategories, setEventCategories] = React.useState([
        /*{ value: 'rock', label: 'Rock' },
      { value: 'pop', label: 'Pop' },
      { value: 'trap', label: 'TRAP' },*/
        { value: 'feria', label: 'Feria' },
        { value: 'circo', label: 'Circo' },
        { value: 'concert', label: 'Concierto' },
        { value: 'teatro', label: 'Teatro' },
        { value: 'ballet', label: 'Ballet' },
        { value: 'ópera', label: 'Ópera' },
        { value: 'deporte', label: 'Deporte' },
        { value: 'festival', label: 'Festival' },
        { value: 'monologo', label: 'Monólogo' },
        { value: 'herri-kirolak', label: 'Herri kirolak' },
        { value: 'talleres', label: 'Talleres' },
        { value: 'clown', label: 'Clown' },
        { value: 'danza', label: 'Danza' },
        { value: 'exposicion', label: 'Exposición' },
        { value: 'conferencia', label: 'Conferencia' },
        { value: 'presentacion', label: 'Presentación' },
    ]);

    const [eventTypes, setEventTypes] = React.useState([
        { value: 'presential', label: 'Presencial' },
        { value: 'streaming', label: 'Streaming' },
        { value: 'rent', label: 'Alquiler' },
        { value: 'online', label: 'On-line' },
    ]);

    const [typesIva, setTypesIva] = React.useState([
        { value: 0, label: '0%' },
        { value: 10, label: '10%' },
        { value: 21, label: '21%' },
    ]);

    const LandingLanguagesList = [
        { name: 'language', value: 'En Blanco', label: 'En blanco' },
        { name: 'language', value: 'Euskera', label: 'Euskera' },
        { name: 'language', value: 'Castellano', label: 'Castellano' },
        { name: 'language', value: 'Ingles', label: 'Ingles' },
    ];

    const InfoLanguagesList = [
        { name: 'language', value: 'En Blanco', label: 'En blanco' },
        { name: 'language', value: 'Euskera', label: 'Euskera' },
        { name: 'language', value: 'Castellano', label: 'Castellano' },
        { name: 'language', value: 'Bilingue', label: 'Bilingue' },
        { name: 'language', value: 'Multilingue', label: 'Multilingue' },
        { name: 'language', value: 'Obra Muda', label: 'Obra Muda' },
        { name: 'language', value: 'Ingles', label: 'Ingles' },
        { name: 'language', value: 'Frances', label: 'Frances' },
        { name: 'language', value: 'Otros', label: 'Otros' },
    ];

    //-------------STEPS-----------//
    function getSteps() {
        if(event === null){
            return ['Información Básica', 'Información Avanzada', 'Detalles', 'Galeria', 'Gestión de entradas'];
        }else{
            return ['Información Básica', 'Información Avanzada', 'Detalles', 'Galeria'];
        }
    }
    
    function getStepContent(stepIndex) {
        switch (stepIndex) {
            case 0:
                return 'Ingresa la información básica del evento';
            case 1:
                return 'Información Avanzada';
            case 2:
                return 'Ingrese los detalles del evento';
            case 3:
                return 'Ingrese las imagenes de cabecera';
            case 3:
                return 'Cree y gestione las entradas del evento';
            default:
                return 'Unknown stepIndex';
        }
    }

    //--------------------//

    useEffect(() => {
        resetForm();
        takeSpacesList();
        takeEmails();
        resetElements();
        SetDateStartEntrance();
        SetDateEndEntrance();
        SetDateStartSellEntrance();
        SetDateEndSellEntrance();
        handleDateSChange(new Date());
        handleDateEChange(new Date());

         if (event !== null) {
             loadData();
         }
    }, [open]);

    const resetElements = () => {
        setActiveStep(0);
        SetDateStart(null);
        SetDateEnd(null);
        SetDateStartEntrance(null);
        SetDateEndEntrance(null);
        SetDateStartSellEntrance(null);
        SetDateEndSellEntrance(null);
        setExpanded('');
    };

    const handleDateSChange = (e) => {
        SetDateStart(e);
        if (e !== '' && e !== null) {
            formik.setFieldValue('formEvent.startDate', e.toISOString());
        } else {
            formik.setFieldValue('formEvent.startDate', e);
        }
    };

    const handleDateEChange = (e) => {
        SetDateEnd(e);
        if (e !== '' && e !== null) {
            formik.setFieldValue('formEvent.endDate', e.toISOString());
        } else {
            formik.setFieldValue('formEvent.endDate', e);
        }
    };

    /** * fechas entradas */
    const handleDateStartEntranceChange = (e) => {
        SetDateStartEntrance(e);
        if (e !== '' && e !== null) {
            formik.setFieldValue('formTicket.startDate', e.toISOString());
        } else {
            formik.setFieldValue('formTicket.startDate', e);
        }
    };

    const handleDateEndEntranceChange = (e) => {
        SetDateEndEntrance(e);
        if (e !== '' && e !== null) {
            formik.setFieldValue('formTicket.endDate', e.toISOString());
        } else {
            formik.setFieldValue('formTicket.endDate', e);
        }
    };

    const handleDateStartSellEntranceChange = (e) => {
        SetDateStartSellEntrance(e);
        if (e !== '' && e !== null) {
            formik.setFieldValue('formTicket.sellStartDate', e.toISOString());
        } else {
            formik.setFieldValue('formTicket.sellStartDate', e);
        }
    };

    const handleDateEndSellEntranceChange = (e) => {
        SetDateEndSellEntrance(e);
        if (e !== '' && e !== null) {
            formik.setFieldValue('formTicket.sellEndDate', e.toISOString());
        } else {
            formik.setFieldValue('formTicket.sellEndDate', e);
        }
    };

    const takeEntranceCategories = async (spaceId) => {
        const query = {
            _id : spaceId
        };
        const { status, data } = await takeSpaces(query);
        if (status === 200) {
            try{
                const space = await(getChart(data.data[0].chartKey));
                setEntranceCategories(space.categories.list);
            } catch(e){

            }
        } 
        
    };

    const takeSpacesList = async () => {
        const { status, data } = await takeSpaces();
        if (status === 200) {
            setSpaces(data.data);
        } else {
            /* console.log(status, data); */
        }
    };

    const takeEmails = async () => {
        const { status, data } = await takeEmailList(user._id);
        if (status === 200) {
            setEmails(data.data);
        } else {
            /* console.log(status, data); */
        }
    };

     const loadData = () => {
         formik.setFieldValue('formEvent.name', event?.name)
         formik.setFieldValue('formEvent._id', event?._id)
         formik.setFieldValue('formEvent.description', event?.description)
         formik.setFieldValue('formEvent.tags', event?.tags)
         formik.setFieldValue('formEvent.type', event?.type)
         formik.setFieldValue('formEvent.banner', event?.banner)
         formik.setFieldValue('formEvent.category', event?.category)
         formik.setFieldValue('formEvent.location', event?.location)     
         formik.setFieldValue('formEvent.ubication', event?.ubication ? event?.ubication : ubication)
         formik.setFieldValue('formEvent.startDate', event?.startDate ? event?.startDate.toLocaleString() : '')
         formik.setFieldValue('formEvent.endDate', event?.endDate ? event?.endDate.toLocaleString() : '')
         formik.setFieldValue('formEvent.isActive', event?.isActive)
         formik.setFieldValue('formEvent.space', event?.space ? event?.space : null)
         formik.setFieldValue('formEvent.email', event?.email)
         formik.setFieldValue('formEvent.language', event?.language)
         formik.setFieldValue('formEvent.language_event', event?.language_event)
         formik.setFieldValue('formEvent.url', event?.url)
         formik.setFieldValue('formEvent.sendEmail', event?.sendEmail)
         formik.setFieldValue('formEvent.resendEmails', event?.resendEmails)
         formik.setFieldValue('formEvent.isVisible', event?.isVisible)
         formik.setFieldValue('formEvent.isWeb', event?.isWeb)
         formik.setFieldValue('formEvent.isApp', event?.isApp)
         formik.setFieldValue('formEvent.totalAmount', event?.totalAmount)
         takeSpacesList(event?.space);
         if (event?.startDate) {
             SetDateStart(format(new Date(event?.startDate.toLocaleString()), "yyy-MM-dd'T'HH:mm"))
         }
         if (event?.endDate) {
             SetDateEnd(format(new Date(event?.endDate.toLocaleString()), "yyy-MM-dd'T'HH:mm"))
         }
         if (event?.ubication) {
             setLatitude(event?.ubication.gps.latitude);
             setLongitude(event?.ubication.gps.longitude);
         }
         if(event?.galery){
            let imageArray = [];
            event.galery.forEach((img_album) => {
                    const image = { data_url: img_album };
                    imageArray.push(image);
                }
            );
            setImages(imageArray);
         }
     }

    const handleNext = () => {
        if (activeStep === 0) {
            validationFirstStep();
        } else if (activeStep === 1) {
            validationSecondStep();
        } else if (activeStep === 2) {
            validationThreeStep();
        } else if (countEntrance) {
            if (activeStep === 3) {
                validationThreeStep();
            }
        } else {
            setActiveStep(4);
            if (activeStep === 4) {
                setActiveStep((prevActiveStep) => prevActiveStep + 1);
            }
        }
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
        resetForm();
        cleanUbication();

    };

    const handleClose = (event, reason) => {
        if (reason !== 'backdropClick') {
            onClose();
        }
    };

    const handleAddTag = (e) => {
        if (tag === undefined || tag === "") return;
        formik.setFieldValue('formEvent.tags', [...formik.values.formEvent.tags, tag]);
        setTag('');
    };

    const handleDeleteTag = (index) => {
        const filterTags = formik.values.formEvent.tags.filter((tag, i) => i !== index);
        formik.setFieldValue('formEvent.tags', filterTags);
        setTag('');
    };

    const handleImages = (imageList, addUpdateIndex) => {
        // data for submit
        setImages(imageList);
    };

    const changeAddressHandler = (e) => {
        let directList = e.value.terms;
        let direction = e.value.description;
        if (directList.length > 0) {
            cleanUbication();
            let direArray = direction.split(',');
            if (direArray.length > 1) {
                direArray = direArray.splice(0, direArray.length - 2);
                ubication.address = direArray.toString();
            } else {
                ubication.address = direction;
            }
            geocodeByAddress(direction).then((results) => {
                const direccion = results[0]['address_components'];
                direccion.forEach((item) => {
                    const type = item.types;
                    type.forEach((itep) => {
                        if (itep === 'locality') {
                            ubication.city = item.long_name;
                        }
                        if (itep === 'country') {
                            ubication.country = item.long_name;
                        }
                        if (itep === 'postal_code') {
                            ubication.postalCode = item.long_name;
                        }
                        if (itep === 'administrative_area_level_2') {
                            ubication.province = item.long_name;
                        }
                    });
                });
                getLatLng(results[0]).then(({ lat, lng }) => {
                    ubication.gps.latitude = lat;
                    ubication.gps.longitude = lng;
                    setLatitude(lat);
                    setLongitude(lng);
                });
                formik.setFieldValue('formEvent.ubication', ubication);
            });
        }
    };

    const cleanUbication = () => {
        ubication.address = '';
        ubication.city = '';
        ubication.country = '';
        ubication.postalCode = '';
        ubication.gps.latitude = '';
        ubication.gps.longitude = '';
    };

    const handleDescription = (e) => {
        formik.setFieldValue('formEvent.description', e.target.value);
    };

    const handleChangeLatitude = (e) => {
        setLatitude(e.target.value);
        formik.setFieldValue('formEvent.ubication.gps.latitude', e.target.value);
    };

    const handleChangeLongitude = (e) => {
        setLongitude(e.target.value);
        formik.setFieldValue('formEvent.ubication.gps.longitude', e.target.value);
    };

    const handleChangeEmail = (e) => {
        // takeSpacesList(e.target.value);
        formik.setFieldValue('formEvent.email', e.target.value);
    };

    const validationFirstStep = () => {
        setErrorInput('');
        /* console.log(formik.values.formEvent); */
        let count = 0;
        if (formik.values.formEvent.name === '') {
            setErrorInput('formEvent.name');
            count++;
        } else if (formik.values.formEvent.email === '') {
            setErrorInput('formEvent.email');
            count++;
        }

        if (count === 0) {
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
    };

    const validationSecondStep = () => {
        setErrorInput('');
        let count = 0;
        /* console.log(formik.values.formEvent); */
        if (formik.values.formEvent.location === '') {
            setErrorInput('formEvent.location');
            count++;
        } else if (formik.values.formEvent.ubication.address === '') {
            setErrorInput('formEvent.address');
            count++;
        } else if (formik.values.formEvent.ubication.postalCode === '') {
            setErrorInput('formEvent.postalCode');
            count++;
        } else if (formik.values.formEvent.ubication.city === '') {
            setErrorInput('formEvent.city');
            count++;
        } else if (formik.values.formEvent.ubication.country === '') {
            setErrorInput('formEvent.country');
            count++;
        } else if (formik.values.formEvent.ubication.gps.latitude === '') {
            setErrorInput('formEvent.latitude');
            count++;
        } else if (formik.values.formEvent.ubication.gps.longitude === '') {
            setErrorInput('formEvent.longitude');
            count++;
        }

        if (count === 0) {
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
    };

    const validationThreeStep = () => {
        setErrorInput('');
        let count = 0;
        /* console.log(formik.values.formEvent); */
        if (formik.values.formEvent.file === '') {
            if(event === null){
                setErrorInput('formEvent.file');
                count++;
            }
            
        } else if (formik.values.formEvent.description === '') {
            setErrorInput('formEvent.description');
            count++;
        } else if (formik.values.formEvent.isChart === true) {
            if (formik.values.formEvent.space === null) {
                setErrorInput('formEvent.space');
                count++;
            }
        }
        console.log(formik.values.formEvent.totalAmount);
        if (count === 0) {
            if(event !== null){
                setActiveStep((prevActiveStep) => prevActiveStep + 1);
            }else{
                setActiveStep((prevActiveStep) => prevActiveStep + 1);
            }
        }
    };

    const generateSlug = (text) => {
        let slug = text;
        if (slug != '') {
            slug = slug.toLowerCase();
            slug = slug.trim();
            slug = slug.replace('"', '');
            slug = slug.replace("'", '');
            slug = slug.replace(',', '');
            slug = slug.replace('&', '');
            slug = slug.replace('“', '');
            slug = slug.replace('”', '');
            slug = slug.replace(':', '');
            slug = slug.replace('+', '');
            slug = slug.replace('/', '');
            slug = slug.replace('!', '');
            slug = slug.replace('¡', '');
            slug = slug.replace('*', '');
            slug = slug.replace('í', 'i');
            slug = slug.replace('á', 'a');
            slug = slug.replace('é', 'e');
            slug = slug.replace('ó', 'o');
            slug = slug.replace('ú', 'u');
            slug = slug.replace(/["']/g, '');
            slug = slug.replace(/\s+/g, '-');
        }
        return slug;
    };

    const uploadS3 = async (images) => {
        let urlfile = '';
        let imageArray = [];
        await Promise.all(images.map(async image => {
            if (image.file) {
                const formData = new FormData();
                formData.append('file', image.file);
                const { status } = await uploadFile(formData);
                if (status === 201) {
                    urlfile = URL_S3 + image.file.name;
                } else {
                    urlfile = '';
                }
                imageArray.push(urlfile);
            } else {
                imageArray.push(image.data_url);
            }

        }));
        return imageArray;
    }

    /* crear evento */
    const onSubmit = async (valuesEvent, valuesTicket) => {
        setLoading(true);
        let urlfile = '';
        if (valuesEvent.file !== '') {
            const formData = new FormData();
            formData.append('file', valuesEvent.file);
            const { status } = await uploadFile(formData);
            if (status === 201) {
                urlfile = URL_S3 + valuesEvent.file.name;
            } else {
                urlfile = '';
            }
            valuesEvent.banner = urlfile;
        }
        if (images.length > 0 ) {
            valuesEvent.galery = await uploadS3(images);
        }

        // if (values.startDate !== '' && values.startDate !== null) {
        //     values.startDate = values.startDate + ':00.000Z';
        // }
        // if (values.endDate !== '' && values.endDate !== null) {
        //     values.endDate = values.endDate + ':00.000Z';
        // }
        if (valuesEvent.ubication.gps.latitude !== '' && valuesEvent.ubication.gps.latitude !== null) {
            valuesEvent.ubication.gps.latitude = latitude.toString();
            /* console.log('LAT', values.ubication.gps.latitude); */
        }
        if (valuesEvent.ubication.gps.longitude !== '' && valuesEvent.ubication.gps.longitude !== null) {
            valuesEvent.ubication.gps.longitude = longitude.toString();
            /* console.log('LON', typeof values.ubication.gps.longitude); */
        }

        if (valuesEvent.location !== '' && valuesEvent.location !== null) {
          valuesEvent.ubication.location = valuesEvent.location;
        }

        // Si url esta vacio, generamos nosotros uno
        if (valuesEvent.url == '') {
            valuesEvent.url = valuesEvent.name + '-' + valuesEvent.language;
        }

        let dataL;
        let statusL;
        let message;
        if (event?._id !== undefined && event?._id !== '') {
            const { status, data } = await modifyEvent(event?._id, valuesEvent);
            dataL = data.data;
            statusL = status;
            message = data.message;
        } else {
            delete valuesEvent._id;
            const { status, data } = await addEvent(valuesEvent);
            dataL = data.data;
            statusL = status;
            message = data.message;
        }
        if (dataL) {
            ticketsArr.forEach((element) => {
                const ticketQuery = {
                    name: element[0],
                    description: element[1],
                    amount: element[2],
                    commission: element[3],
                    amountPerPerson: element[4],
                    category: element[5],
                    chartCategory: element[6],
                    grossPrice: element[7],
                    netPrice: element[8],
                    ivaPercent: element[9],
                    startDate: element[10],
                    endDate: element[11],
                    sellStartDate: element[12],
                    sellEndDate: element[13],
                    type: element[14],
                    promoter: user._id,
                    event: dataL._id,
                };
                const { status, data } = addTicket(ticketQuery);
            });
        }
        if (statusL === 200 || statusL === 201) {
            onOpenSnackBar({ severity: 'success', msg: message });
            setLoading(false);
            onRefresh();
            resetForm();
            onFinish();
        } else {
            setMessageAlert({ severity: 'warning', msg: message });
            setIsOpenAlert(true);
            setLoading(false);
        }
    };

    const resetForm = useCallback(async () => {
        formik.handleReset();
        setTicketsArr([]);
        cleanUbication([]);
        setImages([]);
    });

    const formik = useFormik({
        initialValues: {
            formEvent: {
                ubication: ubication,
                banner: '',
                category: ['concert'],
                startDate: new Date(),
                endDate: new Date(),
                description: '',
                galery: [],
                layout: 'default',
                location: '',
                name: '',
                tags: [],
                file: '',
                ticketRestrictions: [],
                ticketSell: true,
                registry: true,
                invite: true,
                type: 'presential',
                publishDate: new Date(),
                intervalType: 'oneDay',
                sessionsType: 'oneSession',
                publicationMethod: 'now',
                published: false,
                promoter: user._id,
                isVisible: true,
                isApp: true,
                isWeb: true,
                isChart: false,
                space: null,
                email: '',
                language: 'Euskera',
                language_event: 'Euskera',
                validationTickets: false,
                sendEmail: '',
                resendEmails: '',
                url: '',
                totalAmount: 0,
            },
            formTicket: {
                name: '',
                description: '',
                html: '',
                image: '',
                amount: 0,
                commission: 0,
                amountPerPerson: 0,
                category: '',
                chartCategory: '',
                currency: '€',
                grossPrice: 0,
                netPrice: 0,
                ivaPercent: 0,
                startDate: new Date(),
                endDate: new Date(),
                sellStartDate: new Date(),
                sellEndDate: new Date(),
                initialBalance: 0,
                type: 'presential',
                url: '',
                qr: '',
                promoter: '',
                event: '',
                isActive: true,
            },
        },
        // validationSchema: validationSchema,
        onSubmit: (values) => {
            onSubmit(values.formEvent, values.formTicket);
            /*  console.log(values); */
        },
    });

    const handleAddEntrance = () => {
        setLoading(true);

        let name = formik.values.formTicket.name;
        let description = formik.values.formTicket.description;
        let amount = formik.values.formTicket.amount;
        let commission = formik.values.formTicket.commission;
        let amountPerPerson = formik.values.formTicket.amountPerPerson;
        let category = formik.values.formTicket.category;
        let chartCategory = formik.values.formTicket.chartCategory;
        let netPrice = formik.values.formTicket.netPrice;
        let ivaPercent = formik.values.formTicket.ivaPercent;

        /*
        var ivaPercentCalc = formik.values.formTicket.ivaPercent / 100 + 1;
        var importeIVA = netPrice / ivaPercentCalc;
        var base = Math.round(importeIVA * 100) / 100;

        let grossPrice = base;
        */
       // El en dashboard viejo el netPrice y el grossPrice son iguales
        let grossPrice = formik.values.formTicket.netPrice;


        let startDate = formik.values.formTicket.startDate;
        let endDate = formik.values.formTicket.endDate;
        let sellStartDate = formik.values.formTicket.sellStartDate;
        let sellEndDate = formik.values.formTicket.sellEndDate;
        let type = formik.values.formTicket.type;

        const arrayTickets = [
            name,
            description,
            amount,
            commission,
            amountPerPerson,
            category,
            chartCategory,
            grossPrice,
            netPrice,
            ivaPercent,
            startDate,
            endDate,
            sellStartDate,
            sellEndDate,
            type,
        ];
        ticketsArr.push(arrayTickets);
        /* console.log("array tickets",ticketsArr.length);  */
        setNumEntrances(true);
        //onRefresh();
        setCountEntrance(false);
        cleanTicket();
        setLoading(false);
    };

    const showFormEntrance = () => {
        takeEntranceCategories(formik.values.formEvent.space)

        setAddEntrance(true);
        setCountEntrance(true);
    };

    const hideFormEntrance = () => {
        setAddEntrance(false);
        setCountEntrance(false);
        setTicketsArr([]);
        //onRefresh();
        resetForm();
    };

    const calculoPrecio = () => {
        var netPrice = formik.values.formTicket.netPrice;

        var ivaPercent = formik.values.formTicket.ivaPercent / 100 + 1;

        var importeIVA = netPrice / ivaPercent;

        var precioIVAIncluido = netPrice - importeIVA;

        var base = Math.round(importeIVA * 100) / 100;

        var iva = Math.round(precioIVAIncluido * 100) / 100;

        var comision = (netPrice * formik.values.formTicket.commission) / 100;

        var total = base + comision + iva;

        formik.setFieldValue('formTicket.grossPrice', base)

        setBasePrecio(base);
        setIvaPrecio(iva);
        setComisionPrecio(comision);
        setTotalPrecio(total);
    };

    const cleanTicket = () => {
        formik.setFieldValue('formTicket.name', '');
        formik.setFieldValue('formTicket.description', '');
        formik.setFieldValue('formTicket.amount', 0);
        formik.setFieldValue('formTicket.commission', 0);
        formik.setFieldValue('formTicket.netPrice', 0);
        formik.setFieldValue('formTicket.amountPerPerson', 0);
    };

    const MultiDias = () => {
        setMultipleDias(true);
        if (multipleDias === true) {
            setMultipleDias(false);
        }
    };

    return (
        <>
        {loading === true ? (
          <>
            <Loading show={loading} />
            </>
        ) : (
            <>
            <Dialog open={open} onClose={handleClose} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
                {/* <img src="https://www.clipartmax.com/png/full/3-35123_christmas-lights-png-transparent-images-png-all-christmas-garland-border-transparent.png" /> */}
                <DialogTitle>
                    <Grid container direction="row" justifyContent="space-between" alignItems="center">
                        {event ? 'Edita evento '+event._id : 'Crear nuevo evento'}
                        <IconButton aria-label="close" onClick={handleClose}>
                            <CloseIcon />
                        </IconButton>
                    </Grid>
                </DialogTitle>
                <DialogContent>
                    <Stepper activeStep={activeStep} alternativeLabel>
                        {steps.map((label) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                    <div>
                        <form encType="multipart/form-data" onSubmit={formik.handleSubmit}>
                            {activeStep === steps.length ? (
                                <>
                                <Typography className={classes.instructions}>Todos los pasos completados</Typography>
                                {event === null || event.length === 0 ? (
                                    <Grid container spacing={3}>
                                    {/* Checkbox publicar */}
                                    <Grid item xs={4}>
                                        <FormControlLabel
                                            className={classes.formControl}
                                            control={
                                                <Checkbox
                                                    id="formEvent.isVisible"
                                                    checked={formik.values.formEvent.isVisible}
                                                    value={formik.values.formEvent.isVisible}
                                                    onChange={formik.handleChange}
                                                    name="formEvent.isVisible"
                                                />
                                            }
                                            label="Publicar"
                                        />
                                    </Grid>

                                    {/* Checkbox visible web*/}
                                    <Grid item xs={4}>
                                        <FormControlLabel
                                            className={classes.formControl}
                                            control={
                                                <Checkbox
                                                    id="formEvent.isWeb"
                                                    checked={formik.values.formEvent.isWeb}
                                                    value={formik.values.formEvent.isWeb}
                                                    onChange={formik.handleChange}
                                                    name="formEvent.isWeb"
                                                />
                                            }
                                            label="Visible para la web"
                                        />
                                    </Grid>

                                    {/* Checkbox viseble app*/}
                                    <Grid item xs={4}>
                                        <FormControlLabel
                                            className={classes.formControl}
                                            control={
                                                <Checkbox
                                                    id="formEvent.isApp"
                                                    checked={formik.values.formEvent.isApp}
                                                    value={formik.values.formEvent.isApp}
                                                    onChange={formik.handleChange}
                                                    name="formEvent.isApp"
                                                />
                                            }
                                            label="Visible para la App"
                                        />
                                    </Grid>
                                </Grid>
                                ) : ('')}
                                    
                                        <div className={classes.divBtn}>
                                            <Button onClick={handleReset}>Retroceder</Button>
                                            <Button className={classes.buttonRight} type="submit" variant="contained" color="primary">
                                                Guardar
                                                {/* {isLoading && (
                                                    <CircularProgress color="secondary" size={24} className={classes.buttonProgress} />
                                                )} */}
                                            </Button>
                                        </div>

                                </>
                            ) : (
                                <></>
                            )}

                            {/* informacion basica */}
                            {activeStep === 0 ? (
                                <div>
                                    <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                                    <Grid container spacing={3}>
                                        {/* nombre evento */}
                                        <Grid item xs={12}>
                                            <TextField
                                                fullWidth
                                                id="formEvent.name"
                                                name="formEvent.name"
                                                label="Nombre del evento"
                                                value={formik.values.formEvent.name}
                                                onChange={formik.handleChange}
                                                error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                            />
                                            <small
                                                style={{ color: errorInput === 'formEvent.name' ? 'red' : 'black' }}
                                                className={classes.counter}
                                            >
                                                {errorInput === 'formEvent.name' ? 'Ingrese el título.' : ''}
                                            </small>
                                        </Grid>

                                        {/* Plantilla de email */}
                                        <Grid item xs={4}>
                                            <FormControl className={classes.formControl}>
                                                <InputLabel id="label-festival">Plantilla de email</InputLabel>
                                                <Select
                                                    labelId="label-email"
                                                    id="formEvent.email"
                                                    name="formEvent.email"
                                                    value={formik.values.formEvent.email}
                                                    onChange={handleChangeEmail}
                                                    error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                    helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                                >
                                                    {emails.map((email, index) => {
                                                        return (
                                                            <MenuItem key={index} value={email._id}>
                                                                {email.name}
                                                            </MenuItem>
                                                        );
                                                    })}
                                                </Select>
                                                <small
                                                    style={{ color: errorInput === 'formEvent.email' ? 'red' : 'black' }}
                                                    className={classes.counter}
                                                >
                                                    {errorInput === 'formEvent.email' ? 'Seleccione la plantilla.' : ''}
                                                </small>
                                            </FormControl>
                                        </Grid>

                                        {/* Categoría */}
                                        <Grid item xs={4}>
                                            <FormControl className={classes.formControl}>
                                                <InputLabel id="label-category">Categoría</InputLabel>
                                                <Select
                                                    labelId="label-category"
                                                    id="formEvent.category"
                                                    name="formEvent.category"
                                                    value={formik.values.formEvent.category}
                                                    onChange={formik.handleChange}
                                                    error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                    helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                                >
                                                    {eventCategories.map((cagegorie, index) => {
                                                        return (
                                                            <MenuItem key={index} value={cagegorie.value}>
                                                                {cagegorie.label}
                                                            </MenuItem>
                                                        );
                                                    })}
                                                </Select>
                                                <small
                                                    style={{ color: errorInput === 'category' ? 'red' : 'black' }}
                                                    className={classes.counter}
                                                >
                                                    {errorInput === 'category' ? 'Seleccione la categoría' : ''}
                                                </small>
                                            </FormControl>
                                        </Grid>

                                        {/* Tipo de evento */}
                                        <Grid item xs={4}>
                                            <FormControl className={classes.formControl}>
                                                <InputLabel id="label-category">Tipo de evento</InputLabel>
                                                <Select
                                                    labelId="label-type"
                                                    id="formEvent.type"
                                                    name="formEvent.type"
                                                    value={formik.values.formEvent.type}
                                                    onChange={formik.handleChange}
                                                    error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                    helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                                >
                                                    {eventTypes.map((type, index) => {
                                                        return (
                                                            <MenuItem key={index} value={type.value}>
                                                                {type.label}
                                                            </MenuItem>
                                                        );
                                                    })}
                                                </Select>
                                                <small
                                                    style={{ color: errorInput === 'category' ? 'red' : 'black' }}
                                                    className={classes.counter}
                                                >
                                                    {errorInput === 'category' ? 'Seleccione la categoría' : ''}
                                                </small>
                                            </FormControl>
                                        </Grid>

                                        {/* Idioma Landing */}
                                        <Grid item xs={6}>
                                            <FormControl className={classes.formControl}>
                                                <InputLabel id="label-landing-lang">Idioma Landing</InputLabel>
                                                <Select
                                                    labelId="language"
                                                    id="formEvent.language"
                                                    name="formEvent.language"
                                                    value={formik.values.formEvent.language}
                                                    onChange={formik.handleChange}
                                                    error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                    helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                                >
                                                    {LandingLanguagesList.map((language, index) => {
                                                        return (
                                                            <MenuItem key={index} value={language.value}>
                                                                {language.label}
                                                            </MenuItem>
                                                        );
                                                    })}
                                                </Select>
                                                <small
                                                    style={{ color: errorInput === 'category' ? 'red' : 'black' }}
                                                    className={classes.counter}
                                                >
                                                    {errorInput === 'category' ? 'Seleccione la categoría' : ''}
                                                </small>
                                            </FormControl>
                                        </Grid>

                                        {/* Idioma Informativo */}
                                        <Grid item xs={6}>
                                            <FormControl className={classes.formControl}>
                                                <InputLabel id="label-info-lang">Idioma Informativo</InputLabel>
                                                <Select
                                                    labelId="language_event"
                                                    id="formEvent.language_event"
                                                    name="formEvent.language_event"
                                                    value={formik.values.formEvent.language_event}
                                                    onChange={formik.handleChange}
                                                    error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                    helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                                >
                                                    {InfoLanguagesList.map((language_event, index) => {
                                                        return (
                                                            <MenuItem key={index} value={language_event.value}>
                                                                {language_event.label}
                                                            </MenuItem>
                                                        );
                                                    })}
                                                </Select>
                                                <small
                                                    style={{ color: errorInput === 'category' ? 'red' : 'black' }}
                                                    className={classes.counter}
                                                >
                                                    {errorInput === 'category' ? 'Seleccione la categoría' : ''}
                                                </small>
                                            </FormControl>
                                        </Grid>

                                        {/* Checkbox multiples dias */}
                                        {/*  <Grid item xs={8}>
                                            <FormControlLabel
                                                className={classes.formControl}
                                                control={<Checkbox onChange={MultiDias} />}
                                                label="Multiples Dias"
                                            />
                                       
                                         </Grid> */}

                                        {/* fecha inicio */}
                                        <Grid item xs={6}>
                                            <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                                                <DateTimePicker
                                                    autoOk
                                                    fullWidth
                                                    clearable
                                                    id="formEvent.startDate"
                                                    name="formEvent.startDate"
                                                    ampm={false}
                                                    value={dateStart}
                                                    onChange={handleDateSChange}
                                                    format="yyyy-MM-dd HH:mm"
                                                    label="Fecha y hora de inicio"
                                                    minDate={new Date()}
                                                />
                                            </MuiPickersUtilsProvider>
                                        </Grid>

                                         {/* fecha Fin */}
                                         <Grid item xs={6}>
                                            <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                                                <DateTimePicker
                                                    autoOk
                                                    fullWidth
                                                    clearable
                                                    id="formEvent.endDate"
                                                    name="formEvent.endDate"
                                                    ampm={false}
                                                    value={dateEnd}
                                                    onChange={handleDateEChange}
                                                    format="yyyy-MM-dd HH:mm"
                                                    label="Fecha y hora de finalización"
                                                    minDate={formik.values.formEvent.startDate}
                                                    minDateMessage={'la fecha de finalización no puede ser menor que fecha inicio'}
                                                />
                                            </MuiPickersUtilsProvider>
                                        </Grid>

                                        {/* {multipleDias ? (
                                            <Grid item xs={6}>
                                                <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                                                    <DateTimePicker
                                                        autoOk
                                                        fullWidth
                                                        clearable
                                                        id="formEvent.endDate"
                                                        name="formEvent.endDate"
                                                        ampm={false}
                                                        value={dateEnd}
                                                        onChange={handleDateEChange}
                                                        format="yyyy-MM-dd HH:mm"
                                                        label="Fecha y hora de finalización"
                                                        minDate={formik.values.formEvent.startDate}
                                                        minDateMessage={'la fecha de finalización no puede ser menor que fecha inicio'}
                                                    />
                                                </MuiPickersUtilsProvider>
                                            </Grid>
                                        ) : (
                                            <></>
                                        )} */}
                            {/* Editando evento */}
                            {  event?._id !== undefined && event?._id !== '' ? (

                                   <>
                                        {/* Checkbox publicar */}
                                        <Grid item xs={4}>
                                            <FormControlLabel
                                                className={classes.formControl}
                                                control={
                                                    <Checkbox
                                                        id="formEvent.isVisible"
                                                        checked={formik.values.formEvent.isVisible}
                                                        value={formik.values.formEvent.isVisible}
                                                        onChange={formik.handleChange}
                                                        name="formEvent.isVisible"
                                                    />
                                                }
                                                label="Publicarss"
                                            />
                                        </Grid>

                                        {/* Checkbox visible web*/}
                                        <Grid item xs={4}>
                                            <FormControlLabel
                                                className={classes.formControl}
                                                control={
                                                    <Checkbox
                                                        id="formEvent.isWeb"
                                                        checked={formik.values.formEvent.isWeb}
                                                        value={formik.values.formEvent.isWeb}
                                                        onChange={formik.handleChange}
                                                        name="formEvent.isWeb"
                                                    />
                                                }
                                                label="Visible para la web"
                                            />
                                        </Grid>

                                        {/* Checkbox viseble app*/}
                                        <Grid item xs={4}>
                                            <FormControlLabel
                                                className={classes.formControl}
                                                control={
                                                    <Checkbox
                                                        id="formEvent.isApp"
                                                        checked={formik.values.formEvent.isApp}
                                                        value={formik.values.formEvent.isApp}
                                                        onChange={formik.handleChange}
                                                        name="formEvent.isApp"
                                                    />
                                                }
                                                label="Visible para la App"
                                            />
                                        </Grid>
                                                </>
                            ) : (
                                <></>
                            )}
                                       
                                    </Grid>
                                    <div className={classes.divBtn}>
                                        <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>
                                            Atras
                                        </Button>
                                        <Button variant="contained" color="primary" onClick={handleNext}>
                                            {activeStep === steps.length - 1 ? 'Finish' : 'Siguiente'}{' '}
                                        </Button>
                                        { event ? (
                                            <Button className={classes.buttonRight} type="submit" variant="contained" color="primary">
                                            Guardar
                                        </Button>
                                        ) : ('')}
                                    </div>
                                </div>
                            ) : (
                                <></>
                            )}

                            {/* Informacion avanzada */}
                            {activeStep === 1 ? (
                                <>
                                    <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                                    <Grid container spacing={3}>
                                        {/* tags */}
                                        <Grid item xs={6}>
                                            <FormControl className={classes.formControl}>
                                                <TextField
                                                    fullWidth
                                                    id="formEvent.tags"
                                                    name="formEvent.tags"
                                                    label="Tags"
                                                    value={tag}
                                                    onChange={(e) => {
                                                        setTag(e.target.value);
                                                    }}
                                                    InputProps={{endAdornment:  <AddIcon className={classes.iconAdd} onClick={handleAddTag} />}}
                                                    //onKeyPress={handleAddTag}
                                                />

                                                <small
                                                    style={{ color: errorInput === 'formEvent.tags' ? 'red' : 'black' }}
                                                    className={classes.counter}
                                                >
                                                    {errorInput === 'formEvent.tags' ? 'Ingrese un tag.' : ''}
                                                </small>
                                            </FormControl>
                                            {formik.values.formEvent.tags &&
                                                formik.values.formEvent.tags.map((tag, i) => {
                                                    return (
                                                        <Chip
                                                            size="small"
                                                            key={i}
                                                            label={tag}
                                                            color="primary"
                                                            onDelete={() => handleDeleteTag(i)}
                                                            className={classes.chip}
                                                        />
                                                    );
                                                })}
                                        </Grid>

                                        {/* lugar */}
                                        <Grid item xs={6}>
                                            <TextField
                                                fullWidth
                                                id="formEvent.location"
                                                name="formEvent.location"
                                                label="Lugar"
                                                value={formik.values.formEvent.location}
                                                onChange={formik.handleChange}
                                                error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                            />
                                            <small
                                                style={{ color: errorInput === 'formEvent.location' ? 'red' : 'black' }}
                                                className={classes.counter}
                                            >
                                                {errorInput === 'formEvent.location' ? 'Ingrese el lugar' : ''}
                                            </small>
                                        </Grid>

                                        {/* ubicación */}
                                        <Grid item xs={12}>
                                            <Typography component="p">Ubicación</Typography>
                                            <FormControl className={classes.formControl}>
                                                <LocationGM labelId="label-ubication" onChange={changeAddressHandler} />
                                                <small
                                                    style={{ color: errorInput === 'formEvent.address' ? 'red' : 'black' }}
                                                    className={classes.counter}
                                                >
                                                    {errorInput === 'formEvent.address' ? 'Ingrese una ubicación' : ''}
                                                </small>
                                            </FormControl>
                                        </Grid>
                                        {formik.values.formEvent.ubication?.address !== '' ||
                                        formik.values.formEvent.ubication?.city !== '' ||
                                        formik.values.formEvent.ubication?.country !== '' ? (
                                            <>
                                                {/*lugar */}
                                                <Grid item xs={6}>
                                                    <TextField
                                                        fullWidth
                                                        id="formEvent.ubication.address"
                                                        name="formEvent.ubication.address"
                                                        label="Dirección"
                                                        value={formik.values.formEvent.ubication.address}
                                                        onChange={formik.handleChange}
                                                        error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                        helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                                    />
                                                    <small
                                                        style={{ color: errorInput === 'formEvent.address' ? 'red' : 'black' }}
                                                        className={classes.counter}
                                                    >
                                                        {errorInput === 'formEvent.address' ? 'Ingrese el lugar' : ''}
                                                    </small>
                                                </Grid>

                                                {/*CP */}
                                                <Grid item xs={6}>
                                                    <TextField
                                                        fullWidth
                                                        id="formEvent.ubication.postalCode"
                                                        name="formEvent.ubication.postalCode"
                                                        label="Código postal"
                                                        value={formik.values.formEvent.ubication.postalCode}
                                                        onChange={formik.handleChange}
                                                        error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                        helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                                    />
                                                    <small
                                                        style={{ color: errorInput === 'formEvent.postalCode' ? 'red' : 'black' }}
                                                        className={classes.counter}
                                                    >
                                                        {errorInput === 'formEvent.postalCode' ? 'Ingrese el codigo postal' : ''}
                                                    </small>
                                                </Grid>

                                                {/*ciudad */}
                                                <Grid item xs={6}>
                                                    <TextField
                                                        fullWidth
                                                        id="formEvent.ubication.city"
                                                        name="formEvent.ubication.city"
                                                        label="Ciudad"
                                                        value={formik.values.formEvent.ubication.city}
                                                        onChange={formik.handleChange}
                                                        error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                        helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                                    />
                                                    <small
                                                        style={{ color: errorInput === 'formEvent.city' ? 'red' : 'black' }}
                                                        className={classes.counter}
                                                    >
                                                        {errorInput === 'formEvent.city' ? 'Ingrese la ciudad' : ''}
                                                    </small>
                                                </Grid>

                                                {/*provincia */}
                                                <Grid item xs={6}>
                                                    <TextField
                                                        fullWidth
                                                        id="formEvent.ubication.province"
                                                        name="formEvent.ubication.province"
                                                        label="Provincia"
                                                        value={formik.values.formEvent.ubication.province}
                                                        onChange={formik.handleChange}
                                                        error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                        helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                                    />
                                                    <small
                                                        style={{ color: errorInput === 'formEvent.province' ? 'red' : 'black' }}
                                                        className={classes.counter}
                                                    >
                                                        {errorInput === 'formEvent.province' ? 'Ingrese la provincia' : ''}
                                                    </small>
                                                </Grid>

                                                {/*pais */}
                                                <Grid item xs={6}>
                                                    <TextField
                                                        fullWidth
                                                        id="formEvent.ubication.country"
                                                        name="formEvent.ubication.country"
                                                        label="País"
                                                        value={formik.values.formEvent.ubication.country}
                                                        onChange={formik.handleChange}
                                                        error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                        helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                                    />
                                                    <small
                                                        style={{ color: errorInput === 'formEvent.country' ? 'red' : 'black' }}
                                                        className={classes.counter}
                                                    >
                                                        {errorInput === 'formEvent.country' ? 'Ingrese el pais' : ''}
                                                    </small>
                                                </Grid>

                                                {/*latitud */}
                                                <Grid item xs={6}>
                                                    <TextField
                                                        fullWidth
                                                        id="formEvent.ubication.gps.latitude"
                                                        name="formEvent.ubication.gps.latitude"
                                                        label="Latitud"
                                                        value={latitude}
                                                        onChange={handleChangeLatitude}
                                                        error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                        helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                                    />
                                                    <small
                                                        style={{ color: errorInput === 'formEvent.latitude' ? 'red' : 'black' }}
                                                        className={classes.counter}
                                                    >
                                                        {errorInput === 'formEvent.latitude' ? 'Ingrese latitud' : ''}
                                                    </small>
                                                </Grid>

                                                {/*longitud */}
                                                <Grid item xs={6}>
                                                    <TextField
                                                        fullWidth
                                                        id="formEvent.ubication.gps.longitude"
                                                        name="formEvent.ubication.gps.longitude"
                                                        label="Longitud"
                                                        value={longitude}
                                                        onChange={handleChangeLongitude}
                                                        error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                        helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                                    />
                                                    <small
                                                        style={{ color: errorInput === 'formEvent.longitude' ? 'red' : 'black' }}
                                                        className={classes.counter}
                                                    >
                                                        {errorInput === 'formEvent.longitude' ? 'Ingrese longitud' : ''}
                                                    </small>
                                                </Grid>
                                            </>
                                        ) : (
                                            <></>
                                        )}
                                    </Grid>

                                    <div className={classes.divBtn}>
                                        <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>
                                            Atras
                                        </Button>
                                        <Button variant="contained" color="primary" onClick={handleNext}>
                                            {activeStep === steps.length - 1 ? 'Finish' : 'Siguiente'}{' '}
                                        </Button>
                                    </div>
                                    { event ? (
                                        <Button className={classes.buttonRight} type="submit" variant="contained" color="primary">
                                        Guardar
                                    </Button>
                                    ) : ('')}
                                </>
                            ) : (
                                <></>
                            )}

                            {/*Detalles */}
                            {activeStep === 2 ? (
                                <>
                                    <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                                    <Grid container spacing={3}>
                                        {/* banner */}
                                        { event === null ? (
                                            <Grid item xs={6}>
                                                <Typography className={classes.subtitle}>Banner </Typography>
                                                {formik.values.formEvent.file !== '' ? (
                                                    <img src={fileUrl} className={classes.imgBanner} alt={formik.values.formEvent.file.name} />
                                                ) : (
                                                    <></>
                                                )}
                                                <FormControl className={classes.formControl}>
                                                    {formik.values.formEvent.file !== '' ? (
                                                        <p>Imagen seleccionada {formik.values.formEvent.file.name}</p>
                                                    ) : (
                                                        <></>
                                                    )}
                                                    <input
                                                        accept="image/*"
                                                        id="formEvent.file"
                                                        name="formEvent.file"
                                                        type="file"
                                                        onChange={(event) => {
                                                            formik.setFieldValue('formEvent.file', '');
                                                            formik.setFieldValue('formEvent.file', event.currentTarget.files[0]);
                                                            const imageFile = event.target.files[0];
                                                            const imageUrl = URL.createObjectURL(imageFile);
                                                            setFileUrl(imageUrl);
                                                        }}
                                                    />
                                                </FormControl>
                                                <small
                                                    style={{ color: errorInput === 'formEvent.file' ? 'red' : 'black' }}
                                                    className={classes.counter}
                                                >
                                                    {errorInput === 'formEvent.file' ? 'Seleccione una imagen.' : ''}
                                                </small>
                                            </Grid>
                                        ) : ('')}
                                        {/* descripción */}
                                        <Grid item xs={12}>
                                            <Typography className={classes.subtitle}>Descripción </Typography>
                                            {/*<CKEditor content={formik.values.formEvent.description} onChange={handleDescription} />*/}
                                            <textarea cols={60} rows={10} value={formik.values.formEvent.description} onChange={handleDescription} />
                                            <small
                                                style={{ color: errorInput === 'formEvent.description' ? 'red' : 'black' }}
                                                className={classes.counter}
                                            >
                                                {errorInput === 'formEvent.description' ? 'Ingrese una descripción' : ''}
                                            </small>
                                        </Grid>
                                        {/* mapa asientos */}
                                        <Grid item xs={6}>
                                            <FormControlLabel
                                                className={classes.formControl}
                                                control={
                                                    <Checkbox
                                                        id="formEvent.isChart"
                                                        checked={formik.values.formEvent.isChart}
                                                        onChange={formik.handleChange}
                                                        name="formEvent.isChart"
                                                    />
                                                }
                                                label="Tiene mapa de puestos"
                                            />
                                        </Grid>

                                        {formik.values.formEvent.isChart ? (
                                            <Grid item xs={6}>
                                                <FormControl className={classes.formControl}>
                                                    <InputLabel id="label-space">Mapa</InputLabel>
                                                    <Select
                                                        labelId="label-space"
                                                        id="formEvent.space"
                                                        name="formEvent.space"
                                                        value={formik.values.formEvent.space}
                                                        onChange={formik.handleChange}
                                                        error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                        helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                                    >
                                                        {spaces.map((space, index) => {
                                                                return (
                                                                    <MenuItem key={index} value={space._id}>
                                                                        {space.name}
                                                                    </MenuItem>
                                                                );
                                                        })}
                                                    </Select>
                                                    <small
                                                        style={{ color: errorInput === 'formEvent.space' ? 'red' : 'black' }}
                                                        className={classes.counter}
                                                    >
                                                        {errorInput === 'formEvent.space' ? 'Seleccione el mapa' : ''}
                                                    </small>
                                                </FormControl>
                                            </Grid>
                                        ) : (
                                            <Grid item xs={6}></Grid>
                                        )}

                                        {/* url */}
                                        <Grid item xs={4}>
                                            <TextField
                                                fullWidth
                                                id="formEvent.url"
                                                name="formEvent.url"
                                                label="URL"
                                                value={formik.values.formEvent.url}
                                                onChange={formik.handleChange}
                                                error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                            />
                                            <small
                                                style={{ color: errorInput === 'formEvent.url' ? 'red' : 'black' }}
                                                className={classes.counter}
                                            >
                                                {errorInput === 'formEvent.url' ? 'Ingrese url' : ''}
                                            </small>
                                        </Grid>

                                        {/* Email de notificación */}
                                        <Grid item xs={4}>
                                            <TextField
                                                fullWidth
                                                id="formEvent.sendEmail"
                                                name="formEvent.sendEmail"
                                                label="Email de notificación"
                                                value={formik.values.formEvent.sendEmail}
                                                onChange={formik.handleChange}
                                                error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                            />
                                        </Grid>

                                        {/* Email de reenvio */}
                                        <Grid item xs={4}>
                                            <TextField
                                                fullWidth
                                                id="formEvent.resendEmails"
                                                name="formEvent.resendEmails"
                                                label="Email de reenvío"
                                                value={formik.values.formEvent.resendEmails}
                                                onChange={formik.handleChange}
                                                error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                            />
                                        </Grid>
                                        {promoter === "62c58d8544cb3ff794e0d5f6"  || promoter === "63285afc468abd0dec531aed" ? 
                                        (
                                            <>
                                            {/* Aforo total */}
                                                <Grid item xs={4}>
                                                    <TextField
                                                        fullWidth
                                                        type="number"
                                                        id="formEvent.totalAmount"
                                                        name="formEvent.totalAmount"
                                                        label="Aforo Total"
                                                        value={formik.values.formEvent.totalAmount}
                                                        onChange={formik.handleChange}
                                                        error={formik.touched.formEvent && Boolean(formik.errors.formEvent)}
                                                        helpertext={formik.touched.formEvent && formik.errors.formEvent}
                                                    />    
                                                </Grid>
                                            </>
                                        ):
                                        (
                                            <></>
                                        )}
                                        
                                    </Grid>
                                    <div className={classes.divBtn}>
                                        <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>
                                            Atras
                                        </Button>
                                        <Button variant="contained" color="primary" onClick={handleNext}>
                                            {activeStep === steps.length - 1 ? 'Finish' : 'Siguiente'}{' '}
                                        </Button>
                                    </div>
                                    { event ? (
                                        <Button className={classes.buttonRight} type="submit" variant="contained" color="primary">
                                        Guardar
                                    </Button>
                                    ) : ('')}
                                </>
                            ) : (
                                <></>
                            )}

                        {activeStep === 3 ? (
                                <>

                                    <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                                    <Grid container spacing={3}>
                                        <Grid item xs={12}>
                                            <ImageUploading
                                                multiple
                                                value={images}
                                                onChange={handleImages}
                                                maxNumber={maxNumber}
                                                dataURLKey="data_url"
                                            >
                                                {({
                                                      imageList,
                                                      onImageUpload,
                                                      onImageRemoveAll,
                                                      onImageUpdate,
                                                      onImageRemove,
                                                      isDragging,
                                                      dragProps,
                                                  }) => (
                                                    // write your building UI
                                                    <div className="upload__image-wrapper">
                                                        <Button
                                                            style={{
                                                                width: '100%', 
                                                                height: '150px', 
                                                                borderStyle: 'dashed',
                                                                ...( isDragging ? {color:'red'} : {} ),
                                                              }}
                                                            onClick={onImageUpload}
                                                            {...dragProps}
                                                        >
                                                            Click o arrastra las imágenes aquí
                                                        </Button>
                                                        &nbsp;
                                                        <div style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
                                                            {imageList.map((image, index) => (
                                                                <div key={index} className="image-item">
                                                                    <div style={{margin: '5px', height:'80px'}}>
                                                                        <img src={image['data_url']} alt="" width="100" />
                                                                    </div>
                                                                    <div className="image-item__btn-wrapper" style={{display: 'flex', justifyContent:'center', alignItems:'center',}}>
                                                                        <button onClick={() => onImageUpdate(index)} style={{margin: 1}}><Edit/></button>
                                                                        <button onClick={() => onImageRemove(index)} style={{margin: 1}}><Delete/></button>
                                                                    </div>
                                                                </div>
                                                            ))}
                                                        </div>
                                                        <br />
                                                        <br />
                                                        <br />
                                                        <Button onClick={onImageRemoveAll}>Eliminar todas las imágenes</Button>
                                                    </div>
                                                )}
                                            </ImageUploading>
                                        </Grid>
                                    </Grid>

                                     {/* boton siguiente */}
                                     <div>
                                        <div className={classes.divBtn}>
                                            <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>
                                                Atras
                                            </Button>
                                            {event === null ? (
                                                <>
                                                {countEntrance ? (
                                                    <Button disabled={true} variant="contained" color="primary" onClick={handleNext}>
                                                        {activeStep === steps.length - 1 ? 'Finish' : 'Siguiente'}{' '}
                                                    </Button>
                                                ) : (
                                                    <Button variant="contained" color="primary" onClick={handleNext}>
                                                        {activeStep === steps.length - 1 ? 'Finish' : 'Siguiente'}{' '}
                                                    </Button>
                                                )}
                                                </>
                                            ) : ('') }
                                        </div>
                                        { event ? (
                                            <Button className={classes.buttonRight} type="submit" variant="contained" color="primary">
                                            Guardar
                                        </Button>
                                        ) : ('')}
                                    </div>
                                </>
                            ) : <></>}

                            {/*Gestion entradas */}
                            {activeStep === 4 ? (
                                <>
                                    {/* boton añadir entradas */}
                                    <Grid item xs={8}>
                                        <div className={classes.divBtn}>
                                            <Button
                                                variant="contained"
                                                color="primary"
                                                onClick={showFormEntrance}
                                                className={classes.backButton}
                                            >
                                                + añadir entrada
                                            </Button>
                                        </div>
                                    </Grid>
                                    {addEntrance ? (
                                        <div>
                                            <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                                            <Grid container spacing={4}>
                                                {/*nombre entrada */}
                                                <Grid item xs={5}>
                                                    <TextField
                                                        fullWidth
                                                        id="formTicket.name"
                                                        name="formTicket.name"
                                                        label="Nombre entrada"
                                                        value={formik.values.formTicket.name}
                                                        onChange={formik.handleChange}
                                                        error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                                                        helpertext={formik.touched.formTicket && formik.errors.formTicket}
                                                    />
                                                    <small
                                                        style={{ color: errorInput === 'formTicket.name' ? 'red' : 'black' }}
                                                        className={classes.counter}
                                                    >
                                                        {errorInput === 'formTicket.name' ? 'Ingrese nombre de entrada.' : ''}
                                                    </small>
                                                </Grid>
                                                
                                                 {/*Categoria */}
                                                 { entranceCategories !== undefined && entranceCategories.length > 0 ? 
                                                    <Grid item xs={4}>
                                                        <FormControl className={classes.formControl}>
                                                            <InputLabel id="category">Categoria</InputLabel>
                                                            <Select
                                                                labelId="category"
                                                                id="formTicket.chartCategory"
                                                                name="formTicket.chartCategory"
                                                                value={formik.values.formTicket.chartCategory}
                                                                onChange={formik.handleChange}
                                                                error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                                                                helpertext={formik.touched.formTicket && formik.errors.formTicket}
                                                            >
                                                                {entranceCategories.map((categorie, index) => {
                                                                    return (
                                                                        <MenuItem key={index} value={categorie.label}>
                                                                            {categorie.label}
                                                                        </MenuItem>
                                                                    );
                                                                })}
                                                            </Select>
                                                            <small
                                                                style={{ color: errorInput === 'formTicket.chartCategory' ? 'red' : 'black' }}
                                                                className={classes.counter}
                                                            >
                                                                {errorInput === 'formTicket.chartCategory' ? 'Seleccione la categoria' : ''}
                                                            </small>
                                                        </FormControl>
                                                    </Grid>
                                                 : ''}

                                                {/*Descripcion */}
                                                <Grid item xs={12}>
                                                    <TextField
                                                        fullWidth
                                                        id="formTicket.description"
                                                        name="formTicket.description"
                                                        label="Descripcion"
                                                        value={formik.values.formTicket.description}
                                                        onChange={formik.handleChange}
                                                        error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                                                        helpertext={formik.touched.formTicket && formik.errors.formTicket}
                                                    />
                                                    <small
                                                        style={{ color: errorInput === 'formTicket.description' ? 'red' : 'black' }}
                                                        className={classes.counter}
                                                    >
                                                        {errorInput === 'formTicket.description'
                                                            ? 'Ingrese la descripción de la entrada.'
                                                            : ''}
                                                    </small>
                                                </Grid>

                                                {/*Tipo Entrada */}
                                                <Grid item xs={4}>
                                                    <FormControl className={classes.formControl}>
                                                        <InputLabel id="label-category">Tipo Entrada</InputLabel>
                                                        <Select
                                                            labelId="label-category"
                                                            id="formTicket.type"
                                                            name="formTicket.type"
                                                            value={formik.values.formTicket.type}
                                                            onChange={formik.handleChange}
                                                            error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                                                            helpertext={formik.touched.formTicket && formik.errors.formTicket}
                                                        >
                                                            {eventTypes.map((cagegorie, index) => {
                                                                return (
                                                                    <MenuItem key={index} value={cagegorie.value}>
                                                                        {cagegorie.label}
                                                                    </MenuItem>
                                                                );
                                                            })}
                                                        </Select>
                                                        <small
                                                            style={{ color: errorInput === 'formTicket.type' ? 'red' : 'black' }}
                                                            className={classes.counter}
                                                        >
                                                            {errorInput === 'formTicket.type' ? 'Seleccione el tipo de entrada' : ''}
                                                        </small>
                                                    </FormControl>
                                                </Grid>

                                                {/*Tipo Iva */}
                                                <Grid item xs={4}>
                                                    <FormControl className={classes.formControl}>
                                                        <InputLabel id="label-category">Tipo Iva</InputLabel>
                                                        <Select
                                                            labelId="label-category"
                                                            id="formTicket.ivaPercent"
                                                            name="formTicket.ivaPercent"
                                                            value={formik.values.formTicket.ivaPercent}
                                                            onChange={formik.handleChange}
                                                            error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                                                            helpertext={formik.touched.formTicket && formik.errors.formTicket}
                                                        >
                                                            {typesIva.map((iva, index) => {
                                                                return (
                                                                    <MenuItem key={index} value={iva.value}>
                                                                        {iva.label}
                                                                    </MenuItem>
                                                                );
                                                            })}
                                                        </Select>
                                                        <small
                                                            style={{ color: errorInput === 'typesIva' ? 'red' : 'black' }}
                                                            className={classes.counter}
                                                        >
                                                            {errorInput === 'typesIva' ? 'Seleccione el tipo de iva' : ''}
                                                        </small>
                                                    </FormControl>
                                                </Grid>

                                                {/*Cantidad */}
                                                <Grid item xs={4}>
                                                    <TextField
                                                        fullWidth
                                                        type="number"
                                                        id="formTicket.amount"
                                                        name="formTicket.amount"
                                                        label="Cantidad"
                                                        value={formik.values.formTicket.amount}
                                                        onChange={formik.handleChange}
                                                        error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                                                        helpertext={formik.touched.formTicket && formik.errors.formTicket}
                                                    />
                                                    <small
                                                        style={{ color: errorInput === 'formTicket.amount' ? 'red' : 'black' }}
                                                        className={classes.counter}
                                                    >
                                                        {errorInput === 'formTicket.amount' ? 'Ingrese la cantidad de entradas' : ''}
                                                    </small>
                                                </Grid>

                                                {/*Comision */}
                                                <Grid item xs={4}>
                                                    <TextField
                                                        type="number"
                                                        fullWidth
                                                        id="formTicket.commission"
                                                        name="formTicket.commission"
                                                        label="Comision %"
                                                        value={formik.values.formTicket.commission}
                                                        onChange={formik.handleChange}
                                                        error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                                                        helpertext={formik.touched.formTicket && formik.errors.formTicket}
                                                    />
                                                    <small
                                                        style={{ color: errorInput === 'commission' ? 'red' : 'black' }}
                                                        className={classes.counter}
                                                    >
                                                        {errorInput === 'commission' ? 'Ingrese la comisión' : ''}
                                                    </small>
                                                </Grid>

                                                {/*Precio */}
                                                <Grid item xs={4}>
                                                    <TextField
                                                        fullWidth
                                                        type="number"
                                                        id="formTicket.netPrice"
                                                        name="formTicket.netPrice"
                                                        label="Precio"
                                                        value={formik.values.formTicket.netPrice}
                                                        onChange={formik.handleChange}
                                                        error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                                                        helpertext={formik.touched.formTicket && formik.errors.formTicket}
                                                    />
                                                    <small
                                                        style={{ color: errorInput === 'price' ? 'red' : 'black' }}
                                                        className={classes.counter}
                                                    >
                                                        {errorInput === 'price' ? 'Ingrese el precio.' : ''}
                                                    </small>
                                                </Grid>

                                                {/*Cantidad por persona */}
                                                <Grid item xs={4}>
                                                    <TextField
                                                        fullWidth
                                                        type="number"
                                                        id="formTicket.amountPerPerson"
                                                        name="formTicket.amountPerPerson"
                                                        label="Cantidad máxima de entradas/persona"
                                                        value={formik.values.formTicket.amountPerPerson}
                                                        onChange={formik.handleChange}
                                                        error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                                                        helpertext={formik.touched.formTicket && formik.errors.formTicket}
                                                    />
                                                    <small
                                                        style={{ color: errorInput === 'maximumEntrance' ? 'red' : 'black' }}
                                                        className={classes.counter}
                                                    >
                                                        {errorInput === 'maximumEntrance' ? 'Ingrese el maximo por persona.' : ''}
                                                    </small>
                                                </Grid>

                                                {/*Desglose precio */}
                                                <Grid item xs={8}>
                                                    <InputLabel>Desglose precio</InputLabel>
                                                    <br />
                                                    {formik.values.formTicket.netPrice > 0 ? (
                                                        <>
                                                            <InputLabel>Base: {basePrecio} €</InputLabel>
                                                            <InputLabel>Iva: {ivaPrecio} €</InputLabel>
                                                            <InputLabel>Comisión: {comisionPrecio} €</InputLabel>
                                                            <InputLabel>----------------</InputLabel>
                                                            <InputLabel>Precio Total: {totalPrecio} €</InputLabel>
                                                        </>
                                                    ) : (
                                                        <>
                                                            <InputLabel>Base: {basePrecio} €</InputLabel>
                                                            <InputLabel>----------------</InputLabel>
                                                            <InputLabel>Precio Total: Entrada Gratis</InputLabel>
                                                        </>
                                                    )}
                                                    <Button onClick={calculoPrecio} >Calcular Precio Final</Button>
                                                </Grid>

                                                {/*Fecha Inicio */}
                                                <Grid item xs={6}>
                                                    <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                                                        <DateTimePicker
                                                            autoOk
                                                            fullWidth
                                                            clearable
                                                            id="startDateEntrance"
                                                            name="startDateEntrance"
                                                            ampm={false}
                                                            value={dateStartEntrance}
                                                            onChange={handleDateStartEntranceChange}
                                                            format="yyyy-MM-dd HH:mm"
                                                            label="Fecha y hora inicio entrada"
                                                            minDate={formik.values.formEvent.startDate}
                                                            maxDate={formik.values.formEvent.endDate}
                                                        />
                                                    </MuiPickersUtilsProvider>
                                                </Grid>

                                                {/*Fecha Fin */}
                                                <Grid item xs={6}>
                                                    <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                                                        <DateTimePicker
                                                            autoOk
                                                            fullWidth
                                                            clearable
                                                            id="endDateEntrance"
                                                            name="endDateEntrance"
                                                            ampm={false}
                                                            value={dateEndEntrance}
                                                            onChange={handleDateEndEntranceChange}
                                                            format="yyyy-MM-dd HH:mm"
                                                            label="Fecha y hora Fin entrada"
                                                            minDate={formik.values.formTicket.startDate}
                                                            maxDate={formik.values.formEvent.endDate}
                                                            minDateMessage={
                                                                'la fecha fin entrada no puede ser menor que fecha inicio entrada'
                                                            }
                                                        />
                                                    </MuiPickersUtilsProvider>
                                                </Grid>

                                                {/*Inicio venta entradas */}
                                                <Grid item xs={8}>
                                                    <InputLabel>Inicio venta entradas</InputLabel>
                                                </Grid>

                                                {/*Fecha Inicio venta */}
                                                <Grid item xs={6}>
                                                    <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                                                        <DateTimePicker
                                                            autoOk
                                                            fullWidth
                                                            clearable
                                                            id="startDateSellEntrance"
                                                            name="startDateSellEntrance"
                                                            ampm={false}
                                                            value={dateStartSellEntrance}
                                                            onChange={handleDateStartSellEntranceChange}
                                                            format="yyyy-MM-dd HH:mm"
                                                            label="Fecha y hora inicio venta entrada"
                                                        />
                                                    </MuiPickersUtilsProvider>
                                                </Grid>

                                                {/*Fecha Fin venta */}
                                                <Grid item xs={6}>
                                                    <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                                                        <DateTimePicker
                                                            autoOk
                                                            fullWidth
                                                            clearable
                                                            id="endDateSellEntrance"
                                                            name="endDateSellEntrance"
                                                            ampm={false}
                                                            value={dateEndSellEntrance}
                                                            onChange={handleDateEndSellEntranceChange}
                                                            format="yyyy-MM-dd HH:mm"
                                                            label="Fecha y hora Fin venta entrada"
                                                            minDate={formik.values.formTicket.sellStartDate}
                                                            maxDate={formik.values.formTicket.endDate}
                                                            minDateMessage={
                                                                'la fecha fin venta entrada no puede ser menor que fecha inicio venta entrada'
                                                            }
                                                        />
                                                    </MuiPickersUtilsProvider>
                                                </Grid>

                                                {/* boton añadir entradas */}
                                                <Grid item xs={8}>
                                                    <div className={classes.divBtn}>
                                                        {formik.values.formTicket.name === '' ||
                                                        formik.values.formTicket.description === '' ||
                                                        formik.values.formTicket.amount === '' ||
                                                        formik.values.formTicket.commission === '' ||
                                                        formik.values.formTicket.netPrice === '' ||
                                                        formik.values.formTicket.amountPerPerson === '' ? (
                                                            <Button
                                                                variant="contained"
                                                                color="primary"
                                                                disabled={true}
                                                                onClick={handleAddEntrance}
                                                                className={classes.backButton}
                                                            >
                                                                Añadir
                                                            </Button>
                                                        ) : (
                                                            <Button
                                                                variant="contained"
                                                                color="primary"
                                                                onClick={handleAddEntrance}
                                                                className={classes.backButton}
                                                            >
                                                                Añadir
                                                            </Button>
                                                        )}

                                                        <Button
                                                            variant="contained"
                                                            color="primary"
                                                            onClick={hideFormEntrance}
                                                            className={classes.backButton}
                                                        >
                                                            Cancelar
                                                        </Button>
                                                    </div>
                                                </Grid>

                                                {numEntrances ? (
                                                    <Grid item xs={8}>
                                                        {/*Info entrada */}
                                                        <InputLabel>Tickets creados: {ticketsArr.length}</InputLabel>
                                                        <br />
                                                        {ticketsArr.map((data, i) => {
                                                            return (
                                                                <Grid key={i} item xs={8}>
                                                                    <Grid item xs={8}>
                                                                        <div className={classes.ticket}>
                                                                            {data[7] === 0 ? (
                                                                                <>
                                                                                    <InputLabel className={classes.textTicket}>
                                                                                        {i + 1} <LocalActivityIcon color="secondary" />{' '}
                                                                                        {data[0]}
                                                                                    </InputLabel>
                                                                                    <InputLabel className={classes.textTicket}>
                                                                                        {data[6]}
                                                                                    </InputLabel>
                                                                                    <InputLabel className={classes.textTicket}>
                                                                                        {data[2]} ud. Gratis
                                                                                    </InputLabel>
                                                                                </>
                                                                            ) : (
                                                                                <>
                                                                                    <InputLabel className={classes.textTicket}>
                                                                                        {i + 1} <LocalActivityIcon color="secondary" />{' '}
                                                                                        {data[0]}
                                                                                    </InputLabel>
                                                                                    <InputLabel className={classes.textTicket}>
                                                                                        {data[6]}
                                                                                    </InputLabel>
                                                                                    <InputLabel className={classes.textTicket}>
                                                                                        {data[2]} ud. por {data[7]} €
                                                                                    </InputLabel>
                                                                                </>
                                                                            )}
                                                                        </div>
                                                                    </Grid>
                                                                    {/* <Grid item xs={4}>
                                                                        <div className={classes.divBtn}>
                                                                            <Button
                                                                                variant="contained"
                                                                                color="primary"
                                                                                onClick={handleBack} 
                                                                                className={classes.backButton}
                                                                                startIcon={<DeleteIcon />}
                                                                            >
                                                                                Eliminar
                                                                            </Button>
                                                                        </div>
                                                                    </Grid> 
                                                                    */}
                                                                </Grid>
                                                            );
                                                        })}
                                                    </Grid>
                                                ) : (
                                                    <></>
                                                )}
                                            </Grid>
                                        </div>
                                    ) : (
                                        <></>
                                    )}

                                


                                    {/* boton siguiente */}
                                    <div>
                                        <div className={classes.divBtn}>
                                            <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>
                                                Atras
                                            </Button>
                                            {countEntrance ? (
                                                <Button disabled={true} variant="contained" color="primary" onClick={handleNext}>
                                                    {activeStep === steps.length - 1 ? 'Finish' : 'Siguiente'}{' '}
                                                </Button>
                                            ) : (
                                                <Button variant="contained" color="primary" onClick={handleNext}>
                                                    {activeStep === steps.length - 1 ? 'Finish' : 'Siguiente'}{' '}
                                                </Button>
                                            )}
                                        </div>
                                    </div>
                                </>
                            ) : (
                                <></>
                            )}
                            {isOpenAlert && (
                                <Grid item xs={12}>
                                    <AlertMsg severity={messageAlert.severity} msg={messageAlert.msg} />
                                </Grid>
                            )}
                            <Divider />
                        </form>
                    </div>
                </DialogContent>
            </Dialog>
            </>
        )}
        </>
    );
};

export default CreateEvent;
