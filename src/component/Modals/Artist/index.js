import React, { useState, useContext, useEffect, useCallback } from 'react';
import {
    DialogContent, MenuItem, DialogTitle, Dialog, TextField,
    Button, Stepper, Step, StepLabel, Typography, Grid, InputLabel, FormControl, Select, Divider
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useFormik } from 'formik';
import * as yup from 'yup';
import CKEditor from "../../Input/CKEditor";
import { UserContext } from "../../../context/UserContext";
import CircularProgress from '@material-ui/core/CircularProgress';
import { URL_S3 } from "../../../store/constant";
import { uploadFile } from "../../../services/upload.service";
import AlertMsg from "../../Alert";
import { addArtist, modifyArtist } from '../../../services/artist.service';
import ImageUploading from 'react-images-uploading';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    backButton: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    formControl: {
        width: '100%',
    },
    chip: {
        marginTop: '10px',
        marginRight: '5px'
    },
    divBtn: {
        marginTop: '20px',
        marginBottom: '20px',
    },
    buttonProgress: {
        marginLeft: '10px'
    },
    imgBanner: {
        width: '150px',
        height: '130px',
        objectFit: 'contain',
        marginTop: '10px',
        marginBottom: '10px'
    }
}));

function getSteps() {
    return ['Información Básica', 'Imagen, video y descripción', 'Multi Lenguaje', 'Galería'];
}

function getStepContent(stepIndex) {
    switch (stepIndex) {
        case 0:
            return 'Ingresa la información básica del competidor';
        case 1:
            return 'Ingrese la descripción y contenido multimedia';
        case 2:
            return 'Ingrese la traducción en el lenguaje que lo necesite';
        case 3:
            return 'Ingrese las imagenes de cabecera';
        case 4:
            return 'Ingrese información adicional';
        default:
            return 'Unknown stepIndex';
    }
}

const validationSchema = yup.object({
    name: yup.string().required(),
    // datePublication: yup.string().required(),
    /*description: yup.string().required(),
    category: yup.string().required(),*/
});

const CreateArtist = ({ open, onClose, onRefresh, onOpenSnackBar, artist }) => {
    const classes = useStyles();
    const { user } = useContext(UserContext);
    const [activeStep, setActiveStep] = useState(0);
    const [tag, setTag] = useState('');
    const [errorInput, setErrorInput] = useState('');
    const steps = getSteps();
    const [isLoading, setIsLoading] = useState(false);
    const [isOpenAlert, setIsOpenAlert] = useState(false);
    const [messageAlert, setMessageAlert] = useState({});
    const [descriptionEu, setDescriptionEu] = useState('');
    const [images, setImages] = React.useState([]);
    const maxNumber = 20;

    useEffect(() => {
        resetForm();
        resetElements();
        if (artist?._id !== undefined && artist?._id !== '') {
            loadData();
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [ open ]);


    const resetElements = () => {
        setActiveStep(0);
        setDescriptionEu('');
    }

    const loadData = () => {
        formik.setFieldValue('name', artist?.name)
        formik.setFieldValue('_id', artist?._id)
        formik.setFieldValue('description', artist?.description)
        formik.setFieldValue('tags', artist?.tags)
        formik.setFieldValue('image', artist?.image)
        formik.setFieldValue('category', artist?.category[0])
        formik.setFieldValue('festival', artist?.festival)
        formik.setFieldValue('video', artist?.video)
        formik.setFieldValue('country', artist?.country)
        formik.setFieldValue('desc_eu', artist?.desc_eu)
        // formik.setFieldValue('album', artist?.album)
        let imageArray = [];
        artist.album.forEach((img_album) => {
                const image = { data_url: img_album };
                imageArray.push(image);
            }
        );
        setImages(imageArray);
    }

    const handleImages = (imageList, addUpdateIndex) => {
        // data for submit
        //console.log(imageList, addUpdateIndex);
        setImages(imageList);
    };

    const handleNext = () => {
        if (activeStep === 0) {
            validationFirstStep();
        }
        if (activeStep === 1) {
            validationSecondStep();
        }
        if (activeStep === 2) {
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
        if (activeStep === 3) {
            validationThreeStep();
        }
        if (activeStep === 4) {
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    const handleClose = () => {
        onClose();
        resetForm();
    };

    const handleAddTag = (e) => {
        if (e.key === 'Enter') {
            if (tag.length === 0) return;
            formik.setFieldValue('tags', [...formik.values.tags, tag]);
            setTag('');
        } else {
            setTag(e.target.value);
        }
    };

    const handleDeleteTag = (index) => {
        const filterTags = formik.values.tags.filter((tag, i) => i !== index);
        formik.setFieldValue('tags', filterTags);
        setTag('');
    };

    const handleDescription = (value) => {
        formik.setFieldValue('description', value);
    };

    const handleDescriptionEu = (value) => {
        setDescriptionEu(value);
    };

    const validationFirstStep = () => {
        setErrorInput('');
        let count = 0;

        if(formik.values.country === '') {
            setErrorInput('country');
            count++;
        }
        if(formik.values.category === '') {
            setErrorInput('category');
            count++;
        }
        if (formik.values.name === '') {
            setErrorInput('name');
            count++;
        }
        if (count === 0) {
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
    };

    const validationSecondStep = () => {
        setErrorInput('');
        let count = 0;

        if(formik.values.description === '') {
            setErrorInput('description');
            count++;
        }
        if(formik.values.file === '' && formik.values.image === '') {
            setErrorInput('file');
            count++;
        }

        //console.log("file", formik.values.file);

        if (count === 0) {
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
    };

    const validationThreeStep = () => {
        setErrorInput('');
        let count = 0;

        if (count === 0) {
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
    };

    const generateSlug = (text) => {
        let slug = text;
        if (slug != '') {
            slug = slug.toLowerCase();
            slug = slug.trim();
            slug = slug.replace('"', '');
            slug = slug.replace("'", '');
            slug = slug.replace(",", '');
            slug = slug.replace('&', '');
            slug = slug.replace('“', '');
            slug = slug.replace('”', '');
            slug = slug.replace(':', '');
            slug = slug.replace('+', '');
            slug = slug.replace('/', '');
            slug = slug.replace('!', '');
            slug = slug.replace('¡', '');
            slug = slug.replace('*', '');
            slug = slug.replace('í', 'i');
            slug = slug.replace('á', 'a');
            slug = slug.replace('é', 'e');
            slug = slug.replace('ó', 'o');
            slug = slug.replace('ú', 'u');
            slug = slug.replace(/["']/g, "");
            slug = slug.replace(/\s+/g, '-');
        }
        return slug;
    };

    const uploadS3 = async (images) => {
        let urlfile = '';
        let imageArray = [];
        await Promise.all(images.map(async image => {
            //console.log("img", image);
            if (image.file) {
                const formData = new FormData();
                formData.append('file', image.file);
                const { status } = await uploadFile(formData);
                if (status === 201) {
                    urlfile = URL_S3 + image.file.name;
                } else {
                    urlfile = '';
                }
                //console.log("URLFILE", urlfile);
                imageArray.push(urlfile);
            } else {
                imageArray.push(image.data_url);
            }

        }));
        //console.log("imgArray",imageArray);
        return imageArray;
    }

    const onSubmit = async (values) => {
        if (activeStep === 3) {
            return;
        }
        setIsLoading(true);
        let urlfile = '';
        if (values.file !== '') {
            const formData = new FormData();
            formData.append('file', values.file);
            const { status } = await uploadFile(formData);
            if (status === 201) {
                urlfile = URL_S3 + values.file.name;
            } else {
                urlfile = '';
            }
            values.image = urlfile;
        }
        if (images.length > 0 ) {
            values.album = await uploadS3(images);
            //console.log("album", values.album);
        }
        // let cat = [values.category];
        // values.category = cat;
        values.province = '.';
        values.desc_eu = descriptionEu;
        let dataL;
        let statusL;
        if (artist?._id !== undefined && artist?._id !== '') {
            const { status, data } = await modifyArtist(artist?._id, values);
            dataL = data;
            statusL = status;

        } else {
            delete values._id;
            const { status, data } = await addArtist(values);
            dataL = data;
            statusL = status;
        }
        //console.log("status", statusL);
        if (statusL === 201 || statusL === 200) {
            onOpenSnackBar({ severity: 'success', msg: dataL.message })
            setIsLoading(false);
            onRefresh()
            handleClose();
            resetForm();
        } else {
            setMessageAlert({ severity: 'warning', msg: dataL.message })
            setIsOpenAlert(true)
            setIsLoading(false);
        }
    }

    const resetForm = useCallback(async () => {
        setImages([]);
        formik.handleReset();
    })

    const formik = useFormik({
        initialValues: {
            _id: '',
            name: '',
            description: '',
            tags: [],
            image: '',
            category: '',
            album: [],
            country: '',
            festival: '619e179477275183e38adb93',
            file: '',
            desc_eu: '',
            video: null,
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            onSubmit(values);
        },
    });

    return (
        <>
            <Dialog open={open} onClose={handleClose} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Competidor</DialogTitle>
                <DialogContent>

                    <Stepper activeStep={activeStep} alternativeLabel>
                        {steps.map((label) => (
                            <Step key={label} >
                                <StepLabel >{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                    <div>
                        <form encType="multipart/form-data" onSubmit={formik.handleSubmit}>
                            {activeStep === steps.length ? (
                                <div>
                                    <Typography className={classes.instructions}>Todos los pasos completados</Typography>
                                    <div className={classes.divBtn}>
                                        <Button onClick={handleReset}>Retroceder</Button>
                                        <Button type="submit" variant="contained" color="primary">
                                            Guardar
                                            {isLoading && <CircularProgress color="secondary" size={24} className={classes.buttonProgress} />}
                                        </Button>
                                    </div>
                                </div>
                            ) : <></>}
                            {activeStep === 0 ? (
                                <div>
                                    <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <TextField
                                                fullWidth
                                                id="name"
                                                name="name"
                                                label="Nombre"
                                                value={formik.values.name}
                                                onChange={formik.handleChange}
                                                error={formik.touched.name && Boolean(formik.errors.name)}
                                                helpertext={formik.touched.name && formik.errors.name}
                                            />
                                            <small style={{ color: errorInput === 'name' ? 'red' : 'black' }} className={classes.counter}>
                                                {errorInput === 'name' ? 'Ingrese el título.' : ''}
                                            </small>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <FormControl className={classes.formControl}>
                                                <InputLabel id="label-category">Categoría</InputLabel>
                                                <Select
                                                    labelId="label-category"
                                                    id="category"
                                                    name="category"
                                                    value={formik.values.category}
                                                    onChange={formik.handleChange}
                                                    error={formik.touched.category && Boolean(formik.errors.category)}
                                                    helpertext={formik.touched.category && formik.errors.category}
                                                >
                                                    <MenuItem value='local'>Local</MenuItem>
                                                    <MenuItem value='international'>Internacional</MenuItem>
                                                </Select>
                                                <small style={{ color: errorInput === 'category' ? 'red' : 'black' }} className={classes.counter}>
                                                    {errorInput === 'category' ? 'Seleccione la categoría' : ''}
                                                </small>
                                            </FormControl>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <FormControl className={classes.formControl}>
                                                <InputLabel id="label-country">Procedencia</InputLabel>
                                                <Select
                                                    labelId="label-country"
                                                    id="country"
                                                    name="country"
                                                    value={formik.values.country}
                                                    onChange={formik.handleChange}
                                                    error={formik.touched.country && Boolean(formik.errors.country)}
                                                    helpertext={formik.touched.country && formik.errors.country}
                                                >
                                                    <MenuItem value='basque-country'>Euskal Herria</MenuItem>
                                                    <MenuItem value='united-kingdom'>Reino Unido</MenuItem>
                                                    <MenuItem value='australia'>Australia</MenuItem>
                                                    <MenuItem value='portugal'>Portugal</MenuItem>
                                                    <MenuItem value='miami'>Miami</MenuItem>
                                                    <MenuItem value='hawaii'>Hawaii</MenuItem>
                                                    <MenuItem value='ireland'>Irlanda</MenuItem>
                                                    <MenuItem value='italy'>Italia</MenuItem>
                                                    <MenuItem value='south-africa'>Sudafrica</MenuItem>
                                                    <MenuItem value='united-states-of-america'>Estados Unidos</MenuItem>
                                                    <MenuItem value='france'>Francia</MenuItem>
                                                    <MenuItem value='jamaica'>Jamaica</MenuItem>
                                                    <MenuItem value='brazil'>Brasil</MenuItem>
                                                </Select>
                                                <small style={{ color: errorInput === 'country' ? 'red' : 'black' }} className={classes.counter}>
                                                    {errorInput === 'country' ? 'Seleccione la procedencia' : ''}
                                                </small>
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                    <div className={classes.divBtn}>
                                        <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>Atras</Button>
                                        <Button variant="contained" color="primary" onClick={handleNext}>{activeStep === steps.length - 1 ? 'Finish' : 'Siguiente'} </Button>
                                    </div>
                                </div>
                            ) : <></>}

                            {activeStep === 1 ? (
                                <>
                                    <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <Typography className={classes.subtitle}>Imagen </Typography>
                                            {formik.values.image !== '' ? (
                                                <img src={formik.values.image} className={classes.imgBanner} alt={formik.values.name} />
                                            ) : <></>}
                                            <FormControl className={classes.formControl}>
                                                {formik.values.file !== '' ? (
                                                    <p>Imagen seleccionada {formik.values.file.name}</p>

                                                ) : <></>}
                                                <input accept="image/*" id="file" name="file" type="file" onChange={(event) => {
                                                    formik.setFieldValue("image", '');
                                                    formik.setFieldValue("file", event.currentTarget.files[0]);
                                                }} />

                                            </FormControl>
                                            <small style={{ color: errorInput === 'file' ? 'red' : 'black' }} className={classes.counter}>
                                                {errorInput === 'file' ? 'Seleccione una imagen.' : ''}
                                            </small>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Typography className={classes.subtitle}>Descripción </Typography>
                                            <CKEditor content={formik.values.description} onChange={handleDescription} />
                                            <small style={{ color: errorInput === 'description' ? 'red' : 'black' }} className={classes.counter}>
                                                {errorInput === 'description' ? 'Ingrese una descripción' : ''}
                                            </small>
                                        </Grid>

                                        <Grid item xs={12}>
                                            <TextField
                                                fullWidth
                                                id="video"
                                                name="video"
                                                label="URL Video Youtube"
                                                value={formik.values.video}
                                                onChange={formik.handleChange}
                                                error={formik.touched.video && Boolean(formik.errors.video)}
                                                helpertext={formik.touched.video && formik.errors.video}
                                            />
                                            <small style={{ color: errorInput === 'name' ? 'red' : 'black' }} className={classes.counter}>
                                                {errorInput === 'name' ? 'Ingrese el video.' : ''}
                                            </small>
                                        </Grid>

                                    </Grid>

                                    <div className={classes.divBtn}>
                                        <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>Atras</Button>
                                        <Button variant="contained" color="primary" onClick={handleNext}>{activeStep === steps.length - 1 ? 'Finish' : 'Siguiente'} </Button>
                                    </div>
                                </>
                            ) : <></>}

                            {activeStep === 2 ? (
                                <div>
                                    <Typography className={classes.instructions}>{getStepContent(activeStep)} (Opcional)</Typography>
                                    <Grid container spacing={3}>
                                        <Grid item xs={12}>
                                                    <   Typography>Euskera</Typography>
                                                    <Grid container spacing={3}>
                                                        <Grid item xs={12}>
                                                            <Typography className={classes.subtitle}>Descripción </Typography>
                                                            <CKEditor content={formik.values.desc_eu} onChange={handleDescriptionEu} />
                                                        </Grid>
                                                    </Grid>
                                                {/* </AccordionDetails>
                                            </Accordion> */}
                                        </Grid>
                                    </Grid>
                                    <div className={classes.divBtn}>
                                        <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>Atras</Button>
                                        <Button variant="contained" color="primary" onClick={handleNext}>{activeStep === steps.length - 1 ? 'Finish' : 'Siguiente'} </Button>
                                    </div>
                                </div>
                            ) : <></>}

                            {activeStep === 3 ? (
                                <>

                                    <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                                    <Grid container spacing={3}>
                                        <Grid item xs={12}>
                                            <ImageUploading
                                                multiple
                                                value={images}
                                                onChange={handleImages}
                                                maxNumber={maxNumber}
                                                dataURLKey="data_url"
                                            >
                                                {({
                                                      imageList,
                                                      onImageUpload,
                                                      onImageRemoveAll,
                                                      onImageUpdate,
                                                      onImageRemove,
                                                      isDragging,
                                                      dragProps,
                                                  }) => (
                                                    // write your building UI
                                                    <div className="upload__image-wrapper">
                                                        <button
                                                            style={{
                                                                width: '100%', 
                                                                height: '150px', 
                                                                borderStyle: 'dashed',
                                                                ...( isDragging ? {color:'red'} : {} ),
                                                              }}
                                                            onClick={onImageUpload}
                                                            {...dragProps}
                                                        >
                                                            Click o arrastra las imágenes aquí
                                                        </button>
                                                        &nbsp;
                                                        <div style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
                                                            {imageList.map((image, index) => (
                                                                <div key={index} className="image-item">
                                                                    <div style={{margin: '5px', height:'80px'}}>
                                                                        <img src={image['data_url']} alt="" width="100" />
                                                                    </div>
                                                                    <div className="image-item__btn-wrapper" style={{display: 'flex', justifyContent:'center', alignItems:'center',}}>
                                                                        <button onClick={() => onImageUpdate(index)} style={{margin: 1}}><Edit/></button>
                                                                        <button onClick={() => onImageRemove(index)} style={{margin: 1}}><Delete/></button>
                                                                    </div>
                                                                </div>
                                                            ))}
                                                        </div>

                                                        <br />
                                                        <br />
                                                        <br />
                                                        <button onClick={onImageRemoveAll}>Eliminar todas las imágenes</button>
                                                    </div>
                                                )}
                                            </ImageUploading>
                                        </Grid>

                                    </Grid>

                                    <div className={classes.divBtn}>
                                        <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>Atras</Button>
                                        <Button variant="contained" color="primary" onClick={handleNext}>{activeStep === steps.length - 1 ? 'Finish' : 'Siguiente'} </Button>
                                    </div>
                                </>
                            ) : <></>}

                            {/*{activeStep === 4 ? (*/}
                            {/*    <>*/}
                            {/*        <Typography className={classes.instructions}>{getStepContent(activeStep)} (opcional)</Typography>*/}
                            {/*        <Grid container spacing={3}>*/}
                            {/*            <Grid item xs={6}>*/}
                            {/*                <TextField*/}
                            {/*                    fullWidth*/}
                            {/*                    id="contact.name"*/}
                            {/*                    name="contact.name"*/}
                            {/*                    label="Nombre de contacto"*/}
                            {/*                    value={formik.values.contact.name}*/}
                            {/*                    onChange={formik.handleChange}*/}
                            {/*                    error={formik.touched.contact?.name && Boolean(formik.errors.contact?.name)}*/}
                            {/*                    helpertext={formik.touched.contact?.name && formik.errors.contact?.name}*/}
                            {/*                />*/}
                            {/*            </Grid>*/}
                            {/*            <Grid item xs={6}>*/}
                            {/*                <TextField*/}
                            {/*                    fullWidth*/}
                            {/*                    id="contact.email"*/}
                            {/*                    name="contact.email"*/}
                            {/*                    label="Correo Electrónico"*/}
                            {/*                    value={formik.values.contact.email}*/}
                            {/*                    onChange={formik.handleChange}*/}
                            {/*                    error={formik.touched.contact?.email && Boolean(formik.errors.contact?.email)}*/}
                            {/*                    helpertext={formik.touched.contact?.email && formik.errors.contact?.email}*/}
                            {/*                />*/}
                            {/*            </Grid>*/}
                            {/*            <Grid item xs={6}>*/}
                            {/*                <TextField*/}
                            {/*                    fullWidth*/}
                            {/*                    id="contact.phone"*/}
                            {/*                    name="contact.phone"*/}
                            {/*                    label="Teléfono"*/}
                            {/*                    value={formik.values.contact.phone}*/}
                            {/*                    onChange={formik.handleChange}*/}
                            {/*                    error={formik.touched.contact?.phone && Boolean(formik.errors.contact?.phone)}*/}
                            {/*                    helpertext={formik.touched.contact?.phone && formik.errors.contact?.phone}*/}
                            {/*                />*/}
                            {/*            </Grid>*/}
                            {/*            <Grid item xs={6}>*/}
                            {/*                <TextField*/}
                            {/*                    fullWidth*/}
                            {/*                    id="contact.cellphone"*/}
                            {/*                    name="contact.cellphone"*/}
                            {/*                    label="Movil"*/}
                            {/*                    value={formik.values.contact.cellphone}*/}
                            {/*                    onChange={formik.handleChange}*/}
                            {/*                    error={formik.touched.contact?.cellphone && Boolean(formik.errors.contact?.cellphone)}*/}
                            {/*                    helpertext={formik.touched.contact?.cellphone && formik.errors.contact?.cellphone}*/}
                            {/*                />*/}
                            {/*            </Grid>*/}
                            {/*            <Grid item xs={6}>*/}
                            {/*                <TextField*/}
                            {/*                    fullWidth*/}
                            {/*                    id="contact.url"*/}
                            {/*                    name="contact.url"*/}
                            {/*                    label="Url Es"*/}
                            {/*                    value={formik.values.contact.url}*/}
                            {/*                    onChange={formik.handleChange}*/}
                            {/*                    error={formik.touched.contact?.url && Boolean(formik.errors.contact?.url)}*/}
                            {/*                    helpertext={formik.touched.contact?.url && formik.errors.contact?.url}*/}
                            {/*                />*/}
                            {/*            </Grid>*/}
                            {/*            /!* <Grid item xs={6}>*/}
                            {/*                <TextField*/}
                            {/*                    fullWidth*/}
                            {/*                    id="contact.url_en"*/}
                            {/*                    name="contact.url_en"*/}
                            {/*                    label="Url En"*/}
                            {/*                    value={formik.values.contact.url_en}*/}
                            {/*                    onChange={formik.handleChange}*/}
                            {/*                    error={formik.touched.contact?.url_en && Boolean(formik.errors.contact?.url_en)}*/}
                            {/*                    helpertext={formik.touched.contact?.url_en && formik.errors.contact?.url_en}*/}
                            {/*                />*/}
                            {/*            </Grid> *!/*/}
                            {/*            <Grid item xs={6}>*/}
                            {/*                <TextField*/}
                            {/*                    fullWidth*/}
                            {/*                    id="contact.url_eu"*/}
                            {/*                    name="contact.url_eu"*/}
                            {/*                    label="Url Eu"*/}
                            {/*                    value={formik.values.contact.url_eu}*/}
                            {/*                    onChange={formik.handleChange}*/}
                            {/*                    error={formik.touched.contact?.url_eu && Boolean(formik.errors.contact?.url_eu)}*/}
                            {/*                    helpertext={formik.touched.contact?.url_eu && formik.errors.contact?.url_eu}*/}
                            {/*                />*/}
                            {/*            </Grid>*/}
                            {/*            /!* <Grid item xs={6}>*/}
                            {/*                <TextField*/}
                            {/*                    fullWidth*/}
                            {/*                    id="contact.url_fr"*/}
                            {/*                    name="contact.url_fr"*/}
                            {/*                    label="Url Fr"*/}
                            {/*                    value={formik.values.contact.url_fr}*/}
                            {/*                    onChange={formik.handleChange}*/}
                            {/*                    error={formik.touched.contact?.url_fr && Boolean(formik.errors.contact?.url_fr)}*/}
                            {/*                    helpertext={formik.touched.contact?.url_fr && formik.errors.contact?.url_fr}*/}
                            {/*                />*/}
                            {/*            </Grid> *!/*/}
                            {/*            <Grid item xs={6}>*/}
                            {/*                <TextField*/}
                            {/*                    fullWidth*/}
                            {/*                    id="contact.note"*/}
                            {/*                    name="contact.note"*/}
                            {/*                    label="Nota"*/}
                            {/*                    value={formik.values.contact.note}*/}
                            {/*                    onChange={formik.handleChange}*/}
                            {/*                    error={formik.touched.contact?.note && Boolean(formik.errors.contact?.note)}*/}
                            {/*                    helpertext={formik.touched.contact?.note && formik.errors.contact?.note}*/}
                            {/*                />*/}
                            {/*            </Grid>*/}

                            {/*        </Grid>*/}

                            {/*        <div className={classes.divBtn}>*/}
                            {/*            <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>Atras</Button>*/}
                            {/*            <Button variant="contained" color="primary" onClick={handleNext}>{activeStep === steps.length - 1 ? 'Finish' : 'Siguiente'} </Button>*/}
                            {/*        </div>*/}
                            {/*    </>*/}
                            {/*) : <></>}*/}

                            {isOpenAlert &&
                                <Grid item xs={12}>
                                    <AlertMsg severity={messageAlert.severity} msg={messageAlert.msg} />
                                </Grid>
                            }
                            <Divider />
                        </form>
                    </div>

                </DialogContent>
            </Dialog>
        </>
    );
};

export default CreateArtist;
