import 'moment/locale/es';

import {
    Button,
    Checkbox,
    Dialog,
    DialogContent,
    DialogTitle,
    FormControl,
    FormControlLabel,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    TextField
} from '@material-ui/core';
import React, { useContext, useEffect, useState } from 'react';

import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import { UserContext } from '../../../context/UserContext';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import { useFormik } from 'formik';
import { addStock, modifyStock } from '../../../services/stock.js';
import { takeProductsInfo } from '../../../services/product';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    backButton: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    formControl: {
        width: '100%',
    },
    chip: {
        marginTop: '10px',
        marginRight: '5px',
    },
    divBtn: {
        marginTop: '30px',
        marginBottom: '20px',
    },
    buttonProgress: {
        marginLeft: '10px',
    },
    imgBanner: {
        width: '150px',
        height: '130px',
        objectFit: 'contain',
        marginTop: '10px',
        marginBottom: '10px',
    },
    ticket: {
        backgroundColor: 'white',
        boxShadow: '0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)',
        borderRadius: '10px',
        marginBottom: '30px',
        marginLeft: '10px',
        width: '600px',
        height: '70px',
    },
    textTicket: {
        marginLeft: '10px',
        fontSize: '20px',
        marginBottom: '8px',
    },
    buttonRight: {
        float: 'right',
    },
}));

    function Stock({open, onHide, onRefresh, onFinish, onOpenSnackBar, event, stock}) {
    moment.locale('es');
    const classes = useStyles();
    const { user } = useContext(UserContext);
    const [ products, setProducts ] = useState([]);
    const [errorInput, setErrorInput] = useState('');
    const [messageAlert, setMessageAlert] = useState({});

    useEffect(() => {
        setErrorInput('');
        formik.handleReset();
        if (stock !== null) {
            loadData();
        }
        takeProducts(user._id);

    }, [stock]);

    const formik = useFormik({
        initialValues: {
            formStock: {
                product: '',
                amount: 0,
                isActive: true,
                enterprise: ''
            },
        },
        // validationSchema: validationSchema,
        onSubmit: (values) => {
            onSubmit(values.formStock);
            /*  console.log(values); */
        },
    });

    const loadData = () => {
        console.log("Stockkk",stock)
        formik.setFieldValue('formStock.product', stock?.product)
        formik.setFieldValue('formStock.amount', stock?.amount)
    }

    const takeProducts = async (enterpriseID) => {
        const { status, data } = await takeProductsInfo(enterpriseID);
        if (status === 200) {
            setProducts(data.data);
        }else{
            setProducts([]);
        }
    };


const validation = () => {
    setErrorInput('');
    console.log("Sartureee")
    if (formik.values.formStock.product === undefined || formik.values.formStock.product === '') {
        setErrorInput('formStock.product');
        return false;
    }
    else if (formik.values.formStock.amount === undefined || isNaN(formik.values.formStock.amount)) {
        setErrorInput('formStock.amount');
        return false;
    }
    return true;
}

const onSubmit = async (values) => {
    const validated = validation();

    if(validated){
        values.enterprise = user._id;

        let message;
        if(stock._id !== undefined){
            const { status, data } = await modifyStock(stock._id,values);
            if(status === 200){
                message = data.message;
                onOpenSnackBar({ severity: 'success', msg: message });
            }else{
                message = data.message;
                setMessageAlert({ severity: 'warning', msg: message });
            }
            
        }else{           
            const { status, data } = await addStock(values);
            if(status === 200){
                message = data.message;
                onOpenSnackBar({ severity: 'success', msg: message });
            }else{
                message = data.message;
                setMessageAlert({ severity: 'warning', msg: message });
            }
        }
        onFinish();
    }
}

const handleClose = (event, reason) => {
    
    formik.handleReset();
        if (reason !== 'backdropClick') {
            onHide();
        }
    };

    return (
        <>

    <Dialog key={stock} open={open} onHide={handleClose} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
        <DialogTitle>
            <Grid container direction="row" justifyContent="space-between" alignItems="center">
                {stock._id !== undefined ? <>Editar stock {stock._id}</> : <>Crear nuevo stock  </>}
                <IconButton aria-label="close" onClick={handleClose}>
                    <CloseIcon />
                </IconButton>
            </Grid>
        </DialogTitle>
        <DialogContent>
        <form encType="multipart/form-data" onSubmit={formik.handleSubmit}>
          <Grid container spacing={3}>

              {/*Product */}
            <Grid item xs={6}>
                <FormControl className={classes.formControl}>
                    <InputLabel id="label-product">Producto</InputLabel>
                    <Select
                        labelId="label-product"
                        id="formStock.product"
                        name="formStock.product"
                        value={formik.values.formStock.product}
                        onChange={formik.handleChange}
                        error={formik.touched.product && Boolean(formik.errors.product)}
                        helpertext={formik.touched.product && formik.errors.product}
                    >
                        {products.map((product, index) => {
                            return (
                                <MenuItem key={index} value={product._id}>
                                    {product.name}
                                </MenuItem>
                            );
                        })}
                    </Select>
                    <small style={{ color: errorInput === 'product' ? 'red' : 'black' }} className={classes.counter}>
                        {errorInput === 'product' ? 'Seleccione el tipo de iva' : ''}
                    </small>
                </FormControl>
            </Grid>

            {/*Cantidad */}
            <Grid item xs={6}>
                <TextField
                    fullWidth
                    type="number"
                    id="formStock.amount"
                    name="formStock.amount"
                    label="Cantidad"
                    value={formik.values.formStock.amount}
                    onChange={formik.handleChange}
                    error={formik.touched.formStock && Boolean(formik.errors.formStock)}
                    helpertext={formik.touched.formStock && formik.errors.formStock}
                />
                <small
                    style={{ color: errorInput === 'formStock.amount' ? 'red' : 'black' }}
                    className={classes.counter}
                >
                    {errorInput === 'formStock.amount' ? 'Ingrese la cantidad' : ''}
                </small>
            </Grid>

            { stock._id !== undefined ? <>
            
            {/* Checkbox publicar */}
            <Grid item xs={4}>
                <FormControlLabel
                    className={classes.formControl}
                    control={
                        <Checkbox
                            id="formStock.isActive"
                            checked={formik.values.formStock.isActive}
                            value={formik.values.formStock.isActive}
                            onChange={formik.handleChange}
                            name="formStock.isActive"
                        />
                    }
                    label="Activo"
                />
            </Grid>
            </> : ''}

        </Grid>
        <div className={classes.divBtn}>
            <Button onClick={onHide} variant="primary">Cancelar</Button>
            <Button className={classes.buttonRight} type="submit" variant="contained" color="primary">
                Guardar
            </Button>
        </div>
        </form>
        </DialogContent>
    </Dialog>
      </>
      );
}

export default Stock;