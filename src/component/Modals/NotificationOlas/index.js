import React, { useState, useEffect, useContext } from 'react';
import {
    Dialog, DialogTitle, DialogContent, Button, DialogActions, Grid, TextField, FormControl, InputLabel,
    Select, MenuItem, Divider, FormControlLabel, Checkbox
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useFormik } from 'formik';
import { takeAccountsConcert } from "../../../services/blog.service";
import { takeTokensApp } from "../../../services/token.service";
import { addNotificationOlas, modifyNotification } from '../../../services/notification.service';
import * as yup from 'yup';
import CircularProgress from '@material-ui/core/CircularProgress';
import { UserContext } from "../../../context/UserContext";
import AlertMsg from "../../Alert";
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { DateTimePicker } from "@material-ui/pickers";
import { es } from 'date-fns/locale';
import { format } from 'date-fns';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    formControl: {
        width: '100%',
    },
    btnActions: {
        margin: '10px',
    },
    buttonProgress: {
        marginLeft: '10px'
    },
    imgBanner: {
        width: '150px',
        height: '130px',
        objectFit: 'contain',
        marginTop: '10px',
        marginBottom: '10px'
    }
}));

const validationSchema = yup.object({
    title: yup.string().required(),
});

const NotificationOlas = ({ open, onClose, onRefresh, onOpenSnackBar, notification, blogID }) => {
    const classes = useStyles();
    const { user } = useContext(UserContext);
    const [isLoading, setIsLoading] = useState(false);
    const [isLoadingUser, setIsLoadinUser] = useState(false);
    const [isOpenAlert, setIsOpenAlert] = useState(false);
    const [messageAlert, setMessageAlert] = useState({});
    const [concerts, setConcerts] = useState([]);
    const [allUserLength, setAllUserLength] = useState();
    const [datePublication, SetDatePublication] = useState(null);

    useEffect(() => {
        resetForm();
        // takeConcerts();
        if (notification?._id !== undefined && notification?._id !== '') {
            loadData();
        }
    }, [open]);

    const loadData = () => {
        formik.setFieldValue('name', notification?.name)
        formik.setFieldValue('_id', notification?._id)
        formik.setFieldValue('description', notification?.description)
        formik.setFieldValue('banner', notification?.banner)
        formik.setFieldValue('promoter', notification?.promoter)
    }

    // const takeConcerts = async () => {
    //     const { status, data } = await takesBlogs('?category=concert&festival='+blogID+'&limitField=100');
    //     if (status === 200) {
    //         setConcerts(data.data);
    //     } else {
    //         console.log(status, data)
    //     }
    // }

    const handleClose = () => {
        onClose();
    };

    const onSubmit = async (values) => {
        setIsOpenAlert(false)
        setIsLoading(true);
        if (values.scheduledDate !== '' && values.scheduledDate !== null) {
            values.scheduledDate = values.scheduledDate + ':00+02:00';
        }
        if (values.type === 'snap' ) {
            const date = new Date();
            let month = date.getMonth() + 1;
            if (month < 10) month = '0' + month;
            
            let day = date.getDate() * 1;
            if (day < 10) day = '0' + day;
            
            let hour = date.getHours() * 1;
            if (hour < 10) hour = '0' + hour;

            let min = date.getMinutes() * 1;
            if (min < 10) min = '0' + min;

            let seg = date.getSeconds() * 1;
            if (seg < 10) seg = '0' + seg;
            
            values.scheduledDate = date.getFullYear() + '-' + month + '-' + day + 'T' + hour +
                                   ':' + min + ':' + seg + '.000Z';
        }
        let dataL;
        let statusL;
        if (notification?._id !== undefined && notification?._id !== '') {
            const { status, data } = await modifyNotification(notification?._id, values);
            dataL = data;
            statusL = status;
        } else {
            delete values._id;
            const { status, data } = await addNotificationOlas(values);
            dataL = data;
            statusL = status;
        }
        if (statusL === 200) {
            onOpenSnackBar({ severity: 'success', msg: dataL.message })
            setIsLoading(false);
            onRefresh()
            handleClose();
            resetForm();
        } else {
            setMessageAlert({ severity: 'warning', msg: dataL.message })
            setIsOpenAlert(true)
            setIsLoading(false);
        }
    }
    
    const takeAllUser = async () => {
        const { status, data } = await takeTokensApp('?app=ola');
        if (status === 200) {
            const allUsers = data.data;
            let allUserList = [];
            let allUserPublic = [];
            allUsers.forEach(user => {
                allUserList.push(user._id);
                allUserPublic.push({account: user._id, readed: false});
            });
            setAllUserLength(allUserList.length);
            formik.setFieldValue('accounts', allUserList);
            formik.setFieldValue('public', allUserPublic);
        } /*else {
            console.log(status, data)
        }*/
    }

    const handleChangeFilter = (e) => {
        setAllUserLength(null);
        if (e.target.value === 'everybody') {
            takeAllUser();
        }
        formik.setFieldValue('filter', e.target.value);
    }
    
    const takeUserForConcert = async (id) => {
        const { status, data } = await takeAccountsConcert(id);
        if (status === 200) {
            const allUsers = data.data;
            let allUserList = [];
            let allUserPublic = [];
            allUsers.forEach(user => {
                allUserList.push(user);
                allUserPublic.push({account: user, readed: false});
            });
            setAllUserLength(allUserList.length);
            formik.setFieldValue('accounts', allUserList);
            formik.setFieldValue('public', allUserPublic);
        } /*else {
            console.log(status, data)
        }*/
    }

    const handleChangeConcert = (e) => {
        takeUserForConcert(e.target.value);
        formik.setFieldValue('blog', e.target.value)
    }

    const handleDatePChange = (e) => {
        SetDatePublication(e)
        formik.setFieldValue('scheduledDate', format(e, "yyy-MM-dd'T'HH:mm"))
    }

    const resetForm = async () => {
        formik.handleReset();
    }

    const formik = useFormik({
        initialValues: {
            title: '',
            subtitle: '',
            promoter: user._id,
            isActive: true,
            isDetail: false,
            description: '',
            url: null,
            type: 'snap',
            filter: 'everybody',
            action: null,
            to: null,
            state: 'on hold',
            event: null,
            festival: blogID,
            blog: null,
            public: '',
            accounts: [],
            scheduledDate: null,
            app: 'ola',

        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            onSubmit(values);
        },
    });

    return (
        <>
            <Dialog open={open} onClose={handleClose} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Notificación</DialogTitle>
                <form encType="multipart/form-data" onSubmit={formik.handleSubmit}>
                    <DialogContent>
                        <Grid container spacing={3}>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id="title"
                                    name="title"
                                    label="Título"
                                    value={formik.values.title}
                                    onChange={formik.handleChange}
                                    error={formik.touched.title && Boolean(formik.errors.title)}
                                    helpertext={formik.touched.title && formik.errors.title}
                                />
                            </Grid>

                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id="description"
                                    name="description"
                                    label="Mensaje"
                                    value={formik.values.description}
                                    onChange={formik.handleChange}
                                    error={formik.touched.description && Boolean(formik.errors.description)}
                                    helpertext={formik.touched.description && formik.errors.description}
                                />
                            </Grid>

                            {/*<Grid item xs={12}>*/}
                            {/*    <FormControl className={classes.formControl}>*/}
                            {/*        <InputLabel id="label-type">Tipo</InputLabel>*/}
                            {/*        <Select*/}
                            {/*            labelId="label-type"*/}
                            {/*            id="type"*/}
                            {/*            name="type"*/}
                            {/*            value={formik.values.type}*/}
                            {/*            onChange={formik.handleChange}*/}
                            {/*            error={formik.touched.type && Boolean(formik.errors.type)}*/}
                            {/*            helpertext={formik.touched.type && formik.errors.type}*/}
                            {/*        >*/}
                            {/*            <MenuItem value='scheduled'>Programada</MenuItem>*/}
                            {/*            <MenuItem value='snap'>Instantánea</MenuItem>*/}
                            {/*        </Select>*/}
                            {/*    </FormControl>*/}
                            {/*</Grid>*/}

                            {/*<Grid item xs={6}>*/}
                            {/*    <FormControl className={classes.formControl}>*/}
                            {/*        <InputLabel id="label-filter">Filtro para envió</InputLabel>*/}
                            {/*        <Select*/}
                            {/*            labelId="label-filter"*/}
                            {/*            id="filter"*/}
                            {/*            name="filter"*/}
                            {/*            value={formik.values.filter}*/}
                            {/*            onChange={handleChangeFilter}*/}
                            {/*            error={formik.touched.filter && Boolean(formik.errors.filter)}*/}
                            {/*            helpertext={formik.touched.filter && formik.errors.filter}*/}
                            {/*        >*/}
                            {/*            <MenuItem value='everybody'>Todos los usuarios</MenuItem>*/}
                            {/*            <MenuItem value='everybodyevent'>Todos los usuarios de un concierto</MenuItem>*/}
                            {/*        </Select>*/}
                            {/*    </FormControl>*/}
                            {/*</Grid>*/}

                            {formik.values.filter === 'everybodyevent' ? (
                                <Grid item xs={12}>
                                    <FormControl className={classes.formControl}>
                                        <InputLabel id="label-blog">Seleccionar Concierto</InputLabel>
                                        <Select
                                            labelId="label-blog"
                                            id="blog"
                                            name="blog"
                                            value={formik.values.blog}
                                            onChange={handleChangeConcert}
                                            error={formik.touched.blog && Boolean(formik.errors.blog)}
                                            helpertext={formik.touched.blog && formik.errors.blog}
                                        >
                                            {concerts.map((concert, index) => {
                                                return (
                                                    <MenuItem key={index} value={concert._id}>
                                                        {concert.name}
                                                    </MenuItem>
                                                );
                                            })}
                                        </Select>
                                    </FormControl>
                                </Grid>
                            ) : <></>}

                            {formik.values.type === 'scheduled' ? (
                                <Grid item xs={12}>
                                    <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                                        <DateTimePicker
                                            autoOk
                                            fullWidth
                                            clearable
                                            id="datePublication"
                                            name="datePublication"
                                            ampm={false}
                                            value={datePublication}
                                            onChange={handleDatePChange}
                                            format="yyyy-MM-dd HH:mm"
                                            label="Fecha y hora de Envió"
                                        />
                                    </MuiPickersUtilsProvider>
                                </Grid>
                            ) : <></>}

                            {formik.values.filter !== 'everybody' ? (
                                <Grid item xs={3}>
                                    <FormControlLabel
                                        className={classes.formControl}
                                        control={<Checkbox id="isDetail" checked={formik.values.isDetail} onChange={formik.handleChange} name="isDetail" />}
                                        label="Ver Detalle"
                                    />
                                </Grid>
                            ) : <></>}

                            <Grid item xs={12}>
                                {isLoadingUser && <CircularProgress color="secondary" size={24} className={classes.buttonProgress} />}
                                
                                {allUserLength !== undefined && allUserLength !== '' && allUserLength !== null && allUserLength !== 0 && <AlertMsg severity='success' msg={'Se seleccionarón ' + allUserLength + ' usuarios'} />}

                                {allUserLength === 0 && <AlertMsg severity='warning' msg={'No se encontrarón usuarios'} />}
                            </Grid>

                        </Grid>

                    </DialogContent>
                    <DialogActions className={classes.btnActions}>
                        <Button type="button" onClick={handleClose} color="secondary">
                            Cerrar
                        </Button>
                        <Button type="submit" variant="contained" color="primary">
                            Guardar
                            {isLoading && <CircularProgress color="secondary" size={24} className={classes.buttonProgress} />}
                        </Button>
                    </DialogActions>

                    {isOpenAlert &&
                        <Grid item xs={12}>
                            <AlertMsg severity={messageAlert.severity} msg={messageAlert.msg} />
                        </Grid>
                    }
                    <Divider />
                </form>
            </Dialog>

        </>
    )
};

export default NotificationOlas;