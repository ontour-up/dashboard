import 'moment/locale/es';

import {
    Button,
    Checkbox,
    Dialog,
    DialogContent,
    DialogTitle,
    FormControl,
    FormControlLabel,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    TextField
} from '@material-ui/core';
import React, { useContext, useEffect, useState } from 'react';

import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import { UserContext } from '../../../context/UserContext';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import { useFormik } from 'formik';
import { addStock, modifyStock } from '../../../services/stock.js';
import { takeProductsInfo } from '../../../services/product';
import { sendInvitation } from '../../../services/transactions.service';
import { Tab, Tabs } from 'react-bootstrap';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    backButton: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    formControl: {
        width: '100%',
    },
    chip: {
        marginTop: '10px',
        marginRight: '5px',
    },
    divBtn: {
        marginTop: '30px',
        marginBottom: '20px',
    },
    buttonProgress: {
        marginLeft: '10px',
    },
    imgBanner: {
        width: '150px',
        height: '130px',
        objectFit: 'contain',
        marginTop: '10px',
        marginBottom: '10px',
    },
    ticket: {
        backgroundColor: 'white',
        boxShadow: '0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)',
        borderRadius: '10px',
        marginBottom: '30px',
        marginLeft: '10px',
        width: '600px',
        height: '70px',
    },
    textTicket: {
        marginLeft: '10px',
        fontSize: '20px',
        marginBottom: '8px',
    },
    buttonRight: {
        float: 'right',
    },
}));

    function Invitation({open, onHide, onRefresh, onFinish, onOpenSnackBar, event, tickets}) {
    moment.locale('es');
    const classes = useStyles();
    const { user } = useContext(UserContext);
    const [errorInput, setErrorInput] = useState('');
    const [messageAlert, setMessageAlert] = useState({});

    useEffect(() => {
        setErrorInput('');
        formikEmail.handleReset();
        formikPDF.handleReset();
    }, [event]);

    const formikEmail = useFormik({
        initialValues: {
            form: {
            },
        },
        onSubmit: (values) => {
            onSubmitEmail(values.form);
        },
    });

    const formikPDF = useFormik({
        initialValues: {
            form: {
            },
        },
        onSubmit: (values) => {
            onSubmitPDF(values.form);
        },
    });


    const validateEmail = (mail) => {
        if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(mail))
        {
            return (true)
        }
            return (false)
    }


const validationEmail = () => {
    setErrorInput('');
    console.log("Sartureee")
    if (formikEmail.values.form.ticket === undefined || formikEmail.values.form.ticket === '') {
        setErrorInput('form.ticket');
        return false;
    }else if (formikEmail.values.form.amount === undefined || formikEmail.values.form.amount === '') {
        setErrorInput('form.amount');
        return false;
    }else if (formikEmail.values.form.email === undefined || formikEmail.values.form.email === '' || validateEmail(formikEmail.values.form.email)) {
        setErrorInput('form.email');
        return false;
    }
    return true;
}

const validationPDF = () => {
    setErrorInput('');
    console.log("Sartureee")
    if (formikPDF.values.form.ticket === undefined || formikPDF.values.form.ticket === '') {
        setErrorInput('form.ticket');
        return false;
    }else if (formikPDF.values.form.amount === undefined || formikPDF.values.form.amount === '') {
        setErrorInput('form.amount');
        return false;
    }else if (formikPDF.values.form.email !== undefined && formikPDF.values.form.email !== '') {
        if(validateEmail(formikPDF.values.form.email)){
            setErrorInput('form.email');
            return false;
        }
    }
    return true;
}


const onSubmitEmail = async (values) => {
    const validated = validationEmail();
    if(validated){
        let message;
        const buyersInfo = {
            name : values?.name,
            lastname : values?.lastname,
            email: values?.email,
            phone: values?.phone
        }

        const obj = {
            ticket : values.ticket,
            num_entradas : values.amount,
            buyersInfo : buyersInfo
        }

        const { status, data } = await sendInvitation(obj);
        if(status === 200){
            console.log("Invitation SUCCESS",status);
            message = data.message;
            onOpenSnackBar({ severity: 'success', msg: message });
        }else{
            console.log("Invitation Warning",status);
            message = data.message;
            setMessageAlert({ severity: 'warning', msg: message });
        }
            
        //}
        onFinish();
    }   
}

const onSubmitPDF = async (values) => {
    const validated = validationPDF();
    if(validated){
        let message;
        const buyersInfo = {
            name : values?.name,
            lastname : values?.lastname,
            email: values?.email,
            phone: values?.phone
        }

        const obj = {
            ticket : values.ticket,
            num_entradas : values.amount,
            buyersInfo : buyersInfo
        }

        const { status, data } = await sendInvitation(obj);
        if(status === 200){
            console.log("Invitation SUCCESS",status);
            message = data.message;
            onOpenSnackBar({ severity: 'success', msg: message });
        }else{
            console.log("Invitation Warning",status);
            message = data.message;
            setMessageAlert({ severity: 'warning', msg: message });
        }
            
        //}
        onFinish();
    }   
}

const handleClose = (event, reason) => {
    
    formikPDF.handleReset();
    formikEmail.handleReset();
        if (reason !== 'backdropClick') {
            onHide();
        }
    };

    return (
        <>

    <Dialog open={open} onHide={handleClose} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
        <DialogTitle>
            <Grid container direction="row" justifyContent="space-between" alignItems="center">
               Crear invitación
                <IconButton aria-label="close" onClick={handleClose}>
                    <CloseIcon />
                </IconButton>
            </Grid>
        </DialogTitle>
        <DialogContent>
        <Tabs defaultActiveKey="invitacionEmail" id="tab-detail" className="mb-3 mt-3 nav-justified">
            <Tab eventKey="invitacionEmail" title="Invitación por Email">
                <form encType="multipart/form-data" onSubmit={formikEmail.handleSubmit}>
                    <Grid container spacing={3}>

                        {/*ticket */}
                        <Grid item xs={6}>
                            <FormControl className={classes.formControl}>
                                <InputLabel id="label-ticket">Entrada</InputLabel>
                                <Select
                                    labelId="label-ticket"
                                    id="form.ticket"
                                    name="form.ticket"
                                    value={formikEmail.values.form.ticket}
                                    onChange={formikEmail.handleChange}
                                    error={formikEmail.touched.form && Boolean(formikEmail.errors.form)}
                                    helpertext={formikEmail.touched.form && formikEmail.errors.form}
                                >
                                    {tickets.map((ticket, index) => {
                                        return (
                                            <MenuItem key={index} value={ticket._id}>
                                                {ticket.name}
                                            </MenuItem>
                                        );
                                    })}
                                </Select>
                                <small
                                    style={{ color: errorInput === 'form.ticket' ? 'red' : 'black' }}
                                    className={classes.counter}
                                >
                                {errorInput === 'form.ticket' ? 'Seleccione una entrada' : ''}
                            </small>
                            </FormControl>
                        </Grid>

                        {/*Cantidad */}
                        <Grid item xs={6}>
                            <TextField
                                fullWidth
                                type="number"
                                id="form.amount"
                                name="form.amount"
                                label="Cantidad"
                                value={formikEmail.values.form.amount}
                                onChange={formikEmail.handleChange}
                                error={formikEmail.touched.form && Boolean(formikEmail.errors.form)}
                                helpertext={formikEmail.touched.form && formikEmail.errors.form}
                            />
                            <small
                                style={{ color: errorInput === 'form.amount' ? 'red' : 'black' }}
                                className={classes.counter}
                            >
                                {errorInput === 'form.amount' ? 'Ingrese la cantidad' : ''}
                            </small>
                        </Grid>

                        <Grid item xs={6}>
                            <TextField
                                fullWidth
                                id="form.name"
                                name="form.name"
                                label="Nombre"
                                value={formikEmail.values.form.name}
                                onChange={formikEmail.handleChange}
                                error={formikEmail.touched.form && Boolean(formikEmail.errors.form)}
                                helpertext={formikEmail.touched.form && formikEmail.errors.form}
                            />
                            <small
                                style={{ color: errorInput === 'form.name' ? 'red' : 'black' }}
                                className={classes.counter}
                            >
                                {errorInput === 'form.name' ? 'Ingrese el nombre' : ''}
                            </small>
                        </Grid>

                        <Grid item xs={6}>
                            <TextField
                                fullWidth
                                id="form.lastname"
                                name="form.lastname"
                                label="Apellido"
                                value={formikEmail.values.form.lastname}
                                onChange={formikEmail.handleChange}
                                error={formikEmail.touched.form && Boolean(formikEmail.errors.form)}
                                helpertext={formikEmail.touched.form && formikEmail.errors.form}
                            />
                            <small
                                style={{ color: errorInput === 'form.lastname' ? 'red' : 'black' }}
                                className={classes.counter}
                            >
                                {errorInput === 'form.lastname' ? 'Ingrese el apellido.' : ''}
                            </small>
                        </Grid>

                        <Grid item xs={6}>
                            <TextField
                                fullWidth
                                id="form.email"
                                name="form.email"
                                label="Email *"
                                type="email"
                                value={formikEmail.values.form.email}
                                onChange={formikEmail.handleChange}
                                error={formikEmail.touched.form && Boolean(formikEmail.errors.form)}
                                helpertext={formikEmail.touched.form && formikEmail.errors.form}
                            />
                            <small
                                style={{ color: errorInput === 'form.email' ? 'red' : 'black' }}
                                className={classes.counter}
                            >
                                {errorInput === 'form.email' ? 'Ingrese un email valido.' : ''}
                            </small>
                        </Grid>

                        <Grid item xs={6}>
                            <TextField
                                fullWidth
                                id="form.phone"
                                name="form.phone"
                                label="Telefono"
                                type="number"
                                value={formikEmail.values.form.phone}
                                onChange={formikEmail.handleChange}
                                error={formikEmail.touched.form && Boolean(formikEmail.errors.form)}
                                helpertext={formikEmail.touched.form && formikEmail.errors.form}
                            />
                            <small
                                style={{ color: errorInput === 'form.phone' ? 'red' : 'black' }}
                                className={classes.counter}
                            >
                                {errorInput === 'form.phone' ? 'Ingrese un telefono.' : ''}
                            </small>
                        </Grid>
                    </Grid>
                    <div className={classes.divBtn}>
                        <Button onClick={onHide} variant="primary">Cancelar</Button>
                        <Button className={classes.buttonRight} type="submit" variant="contained" color="primary">
                            Enviar invitación
                        </Button>
                    </div>
                </form>
            </Tab>
            <Tab eventKey="invitacionPDF" title="Invitación en PDF">
                <form encType="multipart/form-data" onSubmit={formikPDF.handleSubmit}>
                    <Grid container spacing={3}>

                        {/*ticket */}
                        <Grid item xs={6}>
                            <FormControl className={classes.formControl}>
                                <InputLabel id="label-ticket">Entrada</InputLabel>
                                <Select
                                    labelId="label-ticket"
                                    id="form.ticket"
                                    name="form.ticket"
                                    value={formikPDF.values.form.ticket}
                                    onChange={formikPDF.handleChange}
                                    error={formikPDF.touched.form && Boolean(formikPDF.errors.form)}
                                    helpertext={formikPDF.touched.form && formikPDF.errors.form}
                                >
                                    {tickets.map((ticket, index) => {
                                        return (
                                            <MenuItem key={index} value={ticket._id}>
                                                {ticket.name}
                                            </MenuItem>
                                        );
                                    })}
                                </Select>
                                <small
                                    style={{ color: errorInput === 'form.ticket' ? 'red' : 'black' }}
                                    className={classes.counter}
                                >
                                {errorInput === 'form.ticket' ? 'Seleccione una entrada' : ''}
                            </small>
                            </FormControl>
                        </Grid>

                        {/*Cantidad */}
                        <Grid item xs={6}>
                            <TextField
                                fullWidth
                                type="number"
                                id="form.amount"
                                name="form.amount"
                                label="Cantidad"
                                value={formikPDF.values.form.amount}
                                onChange={formikPDF.handleChange}
                                error={formikPDF.touched.form && Boolean(formikPDF.errors.form)}
                                helpertext={formikPDF.touched.form && formikPDF.errors.form}
                            />
                            <small
                                style={{ color: errorInput === 'form.amount' ? 'red' : 'black' }}
                                className={classes.counter}
                            >
                                {errorInput === 'form.amount' ? 'Ingrese la cantidad' : ''}
                            </small>
                        </Grid>

                        <Grid item xs={6}>
                            <TextField
                                fullWidth
                                id="form.name"
                                name="form.name"
                                label="Nombre"
                                value={formikPDF.values.form.name}
                                onChange={formikPDF.handleChange}
                                error={formikPDF.touched.form && Boolean(formikPDF.errors.form)}
                                helpertext={formikPDF.touched.form && formikPDF.errors.form}
                            />
                            <small
                                style={{ color: errorInput === 'form.name' ? 'red' : 'black' }}
                                className={classes.counter}
                            >
                                {errorInput === 'form.name' ? 'Ingrese el nombre' : ''}
                            </small>
                        </Grid>

                        <Grid item xs={6}>
                            <TextField
                                fullWidth
                                id="form.lastname"
                                name="form.lastname"
                                label="Apellido"
                                value={formikPDF.values.form.lastname}
                                onChange={formikPDF.handleChange}
                                error={formikPDF.touched.form && Boolean(formikPDF.errors.form)}
                                helpertext={formikPDF.touched.form && formikPDF.errors.form}
                            />
                            <small
                                style={{ color: errorInput === 'form.lastname' ? 'red' : 'black' }}
                                className={classes.counter}
                            >
                                {errorInput === 'form.lastname' ? 'Ingrese el apellido.' : ''}
                            </small>
                        </Grid>

                        <Grid item xs={6}>
                            <TextField
                                fullWidth
                                id="form.email"
                                name="form.email"
                                label="Email"
                                type="email"
                                value={formikPDF.values.form.email}
                                onChange={formikPDF.handleChange}
                                error={formikPDF.touched.form && Boolean(formikPDF.errors.form)}
                                helpertext={formikPDF.touched.form && formikPDF.errors.form}
                            />
                            <small
                                style={{ color: errorInput === 'form.email' ? 'red' : 'black' }}
                                className={classes.counter}
                            >
                                {errorInput === 'form.email' ? 'Ingrese un email valido.' : ''}
                            </small>
                        </Grid>

                        <Grid item xs={6}>
                            <TextField
                                fullWidth
                                id="form.phone"
                                name="form.phone"
                                label="Telefono"
                                type="number"
                                value={formikPDF.values.form.phone}
                                onChange={formikPDF.handleChange}
                                error={formikPDF.touched.form && Boolean(formikPDF.errors.form)}
                                helpertext={formikPDF.touched.form && formikPDF.errors.form}
                            />
                            <small
                                style={{ color: errorInput === 'form.phone' ? 'red' : 'black' }}
                                className={classes.counter}
                            >
                                {errorInput === 'form.phone' ? 'Ingrese un telefono.' : ''}
                            </small>
                        </Grid>
                    </Grid>
                    <div className={classes.divBtn}>
                        <Button onClick={onHide} variant="primary">Cancelar</Button>
                        <Button className={classes.buttonRight} type="submit" variant="contained" color="primary">
                            Generar PDF
                        </Button>
                    </div>
                </form>
            </Tab>
        </Tabs>
        
        </DialogContent>
    </Dialog>
      </>
      );
}

export default Invitation;