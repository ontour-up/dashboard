import 'moment/locale/es';

import {
    Button,
    Dialog,
    DialogContent,
    DialogTitle,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    TextField,
    FormControlLabel,
    Checkbox
} from '@material-ui/core';
import React, { useContext, useEffect, useState } from 'react';
import { addTicket, modifyTicket, takeTicket } from '../../../services/ticket.service.js';

import CloseIcon from '@material-ui/icons/Close';
import DateFnsUtils from '@date-io/date-fns';
import { DateTimePicker } from '@material-ui/pickers';
import IconButton from '@material-ui/core/IconButton';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { UserContext } from '../../../context/UserContext';
import { es } from 'date-fns/locale';
import { getChart } from '../../../services/charts.service';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import { takeSpaces } from '../../../services/space.service';
import { useFormik } from 'formik';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    backButton: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    formControl: {
        width: '100%',
    },
    chip: {
        marginTop: '10px',
        marginRight: '5px',
    },
    divBtn: {
        marginTop: '30px',
        marginBottom: '20px',
    },
    buttonProgress: {
        marginLeft: '10px',
    },
    imgBanner: {
        width: '150px',
        height: '130px',
        objectFit: 'contain',
        marginTop: '10px',
        marginBottom: '10px',
    },
    ticket: {
        backgroundColor: 'white',
        boxShadow: '0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)',
        borderRadius: '10px',
        marginBottom: '30px',
        marginLeft: '10px',
        width: '600px',
        height: '70px',
    },
    textTicket: {
        marginLeft: '10px',
        fontSize: '20px',
        marginBottom: '8px',
    },
    buttonRight: {
        float: 'right',
    },
}));

    function Entrance({open, onHide, onRefresh, onFinish, onOpenSnackBar, event, ticket}) {
    moment.locale('es');
    const classes = useStyles();
    const { user } = useContext(UserContext);
    const [ ticketData, setTicketData ] = useState({});
    const [errorInput, setErrorInput] = useState('');
    const [basePrecio, setBasePrecio] = useState();
    const [ivaPrecio, setIvaPrecio] = useState();
    const [totalPrecio, setTotalPrecio] = useState();
    const [comisionPrecio, setComisionPrecio] = useState();
    const [dateStartEntrance, SetDateStartEntrance] = useState(null);
    const [dateEndEntrance, SetDateEndEntrance] = useState(null);
    const [dateStartSellEntrance, SetDateStartSellEntrance] = useState(null);
    const [dateEndSellEntrance, SetDateEndSellEntrance] = useState(null);
    const [messageAlert, setMessageAlert] = useState({});
    const [ categories, setCategories ] = useState(); 

    

    const [eventTypes] = React.useState([
        { value: 'presential', label: 'Presencial' },
        { value: 'streaming', label: 'Streaming' },
        { value: 'rent', label: 'Alquiler' },
        { value: 'online', label: 'On-line' },
    ]);

    const [typesIva] = React.useState([
        { value: 0, label: '0%' },
        { value: 10, label: '10%' },
        { value: 21, label: '21%' },
    ]);

    useEffect(() => {
        setErrorInput('');
        formik.handleReset();
        SetDateStartEntrance(null);
        SetDateEndEntrance(null);
        SetDateStartSellEntrance(null);
        SetDateEndSellEntrance(null);
        if(ticket._id !== undefined){
            takeTicketInfo(ticket._id);
        }else{
            setTicketData([]);
        }
    }, [ticket]);

    useEffect(() =>{
        if(event.isChart){
            takeCategories(event.space);
        }
        if(ticketData._id !== undefined){
            loadData();
            //calculoPrecio();
        }
    }, [ticketData])

    const takeTicketInfo = async (id) => {
        const { status, data } = await takeTicket({ticketID : id});
        if (status === 200) {
            setTicketData(data.data);
        } else {
            setTicketData([]);
        }
    };

    const takeCategories = async (spaceId) => {
        const query = {
            _id : spaceId
        };
        const { status, data } = await takeSpaces(query);
        if (status === 200) {
            const space = await(getChart(data.data[0].chartKey));
            setCategories(space.categories.list);
        } else {
            /* console.log(status, data); */
        }
    };

    const formik = useFormik({
        initialValues: {
            formTicket: {
                name: '',
                description: '',
                //html: '',
                //image: '',
                amount: 0,
                commission: 0,
                amountPerPerson: 0,
                //category: '',
                chartCategory: '',
                //currency: '€',
                grossPrice: 0,
                netPrice: 0,
                ivaPercent: 0,
                startDate: new Date(),
                endDate: new Date(),
                sellStartDate: new Date(),
                sellEndDate: new Date(),
                //initialBalance: null,
                type: 'presential',
                promoter: '',
                event: '',
                isActive: true,
            },
    },
    // validationSchema: validationSchema,
    onSubmit: (values) => {
        onSubmit(values.formTicket);
        /*  console.log(values); */
    },
});

  const loadData = () => {
         formik.setFieldValue('formTicket.name', ticketData?.name)
         formik.setFieldValue('formTicket.description', ticketData?.description)
         formik.setFieldValue('formTicket.amount', ticketData?.amount)
         formik.setFieldValue('formTicket.commission', ticketData?.commission)
         formik.setFieldValue('formTicket.amountPerPerson', ticketData?.amountPerPerson)
         formik.setFieldValue('formTicket.grossPrice', ticketData?.grossPrice)
         formik.setFieldValue('formTicket.netPrice', ticketData?.netPrice)
         formik.setFieldValue('formTicket.ivaPercent', ticketData?.ivaPercent)
         formik.setFieldValue('formTicket.type', ticketData?.type)
         formik.setFieldValue('formTicket.chartCategory', ticketData?.chartCategory)
         formik.setFieldValue('formTicket.isActive', ticketData?.isActive)
           
         if (ticketData?.startDate) {
            formik.setFieldValue('formTicket.startDate', moment(ticketData.startDate).format())
            SetDateStartEntrance(moment(ticketData.startDate).format())
         }
         if (ticketData?.endDate) {
            formik.setFieldValue('formTicket.endDate', moment(ticketData.endDate).format())
            SetDateEndEntrance(moment(ticketData.endDate).format())
         }
         if (ticketData?.sellStartDate) {
            formik.setFieldValue('formTicket.sellStartDate', moment(ticketData.sellStartDate).format())
            SetDateStartSellEntrance(moment(ticketData.sellStartDate).format())
        }
        if (ticketData?.sellEndDate) {
            formik.setFieldValue('formTicket.sellEndDate', moment(ticketData.sellEndDate).format())
            SetDateEndSellEntrance(moment(ticketData.sellEndDate).format())
        }
      }

/** * fechas entradas */
const handleDateStartEntranceChange = (e) => {
    SetDateStartEntrance(e);
    if (e !== '' && e !== null) {
        formik.setFieldValue('formTicket.startDate', e.toISOString());
    } else {
        formik.setFieldValue('formTicket.startDate', e);
    }
};

const handleDateEndEntranceChange = (e) => {
    SetDateEndEntrance(e);
    if (e !== '' && e !== null) {
        formik.setFieldValue('formTicket.endDate', e.toISOString());
    } else {
        formik.setFieldValue('formTicket.endDate', e);
    }
};

const handleDateStartSellEntranceChange = (e) => {
    SetDateStartSellEntrance(e);
    if (e !== '' && e !== null) {
        formik.setFieldValue('formTicket.sellStartDate', e.toISOString());
    } else {
        formik.setFieldValue('formTicket.sellStartDate', e);
    }
};

const handleDateEndSellEntranceChange = (e) => {
    SetDateEndSellEntrance(e);
    if (e !== '' && e !== null) {
        formik.setFieldValue('formTicket.sellEndDate', e.toISOString());
    } else {
        formik.setFieldValue('formTicket.sellEndDate', e);
    }
};

const handleChangeIva = (e) => {
    formik.setFieldValue('formTicket.ivaPercent', e.target.value);
}

const calculoPrecio = () => {
    var netPrice = formik.values.formTicket.netPrice;

    var ivaPercent = formik.values.formTicket.ivaPercent / 100 + 1;

    var importeIVA = netPrice / ivaPercent;

    var precioIVAIncluido = netPrice - importeIVA;

    var base = Math.round(importeIVA * 100) / 100;

    var iva = Math.round(precioIVAIncluido * 100) / 100;

    var comision = (netPrice * formik.values.formTicket.commission) / 100;

    var total = base + comision + iva;

    //formik.setFieldValue('formTicket.grossPrice', base)   
    setBasePrecio(base);
    setIvaPrecio(iva);
    setComisionPrecio(comision);
    setTotalPrecio(total);
};

const validation = () => {
    setErrorInput('');
    //console.log("DateStart",dateStartEntrance);
    if (formik.values.formTicket.name === '') {
        setErrorInput('formTicket.name');
        return false;
    }else if(event.isChart && formik.values.formTicket.chartCategory === ''){
        setErrorInput('formTicket.chartCategory');
        return false;
    } 
    else if (formik.values.formTicket.description === '') {
        setErrorInput('formTicket.description');
        return false;
    }
    else if (formik.values.formTicket.amount === '') {
        setErrorInput('formTicket.amount');
        return false;
    }
    else if (formik.values.formTicket.amountPerPerson === '') {
        setErrorInput('formTicket.amountPerPerson');
        return false;
    }
    else if (formik.values.formTicket.grossPrice === '') {
        setErrorInput('formTicket.grossPrice');
        return false;
    }
    else if (formik.values.formTicket.number === '') {
        setErrorInput('formTicket.number');
        return false;
    }
    else if (dateStartEntrance === null) {
        setErrorInput('formTicket.startDate');
        return false;
    }
    else if (dateEndEntrance === null) {
        setErrorInput('formTicket.endDate');
        return false;
    }
    else if (dateStartSellEntrance === null) {
        setErrorInput('formTicket.sellStartDate');
        return false;
    }
    else if (dateEndSellEntrance === null) {
        setErrorInput('formTicket.sellEndDate');
        return false;
    }
    return true;
}

const onSubmit = async (valuesTicket) => {

    const validated = validation();

    if(validated){

        valuesTicket.event = event._id;
        valuesTicket.promoter = user._id;
        valuesTicket.category = event.category[0];
        
        // Calculate Tickets price
        /*
        var netPrice = valuesTicket.netPrice;
        var ivaPercent = valuesTicket.ivaPercent / 100 + 1;
        var importeIVA = netPrice / ivaPercent;
        var base = Math.round(importeIVA * 100) / 100;

        valuesTicket.grossPrice = base;
        */

        // El en dashboard viejo el netPrice y el grossPrice son iguales
        valuesTicket.grossPrice = formik.values.formTicket.netPrice;

        let message;
        if(ticketData._id !== undefined){
            const { status, data } = await modifyTicket(ticketData._id,valuesTicket);
            if(status === 200){
                message = data.message;
                onOpenSnackBar({ severity: 'success', msg: message });
            }else{
                message = data.message;
                setMessageAlert({ severity: 'warning', msg: message });
            }
        }else{
            const { status, data } = await addTicket(valuesTicket);
            if(status === 200){
                message = data.message;
                onOpenSnackBar({ severity: 'success', msg: message });
            }else{
                message = data.message;
                setMessageAlert({ severity: 'warning', msg: message });
            }
        }
        
        onFinish();
    }
}

const handleClose = (event, reason) => {
    
    formik.handleReset();
        if (reason !== 'backdropClick') {
            onHide();
            setBasePrecio();
            setIvaPrecio();
            setTotalPrecio();
            setComisionPrecio();
            SetDateStartEntrance();
            SetDateEndEntrance();
            SetDateStartSellEntrance();
            SetDateEndSellEntrance();
        }
    };

    return (
        <>

    <Dialog key={ticket} open={open} onHide={handleClose} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
        <DialogTitle>
            <Grid container direction="row" justifyContent="space-between" alignItems="center">
                {ticketData._id !== undefined ? <>Editar entrada {ticketData._id}</> : <>Crear nueva entrada  </>}
                <IconButton aria-label="close" onClick={handleClose}>
                    <CloseIcon />
                </IconButton>
            </Grid>
        </DialogTitle>
        <DialogContent>
        <form encType="multipart/form-data" onSubmit={formik.handleSubmit}>
          <Grid container spacing={3}>
                {/*nombre entrada */}
                <Grid item xs={6}>
                    <TextField
                        fullWidth
                        id="formTicket.name"
                        name="formTicket.name"
                        label="Nombre entrada"
                        value={formik.values.formTicket.name}
                        onChange={formik.handleChange}
                        error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                        helpertext={formik.touched.formTicket && formik.errors.formTicket}
                    />
                    <small
                        style={{ color: errorInput === 'formTicket.name' ? 'red' : 'black' }}
                        className={classes.counter}
                    >
                        {errorInput === 'formTicket.name' ? 'Ingrese nombre de entrada.' : ''}
                    </small>
                </Grid>

                    {/*Categoria */}
                    { categories !== undefined && categories.length > 0 ? 
                    <Grid item xs={4}>
                        <FormControl className={classes.formControl}>
                            <InputLabel id="category">Categoria</InputLabel>
                            <Select
                                labelId="category"
                                id="formTicket.chartCategory"
                                name="formTicket.chartCategory"
                                value={formik.values.formTicket.chartCategory}
                                onChange={formik.handleChange}
                                error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                                helpertext={formik.touched.formTicket && formik.errors.formTicket}
                            >
                                {categories.map((categorie, index) => {
                                    return (
                                        <MenuItem key={index} value={categorie.label}>
                                            {categorie.label}
                                        </MenuItem>
                                    );
                                })}
                            </Select>
                            <small
                                style={{ color: errorInput === 'formTicket.chartCategory' ? 'red' : 'black' }}
                                className={classes.counter}
                            >
                                {errorInput === 'formTicket.chartCategory' ? 'Seleccione la categoria' : ''}
                            </small>
                        </FormControl>
                    </Grid>
                    : ''}

                {/*Descripcion */}
                <Grid item xs={12}>
                    <TextField
                        fullWidth
                        id="formTicket.description"
                        name="formTicket.description"
                        label="Descripcion"
                        value={formik.values.formTicket.description}
                        onChange={formik.handleChange}
                        error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                        helpertext={formik.touched.formTicket && formik.errors.formTicket}
                    />
                    <small
                        style={{ color: errorInput === 'formTicket.description' ? 'red' : 'black' }}
                        className={classes.counter}
                    >
                        {errorInput === 'formTicket.description'
                            ? 'Ingrese la descripción de la entrada.'
                            : ''}
                    </small>
                </Grid>

                {/*Tipo Entrada */}
                <Grid item xs={4}>
                    <FormControl className={classes.formControl}>
                        <InputLabel id="label-category">Tipo Entrada</InputLabel>
                        <Select
                            labelId="label-category"
                            id="formTicket.type"
                            name="formTicket.type"
                            value={formik.values.formTicket.type}
                            onChange={formik.handleChange}
                            error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                            helpertext={formik.touched.formTicket && formik.errors.formTicket}
                        >
                            {eventTypes.map((cagegorie, index) => {
                                return (
                                    <MenuItem key={index} value={cagegorie.value}>
                                        {cagegorie.label}
                                    </MenuItem>
                                );
                            })}
                        </Select>
                        <small
                            style={{ color: errorInput === 'formTicket.type' ? 'red' : 'black' }}
                            className={classes.counter}
                        >
                            {errorInput === 'formTicket.type' ? 'Seleccione el tipo de entrada' : ''}
                        </small>
                    </FormControl>
                </Grid>

                {/*Tipo Iva */}
                <Grid item xs={4}>
                    <FormControl className={classes.formControl}>
                        <InputLabel id="label-category">Tipo Iva</InputLabel>
                        <Select
                            labelId="label-category"
                            id="formTicket.ivaPercent"
                            name="formTicket.ivaPercent"
                            value={formik.values.formTicket.ivaPercent}
                            onChange={handleChangeIva}
                            error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                            helpertext={formik.touched.formTicket && formik.errors.formTicket}
                        >
                            {typesIva.map((iva, index) => {
                                return (
                                    <MenuItem key={index} value={iva.value}>
                                        {iva.label}
                                    </MenuItem>
                                );
                            })}
                        </Select>
                        <small
                            style={{ color: errorInput === 'typesIva' ? 'red' : 'black' }}
                            className={classes.counter}
                        >
                            {errorInput === 'typesIva' ? 'Seleccione el tipo de iva' : ''}
                        </small>
                    </FormControl>
                </Grid>

                {/*Cantidad */}
                <Grid item xs={4}>
                    <TextField
                        fullWidth
                        type="number"
                        id="formTicket.amount"
                        name="formTicket.amount"
                        label="Cantidad"
                        value={formik.values.formTicket.amount}
                        onChange={formik.handleChange}
                        error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                        helpertext={formik.touched.formTicket && formik.errors.formTicket}
                    />
                    <small
                        style={{ color: errorInput === 'formTicket.amount' ? 'red' : 'black' }}
                        className={classes.counter}
                    >
                        {errorInput === 'formTicket.amount' ? 'Ingrese la cantidad de entradas' : ''}
                    </small>
                </Grid>

                {/*Comision */}
                <Grid item xs={4}>
                    <TextField
                        type="number"
                        fullWidth
                        id="formTicket.commission"
                        name="formTicket.commission"
                        label="Comision %"
                        value={formik.values.formTicket.commission}
                        onChange={formik.handleChange}
                        error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                        helpertext={formik.touched.formTicket && formik.errors.formTicket}
                    />
                    <small
                        style={{ color: errorInput === 'commission' ? 'red' : 'black' }}
                        className={classes.counter}
                    >
                        {errorInput === 'commission' ? 'Ingrese la comisión' : ''}
                    </small>
                </Grid>

                {/*Precio */}
                <Grid item xs={4}>
                    <TextField
                        fullWidth
                        type="number"
                        id="formTicket.netPrice"
                        name="formTicket.netPrice"
                        label="Precio"
                        value={formik.values.formTicket.netPrice}
                        onChange={formik.handleChange}
                        error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                        helpertext={formik.touched.formTicket && formik.errors.formTicket}
                    />
                    <small
                        style={{ color: errorInput === 'price' ? 'red' : 'black' }}
                        className={classes.counter}
                    >
                        {errorInput === 'price' ? 'Ingrese el precio.' : ''}
                    </small>
                </Grid>

                {/*Cantidad por persona */}
                <Grid item xs={4}>
                    <TextField
                        fullWidth
                        type="number"
                        id="formTicket.amountPerPerson"
                        name="formTicket.amountPerPerson"
                        label="Cantidad máxima de entradas/persona"
                        value={formik.values.formTicket.amountPerPerson}
                        onChange={formik.handleChange}
                        error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                        helpertext={formik.touched.formTicket && formik.errors.formTicket}
                    />
                    <small
                        style={{ color: errorInput === 'maximumEntrance' ? 'red' : 'black' }}
                        className={classes.counter}
                    >
                        {errorInput === 'maximumEntrance' ? 'Ingrese el maximo por persona.' : ''}
                    </small>
                </Grid>

                {/*Desglose precio */}
                <Grid item xs={8}>
                    <InputLabel>Desglose precio</InputLabel>
                    <br />
                    {formik.values.formTicket.netPrice > 0 ? (
                        <>
                            <InputLabel>Base: {basePrecio} €</InputLabel>
                            <InputLabel>Iva: {ivaPrecio} €</InputLabel>
                            <InputLabel>Comisión: {comisionPrecio} €</InputLabel>
                            <InputLabel>----------------</InputLabel>
                            <InputLabel>Precio Total: {totalPrecio} €</InputLabel>
                        </>
                    ) : (
                        <>
                            <InputLabel>Base: {basePrecio} €</InputLabel>
                            <InputLabel>----------------</InputLabel>
                            <InputLabel>Precio Total: Entrada Gratis</InputLabel>
                        </>
                    )}
                    <Button onClick={calculoPrecio} >Calcular Precio Final</Button>
                </Grid>

                {/*Fecha Inicio */}
                <Grid item xs={6}>
                    <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                        <DateTimePicker
                            autoOk
                            fullWidth
                            clearable
                            id="startDate"
                            name="startDate"
                            ampm={false}
                            value={dateStartEntrance}
                            onChange={handleDateStartEntranceChange}
                            format="yyyy-MM-dd HH:mm"
                            label="Fecha y hora inicio entrada"
                            minDate={event.startDate}
                            maxDate={event.endDate}
                            error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                            helpertext={formik.touched.formTicket && formik.errors.formTicket}
                        />
                        <small
                            style={{ color: errorInput === 'formTicket.startDate' ? 'red' : 'black' }}
                            className={classes.counter}
                        >
                            {errorInput === 'formTicket.startDate' ? 'La fecha no puede estar vacia.' : ''}
                        </small>
                    </MuiPickersUtilsProvider>
                </Grid>

                {/*Fecha Fin */}
                <Grid item xs={6}>
                    <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                        <DateTimePicker
                            autoOk
                            fullWidth
                            clearable
                            id="endDate"
                            name="endDate"
                            ampm={false}
                            value={dateEndEntrance}
                            onChange={handleDateEndEntranceChange}
                            format="yyyy-MM-dd HH:mm"
                            label="Fecha y hora Fin entrada"
                            minDate={event.startDate}
                            maxDate={event.endDate}
                            minDateMessage={
                                'la fecha fin entrada no puede ser menor que fecha inicio entrada'
                            }
                            error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                            helpertext={formik.touched.formTicket && formik.errors.formTicket}
                        />
                        <small
                            style={{ color: errorInput === 'formTicket.endDate' ? 'red' : 'black' }}
                            className={classes.counter}
                        >
                            {errorInput === 'formTicket.endDate' ? 'La fecha no puede estar vacia.' : ''}
                        </small>
                    </MuiPickersUtilsProvider>
                </Grid>

                {/*Inicio venta entradas */}
                <Grid item xs={8}>
                    <InputLabel>Inicio venta entradas</InputLabel>
                </Grid>

                {/*Fecha Inicio venta */}
                <Grid item xs={6}>
                    <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                        <DateTimePicker
                            autoOk
                            fullWidth
                            clearable
                            id="startDateSell"
                            name="startDateSell"
                            ampm={false}
                            value={dateStartSellEntrance}
                            onChange={handleDateStartSellEntranceChange}
                            format="yyyy-MM-dd HH:mm"
                            label="Fecha y hora inicio venta entrada"
                            error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                            helpertext={formik.touched.formTicket && formik.errors.formTicket}
                        />
                        <small
                            style={{ color: errorInput === 'formTicket.startDateSell' ? 'red' : 'black' }}
                            className={classes.counter}
                        >
                            {errorInput === 'formTicket.startDateSell' ? 'La fecha no puede estar vacia.' : ''}
                        </small>
                    </MuiPickersUtilsProvider>
                </Grid>

                {/*Fecha Fin venta */}
                <Grid item xs={6}>
                    <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                        <DateTimePicker
                            autoOk
                            fullWidth
                            clearable
                            id="endDateSell"
                            name="endDateSell"
                            ampm={false}
                            value={dateEndSellEntrance}
                            onChange={handleDateEndSellEntranceChange}
                            format="yyyy-MM-dd HH:mm"
                            label="Fecha y hora Fin venta entrada"
                            minDate={formik.values.formTicket.sellStartDate}
                            maxDate={formik.values.formTicket.endDate}
                            minDateMessage={
                                'la fecha fin venta entrada no puede ser menor que fecha inicio venta entrada'
                            }
                            error={formik.touched.formTicket && Boolean(formik.errors.formTicket)}
                            helpertext={formik.touched.formTicket && formik.errors.formTicket}
                        />
                        <small
                            style={{ color: errorInput === 'formTicket.endDateSell' ? 'red' : 'black' }}
                            className={classes.counter}
                        >
                            {errorInput === 'formTicket.endDateSell' ? 'La fecha no puede estar vacia.' : ''}
                        </small>
                    </MuiPickersUtilsProvider>
                </Grid>

                { ticketData._id !== undefined ? <>
            
                {/* Checkbox publicar */}
                <Grid item xs={4}>
                    <FormControlLabel
                        className={classes.formControl}
                        control={
                            <Checkbox
                                id="formTicket.isActive"
                                checked={formik.values.formTicket.isActive}
                                value={formik.values.formTicket.isActive}
                                onChange={formik.handleChange}
                                name="formTicket.isActive"
                            />
                        }
                        label="Activo"
                    />
                </Grid>
                </> : ''}

            </Grid>
            <div className={classes.divBtn}>
                <Button onClick={onHide} variant="primary">Cancelar</Button>
                <Button className={classes.buttonRight} type="submit" variant="contained" color="primary">
                    Guardar
                </Button>
            </div>
            </form>
          </DialogContent>
        </Dialog>
      </>
      );
}
export default Entrance;