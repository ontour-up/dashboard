import 'moment/locale/es';
import {
    Button,
    Checkbox,
    Dialog,
    DialogContent,
    DialogTitle,
    FormControlLabel,
    Grid,
    TextField
} from '@material-ui/core';
import React, { useContext, useEffect, useState } from 'react';

import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import { UserContext } from '../../../context/UserContext';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import { useFormik } from 'formik';
import { addZone, modifyZone } from '../../../services/zone.js';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    backButton: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    formControl: {
        width: '100%',
    },
    chip: {
        marginTop: '10px',
        marginRight: '5px',
    },
    divBtn: {
        marginTop: '30px',
        marginBottom: '20px',
    },
    buttonProgress: {
        marginLeft: '10px',
    },
    imgBanner: {
        width: '150px',
        height: '130px',
        objectFit: 'contain',
        marginTop: '10px',
        marginBottom: '10px',
    },
    ticket: {
        backgroundColor: 'white',
        boxShadow: '0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)',
        borderRadius: '10px',
        marginBottom: '30px',
        marginLeft: '10px',
        width: '600px',
        height: '70px',
    },
    textTicket: {
        marginLeft: '10px',
        fontSize: '20px',
        marginBottom: '8px',
    },
    buttonRight: {
        float: 'right',
    },
}));

    function Zone({open, onHide, onRefresh, onFinish, onOpenSnackBar, event, zone}) {
    moment.locale('es');
    const classes = useStyles();
    const { user } = useContext(UserContext);
    const [errorInput, setErrorInput] = useState('');
    const [messageAlert, setMessageAlert] = useState({});

    useEffect(() => {
        setErrorInput('');
        formik.handleReset();
        if (zone !== null) {
            loadData();
        }

    }, [zone]);

    const formik = useFormik({
        initialValues: {
            formZone: {
                name: '',
                description: '',
                capacity: 0,
                isActive: true,
                event: '',
                promoter: ''
            },
        },
        // validationSchema: validationSchema,
        onSubmit: (values) => {
            onSubmit(values.formZone);
            /*  console.log(values); */
        },
    });

  const loadData = () => {
         formik.setFieldValue('formZone.name', zone?.name)
         formik.setFieldValue('formZone.description', zone?.description)
         formik.setFieldValue('formZone.capacity', zone?.capacity)
         formik.setFieldValue('formZone.isActive', zone?.isActive)       
      }

const validation = () => {
    setErrorInput('');
    if (formik.values.formZone.name === undefined || formik.values.formZone.name === '') {
        setErrorInput('formZone.name');
        return false;
    }
    else if (formik.values.formZone.description === undefined || formik.values.formZone.description === '') {
        setErrorInput('formZone.description');
        return false;
    }
    else if (formik.values.formZone.capacity === undefined || isNaN(formik.values.formZone.capacity)) {
        setErrorInput('formZone.capacity');
        return false;
    }
    return true;
}

const onSubmit = async (valuesZone) => {
    const validated = validation();

    if(validated){
        valuesZone.event = event._id;
        valuesZone.promoter = user._id;

        let message;
        if(zone._id !== undefined){
            const { status, data } = await modifyZone(zone._id,valuesZone);
            if(status === 200){
                message = data.message;
                onOpenSnackBar({ severity: 'success', msg: message });
            }else{
                message = data.message;
                setMessageAlert({ severity: 'warning', msg: message });
            }
            
        }else{           
            const { status, data } = await addZone(valuesZone);
            if(status === 200){
                message = data.message;
                onOpenSnackBar({ severity: 'success', msg: message });
            }else{
                message = data.message;
                setMessageAlert({ severity: 'warning', msg: message });
            }
        }
        onFinish();
    }
}

const handleClose = (event, reason) => {
    
    formik.handleReset();
        if (reason !== 'backdropClick') {
            onHide();
        }
    };

    return (
        <>

    <Dialog key={zone} open={open} onHide={handleClose} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
        <DialogTitle>
            <Grid container direction="row" justifyContent="space-between" alignItems="center">
                {zone._id !== undefined ? <>Editar zona {zone._id}</> : <>Crear nueva zona  </>}
                <IconButton aria-label="close" onClick={handleClose}>
                    <CloseIcon />
                </IconButton>
            </Grid>
        </DialogTitle>
        <DialogContent>
        <form encType="multipart/form-data" onSubmit={formik.handleSubmit}>
          <Grid container spacing={3}>
            {/*nombre entrada */}
            <Grid item xs={7}>
                <TextField
                    fullWidth
                    id="formZone.name"
                    name="formZone.name"
                    label="Nombre Zona"
                    value={formik.values.formZone.name}
                    onChange={formik.handleChange}
                    error={formik.touched.formZone && Boolean(formik.errors.formZone)}
                    helpertext={formik.touched.formZone && formik.errors.formZone}
                />
                <small
                    style={{ color: errorInput === 'formZone.name' ? 'red' : 'black' }}
                    className={classes.counter}
                >
                    {errorInput === 'formZone.name' ? 'Ingrese nombre de la zona.' : ''}
                </small>
            </Grid>

            {/*Capacidad */}
            <Grid item xs={5}>
                <TextField
                    fullWidth
                    type="number"
                    id="formZone.capacity"
                    name="formZone.capacity"
                    label="Capacidad"
                    value={formik.values.formZone.capacity}
                    onChange={formik.handleChange}
                    error={formik.touched.formZone && Boolean(formik.errors.formZone)}
                    helpertext={formik.touched.formZone && formik.errors.formZone}
                />
                <small
                    style={{ color: errorInput === 'formZone.capacity' ? 'red' : 'black' }}
                    className={classes.counter}
                >
                    {errorInput === 'formZone.capacity' ? 'Ingrese la capacidad de la zona' : ''}
                </small>
            </Grid>

            {/*Descripcion */}
            <Grid item xs={12}>
                <TextField
                    fullWidth
                    id="formZone.description"
                    name="formZone.description"
                    label="Descripcion"
                    value={formik.values.formZone.description}
                    onChange={formik.handleChange}
                    error={formik.touched.formZone && Boolean(formik.errors.formZone)}
                    helpertext={formik.touched.formZone && formik.errors.formZone}
                />
                <small
                    style={{ color: errorInput === 'formZone.description' ? 'red' : 'black' }}
                    className={classes.counter}
                >
                    {errorInput === 'formZone.description'
                        ? 'Ingrese la descripción de la zona.'
                        : ''}
                </small>
            </Grid>

            { zone._id !== undefined ? <>
            
            {/* Checkbox publicar */}
            <Grid item xs={4}>
                <FormControlLabel
                    className={classes.formControl}
                    control={
                        <Checkbox
                            id="formZone.isActive"
                            checked={formik.values.formZone.isActive}
                            value={formik.values.formZone.isActive}
                            onChange={formik.handleChange}
                            name="formZone.isActive"
                        />
                    }
                    label="Activo"
                />
            </Grid>
            </> : ''}
        </Grid>
        <div className={classes.divBtn}>
            <Button onClick={onHide} variant="primary">Cancelar</Button>
            <Button className={classes.buttonRight} type="submit" variant="contained" color="primary">
                Guardar
            </Button>
        </div>
        </form>
        </DialogContent>
    </Dialog>
      </>
      );
}

export default Zone;