import React, {useState, useEffect, useContext} from 'react';
import {Dialog, DialogTitle, DialogContent, Button, DialogActions, Grid, TextField, FormControl, InputLabel, 
        Select, MenuItem, Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useFormik } from 'formik';
import { takeFestival } from "../../../services/festival.service";
import * as yup from 'yup';
import { addPromoter, modifyPromoter } from "../../../services/promoter.service";
import CircularProgress from '@material-ui/core/CircularProgress';
import { UserContext } from "../../../context/UserContext";
import AlertMsg from "../../../component/Alert";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    formControl: {
        width: '100%',
    },
    btnActions: {
        margin: '10px',
    },
    buttonProgress: {
        marginLeft: '10px'
    }
}));

const validationSchema = yup.object({
    names: yup.string().required(),
    surnames: yup.string().required(),
    email: yup.string().required(),
    password: yup.string().required(),
});

const Promoter = ({open, onClose, onRefresh, onOpenSnackBar, promoter}) => {
    const classes = useStyles();
    const { user } = useContext(UserContext);
    const [festivals, setFestivals] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [isOpenAlert, setIsOpenAlert] = useState(false);
    const [messageAlert, setMessageAlert] = useState({});

    useEffect(() => {
        takeFestivals();
        if (promoter?._id !== undefined && promoter?._id !== '') {
            loadData();
        }
    }, [open]);

    const loadData = () => {
        formik.setFieldValue('names', promoter?.names)
        formik.setFieldValue('surnames', promoter?.surnames)
        formik.setFieldValue('_id', promoter?._id)
        formik.setFieldValue('email', promoter?.email)
        formik.setFieldValue('password', promoter?.password)
        formik.setFieldValue('festival', promoter?.festival)
        formik.setFieldValue('roles', promoter?.roles)
        formik.setFieldValue('promoter', promoter?.promoter)
    }

    const handleClose = () => {
        onClose();
    };

    const takeFestivals = async () => {
        const { status, data } = await takeFestival();
        if (status === 200) {
            setFestivals(data.data);
        } /*else {
           console.log(status, data)
        }*/
    }

    const onSubmit = async (values) => {
        setIsLoading(true);
        let dataL; 
        let statusL; 
        if (promoter?._id !== undefined && promoter?._id !== '') {
            const { status, data } = await modifyPromoter(promoter?._id, values);
            dataL = data;
            statusL = status;
        } else {
            delete values._id;
            const { status, data } = await addPromoter(values);
            dataL = data;
            statusL = status;
        }
        if (statusL === 200) {
            onOpenSnackBar({severity: 'success', msg: dataL.message})
            setIsLoading(false);
            onRefresh()
            handleClose();
            resetForm();
        } else {
            setMessageAlert({severity: 'warning', msg: dataL.message})
            setIsOpenAlert(true)
            setIsLoading(false);
        }
    }

    const resetForm = async () => {
        formik.handleReset();
    }

    const formik = useFormik({
        initialValues: {
            names: '',
            surnames: '',
            email: '',
            password: '',
            promoter: user._id,
            festival: '',
            phone:'',
            roles:'promoter',

        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            onSubmit(values);
        },
    });

    return (
        <>
            <Dialog open={open} onClose={handleClose} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Promotor</DialogTitle>
                <form encType="multipart/form-data" onSubmit={formik.handleSubmit}>
                    <DialogContent>
                            <Grid container spacing={3}>
                                <Grid item xs={6}>
                                    <TextField
                                        fullWidth
                                        id="names"
                                        name="names"
                                        label="Nombre"
                                        value={formik.values.names}
                                        onChange={formik.handleChange}
                                        error={formik.touched.names && Boolean(formik.errors.names)}
                                        helpertext={formik.touched.names && formik.errors.names}
                                    />
                                </Grid>
                                <Grid item xs={6}>
                                    <TextField
                                        fullWidth
                                        id="surnames"
                                        name="surnames"
                                        label="Apellidos"
                                        value={formik.values.surnames}
                                        onChange={formik.handleChange}
                                        error={formik.touched.surnames && Boolean(formik.errors.surnames)}
                                        helpertext={formik.touched.surnames && formik.errors.surnames}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        id="phone"
                                        name="phone"
                                        label="Celular"
                                        value={formik.values.phone}
                                        onChange={formik.handleChange}
                                        error={formik.touched.phone && Boolean(formik.errors.phone)}
                                        helpertext={formik.touched.phone && formik.errors.phone}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl className={classes.formControl}>
                                        <InputLabel id="label-festival">Festival</InputLabel>
                                        <Select
                                            labelId="label-festival"
                                            id="festival"
                                            name="festival"
                                            value={formik.values.festival}
                                            onChange={formik.handleChange}
                                            error={formik.touched.festival && Boolean(formik.errors.festival)}
                                            helpertext={formik.touched.festival && formik.errors.festival}
                                        >
                                            {festivals.map((festival, index) => {
                                                return (
                                                    <MenuItem key={index} value={festival._id}>
                                                        {festival.name}
                                                    </MenuItem>
                                                );
                                            })}
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        id="email"
                                        name="email"
                                        label="Email"
                                        value={formik.values.email}
                                        onChange={formik.handleChange}
                                        error={formik.touched.email && Boolean(formik.errors.email)}
                                        helpertext={formik.touched.email && formik.errors.email}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        id="password"
                                        name="password"
                                        label="Contraseña"
                                        type="Password"
                                        value={formik.values.password}
                                        onChange={formik.handleChange}
                                        error={formik.touched.password && Boolean(formik.errors.password)}
                                        helpertext={formik.touched.password && formik.errors.password}
                                    />
                                </Grid>
                            </Grid>
                        
                    </DialogContent>
                    <DialogActions className={classes.btnActions}>
                        <Button type="button" onClick={handleClose} color="secondary">
                            Cerrar
                        </Button>
                        <Button type="submit" variant="contained" color="primary">
                            Guardar
                            {isLoading && <CircularProgress color="secondary" size={24} className={classes.buttonProgress} />}
                        </Button>
                    </DialogActions>
                    {isOpenAlert &&
                        <Grid item xs={12}>
                            <AlertMsg severity={messageAlert.severity} msg={messageAlert.msg} />
                        </Grid>
                    }
                    <Divider />
                </form>
            </Dialog>
        </>
    )
};

export default Promoter;
