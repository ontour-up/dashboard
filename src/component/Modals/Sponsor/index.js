import React, {useState, useEffect, useContext} from 'react';
import {Dialog, DialogTitle, DialogContent, Button, DialogActions, Grid, TextField, FormControl, InputLabel, 
        Select, MenuItem, Typography, Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useFormik } from 'formik';
import { takeFestival } from "../../../services/festival.service";
import { uploadFile } from "../../../services/upload.service";
import * as yup from 'yup';
import { URL_S3 } from "../../../store/constant";
import { addSponsor, modifySponsor } from "../../../services/sponsor.service";
import CircularProgress from '@material-ui/core/CircularProgress';
import { UserContext } from "../../../context/UserContext";
import AlertMsg from "../../../component/Alert";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    formControl: {
        width: '100%',
    },
    btnActions: {
        margin: '10px',
    },
    buttonProgress: {
        marginLeft: '10px'
    },
    imgBanner: {
        width: '150px',
        height: '130px',
        objectFit: 'contain',
        marginTop: '10px',
        marginBottom: '10px',
        backgroundColor: 'gray'
    }
}));

const validationSchema = yup.object({
    name: yup.string().required(),
    category: yup.string().required(),
    festival: yup.string().required(),
});

const Sponsor = ({open, onClose, onRefresh, onOpenSnackBar, sponsor}) => {
    const classes = useStyles();
    const { user } = useContext(UserContext);
    const [festivals, setFestivals] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [isOpenAlert, setIsOpenAlert] = useState(false);
    const [messageAlert, setMessageAlert] = useState({});

    useEffect(() => {
        resetForm();
        takeFestivals();
        resetElements();
        if (sponsor?._id !== undefined && sponsor?._id !== '') {
            loadData();
        }
    }, [open]);

    const loadData = () => {
        formik.setFieldValue('url', sponsor?.url)
        formik.setFieldValue('name', sponsor?.name)
        formik.setFieldValue('_id', sponsor?._id)
        formik.setFieldValue('position', sponsor?.position)
        formik.setFieldValue('festival', sponsor?.festival._id)
        formik.setFieldValue('category', sponsor?.category)
        formik.setFieldValue('isActive', sponsor?.isActive)
        formik.setFieldValue('banner', sponsor?.banner)
        formik.setFieldValue('promoter', sponsor?.promoter)
    }

    const resetElements = () => {
        setIsLoading(false);
        setIsOpenAlert(false);
        setMessageAlert({});
    }

    const handleClose = () => {
        resetForm();
        onClose();
    };

    const takeFestivals = async () => {
        const { status, data } = await takeFestival();
        if (status === 200) {
            setFestivals(data.data);
        } /*else {
           console.log(status, data)
        }*/
    }

    const onSubmit = async (values) => {
        setIsOpenAlert(false);
        setIsLoading(true);
        let urlfile = '';
        if (values.file !== '') {
            const formData = new FormData();
            formData.append('file', values.file);
            const { status } = await uploadFile(formData);
            if (status === 201) {
                urlfile = URL_S3 + values.file.name;
            } else {
                urlfile = '';
            }
            values.banner = urlfile;
        }
        let dataL; 
        let statusL; 
        if (sponsor?._id !== undefined && sponsor?._id !== '') {
            const { status, data } = await modifySponsor(sponsor?._id, values);
            dataL = data;
            statusL = status;
        } else {
            delete values._id;
            const { status, data } = await addSponsor(values);
            dataL = data;
            statusL = status;
        }
        if (statusL === 200) {
            onOpenSnackBar({severity: 'success', msg: dataL.message})
            setIsLoading(false);
            onRefresh();
            handleClose();
            resetForm();
        } else {
            setMessageAlert({severity: 'warning', msg: dataL.message})
            setIsOpenAlert(true);
            setIsLoading(false);
        }
    }

    const resetForm = async () => {
        formik.handleReset();
    }

    const formik = useFormik({
        initialValues: {
            name: '',
            url: '',
            banner: '',
            position: 0,
            promoter: user._id,
            festival: '619e179477275183e38adb93',
            isActive: true,
            description:'',
            file:'',
            category: ''

        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            onSubmit(values);
        },
    });

    return (
        <>
            <Dialog open={open} onClose={handleClose} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Patrocinadores</DialogTitle>
                <form encType="multipart/form-data" onSubmit={formik.handleSubmit}>
                    <DialogContent>
                            <Grid container spacing={3}>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        id="name"
                                        name="name"
                                        label="Nombre"
                                        value={formik.values.name}
                                        onChange={formik.handleChange}
                                        error={formik.touched.name && Boolean(formik.errors.name)}
                                        helpertext={formik.touched.name && formik.errors.name}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        id="description"
                                        name="description"
                                        label="Descripción"
                                        value={formik.values.description}
                                        onChange={formik.handleChange}
                                        error={formik.touched.description && Boolean(formik.errors.description)}
                                        helpertext={formik.touched.description && formik.errors.description}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        id="url"
                                        name="url"
                                        label="Página web o enlace de red social"
                                        value={formik.values.url}
                                        onChange={formik.handleChange}
                                        error={formik.touched.url && Boolean(formik.errors.url)}
                                        helpertext={formik.touched.url && formik.errors.url}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        id="position"
                                        name="position"
                                        label="Posición dentro del listado"
                                        type="number"
                                        value={formik.values.position}
                                        onChange={formik.handleChange}
                                        error={formik.touched.position && Boolean(formik.errors.position)}
                                        helpertext={formik.touched.position && formik.errors.position}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl className={classes.formControl}>
                                        <InputLabel id="label-category">Categoría</InputLabel>
                                        <Select
                                            labelId="label-category"
                                            id="category"
                                            name="category"
                                            value={formik.values.category}
                                            onChange={formik.handleChange}
                                            error={formik.touched.category && Boolean(formik.errors.category)}
                                            helpertext={formik.touched.category && formik.errors.category}
                                        >
                                            <MenuItem value='sponsor'>Patrocinadores</MenuItem>
                                            <MenuItem value='collaborator'>Colabodadores</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                {/*<Grid item xs={12}>*/}
                                {/*    <FormControl className={classes.formControl}>*/}
                                {/*        <InputLabel id="label-festival">Festival</InputLabel>*/}
                                {/*        <Select*/}
                                {/*            labelId="label-festival"*/}
                                {/*            id="festival"*/}
                                {/*            name="festival"*/}
                                {/*            value={formik.values.festival}*/}
                                {/*            onChange={formik.handleChange}*/}
                                {/*            error={formik.touched.festival && Boolean(formik.errors.festival)}*/}
                                {/*            helpertext={formik.touched.festival && formik.errors.festival}*/}
                                {/*        >*/}
                                {/*            {festivals.map((festival, index) => {*/}
                                {/*                return (*/}
                                {/*                    <MenuItem key={index} value={festival._id}>*/}
                                {/*                        {festival.name}*/}
                                {/*                    </MenuItem>*/}
                                {/*                );*/}
                                {/*            })}*/}
                                {/*        </Select>*/}
                                {/*    </FormControl>*/}
                                {/*</Grid>*/}
                                <Grid item xs={12}>
                                    <Typography component="p">Imagen</Typography>
                                    {formik.values.banner !== '' ? (
                                        <img src={formik.values.banner} className={classes.imgBanner} alt={formik.values.name} />
                                    ) : <></>}
                                    <FormControl className={classes.formControl}>
                                        <input id="file" name="file" type="file" onChange={(event) => {
                                            formik.setFieldValue("banner", '');
                                            formik.setFieldValue("file", event.currentTarget.files[0]);
                                        }} />
                                    </FormControl>
                                </Grid>
                            </Grid>
                        
                    </DialogContent>
                    <DialogActions className={classes.btnActions}>
                        <Button type="button" onClick={handleClose} color="secondary">
                            Cerrar
                        </Button>
                        <Button type="submit" variant="contained" color="primary">
                            Guardar
                            {isLoading && <CircularProgress color="secondary" size={24} className={classes.buttonProgress} />}
                        </Button>
                    </DialogActions>
                    {isOpenAlert &&
                        <Grid item xs={12}>
                            <AlertMsg severity={messageAlert.severity} msg={messageAlert.msg} />
                        </Grid>
                    }
                    <Divider />
                </form>
            </Dialog>
        </>
    )
};

export default Sponsor;
