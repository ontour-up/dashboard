import React, {useState} from 'react';
import {DialogActions, DialogContent, DialogContentText, DialogTitle, Dialog, TextField, Button} from '@material-ui/core';

const ChangePassword = ({open, onClose, onChangePassword}) => {

    const [form, setForm] = useState({
        password: "",
    });

    const handleClose = () => {
        onClose();
    };

    const handlePassword = (e) => {
        setForm({ ...form, password: e.target.value });
    };

    const handleChangePassword = () => {
        onChangePassword(form.password);
    };

    return (
        <>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Cambio de contraseña</DialogTitle>
                <DialogContent>
                <DialogContentText>
                    Por favor actualice su contraseña.
                </DialogContentText>
                <TextField
                    autoFocus
                    margin="dense"
                    id="new-password"
                    label="Nueva Contraseña"
                    type="password"
                    fullWidth
                    value={form.password}
                    onChange={handlePassword}
                />
                </DialogContent>
                <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Cancel
                </Button>
                <Button onClick={handleChangePassword} color="primary">
                    Actualizar
                </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default ChangePassword;
