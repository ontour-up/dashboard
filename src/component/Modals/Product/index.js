import 'moment/locale/es';

import {
    Button,
    Checkbox,
    Dialog,
    DialogContent,
    DialogTitle,
    FormControl,
    FormControlLabel,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    TextField
} from '@material-ui/core';
import React, { useContext, useEffect, useState } from 'react';

import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import { UserContext } from '../../../context/UserContext';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import { useFormik } from 'formik';
import { addProduct, modifyProduct } from '../../../services/product.js';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    backButton: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    formControl: {
        width: '100%',
    },
    chip: {
        marginTop: '10px',
        marginRight: '5px',
    },
    divBtn: {
        marginTop: '30px',
        marginBottom: '20px',
    },
    buttonProgress: {
        marginLeft: '10px',
    },
    imgBanner: {
        width: '150px',
        height: '130px',
        objectFit: 'contain',
        marginTop: '10px',
        marginBottom: '10px',
    },
    ticket: {
        backgroundColor: 'white',
        boxShadow: '0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)',
        borderRadius: '10px',
        marginBottom: '30px',
        marginLeft: '10px',
        width: '600px',
        height: '70px',
    },
    textTicket: {
        marginLeft: '10px',
        fontSize: '20px',
        marginBottom: '8px',
    },
    buttonRight: {
        float: 'right',
    },
}));

    function Product({open, onHide, onRefresh, onFinish, onOpenSnackBar, product}) {
    moment.locale('es');
    const classes = useStyles();
    const { user } = useContext(UserContext);
    const [errorInput, setErrorInput] = useState('');
    const [messageAlert, setMessageAlert] = useState({});

    useEffect(() => {
        setErrorInput('');
        formik.handleReset();
        if (product !== null) {
            loadData();
        }

    }, [product]);

    const [types, setTypes] = React.useState([
        { value: 'bebida', label: 'Bebida' },
        { value: 'comida', label: 'Comida' },
        { value: 'menu', label: 'Menu' },
    ]);

    const [typesIva, setTypesIva] = React.useState([
        { value: 0, label: '0%' },
        { value: 10, label: '10%' },
        { value: 21, label: '21%' },
    ]);


const formik = useFormik({
    initialValues: {
        formProduct: {
            name: '',
            description: '',
            grossPrice: 0,
            netPrice: 0,
            ivaPercent: 0,
            isActive: true,
            enterprise: '',
            category:''
        },
    },
    // validationSchema: validationSchema,
    onSubmit: (values) => {
        onSubmit(values.formProduct);
        /*  console.log(values); */
    },
});

  const loadData = () => {
         formik.setFieldValue('formProduct.name', product?.name)
         formik.setFieldValue('formProduct.description', product?.description)
         formik.setFieldValue('formProduct.type', product?.type)
         formik.setFieldValue('formProduct.grossPrice', product?.grossPrice)
         formik.setFieldValue('formProduct.ivaPercent', product?.ivaPercent)
         formik.setFieldValue('formProduct.isActive', product?.isActive)       
      }

const validation = () => {
    setErrorInput('');
    if (formik.values.formProduct.name === undefined || formik.values.formProduct.name === '') {
        setErrorInput('formProduct.name');
        return false;
    }
    else if (formik.values.formProduct.description === undefined || formik.values.formProduct.description === '') {
        setErrorInput('formProduct.description');
        return false;
    }
    else if (formik.values.formProduct.type === undefined || formik.values.formProduct.type === '') {
        setErrorInput('formProduct.type');
        return false;
    }
    else if (formik.values.formProduct.grossPrice === undefined || isNaN(formik.values.formProduct.grossPrice)) {
        setErrorInput('formProduct.grossPrice');
        return false;
    }
    else if (formik.values.formProduct.ivaPercent === undefined || formik.values.formProduct.ivaPercent === '') {
        setErrorInput('formProduct.ivaPercent');
        return false;
    }
    return true;
}

const onSubmit = async (valuesProduct) => {
    const validated = validation();

    if(validated){
        valuesProduct.enterprise = user._id;

        valuesProduct.netPrice = (valuesProduct.grossPrice * (valuesProduct.ivaPercent / 100).toFixed(2));

        let message;
        if(product._id !== undefined){
            const { status, data } = await modifyProduct(product._id,valuesProduct);
            if(status === 200){
                onFinish();
                message = data.message;
                onOpenSnackBar({ severity: 'success', msg: message });
            }else{
                message = data.message;
                setMessageAlert({ severity: 'warning', msg: message });
            }
            
        }else{           
            const { status, data } = await addProduct(valuesProduct);
            if(status === 200 || status === 201){
                onFinish();
                message = data.message;
                onOpenSnackBar({ severity: 'success', msg: message });
            }else{
                message = data.message;
                setMessageAlert({ severity: 'warning', msg: message });
            }
        }
        
    }
}

const handleClose = (event, reason) => {
    
    formik.handleReset();
        if (reason !== 'backdropClick') {
            onHide();
        }
    };

    return (
        <>

    <Dialog key={product} open={open} onHide={handleClose} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
        <DialogTitle>
            <Grid container direction="row" justifyContent="space-between" alignItems="center">
                {product._id !== undefined ? <>Editar producto {product._id}</> : <>Crear nuevo producto  </>}
                <IconButton aria-label="close" onClick={handleClose}>
                    <CloseIcon />
                </IconButton>
            </Grid>
        </DialogTitle>
        <DialogContent>
        <form encType="multipart/form-data" onSubmit={formik.handleSubmit}>
          <Grid container spacing={3}>
            {/*nombre entrada */}
            <Grid item xs={7}>
                <TextField
                    fullWidth
                    id="formProduct.name"
                    name="formProduct.name"
                    label="Nombre producto"
                    value={formik.values.formProduct.name}
                    onChange={formik.handleChange}
                    error={formik.touched.formProduct && Boolean(formik.errors.formProduct)}
                    helpertext={formik.touched.formProduct && formik.errors.formProduct}
                />
                <small
                    style={{ color: errorInput === 'formProduct.name' ? 'red' : 'black' }}
                    className={classes.counter}
                >
                    {errorInput === 'formProduct.name' ? 'Ingrese nombre del producto.' : ''}
                </small>
            </Grid>

            {/*TIPO */}
            <Grid item xs={5}>
                <FormControl className={classes.formControl}>
                    <InputLabel id="label-type">Tipo</InputLabel>
                    <Select
                        labelId="label-type"
                        id="formProduct.type"
                        name="formProduct.type"
                        value={formik.values.formProduct.type}
                        onChange={formik.handleChange}
                        error={formik.touched.type && Boolean(formik.errors.type)}
                        helpertext={formik.touched.type && formik.errors.type}
                    >
                       {types.map((type, index) => {
                            return (
                                <MenuItem key={index} value={type.value}>
                                    {type.label}
                                </MenuItem>
                            );
                        })}
                    </Select>
                    <small style={{ color: errorInput === 'type' ? 'red' : 'black' }} className={classes.counter}>
                        {errorInput === 'type' ? 'Seleccione el tipo' : ''}
                    </small>
                </FormControl>
            </Grid>

            {/*Descripcion */}
            <Grid item xs={12}>
                <TextField
                    fullWidth
                    id="formProduct.description"
                    name="formProduct.description"
                    label="Descripcion"
                    value={formik.values.formProduct.description}
                    onChange={formik.handleChange}
                    error={formik.touched.formProduct && Boolean(formik.errors.formProduct)}
                    helpertext={formik.touched.formProduct && formik.errors.formProduct}
                />
                <small
                    style={{ color: errorInput === 'formProduct.description' ? 'red' : 'black' }}
                    className={classes.counter}
                >
                    {errorInput === 'formProduct.description'
                        ? 'Ingrese la descripción de la zona.'
                        : ''}
                </small>
            </Grid>

             {/*Price */}
             <Grid item xs={6}>
                <TextField
                    fullWidth
                    type="number"
                    id="formProduct.grossPrice"
                    name="formProduct.grossPrice"
                    label="Precio"
                    value={formik.values.formProduct.grossPrice}
                    onChange={formik.handleChange}
                    error={formik.touched.formProduct && Boolean(formik.errors.formProduct)}
                    helpertext={formik.touched.formProduct && formik.errors.formProduct}
                />
                <small
                    style={{ color: errorInput === 'formProduct.grossPrice' ? 'red' : 'black' }}
                    className={classes.counter}
                >
                    {errorInput === 'formProduct.grossPrice' ? 'Ingrese el precio del producto' : ''}
                </small>
            </Grid>

            {/*IVA */}
            <Grid item xs={6}>
                <FormControl className={classes.formControl}>
                    <InputLabel id="label-iva">Iva</InputLabel>
                    <Select
                        labelId="label-iva"
                        id="formProduct.ivaPercent"
                        name="formProduct.ivaPercent"
                        value={formik.values.formProduct.iva}
                        onChange={formik.handleChange}
                        error={formik.touched.iva && Boolean(formik.errors.iva)}
                        helpertext={formik.touched.iva && formik.errors.iva}
                    >
                        {typesIva.map((iva, index) => {
                            return (
                                <MenuItem key={index} value={iva.value}>
                                    {iva.label}
                                </MenuItem>
                            );
                        })}
                    </Select>
                    <small style={{ color: errorInput === 'ivaPercent' ? 'red' : 'black' }} className={classes.counter}>
                        {errorInput === 'ivaPercent' ? 'Seleccione el tipo de iva' : ''}
                    </small>
                </FormControl>
            </Grid>

            { product._id !== undefined ? <>
            
            {/* Checkbox publicar */}
            <Grid item xs={4}>
                <FormControlLabel
                    className={classes.formControl}
                    control={
                        <Checkbox
                            id="formProduct.isActive"
                            checked={formik.values.formProduct.isActive}
                            value={formik.values.formProduct.isActive}
                            onChange={formik.handleChange}
                            name="formProduct.isActive"
                        />
                    }
                    label="Activo"
                />
            </Grid>
            </> : ''}

        </Grid>
        <div className={classes.divBtn}>
            <Button onClick={onHide} variant="primary">Cancelar</Button>
            <Button className={classes.buttonRight} type="submit" variant="contained" color="primary">
                Guardar
            </Button>
        </div>
        </form>
        </DialogContent>
    </Dialog>
      </>
      );
}

export default Product;