
export default {
    items: [
        {
            id: 'navigation',
            title: '',
            caption: '',
            type: 'group',
            children: [
                {
                    id: 'resume',
                    title: 'Resumen',
                    type: 'item',
                    url: 'resume',
                },
                /*
                {
                    id: 'ticketManagement',
                    title: 'Gestión Entradas',
                    type: 'item',
                    url: 'tickets',
                },
                */

                {
                    id: 'assistants',
                    title: 'Gestion Asistentes',
                    type: 'collapse',
                    children: [
                        {
                            id: 'listadoAsistentes',
                            title: 'Listado Asistentes',
                            type: 'item',
                            url: 'assistants',
                        },
                        {
                            id: 'comprasAsistentes',
                            title: 'Listado Compras',
                            type: 'item',
                            url: 'buyers',
                        },
                    ],
                },

                {
                    id: 'accessControl',
                    title: 'Control de Accesos',
                    type: 'collapse',
                    children: [
                        {
                            id: 'zones',
                            title: 'Zonas',
                            type: 'item',
                            url: 'zones',
                        },
                        {
                            id: 'access',
                            title: 'Accesos',
                            type: 'item',
                            url: 'access',
                        },
                    ],
                },
                {
                    id: 'cashlessControl',
                    title: 'Gestion de Cashless',
                    type: 'collapse',
                    children: [
                        {
                            id: 'cashless',
                            title: 'Cashless',
                            type: 'item',
                            url: 'cashless',
                        },
                        /*
                        {
                            id: 'sales',
                            title: 'Ventas',
                            type: 'item',
                            url: 'sales',
                        },*/
                    ],
                },
                {
                    id: 'stocks',
                    title: 'Stocks',
                    type: 'item',
                    url: 'stocks',
                }
                
            ],
        },
        /*
        {
            id: 'configuration',
            title: 'Configuraciones',
            caption: '',
            type: 'group',
            icon: icons['NavigationOutlinedIcon'],
            children: [

                /*{
                    id: 'translations',
                    title: 'Traducciones',
                    type: 'item',
                    url: '/translations',
                    icon: icons['TranslateIcon'],
                },*/
                /*
                {
                    id: 'profile',
                    title: 'Perfil de usuario',
                    type: 'item',
                    url: '/user/profile',
                    icon: icons['AccountCircleOutlinedIcon'],
                },
            ],
        },
        */

    ],
};
