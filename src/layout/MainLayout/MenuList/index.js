import React, { useContext, useEffect } from 'react';
import { Typography } from '@material-ui/core';
import { Link, Switch, Route, Redirect, useLocation } from 'react-router-dom';
import NavGroup from './NavGroup';
import menuItem from '../../Items-Header/menu-items';
import menuItemAdmin from '../../Items-Header/menu-items-admin';
import menuItemOlas from '../../Items-Header/menu-items-olas';
import sidebarItemEvent from '../../Items-Sidebar/sidebar-items-event';
import sidebarItemEventPortu from '../../Items-Sidebar/sidebar-items-event-portu';
import { UserContext } from "../../../context/UserContext";


const MenuList = (props) => {
    const { role, user } = useContext(UserContext);
    const location = useLocation();
    const locationPath = location.pathname.split("/");

    let navItems = [];
    if(user.roles !== undefined){
        if(props.site === "header"){
			if (user.roles.indexOf('admin') >= 0) {
             navItems = menuItemAdmin.items.map((item) => {
            switch (item.type) {
                case 'group':
                    return <NavGroup key={item.id} item={item} />;
                default:
                    return (
                        <Typography key={item.id} variant="h6" color="error" align="center">
                            Menu Items Error
                        </Typography>
                    );
            }
        });
    } else if (user.roles.indexOf('olas') >= 0) {
                navItems = menuItemOlas.items.map((item) => {
                    switch (item.type) {
                        case 'group':
                            return <NavGroup key={item.id} item={item} />;
                        default:
                            return (
                                <Typography key={item.id} variant="h6" color="error" align="center">
                                    Menu Items Error
                                </Typography>
                            );
                    }
                });
            } else {
        navItems = menuItem.items.map((item) => {
            switch (item.type) {
                case 'group':
                    return <NavGroup key={item.id} item={item} />;
                default:
                    return (
                        <Typography key={item.id} variant="h6" color="error" align="center">
                            Menu Items Error
                        </Typography>
                    );
            }
        });
    }
    } else if(props.site === "sidebar"){
        if (location !== undefined || locationPath[1] === "event") {
            let sidebarElements;
            if (user.email === 'portu@ontourup.com') {
                sidebarElements = sidebarItemEventPortu;
            }else{
                sidebarElements = sidebarItemEvent;
            }
            navItems = sidebarElements.items.map((item) => {
            switch (item.type) {
                case 'group':
                    return <NavGroup key={item.id} item={item} />;
                default:
                    return (
                        <Typography key={item.id} variant="h6" color="error" align="center">
                            Menu Items Error
                        </Typography>
                    );
            }
        });
        }
    }
}

    return navItems;
};

export default MenuList;
