import { Box, Divider, Drawer, Grid, Hidden, IconButton, makeStyles, useMediaQuery, useTheme } from '@material-ui/core';
import React, { useContext }  from 'react';

import MenuList from '../MenuList';
import MenuTwoToneIcon from '@material-ui/icons/MenuTwoTone';
import PerfectScrollbar from 'react-perfect-scrollbar';
import ProfileSection from './ProfileSection';
import SearchSection from './SearchSection';
import { UserContext } from '../../../context/UserContext'
import { drawerWidth } from './../../../store/constant';
import logo from './../../../assets/images/ontourup.png';
import { useHistory } from 'react-router-dom';

//import Customization from './Customization';
//import NotificationSection from './NotificationSection';







const useStyles = makeStyles((theme) => ({
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(1.25),
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
    menuIcon: {
        fontSize: '1.5rem',
    },
    logo: {
        width: '190px',
    },
}));

const Header = (props) => {
    const { drawerToggle } = props;
    const classes = useStyles();
    const { isAuth } = useContext(UserContext);
    let history = useHistory();

    React.useEffect(() => {

        if (!isAuth) {
            history.push('/login');
        }
    
    
}, [props]);
    

    return (
        <React.Fragment>
            <Box>
                <Grid container justifyContent="space-between" alignItems="center">
                    <Hidden smDown>
                        <Grid item>
                            <Box mt={0.5}>
                                <img className={classes.logo} src={logo} alt="Logo" />
                            </Box>
                        </Grid>
                    </Hidden>
                    <Grid item>
                        <IconButton
                            edge="start"
                            className={classes.menuButton}
                            color="inherit"
                            aria-label="open drawer"
                            onClick={drawerToggle}
                        >
                            <MenuTwoToneIcon className={classes.menuIcon} />
                        </IconButton>
                    </Grid>

                </Grid>
            </Box>
            
            <Divider />
                    <PerfectScrollbar className="topNavBar">
                        <MenuList site="header"/>
                    </PerfectScrollbar>
                     
            <div className={classes.grow} />
           
                    
            <SearchSection theme="light" />
            {/* <Customization /> 
                <NotificationSection /> 
            */}
            <ProfileSection />
        </React.Fragment>
    );
};

export default Header;
