import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { hydrate, render } from "react-dom";
import App from './layout/App';
import reducer from './store/reducer';
import config from './config';
import './assets/scss/style.scss';
import * as serviceWorker from './serviceWorker';
import UserContextProvider from "./context/UserContext";
import 'bootstrap/dist/css/bootstrap.min.css';
const store = createStore(reducer);


const rootElement = document.getElementById("root");

    render(<Provider store={store}>
        <UserContextProvider>
            <BrowserRouter basename={config.basename}>
                <App />
            </BrowserRouter>
        </UserContextProvider>
    </Provider>, rootElement);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
